﻿using System;
using System.Linq;

using Goldfynger.UniversalDevice;
using Goldfynger.UniversalDevice.Connector;
using Goldfynger.UniversalDevice.FlashStorage;
using Goldfynger.UniversalDevice.Info;

namespace UniversalDeviceConsole
{
    internal sealed class Program
    {
        private static void Main(string[] args)
        {
            while (true)
            {
                try
                {
                    using var connector = SelectConnector();

                    Console.WriteLine($"Selected connector: {connector}");

                    using var device = new DeviceFactory().Create(connector);

                    try
                    {
                        var interfaceType = SelectInterface();

                        Console.WriteLine($"Selected interface: {interfaceType.Name}");

                        switch (interfaceType)
                        {
                            case Type t when t == typeof(IDeviceInfo):
                                ProcessDeviceInfo(device.CreateDeviceInfo());
                                break;

                            default: throw new NotImplementedException();
                        }
                    }
                    catch (OperationCanceledException)
                    {

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Can not select interface: {ex.Message}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Can not select connector: {ex.Message}");
                }


                Console.ReadLine();
            }
        }


        private static IDeviceConnector SelectConnector()
        {
            const string listConst = "LIST";
            const string selectConst = "SELECT";
            const string breakConst = "BREAK";
            const string exitConst = "EXIT";

            IDeviceConnectorType connectorType;
            var factory = new DeviceConnectorFactory();
            var connectorTypes = factory.AvailableTypes;

        ConnectorSelection:
            while (true)
            {
                Console.Write($"{listConst} or {selectConst} connector:{Environment.NewLine}>");
                var input = Normalize(Console.ReadLine());

                if (input.Length < 1) goto ConnectorSelectionError;

                switch (input[0])
                {
                    case listConst:
                        if (input.Length != 1) goto ConnectorSelectionError;                        
                        for (var index = 0; index < connectorTypes.Count; index++)
                        {
                            Console.WriteLine($"{index + 1}: {connectorTypes[index].Description}");
                        }
                        break;

                    case selectConst:
                        if (input.Length != 2) goto ConnectorSelectionError;
                        if (!int.TryParse(input[1], out int connectorIndex)) goto ConnectorSelectionError;
                        if (connectorIndex == 0 || connectorIndex > connectorTypes.Count) goto ConnectorSelectionError;
                        connectorType = connectorTypes[connectorIndex - 1];
                        goto DeviceSelection;

                    case breakConst:
                    case exitConst:
                        if (input.Length != 1) goto ConnectorSelectionError;
                        Environment.Exit(0);
                        break;

                    default:
                        goto ConnectorSelectionError;
                }

            ConnectorSelectionError:;
            }


        DeviceSelection:
            while (true)
            {
                Console.Write($"{listConst} or {selectConst} device:{Environment.NewLine}>");
                var input = Normalize(Console.ReadLine());

                if (input.Length < 1) goto DeviceSelectionError;

                var availableIdentifiers = factory.GetAvailableIdentifiers(connectorType);

                switch (input[0])
                {
                    case listConst:
                        if (input.Length != 1) goto DeviceSelectionError;
                        if (availableIdentifiers.Count == 0)
                        {
                            Console.WriteLine($"No devices found for selected connector: {connectorType.Description}");
                        }
                        else
                        {
                            for (var index = 0; index < availableIdentifiers.Count; index++)
                            {
                                Console.WriteLine($"{index + 1}: {availableIdentifiers[index].Identifier}");
                            }
                        }
                        break;

                    case selectConst:
                        if (input.Length != 2) goto DeviceSelectionError;
                        if (!int.TryParse(input[1], out int identifierIndex)) goto DeviceSelectionError;
                        if (identifierIndex == 0 || identifierIndex > availableIdentifiers.Count) goto DeviceSelectionError;
                        return factory.Create(connectorType, availableIdentifiers[identifierIndex - 1]);

                    case breakConst:
                        goto ConnectorSelection;

                    case exitConst:
                        if (input.Length != 1) goto DeviceSelectionError;
                        Environment.Exit(0);
                        break;

                    default:
                        goto DeviceSelectionError;
                }

            DeviceSelectionError:;
            }
        }

        private static Type SelectInterface()
        {
            const string listConst = "LIST";
            const string selectConst = "SELECT";
            const string breakConst = "BREAK";
            const string exitConst = "EXIT";

            while (true)
            {
                Console.Write($"{listConst} or {selectConst} interface:{Environment.NewLine}>");
                var input = Normalize(Console.ReadLine());

                if (input.Length < 1) goto InterfaceSelectionError;

                switch (input[0])
                {
                    case listConst:
                        if (input.Length != 1) goto InterfaceSelectionError;
                        Console.WriteLine($"1: {typeof(IDeviceInfo).Name}");
                        Console.WriteLine($"2: {typeof(IFlashStorageDevice).Name}");
                        break;

                    case selectConst:
                        if (input.Length != 2) goto InterfaceSelectionError;
                        switch (input[1])
                        {
                            case "1":
                                return typeof(IDeviceInfo);

                            case "2":
                                return typeof(IFlashStorageDevice);

                            default:
                                goto InterfaceSelectionError;
                        }

                    case breakConst:
                        throw new OperationCanceledException();

                    case exitConst:
                        if (input.Length != 1) goto InterfaceSelectionError;
                        Environment.Exit(0);
                        break;

                    default:
                        goto InterfaceSelectionError;
                }

            InterfaceSelectionError:;
            }
        }

        private static void ProcessDeviceInfo(IDeviceInfo deviceInfo)
        {
            const string listConst = "LIST";
            const string readConst = "READ";
            const string infoConst = "INFO";
            const string uidConst = "UID";
            const string breakConst = "BREAK";
            const string exitConst = "EXIT";

            while (true)
            {
                Console.Write($"{listConst} or input command:{Environment.NewLine}>");
                var input = Normalize(Console.ReadLine());

                if (input.Length < 1) goto CommandSelectionError;

                switch (input[0])
                {
                    case listConst:
                        if (input.Length != 1) goto CommandSelectionError;
                        Console.WriteLine($"{readConst} {infoConst}");
                        Console.WriteLine($"{readConst} {uidConst}");
                        break;

                    case readConst:
                        if (input.Length != 2) goto CommandSelectionError;
                        switch (input[1])
                        {
                            case infoConst:
                                var infoTask = deviceInfo.GetInfo();
                                infoTask.Wait();
                                Console.WriteLine($"{infoTask.Result}");
                                break;

                            case uidConst:
                                var uidTask = deviceInfo.GetUid();
                                uidTask.Wait();
                                Console.WriteLine($"{uidTask.Result}");
                                break;

                            default:
                                goto CommandSelectionError;
                        }
                        break;

                    case breakConst:
                        throw new OperationCanceledException();

                    case exitConst:
                        if (input.Length != 1) goto CommandSelectionError;
                        Environment.Exit(0);
                        break;

                    default:
                        goto CommandSelectionError;
                }

            CommandSelectionError:;
            }
        }

        private static string[] Normalize(string input)
        {
            return input.Split(' ').Where(s => s != " ").Select(s => s.ToUpper()).ToArray();
        }
    }
}
