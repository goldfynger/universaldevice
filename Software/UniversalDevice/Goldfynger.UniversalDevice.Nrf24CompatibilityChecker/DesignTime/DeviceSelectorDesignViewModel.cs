﻿using Goldfynger.UniversalDevice.Nrf24CompatibilityChecker.ViewModels;

namespace Goldfynger.UniversalDevice.Nrf24CompatibilityChecker.DesignTime
{
    public class DeviceSelectorDesignViewModel : DeviceSelectorViewModel
    {
        public static readonly DeviceSelectorDesignViewModel Instance = new();
    }
}
