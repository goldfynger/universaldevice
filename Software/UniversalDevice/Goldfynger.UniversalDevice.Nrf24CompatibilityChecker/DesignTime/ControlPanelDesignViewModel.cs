﻿
using Goldfynger.UniversalDevice.Nrf24CompatibilityChecker.ViewModels;

namespace Goldfynger.UniversalDevice.Nrf24CompatibilityChecker.DesignTime
{
    public class ControlPanelDesignViewModel : ControlPanelViewModel
    {
        public static readonly ControlPanelDesignViewModel Instance = new();
    }
}
