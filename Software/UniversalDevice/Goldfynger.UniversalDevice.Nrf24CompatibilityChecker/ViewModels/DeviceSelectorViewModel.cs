﻿using System.Collections.ObjectModel;
using System.Windows;

namespace Goldfynger.UniversalDevice.Nrf24CompatibilityChecker.ViewModels
{
    public class DeviceSelectorViewModel : DependencyObject
    {
        private static readonly DependencyProperty DevicesCollectionProperty =
            DependencyProperty.Register(nameof(DevicesCollection), typeof(ObservableCollection<DeviceViewModel>), typeof(DeviceSelectorViewModel),
                new PropertyMetadata(defaultValue: new ObservableCollection<DeviceViewModel>()));

        public static readonly DependencyProperty SelectedDeviceProperty =
            DependencyProperty.Register(nameof(SelectedDevice), typeof(DeviceViewModel), typeof(DeviceSelectorViewModel));


        public ObservableCollection<DeviceViewModel> DevicesCollection
        {
            get => (ObservableCollection<DeviceViewModel>)GetValue(DevicesCollectionProperty);
            set => SetValue(DevicesCollectionProperty, value);
        }

        public DeviceViewModel? SelectedDevice
        {
            get => (DeviceViewModel?)GetValue(SelectedDeviceProperty);
            set => SetValue(SelectedDeviceProperty, value);
        }
    }
}
