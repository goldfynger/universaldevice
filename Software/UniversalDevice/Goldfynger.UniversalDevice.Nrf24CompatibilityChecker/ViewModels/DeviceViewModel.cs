﻿using System.Windows;

namespace Goldfynger.UniversalDevice.Nrf24CompatibilityChecker.ViewModels
{
    public class DeviceViewModel : DependencyObject
    {
        public static readonly DependencyProperty OwnerSelectorDeviceProperty =
            DependencyProperty.Register(nameof(OwnerSelector), typeof(DeviceSelectorViewModel), typeof(DeviceViewModel));

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register(nameof(Description), typeof(string), typeof(DeviceViewModel));


        public DeviceSelectorViewModel? OwnerSelector
        {
            get => (DeviceSelectorViewModel?)GetValue(OwnerSelectorDeviceProperty);
            set => SetValue(OwnerSelectorDeviceProperty, value);
        }

        public string? Description
        {
            get => (string?)GetValue(DescriptionProperty);
            set => SetValue(DescriptionProperty, value);
        }
    }
}
