﻿using System.Collections.ObjectModel;
using System.Windows;

namespace Goldfynger.UniversalDevice.Nrf24CompatibilityChecker.ViewModels
{
    public class ControlPanelViewModel : DependencyObject
    {
        private static readonly DependencyProperty DevicesCollectionProperty =
            DependencyProperty.Register(nameof(DevicesCollection), typeof(ObservableCollection<DeviceViewModel>), typeof(ControlPanelViewModel),
                new PropertyMetadata(defaultValue: new ObservableCollection<DeviceViewModel>()));

        private static readonly DependencyPropertyKey DeviceSelectorAPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DeviceSelectorA), typeof(DeviceSelectorViewModel), typeof(ControlPanelViewModel), new PropertyMetadata());
        public static readonly DependencyProperty DeviceSelectorAProperty = DeviceSelectorAPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey DeviceSelectorBPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DeviceSelectorB), typeof(DeviceSelectorViewModel), typeof(ControlPanelViewModel), new PropertyMetadata());
        public static readonly DependencyProperty DeviceSelectorBProperty = DeviceSelectorBPropertyKey.DependencyProperty;


        public ControlPanelViewModel()
        {
            DeviceSelectorA = new DeviceSelectorViewModel();
            DeviceSelectorB = new DeviceSelectorViewModel();
        }


        public ObservableCollection<DeviceViewModel> DevicesCollection
        {
            get => (ObservableCollection<DeviceViewModel>)GetValue(DevicesCollectionProperty);
            set => SetValue(DevicesCollectionProperty, value);
        }

        public DeviceSelectorViewModel DeviceSelectorA
        {
            get => (DeviceSelectorViewModel)GetValue(DeviceSelectorAProperty);
            private set => SetValue(DeviceSelectorAPropertyKey, value);
        }

        public DeviceSelectorViewModel DeviceSelectorB
        {
            get => (DeviceSelectorViewModel)GetValue(DeviceSelectorBProperty);
            private set => SetValue(DeviceSelectorBPropertyKey, value);
        }
    }
}
