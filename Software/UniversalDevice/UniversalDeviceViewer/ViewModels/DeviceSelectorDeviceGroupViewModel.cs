﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;

using Prism.Mvvm;

namespace UniversalDeviceViewer.ViewModels
{
    [DebuggerDisplay("DeviceSelectorDeviceGroupViewModel: {Description}|{SelectedDevice.DeviceModel.Identifier.Identifier}")]
    public sealed class DeviceSelectorDeviceGroupViewModel : BindableBase
    {
        private DeviceSelectorDeviceViewModel _selectedDevice;


        public DeviceSelectorDeviceGroupViewModel(string description, ReadOnlyObservableCollection<DeviceSelectorDeviceViewModel> devices)
        {
            Description = description ?? throw new ArgumentNullException(nameof(description));
            Devices = devices ?? throw new ArgumentNullException(nameof(devices));
        }


        public string Description { get; }

        public ReadOnlyObservableCollection<DeviceSelectorDeviceViewModel> Devices { get; }

        public DeviceSelectorDeviceViewModel SelectedDevice
        {
            get => _selectedDevice;
            set
            {
                SetProperty(ref _selectedDevice, value);
            }
        }
    }
}
