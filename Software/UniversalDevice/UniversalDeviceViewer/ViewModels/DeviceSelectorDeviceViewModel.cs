﻿using System;
using System.Diagnostics;

using UniversalDeviceViewer.Models;

namespace UniversalDeviceViewer.ViewModels
{
    [DebuggerDisplay("DeviceSelectorDeviceViewModel: {DeviceModel.Type.Type}|{Identifier}")]
    public sealed class DeviceSelectorDeviceViewModel
    {
        public DeviceSelectorDeviceViewModel(UniversalDeviceModel deviceModel)
        {
            DeviceModel = deviceModel ?? throw new ArgumentNullException(nameof(deviceModel));
        }


        public UniversalDeviceModel DeviceModel { get; }

        public string Identifier => DeviceModel.Identifier.Identifier;
    }
}
