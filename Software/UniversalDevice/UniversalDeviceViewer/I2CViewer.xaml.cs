﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

using Goldfynger.UniversalDevice;
using Goldfynger.UniversalDevice.I2C;
using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Parser;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace UniversalDeviceViewer
{
    internal sealed partial class I2CViewer : Window
    {
        private static readonly DependencyProperty IsAddressValidProperty =
            DependencyProperty.Register(nameof(IsAddressValid), typeof(bool), typeof(I2CViewer), new PropertyMetadata(false, IsAddressValidPropertyChanged));

        private static readonly DependencyProperty IsReadLengthValidProperty =
            DependencyProperty.Register(nameof(IsReadLengthValid), typeof(bool), typeof(I2CViewer), new PropertyMetadata(false, IsReadLengthValidPropertyChanged));

        private static readonly DependencyProperty IsReadMemoryAddressValidProperty =
            DependencyProperty.Register(nameof(IsReadMemoryAddressValid), typeof(bool), typeof(I2CViewer), new PropertyMetadata(false, IsReadMemoryAddressValidPropertyChanged));

        private static readonly DependencyProperty IsWriteMemoryAddressValidProperty =
            DependencyProperty.Register(nameof(IsWriteMemoryAddressValid), typeof(bool), typeof(I2CViewer), new PropertyMetadata(false, IsWriteMemoryAddressValidPropertyChanged));

        private static readonly DependencyProperty IsDataToWriteValidProperty =
            DependencyProperty.Register(nameof(IsDataToWriteValid), typeof(bool), typeof(I2CViewer), new PropertyMetadata(false, IsDataToWriteValidPropertyChanged));

        private static readonly DependencyProperty IsReadMemoryAddressInUseProperty =
            DependencyProperty.Register(nameof(IsReadMemoryAddressInUse), typeof(bool?), typeof(I2CViewer), new PropertyMetadata(null, IsReadMemoryAddressInUsePropertyChanged));

        private static readonly DependencyProperty IsWriteMemoryAddressInUseProperty =
            DependencyProperty.Register(nameof(IsWriteMemoryAddressInUse), typeof(bool?), typeof(I2CViewer), new PropertyMetadata(null, IsWriteMemoryAddressInUsePropertyChanged));

        private static readonly DependencyProperty CanReadProperty = DependencyProperty.Register(nameof(CanRead), typeof(bool), typeof(I2CViewer), new PropertyMetadata(false));

        private static readonly DependencyProperty CanWriteProperty = DependencyProperty.Register(nameof(CanWrite), typeof(bool), typeof(I2CViewer), new PropertyMetadata(false));

        private static readonly DependencyProperty AddressProperty = DependencyProperty.Register(nameof(Address), typeof(I2CAddress), typeof(I2CViewer), new PropertyMetadata(null, AddressPropertyChanged));

        private static readonly DependencyProperty ReadLengthProperty = DependencyProperty.Register(nameof(ReadLength), typeof(ushort?), typeof(I2CViewer), new PropertyMetadata(null, ReadLengthPropertyChanged));

        private static readonly DependencyProperty ReadMemoryAddressProperty =
            DependencyProperty.Register(nameof(ReadMemoryAddress), typeof(I2CMemoryAddress), typeof(I2CViewer), new PropertyMetadata(null, ReadMemoryAddressPropertyChanged));

        private static readonly DependencyProperty WriteMemoryAddressProperty =
            DependencyProperty.Register(nameof(WriteMemoryAddress), typeof(I2CMemoryAddress), typeof(I2CViewer), new PropertyMetadata(null, WriteMemoryAddressPropertyChanged));

        private static readonly DependencyProperty DataToWriteProperty =
            DependencyProperty.Register(nameof(DataToWrite), typeof(ReadOnlyCollection<ParseValueContainer>), typeof(I2CViewer), new PropertyMetadata(null, DataToWritePropertyChanged));

        private static readonly DependencyProperty DataToWriteNormalizedProperty = DependencyProperty.Register(nameof(DataToWriteNormalized), typeof(string), typeof(I2CViewer), new PropertyMetadata(null));


        private static readonly ReadOnlyDictionary<string, I2CAddress.Types> __addressTypes = new ReadOnlyDictionary<string, I2CAddress.Types>(new Dictionary<string, I2CAddress.Types>
        {
            { "7-bit address", I2CAddress.Types.Wide7 },
            { "10-bit address", I2CAddress.Types.Wide10 },
        });

        private static readonly ReadOnlyDictionary<string, I2CMemoryAddress.Sizes> __memoryAddressSizes = new ReadOnlyDictionary<string, I2CMemoryAddress.Sizes>(new Dictionary<string, I2CMemoryAddress.Sizes>
        {
            { "8-bit memory address", I2CMemoryAddress.Sizes.Size8 },
            { "16-bit memory address", I2CMemoryAddress.Sizes.Size16 },
        });


        private static readonly StringParser __addressParser = new StringParser(StringParser.AllowedInputs.Binary | StringParser.AllowedInputs.Decimal | StringParser.AllowedInputs.Hexadecimal);

        private static readonly StringParser __readLengthParser = new StringParser(StringParser.AllowedInputs.Decimal);

        private static readonly StringParser __readWriteMemoryAddressParser = new StringParser(StringParser.AllowedInputs.Decimal | StringParser.AllowedInputs.Hexadecimal);

        private static readonly StringParser __dataToWriteParser = new StringParser();


        private readonly II2CDevice _i2cDevice;

        private readonly DependencyPropertyObserverSettingsTreeNode _dependencyPropertyObserver;


        public I2CViewer(II2CDevice i2cDevice, DependencyPropertyObserverSettingsTreeNode dependencyPropertyObserver = null)
        {
            _i2cDevice = i2cDevice ?? throw new ArgumentNullException(nameof(i2cDevice));

            _dependencyPropertyObserver = dependencyPropertyObserver ?? new DependencyPropertyObserverSettingsTreeNode();

            InitializeComponent();

            Loaded += I2CViewer_Loaded;
            Unloaded += I2CViewer_Unloaded;

            tbAddress.TextChanged += TbAddress_TextChanged;
            cbAddressType.SelectionChanged += CbAddressType_SelectionChanged;

            tbReadLength.TextChanged += TbReadLength_TextChanged;

            tbReadMemoryAddress.TextChanged += TbReadMemoryAddress_TextChanged;
            cbReadMemoryAddressSize.SelectionChanged += CbReadMemoryAddressSize_SelectionChanged;

            tbWriteMemoryAddress.TextChanged += TbWriteMemoryAddress_TextChanged;
            cbWriteMemoryAddressSize.SelectionChanged += CbWriteMemoryAddressSize_SelectionChanged;

            tbDataToWrite.TextChanged += TbDataToWrite_TextChanged;

            bRead.Click += BRead_Click;
            bWrite.Click += BWrite_Click;
            bClear.Click += BClear_Click;

            if (_dependencyPropertyObserver.TryGetValue(DependencyPropertyObserverSettingsTreeNode.CreatePropertyName(wI2CViewer, WindowStateProperty), out var value))
            {
                wI2CViewer.WindowState = value.Content is string s ? Enum.Parse<WindowState>(s) : (WindowState)value.Content;
            }
            _dependencyPropertyObserver.RegisterProperty(wI2CViewer, WindowStateProperty);
        }


        private void I2CViewer_Loaded(object sender, EventArgs e)
        {
            __addressTypes.ForEach(p => cbAddressType.Items.Add(p.Key));
            _dependencyPropertyObserver.SetStoredPropertyValueOrDefaultAndRegisterProperty<string>(cbAddressType, Selector.SelectedItemProperty, corrector: value =>
            {
                var addressTypes = cbAddressType.Items.OfType<string>().ToList();
                return (value != null && addressTypes.Any(i => i == value)) ? value : addressTypes.FirstOrDefault();
            });

            __memoryAddressSizes.ForEach(p =>
            {
                cbReadMemoryAddressSize.Items.Add(p.Key);
                cbWriteMemoryAddressSize.Items.Add(p.Key);
            });
            _dependencyPropertyObserver.SetStoredPropertyValueOrDefaultAndRegisterProperty<string>(cbReadMemoryAddressSize, Selector.SelectedItemProperty, corrector: value =>
            {
                var readMemoryAddressSizes = cbReadMemoryAddressSize.Items.OfType<string>().ToList();
                return (value != null && readMemoryAddressSizes.Any(i => i == value)) ? value : readMemoryAddressSizes.FirstOrDefault();
            });
            _dependencyPropertyObserver.SetStoredPropertyValueOrDefaultAndRegisterProperty<string>(cbWriteMemoryAddressSize, Selector.SelectedItemProperty, corrector: value =>
            {
                var writeMemoryAddressSizes = cbWriteMemoryAddressSize.Items.OfType<string>().ToList();
                return (value != null && writeMemoryAddressSizes.Any(i => i == value)) ? value : writeMemoryAddressSizes.FirstOrDefault();
            });

            _dependencyPropertyObserver.SetStoredPropertyValueOrDefaultAndRegisterProperty<string>(tbAddress, TextBox.TextProperty);

            _dependencyPropertyObserver.SetStoredPropertyValueOrDefaultAndRegisterProperty<string>(tbReadLength, TextBox.TextProperty);

            _dependencyPropertyObserver.SetStoredPropertyValueOrDefaultAndRegisterProperty<bool?>(cbIsReadMemoryAddressInUse, ToggleButton.IsCheckedProperty, corrector: value => value == null ? false : value);
            _dependencyPropertyObserver.SetStoredPropertyValueOrDefaultAndRegisterProperty<string>(tbReadMemoryAddress, TextBox.TextProperty);

            _dependencyPropertyObserver.SetStoredPropertyValueOrDefaultAndRegisterProperty<bool?>(cbIsWriteMemoryAddressInUse, ToggleButton.IsCheckedProperty, corrector: value => value == null ? false : value);
            _dependencyPropertyObserver.SetStoredPropertyValueOrDefaultAndRegisterProperty<string>(tbWriteMemoryAddress, TextBox.TextProperty);

            _dependencyPropertyObserver.SetStoredPropertyValueOrDefaultAndRegisterProperty<string>(tbDataToWrite, TextBox.TextProperty);
        }

        private void I2CViewer_Unloaded(object sender, RoutedEventArgs e)
        {
            _dependencyPropertyObserver.UnregisterProperty(tbAddress, TextBox.TextProperty);
            _dependencyPropertyObserver.UnregisterProperty(cbAddressType, Selector.SelectedItemProperty);

            _dependencyPropertyObserver.UnregisterProperty(tbReadLength, TextBox.TextProperty);

            _dependencyPropertyObserver.UnregisterProperty(cbIsReadMemoryAddressInUse, ToggleButton.IsCheckedProperty);
            _dependencyPropertyObserver.UnregisterProperty(tbReadMemoryAddress, TextBox.TextProperty);
            _dependencyPropertyObserver.UnregisterProperty(cbReadMemoryAddressSize, Selector.SelectedItemProperty);

            _dependencyPropertyObserver.UnregisterProperty(cbIsWriteMemoryAddressInUse, ToggleButton.IsCheckedProperty);
            _dependencyPropertyObserver.UnregisterProperty(tbWriteMemoryAddress, TextBox.TextProperty);
            _dependencyPropertyObserver.UnregisterProperty(cbWriteMemoryAddressSize, Selector.SelectedItemProperty);

            _dependencyPropertyObserver.UnregisterProperty(tbDataToWrite, TextBox.TextProperty);

            _dependencyPropertyObserver.UnregisterProperty(wI2CViewer, WindowStateProperty);
        }

        private void TbAddress_TextChanged(object sender, TextChangedEventArgs e) => UpdateAddressProperty();
        private void CbAddressType_SelectionChanged(object sender, SelectionChangedEventArgs e) => UpdateAddressProperty();

        private void TbReadLength_TextChanged(object sender, TextChangedEventArgs e) => UpdateReadLengthProperty();

        private void TbReadMemoryAddress_TextChanged(object sender, TextChangedEventArgs e) => UpdateReadMemoryAddressProperty();
        private void CbReadMemoryAddressSize_SelectionChanged(object sender, SelectionChangedEventArgs e) => UpdateReadMemoryAddressProperty();

        private void TbWriteMemoryAddress_TextChanged(object sender, TextChangedEventArgs e) => UpdateWriteMemoryAddressProperty();
        private void CbWriteMemoryAddressSize_SelectionChanged(object sender, SelectionChangedEventArgs e) => UpdateWriteMemoryAddressProperty();

        private void TbDataToWrite_TextChanged(object sender, TextChangedEventArgs e) => UpdateDataToWriteProperty();

        private async void BRead_Click(object sender, RoutedEventArgs e) => await Read();
        private async void BWrite_Click(object sender, RoutedEventArgs e) => await Write();

        private void BClear_Click(object sender, RoutedEventArgs e) => tbLog.Clear();


        public bool IsAddressValid
        {
            get { return (bool)GetValue(IsAddressValidProperty); }
            private set { SetValue(IsAddressValidProperty, value); }
        }

        public bool IsReadLengthValid
        {
            get { return (bool)GetValue(IsReadLengthValidProperty); }
            private set { SetValue(IsReadLengthValidProperty, value); }
        }

        public bool IsReadMemoryAddressValid
        {
            get { return (bool)GetValue(IsReadMemoryAddressValidProperty); }
            private set { SetValue(IsReadMemoryAddressValidProperty, value); }
        }

        public bool IsWriteMemoryAddressValid
        {
            get { return (bool)GetValue(IsWriteMemoryAddressValidProperty); }
            private set { SetValue(IsWriteMemoryAddressValidProperty, value); }
        }

        public bool IsDataToWriteValid
        {
            get { return (bool)GetValue(IsDataToWriteValidProperty); }
            private set { SetValue(IsDataToWriteValidProperty, value); }
        }

        public bool? IsReadMemoryAddressInUse
        {
            get { return (bool?)GetValue(IsReadMemoryAddressInUseProperty); }
            private set { SetValue(IsReadMemoryAddressInUseProperty, value); }
        }

        public bool? IsWriteMemoryAddressInUse
        {
            get { return (bool?)GetValue(IsWriteMemoryAddressInUseProperty); }
            private set { SetValue(IsWriteMemoryAddressInUseProperty, value); }
        }

        public bool CanRead
        {
            get { return (bool)GetValue(CanReadProperty); }
            private set { SetValue(CanReadProperty, value); }
        }

        public bool CanWrite
        {
            get { return (bool)GetValue(CanWriteProperty); }
            private set { SetValue(CanWriteProperty, value); }
        }

        public I2CAddress Address
        {
            get { return (I2CAddress)GetValue(AddressProperty); }
            private set { SetValue(AddressProperty, value); }
        }

        public ushort? ReadLength
        {
            get { return (ushort?)GetValue(ReadLengthProperty); }
            private set { SetValue(ReadLengthProperty, value); }
        }

        public I2CMemoryAddress ReadMemoryAddress
        {
            get { return (I2CMemoryAddress)GetValue(ReadMemoryAddressProperty); }
            private set { SetValue(ReadMemoryAddressProperty, value); }
        }

        public I2CMemoryAddress WriteMemoryAddress
        {
            get { return (I2CMemoryAddress)GetValue(WriteMemoryAddressProperty); }
            private set { SetValue(WriteMemoryAddressProperty, value); }
        }

        public ReadOnlyCollection<ParseValueContainer> DataToWrite
        {
            get { return (ReadOnlyCollection<ParseValueContainer>)GetValue(DataToWriteProperty); }
            private set { SetValue(DataToWriteProperty, value); }
        }

        public string DataToWriteNormalized
        {
            get { return (string)GetValue(DataToWriteNormalizedProperty); }
            private set { SetValue(DataToWriteNormalizedProperty, value); }
        }


        private static void IsAddressValidPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UpdateCanReadProperty(d);
            UpdateCanWriteProperty(d);
        }

        private static void IsReadLengthValidPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) => UpdateCanReadProperty(d);
        private static void IsReadMemoryAddressValidPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) => UpdateCanReadProperty(d);        
        private static void IsWriteMemoryAddressValidPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) => UpdateCanWriteProperty(d);
        private static void IsDataToWriteValidPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) => UpdateCanWriteProperty(d);
        private static void IsReadMemoryAddressInUsePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) => UpdateCanReadProperty(d);
        private static void IsWriteMemoryAddressInUsePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) => UpdateCanWriteProperty(d);

        private static void AddressPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) => ((I2CViewer)d).IsAddressValid = e.NewValue != null;
        private static void ReadLengthPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) => ((I2CViewer)d).IsReadLengthValid = e.NewValue != null;
        private static void ReadMemoryAddressPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) => ((I2CViewer)d).IsReadMemoryAddressValid = e.NewValue != null;        
        private static void WriteMemoryAddressPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) => ((I2CViewer)d).IsWriteMemoryAddressValid = e.NewValue != null;

        private static void DataToWritePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var viewer = (I2CViewer)d;
            viewer.IsDataToWriteValid = e.NewValue != null;
            viewer.UpdateDataToWriteNormalizedProperty();
        }

        private static void UpdateCanReadProperty(DependencyObject d)
        {
            var viewer = (I2CViewer)d;
            viewer.CanRead = viewer.IsAddressValid && viewer.IsReadLengthValid && (viewer.IsReadMemoryAddressInUse != true || viewer.IsReadMemoryAddressValid);
        }

        private static void UpdateCanWriteProperty(DependencyObject d)
        {
            var viewer = (I2CViewer)d;
            viewer.CanWrite = viewer.IsAddressValid && viewer.IsDataToWriteValid && (viewer.IsWriteMemoryAddressInUse != true || viewer.IsWriteMemoryAddressValid);
        }

        private void UpdateAddressProperty()
        {
            var parseResults = __addressParser.Parse(tbAddress.Text);

            switch (__addressTypes[(string)cbAddressType.SelectedItem])
            {
                case I2CAddress.Types.Wide7:
                    {
                        if (parseResults == null || parseResults.Count != 1 || !parseResults[0].U8Value.HasValue)
                        {
                            goto default;
                        }

                        try
                        {
                            Address = I2CAddress.Create7(parseResults[0].U8Value.Value);
                        }
                        catch
                        {
                            goto default;
                        }
                    }
                    break;

                case I2CAddress.Types.Wide10:
                    {
                        if (parseResults == null || parseResults.Count != 1 || !parseResults[0].U16Value.HasValue)
                        {
                            goto default;
                        }

                        try
                        {
                            Address = I2CAddress.Create10(parseResults[0].U16Value.Value);
                        }
                        catch
                        {
                            goto default;
                        }
                    }
                    break;

                default:
                    Address = null;
                    break;
            }
        }

        private void UpdateReadLengthProperty()
        {
            var parseResults = __readLengthParser.Parse(tbReadLength.Text);

            if (parseResults != null && parseResults.Count == 1 && parseResults[0].U16Value.HasValue && parseResults[0].U16Value.Value > 0)
            {
                ReadLength = parseResults[0].U16Value.Value;
            }
            else
            {
                ReadLength = null;
            }
        }

        private void UpdateReadMemoryAddressProperty()
        {
            var parseResults = __readWriteMemoryAddressParser.Parse(tbReadMemoryAddress.Text);

            switch (__memoryAddressSizes[(string)cbReadMemoryAddressSize.SelectedItem])
            {
                case I2CMemoryAddress.Sizes.Size8:
                    {
                        if (parseResults == null || parseResults.Count != 1 || !parseResults[0].U8Value.HasValue)
                        {
                            goto default;
                        }

                        try
                        {
                            ReadMemoryAddress = I2CMemoryAddress.Create8(parseResults[0].U8Value.Value);
                        }
                        catch
                        {
                            goto default;
                        }
                    }
                    break;

                case I2CMemoryAddress.Sizes.Size16:
                    {
                        if (parseResults == null || parseResults.Count != 1 || !parseResults[0].U16Value.HasValue)
                        {
                            goto default;
                        }

                        try
                        {
                            ReadMemoryAddress = I2CMemoryAddress.Create16(parseResults[0].U16Value.Value);
                        }
                        catch
                        {
                            goto default;
                        }
                    }
                    break;

                default:
                    ReadMemoryAddress = null;
                    break;
            }
        }

        private void UpdateWriteMemoryAddressProperty()
        {
            var parseResults = __readWriteMemoryAddressParser.Parse(tbWriteMemoryAddress.Text);

            switch (__memoryAddressSizes[(string)cbWriteMemoryAddressSize.SelectedItem])
            {
                case I2CMemoryAddress.Sizes.Size8:
                    {
                        if (parseResults == null || parseResults.Count != 1 || !parseResults[0].U8Value.HasValue)
                        {
                            goto default;
                        }

                        try
                        {
                            WriteMemoryAddress = I2CMemoryAddress.Create8(parseResults[0].U8Value.Value);
                        }
                        catch
                        {
                            goto default;
                        }
                    }
                    break;

                case I2CMemoryAddress.Sizes.Size16:
                    {
                        if (parseResults == null || parseResults.Count != 1 || !parseResults[0].U16Value.HasValue)
                        {
                            goto default;
                        }

                        try
                        {
                            WriteMemoryAddress = I2CMemoryAddress.Create16(parseResults[0].U16Value.Value);
                        }
                        catch
                        {
                            goto default;
                        }
                    }
                    break;

                default:
                    WriteMemoryAddress = null;
                    break;
            }
        }

        private void UpdateDataToWriteProperty()
        {
            DataToWrite = __dataToWriteParser.Parse(tbDataToWrite.Text);
        }

        private void UpdateDataToWriteNormalizedProperty()
        {
            DataToWriteNormalized = DataToWrite != null && DataToWrite.Count != 0 ? DataToWrite.ConvertToStringWithSeparator() : null;
        }

        private async Task Read()
        {
            LogLine($"Device address: {Address}, read length: {ReadLength}{(IsReadMemoryAddressInUse == true ? $", memory address: {ReadMemoryAddress}" : string.Empty)}.");

            try
            {
                LogOpenLine("Is interface already acquired?..");

                var isAcquired = await _i2cDevice.IsAcquired();

                LogCloseLine(isAcquired ? "Yes." : "No.");

                if (!isAcquired)
                {
                    LogOpenLine("Acquire interface...");

                    await _i2cDevice.Acquire();

                    LogCloseLine("Success.");
                }

                try
                {
                    if (IsReadMemoryAddressInUse == true)
                    {
                        LogOpenLine($"Read {ReadLength} bytes at address {ReadMemoryAddress} from device {Address}...");

                        var data = await _i2cDevice.ReadMemory(Address, I2CSpeeds.Standart, ReadMemoryAddress, ReadLength.Value);

                        LogCloseLine("Success.");

                        tbRead.Text = data.ConvertToStringWithSeparator(converter: b => b.ToString("X2").AddHexHeader());
                    }
                    else
                    {
                        LogOpenLine($"Read {ReadLength} bytes from device {Address}...");

                        var data = await _i2cDevice.Read(Address, I2CSpeeds.Standart, ReadLength.Value);

                        LogCloseLine("Success.");

                        tbRead.Text = data.ConvertToStringWithSeparator(converter: b => b.ToString("X2").AddHexHeader());
                    }
                }
                finally
                {
                    if (!_i2cDevice.IsCorrupted)
                    {
                        LogOpenLine("Release interface...");

                        await _i2cDevice.Release();

                        LogCloseLine("Success.");
                    }
                }
            }
            catch (I2CHalException ex)
            {
                LogCloseLine(null);
                LogLine($"{nameof(I2CHalException)}: {ex.DeviceError}|{ex.I2CHalError:X8}.");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.I2CHalError:X8}", $"{nameof(I2CHalException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                LogCloseLine(null);
                LogLine($"{nameof(UniversalDeviceException)}: {ex.DeviceError}.");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                LogCloseLine(null);
                LogLine($"{nameof(Exception)}: {ex.Message}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task Write()
        {
            LogLine($"Device address: {Address}{(IsWriteMemoryAddressInUse == true ? $", memory address: {WriteMemoryAddress}" : string.Empty)}.");

            try
            {
                LogOpenLine("Is interface already acquired?..");

                var isAcquired = await _i2cDevice.IsAcquired();

                LogCloseLine(isAcquired ? "Yes." : "No.");

                if (!isAcquired)
                {
                    LogOpenLine("Acquire interface...");

                    await _i2cDevice.Acquire();

                    LogCloseLine("Success.");
                }

                try
                {
                    var data = DataToWrite.Select(container => ConvertContainer(container)).SelectMany(i => i).ToList().AsReadOnly();

                    if (IsWriteMemoryAddressInUse == true)
                    {
                        LogOpenLine($"Write {data.Count} bytes at address {WriteMemoryAddress} to device {Address}...");

                        await _i2cDevice.WriteMemory(Address, I2CSpeeds.Standart, WriteMemoryAddress, data);

                        LogCloseLine("Success.");
                    }
                    else
                    {
                        LogOpenLine($"Write {data.Count} bytes to device {Address}...");

                        await _i2cDevice.Write(Address, I2CSpeeds.Standart, data);

                        LogCloseLine("Success.");
                    }
                }
                finally
                {
                    if (!_i2cDevice.IsCorrupted)
                    {
                        LogOpenLine("Release interface...");

                        await _i2cDevice.Release();

                        LogCloseLine("Success.");
                    }
                }
            }
            catch (I2CHalException ex)
            {
                LogCloseLine(null);
                LogLine($"{nameof(I2CHalException)}: {ex.DeviceError}|{ex.I2CHalError:X8}.");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.I2CHalError:X8}", $"{nameof(I2CHalException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                LogCloseLine(null);
                LogLine($"{nameof(UniversalDeviceException)}: {ex.DeviceError}.");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                LogCloseLine(null);
                LogLine($"{nameof(Exception)}: {ex.Message}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private IEnumerable<byte> ConvertContainer(ParseValueContainer container)
        {
            switch (container.ValueType)
            {
                case ParseValueContainer.ValueTypes.U8:
                    {
                        yield return container.U8Value ?? throw new ApplicationException();
                    }
                    break;

                case ParseValueContainer.ValueTypes.U16:
                    {
                        var bytes = BitConverter.GetBytes(container.U16Value ?? throw new ApplicationException());
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                case ParseValueContainer.ValueTypes.U32:
                    {
                        var bytes = BitConverter.GetBytes(container.U32Value ?? throw new ApplicationException());
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                case ParseValueContainer.ValueTypes.U64:
                    {
                        var bytes = BitConverter.GetBytes(container.U64Value ?? throw new ApplicationException());
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                case ParseValueContainer.ValueTypes.String:
                    {
                        var bytes = Encoding.ASCII.GetBytes(container.String);
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                default:
                    throw new ApplicationException("Unknown container value type.");
            }
        }

        private void LogString(string s)
        {
            tbLog.AppendText(s);
            tbLog.ScrollToEnd();
        }

        private void LogLine(string s, [CallerMemberName] string callerName = "", bool useCallerName = true, bool openWithDateTime = true, bool closeWithNewLine = true)
        {
            if (openWithDateTime)
            {
                LogString($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff}: ");
            }

            if (useCallerName)
            {
                LogString($"{callerName}: ");
            }

            LogString(s);

            if (closeWithNewLine)
            {
                LogString($"{Environment.NewLine}");
            }
        }

        private void LogOpenLine(string s, [CallerMemberName] string callerName = "")
        {
            LogLine(s, callerName: callerName, closeWithNewLine: false);
        }

        private void LogCloseLine(string s)
        {
            LogLine(s, callerName: null, openWithDateTime: false, useCallerName: false);
        }
    }



    /*
    public sealed class StringParser
    {
        private static readonly ReadOnlyCollection<string> __binStartSymbolsDefault = new ReadOnlyCollection<string>(new string[] { "b", "0b", "B", "0B" });
        private static readonly ReadOnlyCollection<string> __hexStartSymbolsDefault = new ReadOnlyCollection<string>(new string[] { "x", "0x", "X", "0X" });
        private static readonly ReadOnlyCollection<char> __substringSplitSymbolsDefault = new ReadOnlyCollection<char>(new char[] { ' ', ',', '\r', '\n' });


        public StringParser(AllowedInputs allowedInput = AllowedInputs.String | AllowedInputs.Binary | AllowedInputs.Decimal | AllowedInputs.Hexadecimal)
        {
            AllowedInput = allowedInput;
        }


        public AllowedInputs AllowedInput { get; }


        public ReadOnlyCollection<ParseValueContainer> Parse(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                Trace.WriteLine("Parser input is null or white space.");

                return null;
            }

            var split = input.Split(__substringSplitSymbolsDefault.ToArray(), StringSplitOptions.RemoveEmptyEntries);
            var array = new ParseValueContainer[split.Length];
            var index = 0;

            foreach (var str in split)
            {
                ParseValueContainer value = null;

                if (AllowedInput.HasFlag(AllowedInputs.Binary))
                {
                    foreach (var symbol in __binStartSymbolsDefault)
                    {
                        if (str.StartsWith(symbol))
                        {
                            value = StringToParseValueConverter.ConvertFromBin(str.Split(symbol, 2)[1]);
                            if (value != null)
                            {
                                break;
                            }
                        }
                    }
                }

                if (value == null && AllowedInput.HasFlag(AllowedInputs.Hexadecimal))
                {
                    foreach (var symbol in __hexStartSymbolsDefault)
                    {
                        if (str.StartsWith(symbol))
                        {
                            value = StringToParseValueConverter.ConvertFromHex(str.Split(symbol, 2)[1]);
                            if (value != null)
                            {
                                break;
                            }
                        }
                    }
                }

                if (value == null && AllowedInput.HasFlag(AllowedInputs.Decimal))
                {
                    value = StringToParseValueConverter.ConvertFromDec(str);
                }

                if (value == null && AllowedInput.HasFlag(AllowedInputs.String))
                {
                    value = new ParseValueContainer(str);
                }

                if (value != null)
                {
                    array[index++] = value;

                    Trace.WriteLine($"Substring \"{str}\" converted to \"{value.ValueType}\": \"{value}\".");
                }
                else
                {
                    Trace.WriteLine($"Substring \"{str}\" conversion failed.");

                    return null;
                }
            }

            return new ReadOnlyCollection<ParseValueContainer>(array);
        }


        [Flags]
        public enum AllowedInputs
        {
            None = 0x00,
            String = 0x01,
            Binary = 0x02,
            Decimal = 0x04,
            Hexadecimal = 0x08,
        }
    }

    public sealed class ParseValueContainer
    {
        private readonly ulong _value;
        private readonly string _string;


        public ParseValueContainer(byte value)
        {
            _value = value;
            ValueType = ValueTypes.U8;
        }

        public ParseValueContainer(ushort value)
        {
            _value = value;
            ValueType = ValueTypes.U16;
        }

        public ParseValueContainer(uint value)
        {
            _value = value;
            ValueType = ValueTypes.U32;
        }

        public ParseValueContainer(ulong value)
        {
            _value = value;
            ValueType = ValueTypes.U64;
        }

        public ParseValueContainer(string str)
        {
            _string = str;
            ValueType = ValueTypes.String;
        }


        public byte? U8Value
        {
            get
            {
                if (ValueType == ValueTypes.U8)
                {
                    return (byte?)_value;
                }
                else
                {
                    return null;
                }
            }
        }

        public ushort? U16Value
        {
            get
            {
                if (ValueType == ValueTypes.U8 || ValueType == ValueTypes.U16)
                {
                    return (ushort?)_value;
                }
                else
                {
                    return null;
                }
            }
        }

        public uint? U32Value
        {
            get
            {
                if (ValueType == ValueTypes.U8 || ValueType == ValueTypes.U16 || ValueType == ValueTypes.U32)
                {
                    return (uint?)_value;
                }
                else
                {
                    return null;
                }
            }
        }

        public ulong? U64Value
        {
            get
            {
                if (ValueType == ValueTypes.U8 || ValueType == ValueTypes.U16 || ValueType == ValueTypes.U32 || ValueType == ValueTypes.U64)
                {
                    return (uint?)_value;
                }
                else
                {
                    return null;
                }
            }
        }

        public string String
        {
            get
            {
                if (ValueType == ValueTypes.String)
                {
                    return _string;
                }
                else
                {
                    return null;
                }
            }
        }

        public ValueTypes ValueType { get; }


        public override string ToString()
        {
            return ValueType switch
            {
                ValueTypes.U8 => _value.ToString("X2").AddHexHeader(),
                ValueTypes.U16 => _value.ToString("X4").AddHexHeader(),
                ValueTypes.U32 => _value.ToString("X8").AddHexHeader(),
                ValueTypes.U64 => _value.ToString("X16").AddHexHeader(),
                ValueTypes.String => $"\"{_string}\"",
                _ => "Unknown value type",
            };
        }


        public enum ValueTypes
        {
            U8,
            U16,
            U32,
            U64,
            String,
        }
    }

    public static class ParseExtensions
    {
        public static string AddHexHeader(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            return $"0x{text}";
        }

        public static string AddBinHeader(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            return $"0b{text}";
        }

        public static string ConvertToStringWithSeparator<T>(this IEnumerable<T> collection, string separator = " ", Func<T, string> converter = null)
        {
            return collection == null ? null : string.Join(separator, collection.Select(t => converter == null ? t.ToString() : converter(t)));
        }

        public static string ConvertToBinString(this sbyte value)
        {
            var bits = new char[8];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        public static string ConvertToBinString(this byte value)
        {
            var bits = new char[8];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        public static string ConvertToBinString(this short value)
        {
            var bits = new char[16];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        public static string ConvertToBinString(this ushort value)
        {
            var bits = new char[16];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        public static string ConvertToBinString(this int value)
        {
            var bits = new char[32];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        public static string ConvertToBinString(this uint value)
        {
            var bits = new char[32];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        public static string ConvertToBinString(this long value)
        {
            var bits = new char[64];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }

        public static string ConvertToBinString(this ulong value)
        {
            var bits = new char[64];
            var i = 0;

            while (i < bits.Length)
            {
                bits[i++] = (value & 1) == 1 ? '1' : '0';
                value >>= 1;
            }

            Array.Reverse(bits, 0, i);
            return new string(bits);
        }
    }

    public static class StringToParseValueConverter
    {
        private static readonly char[] __knownBinDigits = { '0', '1' };
        private static readonly char[] __knownDecDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        private static readonly char[] __knownHexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

        private const int __bin8MaxLen = 8;
        private const int __bin16MaxLen = 16;
        private const int __bin32MaxLen = 32;
        private const int __bin64MaxLen = 64;

        private const int __hex8MaxLen = 2;
        private const int __hex16MaxLen = 4;
        private const int __hex32MaxLen = 8;
        private const int __hex64MaxLen = 16;


        public static ParseValueContainer ConvertFromBin(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            foreach (var ch in text)
            {
                if (!__knownBinDigits.Contains(ch))
                {
                    return null;
                }
            }

            if (text.Length <= __bin8MaxLen)
            {
                return new ParseValueContainer(Convert.ToByte(text, 2));
            }
            else if (text.Length <= __bin16MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt16(text, 2));
            }
            else if (text.Length <= __bin32MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt32(text, 2));
            }
            else if (text.Length <= __bin64MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt64(text, 2));
            }
            else
            {
                return null;
            }
        }

        public static ParseValueContainer ConvertFromDec(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            var isNegative = false;
            var positive = text;

            if (text.StartsWith('-'))
            {
                isNegative = true;
                positive = text.Split('-', 2)[1];
            }

            if (string.IsNullOrWhiteSpace(positive))
            {
                return null;
            }

            foreach (var ch in positive)
            {
                if (!__knownDecDigits.Contains(ch))
                {
                    return null;
                }
            }

            if (isNegative)
            {
                if (long.TryParse(text, out long result))
                {
                    if (result >= sbyte.MinValue)
                    {
                        unchecked
                        {
                            return new ParseValueContainer((byte)result);
                        }
                    }
                    else if (result >= short.MinValue)
                    {
                        unchecked
                        {
                            return new ParseValueContainer((ushort)result);
                        }
                    }
                    else if (result >= int.MinValue)
                    {
                        unchecked
                        {
                            return new ParseValueContainer((uint)result);
                        }
                    }
                    else
                    {
                        unchecked
                        {
                            return new ParseValueContainer((ulong)result);
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                if (ulong.TryParse(text, out ulong result))
                {
                    if (result <= byte.MaxValue)
                    {
                        return new ParseValueContainer((byte)result);
                    }
                    else if (result <= ushort.MaxValue)
                    {
                        return new ParseValueContainer((ushort)result);
                    }
                    else if (result <= uint.MaxValue)
                    {
                        return new ParseValueContainer((uint)result);
                    }
                    else
                    {
                        return new ParseValueContainer(result);
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public static ParseValueContainer ConvertFromHex(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return null;
            }

            var textLower = text.ToLowerInvariant();

            foreach (var ch in textLower)
            {
                if (!__knownHexDigits.Contains(ch))
                {
                    return null;
                }
            }

            if (text.Length <= __hex8MaxLen)
            {
                return new ParseValueContainer(Convert.ToByte(text, 16));
            }
            else if (text.Length <= __hex16MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt16(text, 16));
            }
            else if (text.Length <= __hex32MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt32(text, 16));
            }
            else if (text.Length <= __hex64MaxLen)
            {
                return new ParseValueContainer(Convert.ToUInt64(text, 16));
            }
            else
            {
                return null;
            }
        }
    }
    */




    public delegate void SettingsTreeNodeContentChangedEventHandler(SettingsTreeNode sender, EventArgs e);

    public delegate void SettingsTreeNodeCollectionChangedEventHandler(SettingsTreeNode sender, SettingsTreeNodeCollectionChangedEventArgs e);


    public sealed class SettingsTreeNodeCollectionChangedEventArgs : EventArgs
    {
        private SettingsTreeNodeCollectionChangedEventArgs(SettingsTreeNodeCollectionChangedAction action, IList<SettingsTreeNode> items)
        {
            action.ThrowIfNotDefined();

            if (items == null || items.Count == 0)
            {
                throw new ArgumentNullException(nameof(items));
            }

            switch (action)
            {
                case SettingsTreeNodeCollectionChangedAction.Add:
                    {
                        Action = SettingsTreeNodeCollectionChangedAction.Add;
                        NewItems = new ReadOnlyCollection<SettingsTreeNode>(items.ToArray());
                    }
                    break;

                case SettingsTreeNodeCollectionChangedAction.Remove:
                    {
                        Action = SettingsTreeNodeCollectionChangedAction.Remove;
                        OldItems = new ReadOnlyCollection<SettingsTreeNode>(items.ToArray());
                    }
                    break;

                default:
                    throw new ArgumentException($"Action {action} not supported.");
            }
        }


        public SettingsTreeNodeCollectionChangedAction Action { get; } = SettingsTreeNodeCollectionChangedAction.None;

        public ReadOnlyCollection<SettingsTreeNode> NewItems { get; } = null;

        public ReadOnlyCollection<SettingsTreeNode> OldItems { get; } = null;


        public static SettingsTreeNodeCollectionChangedEventArgs CreateAdd(IList<SettingsTreeNode> newItems)
        {
            return new SettingsTreeNodeCollectionChangedEventArgs(SettingsTreeNodeCollectionChangedAction.Add, newItems);
        }

        public static SettingsTreeNodeCollectionChangedEventArgs CreateRemove(IList<SettingsTreeNode> oldItems)
        {
            return new SettingsTreeNodeCollectionChangedEventArgs(SettingsTreeNodeCollectionChangedAction.Remove, oldItems);
        }
    }


    public enum SettingsTreeNodeCollectionChangedAction
    {
        None,
        Add,
        Remove,
    }


    [JsonObject]
    public abstract class SettingsTreeNode : IReadOnlyDictionary<string, SettingsTreeNode>
    {
        [JsonProperty("Children")]
        private readonly SortedDictionary<string, SettingsTreeNode> _dictionary = new SortedDictionary<string, SettingsTreeNode>();
        [JsonProperty("Content")]
        private object _content = null;


        public event SettingsTreeNodeContentChangedEventHandler ContentChanged;

        public event SettingsTreeNodeCollectionChangedEventHandler CollectionChanged;


        protected SettingsTreeNode()
        {
        }

        
        [JsonIgnore]
        public SettingsTreeNode Parent { get; private set; } = null;

        [JsonIgnore]
        public object Content
        {
            get => _content;
            set
            {
                if (_content != value)
                {
                    _content = value;
                    OnContentChanged(this, EventArgs.Empty);
                }
            }
        }

        [JsonIgnore]
        public bool IsRoot => Parent == null;

        [JsonIgnore]
        public SettingsTreeNode Root
        {
            get
            {
                var child = this;
                var parent = Parent;

                while (parent != null)
                {
                    child = parent;
                    parent = parent.Parent;                    
                }

                return child;
            }
        }


        public TNode AddChild<TNode>(string name) where TNode : SettingsTreeNode, new()
        {
            var child = new TNode { Parent = this };

            _dictionary.Add(name, child);

            OnCollectionChanged(this, SettingsTreeNodeCollectionChangedEventArgs.CreateAdd(new SettingsTreeNode[] { child }));

            child.ContentChanged += Child_ContentChanged;
            child.CollectionChanged += Child_CollectionChanged;

            return child;
        }

        public TNode GetChild<TNode>(string name, bool createIfNotExist = true) where TNode : SettingsTreeNode, new()
        {
            return TryGetValue(name, out var value) ? (TNode)value : createIfNotExist ? AddChild<TNode>(name) : throw new KeyNotFoundException($"Child with name {name} not found.");
        }

        public void RemoveChild(string name)
        {
            var child = _dictionary[name];

            child.ContentChanged -= Child_ContentChanged;
            child.CollectionChanged -= Child_CollectionChanged;

            _dictionary.Remove(name);

            var oldItems = child.GetAllChildren().ToList();
            oldItems.Add(child);
            OnCollectionChanged(this, SettingsTreeNodeCollectionChangedEventArgs.CreateRemove(oldItems));
        }

        public IReadOnlyCollection<SettingsTreeNode> GetAllChildren()
        {
            var children = new List<SettingsTreeNode>();

            foreach (var child in _dictionary.Values)
            {
                children.AddRange(child.GetAllChildren());

                children.Add(child);
            }

            return children.AsReadOnly();
        }        

        public T GetChildTypedContent<T>(string name) => (T)this[name].Content;

        private void OnContentChanged(SettingsTreeNode sender, EventArgs e) => ContentChanged?.Invoke(sender, e);
        private void OnCollectionChanged(SettingsTreeNode sender, SettingsTreeNodeCollectionChangedEventArgs e) => CollectionChanged?.Invoke(sender, e);

        private void Child_ContentChanged(SettingsTreeNode sender, EventArgs e) => OnContentChanged(sender, e);
        private void Child_CollectionChanged(SettingsTreeNode sender, SettingsTreeNodeCollectionChangedEventArgs e) => OnCollectionChanged(sender, e);

        [OnDeserialized]
        private void OnDeserializedMethod(StreamingContext context)
        {
            foreach (var child in _dictionary.Values)
            {
                child.Parent = this;

                child.ContentChanged += Child_ContentChanged;
                child.CollectionChanged += Child_CollectionChanged;
            }
        }


        [JsonIgnore]
        public SettingsTreeNode this[string key] => _dictionary[key];
        [JsonIgnore]
        public IEnumerable<string> Keys => _dictionary.Keys;
        [JsonIgnore]
        public IEnumerable<SettingsTreeNode> Values => _dictionary.Values;
        [JsonIgnore]
        public int Count => _dictionary.Count;

        public bool ContainsKey(string key) => _dictionary.ContainsKey(key);
        public IEnumerator<KeyValuePair<string, SettingsTreeNode>> GetEnumerator() => (_dictionary as IEnumerable<KeyValuePair<string, SettingsTreeNode>>).GetEnumerator();
        public bool TryGetValue(string key, [MaybeNullWhen(false)] out SettingsTreeNode value) => _dictionary.TryGetValue(key, out value);

        IEnumerator IEnumerable.GetEnumerator() => (_dictionary as IEnumerable).GetEnumerator();
    }

    public sealed class JsonRootSettingsTreeNode : SettingsTreeNode
    {
        public const string DefaultSettingsLocalPath = "settingstree.json";

        public static readonly string AppDataSettingsFileName = null;
        public static readonly string AppDataSettingsDirectoryName = null;
        public static readonly string AppDataSettingsDirectoryPath = null;
        public static readonly string AppDataSettingsFullPath = null;

        static JsonRootSettingsTreeNode()
        {
            AppDataSettingsFileName = DefaultSettingsLocalPath;

            var executablePath = Environment.GetCommandLineArgs()[0];
            var executableName = Path.GetFileNameWithoutExtension(executablePath);
            AppDataSettingsDirectoryName = executableName;

            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            AppDataSettingsDirectoryPath = appDataPath;

            AppDataSettingsFullPath = $"{AppDataSettingsDirectoryPath}\\{AppDataSettingsDirectoryName}\\{AppDataSettingsFileName}";
        }


        private JsonRootSettingsTreeNode()
        {
            ContentChanged += JsonRootSettingsTreeNode_ContentChanged;
            CollectionChanged += JsonRootSettingsTreeNode_CollectionChanged;
        }


        private void JsonRootSettingsTreeNode_CollectionChanged(SettingsTreeNode sender, SettingsTreeNodeCollectionChangedEventArgs e) => Serialize();

        private void JsonRootSettingsTreeNode_ContentChanged(SettingsTreeNode sender, EventArgs e) => Serialize();


        [JsonIgnore]
        public string SettingsPath { get; private set; } = DefaultSettingsLocalPath;


        private void Serialize()
        {
            var jsonString = JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto,
                Converters = { new StringEnumConverter() }
            });
            new FileInfo(SettingsPath).Directory.Create();
            File.WriteAllText(SettingsPath, jsonString);
        }

        public static JsonRootSettingsTreeNode Open(string settingsPath = DefaultSettingsLocalPath, bool createIfNotExist = true)
        {
            if (settingsPath == null)
            {
                throw new ArgumentNullException(nameof(settingsPath));
            }

            try
            {
                var jsonString = File.ReadAllText(settingsPath);
                var jsonSettingsTreeNode = JsonConvert.DeserializeObject<JsonRootSettingsTreeNode>(jsonString, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Auto,
                    Converters = { new StringEnumConverter() }
                });
                jsonSettingsTreeNode.SettingsPath = settingsPath;
                return jsonSettingsTreeNode;
            }
            catch
            {
                if (!createIfNotExist)
                {
                    throw;
                }

                return new JsonRootSettingsTreeNode { SettingsPath = settingsPath };
            }
        }
    }

    public sealed class DependencyPropertyObserverSettingsTreeNode : SettingsTreeNode
    {
        public void RegisterProperty(FrameworkElement frameworkElement, DependencyProperty dependencyProperty)
        {
            GetChild<DependencyPropertyValueSettingsTreeNode>(CreatePropertyName(frameworkElement, dependencyProperty)).RegisterProperty(frameworkElement, dependencyProperty);
        }

        public void UnregisterProperty(FrameworkElement frameworkElement, DependencyProperty dependencyProperty)
        {
            if (TryGetValue(CreatePropertyName(frameworkElement, dependencyProperty), out var value))
            {
                ((DependencyPropertyValueSettingsTreeNode)value).UnregisterProperty();
            }
        }        

        public T GetStoredPropertyValueOrDefault<T>(FrameworkElement frameworkElement, DependencyProperty dependencyProperty)
        {
            return (TryGetValue(CreatePropertyName(frameworkElement, dependencyProperty), out var value) && value.Content is T typedValue) ? typedValue : default;
        }

        public void SetStoredPropertyValueOrDefaultAndRegisterProperty<T>(FrameworkElement frameworkElement, DependencyProperty dependencyProperty, T defaultValue = default, Func<T, T> corrector = null)
        {
            var name = CreatePropertyName(frameworkElement, dependencyProperty);
            var isStored = TryGetValue(name, out var value);

            var content = isStored ? (T)value.Content : defaultValue;

            if (corrector != null)
            {
                content = corrector(content);
            }

            frameworkElement.SetValue(dependencyProperty, content);

            (isStored ? (DependencyPropertyValueSettingsTreeNode)value : AddChild<DependencyPropertyValueSettingsTreeNode>(name)).RegisterProperty(frameworkElement, dependencyProperty);
        }

        public static string CreatePropertyName(FrameworkElement frameworkElement, DependencyProperty dependencyProperty) => $"{frameworkElement.Name}.{dependencyProperty.Name}";

        public static DependencyPropertyObserverSettingsTreeNode AddToParent(SettingsTreeNode parent, string name) => parent.AddChild<DependencyPropertyObserverSettingsTreeNode>(name);

        public static DependencyPropertyObserverSettingsTreeNode GetFromParent(SettingsTreeNode parent, string name, bool createIfNotExist = true)
        {
            return parent.GetChild<DependencyPropertyObserverSettingsTreeNode>(name, createIfNotExist);
        }


        [DebuggerDisplay("Content = {Content}")]
        private sealed class DependencyPropertyValueSettingsTreeNode : SettingsTreeNode
        {
            private FrameworkElement _frameworkElement;
            private DependencyProperty _dependencyProperty;
            private DependencyPropertyDescriptor _dependencyPropertyDescriptor;


            public void RegisterProperty(FrameworkElement frameworkElement, DependencyProperty dependencyProperty)
            {
                _frameworkElement = frameworkElement;
                _dependencyProperty = dependencyProperty;
                _dependencyPropertyDescriptor = DependencyPropertyDescriptor.FromProperty(_dependencyProperty, _dependencyProperty.OwnerType);

                _dependencyPropertyDescriptor.AddValueChanged(_frameworkElement, Property_ValueChanged);

                Content = _frameworkElement.GetValue(_dependencyProperty);
            }

            public void UnregisterProperty()
            {
                _dependencyPropertyDescriptor.RemoveValueChanged(_frameworkElement, Property_ValueChanged);

                _frameworkElement = null;
                _dependencyProperty = null;
                _dependencyPropertyDescriptor = null;
            }

            private void Property_ValueChanged(object sender, EventArgs e)
            {
                Content = _frameworkElement.GetValue(_dependencyProperty);
            }

            public override string ToString() => Content?.ToString();
        }
    }

    public sealed class ContentSettingsTreeNode<T> : SettingsTreeNode
    {
        public new T Content
        {
            get => (T)base.Content;
            set => base.Content = value;
        }
    }
}
