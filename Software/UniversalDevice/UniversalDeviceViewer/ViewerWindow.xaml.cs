﻿using System.Windows;
using System.Windows.Data;

using UniversalDeviceViewer.Models;
using UniversalDeviceViewer.Views;

namespace UniversalDeviceViewer
{
    public partial class ViewerWindow : Window
    {
        private static readonly DependencyProperty ActiveDeviceProperty = DependencyProperty.Register(nameof(ActiveDevice), typeof(UniversalDeviceModel), typeof(ViewerWindow));


        public ViewerWindow()
        {
            InitializeComponent();

            SetBinding(ActiveDeviceProperty, new Binding(nameof(DeviceSelectorView.SelectedDevice)) { Source = deviceSelectorView, Mode = BindingMode.OneWay });
            deviceView.SetBinding(DeviceView.DeviceProperty, new Binding(nameof(ActiveDevice)) { Source = this, Mode = BindingMode.OneWay });
        }


        public UniversalDeviceModel ActiveDevice
        {
            get { return (UniversalDeviceModel)GetValue(ActiveDeviceProperty); }
            private set { SetValue(ActiveDeviceProperty, value); }
        }
    }
}
