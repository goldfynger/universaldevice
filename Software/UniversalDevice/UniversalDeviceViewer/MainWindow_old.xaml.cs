﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

using Goldfynger.UniversalDevice;
using Goldfynger.UniversalDevice.Connector;
using Goldfynger.UniversalDevice.FlashStorage;
using Goldfynger.UniversalDevice.I2C;
using Goldfynger.UniversalDevice.Info;
using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.Utils.Extensions;

namespace UniversalDeviceViewer
{
    internal partial class MainWindow_old : Window, IDisposable
    {
        private IDeviceConnector _deviceConnector;

        private IDevice _device;

        private IDeviceInfo _deviceInfo;
        private IFlashStorageDevice _flashStorageDevice;
        private INrf24Device _nrf24Device;
        private Listener<Nrf24RxData> _nrf24Listener;
        private CancellationTokenSource _nrf24ListenCancellationTokenSource;

        private readonly JsonRootSettingsTreeNode _jsonRootSettings;
        private readonly DependencyPropertyObserverSettingsTreeNode _dependencyPropertyObserver;


        public MainWindow_old()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;

            cbDeviceConnectorNames.SelectionChanged += CbDeviceConnectorNames_SelectionChanged;
            cbDevices.SelectionChanged += CbDevices_SelectionChanged;
            bReconnect.Click += BReconnect_Click;
            bClear.Click += BClear_Click;
            bI2CP1.Click += BI2CP1_Click;
            bI2CP2.Click += BI2CP2_Click;
            bReadInfo.Click += BReadInfo_Click;

            bOpen.Click += BOpen_Click;
            bClose.Click += BClose_Click;
            bReadCount.Click += BReadCount_Click;
            bReadNames.Click += BReadNames_Click;
            bReadRecords.Click += BReadRecords_Click;

            bGetDefaultConfig.Click += BGetDefaultConfig_Click;
            bGetPreselectedConfig.Click += BGetPreselectedConfig_Click;
            bGetActiveConfig.Click += BGetActiveConfig_Click;
            bStartListen.Click += BStartListen_Click;
            bStopListen.Click += BStopListen_Click;

            _jsonRootSettings = JsonRootSettingsTreeNode.Open(JsonRootSettingsTreeNode.AppDataSettingsFullPath);
            _dependencyPropertyObserver = DependencyPropertyObserverSettingsTreeNode.GetFromParent(_jsonRootSettings, nameof(MainWindow_old));
        }


        private void MainWindow_Loaded(object sender, RoutedEventArgs e) => FillConnectorNames();

        private void CbDeviceConnectorNames_SelectionChanged(object sender, SelectionChangedEventArgs e) => UpdateDevicesList();

        private void CbDevices_SelectionChanged(object sender, SelectionChangedEventArgs e) => UpdateDevice();

        private void BReconnect_Click(object sender, RoutedEventArgs e) => Connect();

        private void BClear_Click(object sender, RoutedEventArgs e) => tbLog.Clear();

        private void BI2CP1_Click(object sender, RoutedEventArgs e) => ShowDialogI2CViewer(I2CPortNumbers.Port1);
        private void BI2CP2_Click(object sender, RoutedEventArgs e) => ShowDialogI2CViewer(I2CPortNumbers.Port2);

        private async void BReadInfo_Click(object sender, RoutedEventArgs e) => await ReadInfo();

        private async void BOpen_Click(object sender, RoutedEventArgs e) => await FlashStorageOpen();
        private async void BClose_Click(object sender, RoutedEventArgs e) => await FlashStorageClose();
        private async void BReadCount_Click(object sender, RoutedEventArgs e) => await FlashStorageGetRecordsCount();
        private async void BReadNames_Click(object sender, RoutedEventArgs e) => await FlashStorageGetRecordsNames();
        private async void BReadRecords_Click(object sender, RoutedEventArgs e) => await FlashStorageReadAllRecords();

        private async void BGetDefaultConfig_Click(object sender, RoutedEventArgs e) => await Nrf24GetDefaultConig();
        private async void BGetPreselectedConfig_Click(object sender, RoutedEventArgs e) => await Nrf24GetPreselectedConig();
        private async void BGetActiveConfig_Click(object sender, RoutedEventArgs e) => await Nrf24GetActiveConig();
        private async void BStartListen_Click(object sender, RoutedEventArgs e) => await Nrf24StartListen();
        private async void BStopListen_Click(object sender, RoutedEventArgs e) => await Nrf24StopListen();


        private void FillConnectorNames()
        {
            new DeviceConnectorFactory().AvailableTypes.ForEach(type => cbDeviceConnectorNames.Items.Add(type.Description));

            _dependencyPropertyObserver.SetStoredPropertyValueOrDefaultAndRegisterProperty<string>(cbDeviceConnectorNames, Selector.SelectedItemProperty, corrector: value =>
            {
                var deviceConnectorNames = cbDeviceConnectorNames.Items.OfType<string>().ToList();
                return (value != null && deviceConnectorNames.Any(i => i == value)) ? value : deviceConnectorNames.FirstOrDefault();
            });
        }

        private void UpdateDevicesList()
        {
            cbDevices.Items.Clear();

            var connectorFactory = new DeviceConnectorFactory();
            var connectorTypes = connectorFactory.AvailableTypes;
            var connectorType = connectorTypes.Where(type => type.Description == (string)cbDeviceConnectorNames.SelectedItem).First();

            connectorFactory.GetAvailableIdentifiers(connectorType).ForEach(id => cbDevices.Items.Add(id.Identifier));

            var devices = cbDevices.Items.OfType<string>().ToList();

            var deviceConnectorNamesPropertyName = DependencyPropertyObserverSettingsTreeNode.CreatePropertyName(cbDeviceConnectorNames, Selector.SelectedItemProperty);
            var deviceConnectorNamesPropertyValue = cbDeviceConnectorNames.SelectedItem as string;
            var deivicesPropertyName = DependencyPropertyObserverSettingsTreeNode.CreatePropertyName(cbDevices, Selector.SelectedItemProperty);

            var deviceToSelection = _dependencyPropertyObserver.GetChild<ContentSettingsTreeNode<string>>($"{deviceConnectorNamesPropertyName}+{deviceConnectorNamesPropertyValue}+{deivicesPropertyName}").Content;

            cbDevices.SelectedItem = (deviceToSelection != null && devices.Any(d => d == deviceToSelection)) ? deviceToSelection : null;
        }

        private void UpdateDevice()
        {
            var deviceConnectorNamesPropertyName = DependencyPropertyObserverSettingsTreeNode.CreatePropertyName(cbDeviceConnectorNames, Selector.SelectedItemProperty);
            var deviceConnectorNamesPropertyValue = cbDeviceConnectorNames.SelectedItem as string;
            var deivicesPropertyName = DependencyPropertyObserverSettingsTreeNode.CreatePropertyName(cbDevices, Selector.SelectedItemProperty);

            _dependencyPropertyObserver.GetChild<ContentSettingsTreeNode<string>>($"{deviceConnectorNamesPropertyName}+{deviceConnectorNamesPropertyValue}+{deivicesPropertyName}").Content =
                cbDevices.SelectedItem as string;

            Connect();
        }

        private void Connect()
        {
            _deviceInfo = null;
            _flashStorageDevice = null;
            _nrf24ListenCancellationTokenSource?.Cancel();
            _nrf24ListenCancellationTokenSource = null;
            _nrf24Listener?.Dispose();
            _nrf24Listener = null;
            _nrf24Device = null;
            _device?.Dispose();
            _device = null;
            _deviceConnector?.Dispose();
            _deviceConnector = null;


            var connectorFactory = new DeviceConnectorFactory();
            var connectorTypes = connectorFactory.AvailableTypes;
            var connectorType = connectorTypes.Where(type => type.Description == (string)cbDeviceConnectorNames.SelectedItem).First();
            var connectorIdentifiers = connectorFactory.GetAvailableIdentifiers(connectorType);
            var connectorIdenitifier = connectorIdentifiers.Where(id => id.Identifier == (string)cbDevices.SelectedItem).First();

            try
            {
                _deviceConnector = connectorFactory.Create(connectorType, connectorIdenitifier);

                var infoBuilder = new StringBuilder();
                var connectorInfo = _deviceConnector.Info;

                for (var index = 0; index < connectorInfo.Count; index++)
                {
                    if (index > 0)
                    {
                        infoBuilder.Append('|');
                    }
                    var pair = connectorInfo.ElementAt(index);
                    infoBuilder.Append($"{pair.Key}:{pair.Value}");
                }
            }
            catch (Exception ex)
            {
                var message = ex.InnerException == null ? $"{ex.Message}" : $"{ex.Message}{Environment.NewLine}{ex.InnerException.Message}";
                MessageBox.Show($"{message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (_deviceConnector != null)
            {
                try
                {
                    _device = new DeviceFactory().Create(_deviceConnector);
                }
                catch (Exception ex)
                {
                    _deviceConnector.Dispose();
                    _deviceConnector = null;

                    MessageBox.Show($"Device creation: {ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            if (_device != null)
            {
                try
                {
                    _deviceInfo = _device.CreateDeviceInfo();
                    _flashStorageDevice = _device.CreateFlashStorageDevice();
                    _nrf24Device = _device.CreateNrf24Device();
                    _nrf24Listener = _nrf24Device.GetListener();
                }
                catch (Exception ex)
                {
                    _deviceInfo = null;
                    _flashStorageDevice = null;
                    _nrf24Listener?.Dispose();
                    _nrf24Listener = null;
                    _nrf24Device = null;
                    _device.Dispose();
                    _deviceConnector.Dispose();
                    _device = null;
                    _deviceConnector = null;

                    MessageBox.Show($"Flash storage creation: {ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ShowDialogI2CViewer(I2CPortNumbers portNumber)
        {
            portNumber.ThrowIfNotDefined();            

            new I2CViewer(_device.CreateI2CDevice(portNumber), DependencyPropertyObserverSettingsTreeNode.GetFromParent(_jsonRootSettings, $"{nameof(I2CViewer)}+{portNumber}")).ShowDialog();
        }


        private async Task ReadInfo()
        {
            try
            {
                var info = await _deviceInfo.GetInfo();
                tbInfo.Text = info.ToString();
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(ReadInfo)}: {info}.{Environment.NewLine}");

                var uid = await _deviceInfo.GetUid();
                tbUid.Text = uid.ToString();
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(ReadInfo)}: {uid}.{Environment.NewLine}");
            }
            catch (UniversalDeviceException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(ReadInfo)}: {nameof(UniversalDeviceException)}: {ex.DeviceError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(ReadInfo)}: {nameof(Exception)}: {ex.Message}.{Environment.NewLine}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private async Task FlashStorageOpen()
        {
            try
            {
                await _flashStorageDevice.OpenAsync();

                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageOpen)}: opened.{Environment.NewLine}");
            }
            catch (FlashStorageException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageOpen)}: {nameof(FlashStorageException)}: {ex.DeviceError}|{ex.FlashStorageError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.FlashStorageError}", $"{nameof(FlashStorageException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageOpen)}: {nameof(UniversalDeviceException)}: {ex.DeviceError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageOpen)}: {nameof(Exception)}: {ex.Message}.{Environment.NewLine}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task FlashStorageClose()
        {
            try
            {
                await _flashStorageDevice.CloseAsync();

                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageClose)}: closed.{Environment.NewLine}");
            }
            catch (FlashStorageException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageClose)}: {nameof(FlashStorageException)}: {ex.DeviceError}|{ex.FlashStorageError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.FlashStorageError}", $"{nameof(FlashStorageException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageClose)}: {nameof(UniversalDeviceException)}: {ex.DeviceError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageClose)}: {nameof(Exception)}: {ex.Message}.{Environment.NewLine}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task FlashStorageGetRecordsCount()
        {
            try
            {
                var count = await _flashStorageDevice.GetRecordsCountAsync();

                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageGetRecordsCount)}: {count}.{Environment.NewLine}");
            }
            catch (FlashStorageException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageGetRecordsCount)}: {nameof(FlashStorageException)}: {ex.DeviceError}|{ex.FlashStorageError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.FlashStorageError}", $"{nameof(FlashStorageException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageGetRecordsCount)}: {nameof(UniversalDeviceException)}: {ex.DeviceError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageGetRecordsCount)}: {nameof(Exception)}: {ex.Message}.{Environment.NewLine}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task FlashStorageGetRecordsNames()
        {
            try
            {
                var names = await _flashStorageDevice.GetRecordsNamesAsync();

                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageGetRecordsNames)}:{Environment.NewLine}");
                foreach (var name in names)
                {
                    tbLog.AppendText($" - {name}{Environment.NewLine}");
                }
            }
            catch (FlashStorageException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageGetRecordsNames)}: {nameof(FlashStorageException)}: {ex.DeviceError}|{ex.FlashStorageError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.FlashStorageError}", $"{nameof(FlashStorageException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageGetRecordsNames)}: {nameof(UniversalDeviceException)}: {ex.DeviceError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageGetRecordsNames)}: {nameof(Exception)}: {ex.Message}.{Environment.NewLine}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task FlashStorageReadAllRecords()
        {
            try
            {
                try
                {
                    tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageReadAllRecords)} open...");

                    await _flashStorageDevice.OpenAsync();

                    tbLog.AppendText($" success.{Environment.NewLine}");
                }
                catch (FlashStorageException ex)
                {
                    if (ex.DeviceError == DeviceErrors.UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_ALREADY_ACQUIRED)
                    {
                        tbLog.AppendText($" already opened.{Environment.NewLine}");
                    }
                    else
                    {
                        throw;
                    }
                }

                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageReadAllRecords)} read names...");

                var names = await _flashStorageDevice.GetRecordsNamesAsync();

                tbLog.AppendText($" success:{Environment.NewLine}");
                foreach (var name in names)
                {
                    tbLog.AppendText($" - {name}{Environment.NewLine}");
                }

                foreach (var name in names)
                {
                    tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageReadAllRecords)} read record {name}...");

                    var record = await _flashStorageDevice.ReadRecordAsync(name);

                    tbLog.AppendText($" success:{Environment.NewLine}");
                    tbLog.AppendText($" - {string.Join(" ", record.Select(x => x.ToString("X2")))}{Environment.NewLine}");
                }

                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(FlashStorageReadAllRecords)} close...");

                await _flashStorageDevice.CloseAsync();

                tbLog.AppendText($" success.{Environment.NewLine}");
            }
            catch (FlashStorageException ex)
            {
                tbLog.AppendText($" {nameof(FlashStorageException)}: {ex.DeviceError}|{ex.FlashStorageError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.FlashStorageError}", $"{nameof(FlashStorageException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                tbLog.AppendText($" {nameof(UniversalDeviceException)}: {ex.DeviceError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                tbLog.AppendText($" {nameof(Exception)}: {ex.Message}{Environment.NewLine}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private async Task Nrf24GetDefaultConig()
        {
            try
            {
                var config = await _nrf24Device.GetDefaultConfig();

                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetDefaultConig)}: {config}.{Environment.NewLine}");
            }
            catch (Nrf24Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetDefaultConig)}: {nameof(Nrf24Exception)}: {ex.DeviceError}|{ex.Nrf24Error}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.Nrf24Error}", $"{nameof(Nrf24Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetDefaultConig)}: {nameof(UniversalDeviceException)}: {ex.DeviceError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetDefaultConig)}: {nameof(Exception)}: {ex.Message}.{Environment.NewLine}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task Nrf24GetPreselectedConig()
        {
            try
            {
                var config = await _nrf24Device.GetPreselectedConfig();

                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetPreselectedConig)}: {config}.{Environment.NewLine}");
            }
            catch (Nrf24Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetPreselectedConig)}: {nameof(Nrf24Exception)}: {ex.DeviceError}|{ex.Nrf24Error}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.Nrf24Error}", $"{nameof(Nrf24Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetPreselectedConig)}: {nameof(UniversalDeviceException)}: {ex.DeviceError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetPreselectedConig)}: {nameof(Exception)}: {ex.Message}.{Environment.NewLine}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task Nrf24GetActiveConig()
        {
            try
            {
                var config = await _nrf24Device.GetActiveConfig();

                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetActiveConig)}: {config}.{Environment.NewLine}");
            }
            catch (Nrf24Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetActiveConig)}: {nameof(Nrf24Exception)}: {ex.DeviceError}|{ex.Nrf24Error}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.Nrf24Error}", $"{nameof(Nrf24Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetActiveConig)}: {nameof(UniversalDeviceException)}: {ex.DeviceError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24GetActiveConig)}: {nameof(Exception)}: {ex.Message}.{Environment.NewLine}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task Nrf24StartListen()
        {
            try
            {
                await _nrf24Device.StartListen();

                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24StartListen)}: start.{Environment.NewLine}");
            }
            catch (Nrf24Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24StartListen)}: {nameof(Nrf24Exception)}: {ex.DeviceError}|{ex.Nrf24Error}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.Nrf24Error}", $"{nameof(Nrf24Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24StartListen)}: {nameof(UniversalDeviceException)}: {ex.DeviceError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24StartListen)}: {nameof(Exception)}: {ex.Message}.{Environment.NewLine}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }


            if (_nrf24ListenCancellationTokenSource == null)
            {
                _nrf24ListenCancellationTokenSource = new CancellationTokenSource();
                Listen(_nrf24ListenCancellationTokenSource.Token);
            }
        }

        private async Task Nrf24StopListen()
        {
            try
            {
                await _nrf24Device.StopListen();

                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24StopListen)}: stop.{Environment.NewLine}");
            }
            catch (Nrf24Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24StopListen)}: {nameof(Nrf24Exception)}: {ex.DeviceError}|{ex.Nrf24Error}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}{Environment.NewLine}{ex.Nrf24Error}", $"{nameof(Nrf24Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (UniversalDeviceException ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24StopListen)}: {nameof(UniversalDeviceException)}: {ex.DeviceError}.{Environment.NewLine}");

                MessageBox.Show($"{ex.DeviceError}", $"{nameof(UniversalDeviceException)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} {nameof(Nrf24StopListen)}: {nameof(Exception)}: {ex.Message}.{Environment.NewLine}");

                MessageBox.Show($"{ex.Message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (_nrf24ListenCancellationTokenSource != null)
            {
                _nrf24ListenCancellationTokenSource.Cancel();
                _nrf24ListenCancellationTokenSource = null;
            }
        }

        private void Listen(CancellationToken token)
        {
            Task.Run(() =>
            {
                while (true)
                {
                    token.ThrowIfCancellationRequested();

                    try
                    {
                        var rxDataTask = _nrf24Listener.ReadAsync(token);
                        rxDataTask.Wait();
                        var rxData = rxDataTask.Result;

                        tbLog.Dispatcher.Invoke(() =>
                        {
                            tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} ListenerRx: {string.Join(" ", rxData.Data.Select(x => x.ToString("X2")))}.{Environment.NewLine}");
                            tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} ListenerRx: {rxData.Address}|{rxData.RxPipe}.{Environment.NewLine}");
                        });
                    }
                    catch (Exception ex)
                    {
                        tbLog.Dispatcher.Invoke(() =>
                        {
                            tbLog.AppendText($"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff} ListenerException: {nameof(Exception)}: {ex.Message}{Environment.NewLine}");
                        });

                        token.ThrowIfCancellationRequested();
                        return;
                    }
                }
            }, token);
        }


        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _nrf24ListenCancellationTokenSource?.Cancel();
                    _nrf24Listener?.Dispose();
                    _device?.Dispose();
                    _deviceConnector?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~MainWindow()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
