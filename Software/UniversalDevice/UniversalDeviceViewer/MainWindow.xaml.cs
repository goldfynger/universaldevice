﻿using System.Collections.Generic;
using System.Windows;

using Goldfynger.UniversalDevice.Connector;
using Goldfynger.Utils.Collections;
using Goldfynger.Utils.Extensions;

namespace UniversalDeviceViewer
{
    internal partial class MainWindow : Window
    {
        private static readonly DependencyProperty ConnectorsProperty =
            DependencyProperty.Register(nameof(Connectors), typeof(Dictionary<string, ExtendedObservableCollection<IDeviceConnectorIdentifier>>), typeof(MainWindow),
                new PropertyMetadata(new Dictionary<string, ExtendedObservableCollection<IDeviceConnectorIdentifier>>()));


        public MainWindow()
        {
            var deviceConnectorFactory = new DeviceConnectorFactory();
            deviceConnectorFactory.AvailableTypes.ForEach(t => Connectors.Add(t.Description, new ExtendedObservableCollection<IDeviceConnectorIdentifier>(deviceConnectorFactory.GetAvailableIdentifiers(t))));

            InitializeComponent();
        }


        public Dictionary<string, ExtendedObservableCollection<IDeviceConnectorIdentifier>> Connectors
        {
            get { return (Dictionary<string, ExtendedObservableCollection<IDeviceConnectorIdentifier>>)GetValue(ConnectorsProperty); }
            private set { SetValue(ConnectorsProperty, value); }
        }
    }
}
