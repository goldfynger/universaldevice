﻿using System;
using System.Diagnostics;
using System.Windows;

using Goldfynger.UniversalDevice;
using Goldfynger.UniversalDevice.Connector;

using Prism.Mvvm;

using UniversalDeviceViewer.Services;

namespace UniversalDeviceViewer.Models
{
    [DebuggerDisplay("UniversalDeviceModel: {Type.Type}|{Identifier.Identifier}|{}")]
    public sealed class UniversalDeviceModel : BindableBase
    {
        private UniversalDeviceModelStates _state = UniversalDeviceModelStates.NotConnected;

        private IDeviceConnector _deviceConnector;
        private IDevice _device;


        public UniversalDeviceModel(IDeviceConnectorType type, IDeviceConnectorIdentifier identifier)
        {
            Type = type ?? throw new ArgumentNullException(nameof(type));
            Identifier = identifier ?? throw new ArgumentNullException(nameof(identifier));
        }


        private void Device_Corrupted(object sender, EventArgs e)
        {
            State = UniversalDeviceModelStates.Corrupted;
        }


        public IDeviceConnectorType Type { get; }

        public IDeviceConnectorIdentifier Identifier { get; }

        public UniversalDeviceModelStates State
        {
            get => _state;
            private set
            {
                SetProperty(ref _state, value);
            }
        }


        public void Connect()
        {
            Disconnect();

            try
            {
                _deviceConnector = DeviceConnectorFactoryService.Factory.Create(Type, Identifier);
            }
            catch (Exception ex)
            {
                Disconnect();

                var message = ex.InnerException == null ? $"{ex.Message}" : $"{ex.Message}{Environment.NewLine}{ex.InnerException.Message}";
                MessageBox.Show($"Connector creation: {message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (_deviceConnector != null)
            {
                try
                {
                    _device = DeviceFactoryService.Factory.Create(_deviceConnector);

                    _device.Corrupted += Device_Corrupted;

                    State = _device.IsCorrupted ? UniversalDeviceModelStates.Corrupted : UniversalDeviceModelStates.Connected;
                }
                catch (Exception ex)
                {
                    Disconnect();

                    var message = ex.InnerException == null ? $"{ex.Message}" : $"{ex.Message}{Environment.NewLine}{ex.InnerException.Message}";
                    MessageBox.Show($"Device creation: {message}", $"{nameof(Exception)}", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        public void Disconnect()
        {
            if (_device != null)
            {
                _device.Corrupted -= Device_Corrupted;
                _device.Dispose();
            }
            _device = null;

            if (_deviceConnector != null)
            {
                _deviceConnector.Dispose();
            }
            _deviceConnector = null;

            State = UniversalDeviceModelStates.NotConnected;
        }


        public enum UniversalDeviceModelStates : int
        {
            NotConnected,
            Connected,
            Corrupted,
        }
    }
}
