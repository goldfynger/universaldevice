﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

using Goldfynger.Utils.Extensions;

using UniversalDeviceViewer.Models;
using UniversalDeviceViewer.Services;
using UniversalDeviceViewer.ViewModels;

namespace UniversalDeviceViewer.Views
{
    public partial class DeviceSelectorView : UserControl
    {
        private static readonly DependencyPropertyKey DevicesPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Devices), typeof(ReadOnlyObservableCollection<DeviceSelectorDeviceGroupViewModel>), typeof(DeviceSelectorView), new PropertyMetadata());
        public static readonly DependencyProperty DevicesProperty = DevicesPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey SelectedDevicePropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(SelectedDevice), typeof(UniversalDeviceModel), typeof(DeviceSelectorView), new PropertyMetadata());
        public static readonly DependencyProperty SelectedDeviceProperty = SelectedDevicePropertyKey.DependencyProperty;


        public DeviceSelectorView()
        {
            Devices =
                new ReadOnlyObservableCollection<DeviceSelectorDeviceGroupViewModel>(
                    new ObservableCollection<DeviceSelectorDeviceGroupViewModel>(
                        AvailableConnectorsService.AvailableConnectors
                            .Select(c =>
                                new DeviceSelectorDeviceGroupViewModel(c.Type.Description,
                                    new ReadOnlyObservableCollection<DeviceSelectorDeviceViewModel>(
                                        new ObservableCollection<DeviceSelectorDeviceViewModel>(c
                                            .Select(i =>
                                                new DeviceSelectorDeviceViewModel(
                                                    new UniversalDeviceModel(c.Type, i)))))))));

            Devices.ForEach(g => g.PropertyChanged += (sender, e) =>
            {
                switch (e.PropertyName)
                {
                    case nameof(DeviceSelectorDeviceGroupViewModel.SelectedDevice):
                        {
                            var group = (DeviceSelectorDeviceGroupViewModel)sender;

                            if (group.SelectedDevice != null)
                            {
                                if (SelectedDevice != null)
                                {
                                    Devices.Where(g => g.Description != group.Description).ForEach(g => g.SelectedDevice = null);
                                }

                                SelectedDevice = group.SelectedDevice.DeviceModel;
                            }
                        }
                        break;
                }
            });

            InitializeComponent();
        }


        public ReadOnlyObservableCollection<DeviceSelectorDeviceGroupViewModel> Devices
        {
            get { return (ReadOnlyObservableCollection<DeviceSelectorDeviceGroupViewModel>)GetValue(DevicesProperty); }
            private set { SetValue(DevicesPropertyKey, value); }
        }

        public UniversalDeviceModel SelectedDevice
        {
            get { return (UniversalDeviceModel)GetValue(SelectedDeviceProperty); }
            private set { SetValue(SelectedDevicePropertyKey, value); }
        }
    }
}
