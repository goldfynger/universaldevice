﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using Prism.Commands;

using UniversalDeviceViewer.Models;

namespace UniversalDeviceViewer.Views
{
    public partial class DeviceView : UserControl
    {
        public static readonly DependencyProperty DeviceProperty = DependencyProperty.Register(nameof(Device), typeof(UniversalDeviceModel), typeof(DeviceView));

        private static readonly DependencyProperty IsDeviceSetProperty = DependencyProperty.Register(nameof(IsDeviceSet), typeof(bool), typeof(DeviceView));

        private static readonly DependencyProperty IdentifierProperty = DependencyProperty.Register(nameof(Identifier), typeof(string), typeof(DeviceView));

        private static readonly DependencyProperty StateProperty = DependencyProperty.Register(nameof(State), typeof(string), typeof(DeviceView));


        public DeviceView()
        {
            InitializeComponent();

            SetBinding(IsDeviceSetProperty, new Binding(nameof(Device)) { Source = this, Converter = DeviceToIsDeviceSetConverter.Instance });
            SetBinding(IdentifierProperty, new Binding(nameof(Device)) { Source = this, Converter = DeviceToIdentifierConverter.Instance } );
            SetBinding(StateProperty, new Binding($"{nameof(Device)}.{nameof(UniversalDeviceModel.State)}") { Source = this, Converter = DeviceStateToStateConverter.Instance });

            ConnectCommand = new DelegateCommand(() => Device?.Connect());
            DisconnectCommand = new DelegateCommand(() => Device?.Disconnect());
        }


        public UniversalDeviceModel Device
        {
            get { return (UniversalDeviceModel)GetValue(DeviceProperty); }
            set { SetValue(DeviceProperty, value); }
        }

        public bool IsDeviceSet
        {
            get { return (bool)GetValue(IsDeviceSetProperty); }
            private set { SetValue(IsDeviceSetProperty, value); }
        }

        public string Identifier
        {
            get { return (string)GetValue(IdentifierProperty); }
            private set { SetValue(IdentifierProperty, value); }
        }

        public string State
        {
            get { return (string)GetValue(StateProperty); }
            private set { SetValue(StateProperty, value); }
        }

        public DelegateCommand ConnectCommand { get; }

        public DelegateCommand DisconnectCommand { get; }


        private class DeviceToIsDeviceSetConverter : IValueConverter
        {
            private static readonly Lazy<DeviceToIsDeviceSetConverter> _lazyInstance = new Lazy<DeviceToIsDeviceSetConverter>(() => new DeviceToIsDeviceSetConverter());


            public static DeviceToIsDeviceSetConverter Instance => _lazyInstance.Value;


            public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => (value as UniversalDeviceModel) != null;

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotSupportedException();
        }

        private class DeviceToIdentifierConverter : IValueConverter
        {
            private static readonly Lazy<DeviceToIdentifierConverter> _lazyInstance = new Lazy<DeviceToIdentifierConverter>(() => new DeviceToIdentifierConverter());


            public static DeviceToIdentifierConverter Instance => _lazyInstance.Value;


            public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => (value as UniversalDeviceModel)?.Identifier.Identifier;

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotSupportedException();
        }

        private class DeviceStateToStateConverter : IValueConverter
        {
            private static readonly Lazy<DeviceStateToStateConverter> _lazyInstance = new Lazy<DeviceStateToStateConverter>(() => new DeviceStateToStateConverter());


            public static DeviceStateToStateConverter Instance => _lazyInstance.Value;


            public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => ((UniversalDeviceModel.UniversalDeviceModelStates)value).ToString();

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotSupportedException();
        }
    }
}
