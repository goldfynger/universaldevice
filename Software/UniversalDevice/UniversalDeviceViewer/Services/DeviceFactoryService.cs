﻿using Goldfynger.UniversalDevice;

namespace UniversalDeviceViewer.Services
{
    public static class DeviceFactoryService
    {
        public static DeviceFactory Factory { get; } = new DeviceFactory();
    }
}
