﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

using Goldfynger.UniversalDevice.Connector;

namespace UniversalDeviceViewer.Services
{
    public static class AvailableConnectorsService
    {
        static AvailableConnectorsService()
        {
            AvailableConnectors =
                new DeviceConnectorTypesObservableCollection(
                    new ObservableCollection<DeviceConnectorIdentifiersObservableCollection>(
                        DeviceConnectorFactoryService.Factory.AvailableTypes
                            .Select(t =>
                                new DeviceConnectorIdentifiersObservableCollection(t,
                                    new ObservableCollection<IDeviceConnectorIdentifier>(
                                        DeviceConnectorFactoryService.Factory.GetAvailableIdentifiers(t))))));
        }


        public static DeviceConnectorTypesObservableCollection AvailableConnectors { get; }


        public sealed class DeviceConnectorTypesObservableCollection : ReadOnlyObservableCollection<DeviceConnectorIdentifiersObservableCollection>
        {
            public DeviceConnectorTypesObservableCollection(ObservableCollection<DeviceConnectorIdentifiersObservableCollection> identifiers) : base(identifiers)
            {
            }
        }

        public sealed class DeviceConnectorIdentifiersObservableCollection : ReadOnlyObservableCollection<IDeviceConnectorIdentifier>
        {
            public DeviceConnectorIdentifiersObservableCollection(IDeviceConnectorType type, ObservableCollection<IDeviceConnectorIdentifier> identifiers) : base(identifiers)
            {
                Type = type ?? throw new ArgumentNullException(nameof(type));
            }


            public IDeviceConnectorType Type { get; }
        }
    }
}
