﻿using Goldfynger.UniversalDevice.Connector;

namespace UniversalDeviceViewer.Services
{
    public static class DeviceConnectorFactoryService
    {
        public static DeviceConnectorFactory Factory { get; } = new DeviceConnectorFactory();
    }
}
