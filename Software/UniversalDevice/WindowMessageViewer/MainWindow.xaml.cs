﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;

namespace WindowMessageViewer
{
    public partial class MainWindow : Window
    {
        private int _index = 0;


        public MainWindow()
        {
            InitializeComponent();

            SourceInitialized += MainWindow_SourceInitialized;
            bSetLeft.Click += BSetLeft_Click;
            bSetTop.Click += BSetTop_Click;
            bSetWidth.Click += BSetWidth_Click;
            bSetHeight.Click += BSetHeight_Click;
        }


        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            var handle = new WindowInteropHelper(Application.Current.MainWindow).Handle;
            HwndSource.FromHwnd(handle).AddHook(new HwndSourceHook(WndProc));
        }

        private void BSetLeft_Click(object sender, RoutedEventArgs e) => Left = 50;

        private void BSetTop_Click(object sender, RoutedEventArgs e) => Top = 50;

        private void BSetWidth_Click(object sender, RoutedEventArgs e) => Width = 1000;

        private void BSetHeight_Click(object sender, RoutedEventArgs e) => Height = 700;

        private IntPtr WndProc(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            var vm = (WM)msg;

            switch (vm)
            {
                case WM.WM_ENTERSIZEMOVE:
                case WM.WM_EXITSIZEMOVE:
                case WM.WM_MOVE:
                case WM.WM_MOVING:
                case WM.WM_SIZE:
                case WM.WM_SIZING:
                    tbMessages.AppendText($"({++_index}) {vm}{Environment.NewLine}");
                    break;
            }

            return IntPtr.Zero;
        }


        /// <summary>
        /// Windows messages.
        /// </summary>
        internal enum WM : uint
        {
            /// <summary>Sent one time to a window after it enters the moving or sizing modal loop.</summary>
            WM_ENTERSIZEMOVE = 0x00000231,
            /// <summary>Sent one time to a window, after it has exited the moving or sizing modal loop.</summary>
            WM_EXITSIZEMOVE = 0x00000232,

            /// <summary>Sent after a window has been moved.</summary>
            WM_MOVE = 0x00000003,
            /// <summary>Sent to a window that the user is moving.</summary>
            WM_MOVING = 0x00000216,

            /// <summary>Sent to a window after its size has changed.</summary>
            WM_SIZE = 0x00000005,
            /// <summary>Sent to a window that the user is resizing.</summary>
            WM_SIZING = 0x00000214,
        }


        /// <summary>
        /// Contains information about the placement of a window on the screen.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct WINDOWPLACEMENT
        {
            public uint length;
            public uint flags;
            public uint showCmd;
            public POINT ptMinPosition;
            public POINT ptMaxPosition;
            public RECT rcNormalPosition;
        }

        /// <summary>
        /// The <see cref="POINT"/> structure defines the x- and y- coordinates of a point.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct POINT
        {
            public uint x;
            public uint y;
        }

        /// <summary>
        /// The <see cref="RECT"/> structure defines a rectangle by the coordinates of its upper-left and lower-right corners.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct RECT
        {
            public uint left;
            public uint top;
            public uint right;
            public uint bottom;
        }


        /// <summary>
        /// Retrieves the show state and the restored, minimized, and maximized positions of the specified window.
        /// </summary>
        /// <param name="hWnd">A handle to the window.</param>
        /// <param name="lpwndpl">A pointer to the <see cref="WINDOWPLACEMENT"/> structure that receives the show state and position information.</param>
        /// <returns>True if successful.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool GetWindowPlacement(
            IntPtr hWnd,
            ref WINDOWPLACEMENT lpwndpl);

        /// <summary>
        /// Sets the show state and the restored, minimized, and maximized positions of the specified window.
        /// </summary>
        /// <param name="hWnd">A handle to the window.</param>
        /// <param name="lpwndpl">A pointer to a <see cref="WINDOWPLACEMENT"/> structure that specifies the new show state and window positions.</param>
        /// <returns>True if successful.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool SetWindowPlacement(
            IntPtr hWnd,
            [In] ref WINDOWPLACEMENT lpwndpl);
    }


    /// <summary>
    /// <see href="https://stackoverflow.com/a/10132405">How do I create an Autoscrolling TextBox</see>
    /// </summary>
    public sealed class TextBoxBehaviour
    {
        public static readonly DependencyProperty ScrollOnTextChangedProperty =
            DependencyProperty.RegisterAttached("ScrollOnTextChanged", typeof(bool), typeof(TextBoxBehaviour), new UIPropertyMetadata(false, OnScrollOnTextChanged));


        private static readonly Dictionary<TextBox, Capture> __associations = new();


        public static bool GetScrollOnTextChanged(DependencyObject dependencyObject)
        {
            return (bool)dependencyObject.GetValue(ScrollOnTextChangedProperty);
        }

        public static void SetScrollOnTextChanged(DependencyObject dependencyObject, bool value)
        {
            dependencyObject.SetValue(ScrollOnTextChangedProperty, value);
        }


        private static void OnScrollOnTextChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            if (dependencyObject is not TextBox textBox)
            {
                return;
            }

            var oldValue = (bool)e.OldValue;
            var newValue = (bool)e.NewValue;

            if (newValue == oldValue)
            {
                return;
            }

            if (newValue)
            {
                textBox.Loaded += TextBoxLoaded;
                textBox.Unloaded += TextBoxUnloaded;
            }
            else
            {
                textBox.Loaded -= TextBoxLoaded;
                textBox.Unloaded -= TextBoxUnloaded;

                if (__associations.ContainsKey(textBox))
                {
                    __associations[textBox].Dispose();
                }
            }
        }

        private static void TextBoxUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            var textBox = (TextBox)sender;
            __associations[textBox].Dispose();
            textBox.Unloaded -= TextBoxUnloaded;
        }

        private static void TextBoxLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            var textBox = (TextBox)sender;
            textBox.Loaded -= TextBoxLoaded;
            __associations[textBox] = new Capture(textBox);
        }


        private class Capture : IDisposable
        {
            public Capture(TextBox textBox)
            {
                TextBox = textBox;
                TextBox.TextChanged += OnTextBoxOnTextChanged;
            }


            private TextBox TextBox { get; set; }


            private void OnTextBoxOnTextChanged(object sender, TextChangedEventArgs args)
            {
                TextBox.ScrollToEnd();
            }


            public void Dispose()
            {
                TextBox.TextChanged -= OnTextBoxOnTextChanged;
            }
        }
    }
}
