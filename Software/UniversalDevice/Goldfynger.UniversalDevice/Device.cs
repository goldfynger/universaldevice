﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using Goldfynger.UniversalDevice.Connector;
using Goldfynger.UniversalDevice.FlashStorage;
using Goldfynger.UniversalDevice.Fragmentation;
using Goldfynger.UniversalDevice.I2C;
using Goldfynger.UniversalDevice.Info;
using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.Utils.DataContainers;
using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice
{
    internal sealed class Device : IDevice
    {
        internal const ushort MessageIdMin = 0;
        internal const ushort MessageIdMax = (ushort)short.MaxValue;
        internal const ushort InitiativeMessageIdMin = (ushort)short.MaxValue + 1;
        internal const ushort InitiativeMessageIdMax = ushort.MaxValue;


        private ushort _messageId = MessageIdMin;
        private readonly object _messageIdLocker = new();

        private readonly DeviceConnector _connector;

        private readonly List<WeakReference<Listener<Message>>> _listerners = new();
        private readonly object _listernersLocker;

        private readonly object _exceptionLocker = new();
        private Exception? _exception;

        private readonly DefragmentPool _defragmentPool = new();


        public event EventHandler? Corrupted;


        internal Device(DeviceConnector connector)
        {
            _listernersLocker = (_listerners as ICollection).SyncRoot;

            if (connector == null)
            {
                throw new ArgumentNullException(nameof(connector));
            }

            if (connector.IsCorrupted)
            {
                throw new ArgumentException("Specified connector is corrupted.");
            }

            _connector = connector;

            _connector.ReadCompleted += Connector_ReadCompleted;
            _connector.ReadError += Connector_ReadError;
        }


        private void Connector_ReadCompleted(object? sender, DataEventArgs<Message> e)
        {
            if (_defragmentPool.Defragment(e.Data, out Message? whole))
            {
                lock (_listernersLocker)
                {
                    var outdatedReferences = new List<WeakReference<Listener<Message>>>(_listerners.Count);

                    foreach (var reference in _listerners)
                    {
                        if (reference.TryGetTarget(out Listener<Message>? listener))
                        {
                            try
                            {
                                listener.Write(whole);
                            }
                            catch
                            {
                            }
                        }
                        else
                        {
                            outdatedReferences.Add(reference);
                        }
                    }

                    foreach (var reference in outdatedReferences)
                    {
                        _listerners.Remove(reference);
                    }
                }
            }
        }

        private void Connector_ReadError(object? sender, DataEventArgs<Exception> e)
        {
            AddException(e.Data);

            lock (_listernersLocker)
            {
                var outdatedReferences = new List<WeakReference<Listener<Message>>>(_listerners.Count);

                foreach (var reference in _listerners)
                {
                    if (reference.TryGetTarget(out Listener<Message>? listener))
                    {
                        try
                        {
                            listener.SetException(e.Data);
                        }
                        catch
                        {
                        }
                    }
                    else
                    {
                        outdatedReferences.Add(reference);
                    }
                }

                foreach (var reference in outdatedReferences)
                {
                    _listerners.Remove(reference);
                }
            }
        }


        public IDeviceConnector Connector => _connector;

        public bool IsCorrupted
        {
            get
            {
                lock (_exceptionLocker)
                {
                    return _exception != null;
                }
            }
        }


        private void AddException(Exception ex)
        {
            bool wasCorrupted = false;

            lock (_exceptionLocker)
            {
                wasCorrupted = _exception != null;

                _exception = _exception == null ? ex : new AggregateException(_exception, ex);
            }

            if (!wasCorrupted)
            {
                Corrupted?.Invoke(this, EventArgs.Empty);
            }
        }

        internal Listener<Message> CreateListener(Func<Message, bool>? validator = null)
        {
            ThrowIfDisposed();
            ThrowIfException();

            lock (_listernersLocker)
            {
                var listener = Listener<Message>.Create(validator);

                _listerners.Add(new WeakReference<Listener<Message>>(listener));

                return listener;
            }
        }

        internal static bool IsInitiativeMessageId(uint messageId)
        {
            return messageId >= InitiativeMessageIdMin;
        }

        internal ushort NextMessageId()
        {
            ThrowIfDisposed();
            ThrowIfException();

            lock (_messageIdLocker)
            {
                if (_messageId == MessageIdMax)
                {
                    _messageId = MessageIdMin;
                }
                else
                {
                    _messageId += 1;
                }

                return _messageId;
            }
        }

        private void ThrowIfException()
        {
            lock (_exceptionLocker)
            {
                if (_exception != null)
                {
                    throw new InvalidOperationException($"Device state is corrupted.{Environment.NewLine}{_exception.Message}", _exception);
                }
            }
        }

        internal async Task WriteAsync(Message message)
        {
            ThrowIfDisposed();
            ThrowIfException();

            foreach (var fragment in message.Split(_connector.WriteMessageMaxSize))
            {
                await _connector.WriteAsync(fragment).ConfigureAwait(false);
            }
        }


        public IInfoDevice CreateDeviceInfo()
        {
            ThrowIfDisposed();
            ThrowIfException();

            return new InfoDevice(this);
        }

        public II2CDevice CreateI2CDevice(I2CPortNumbers portNumber)
        {
            ThrowIfDisposed();
            ThrowIfException();
            portNumber.ThrowIfNotDefined();

            return new I2CDevice(this, portNumber);
        }

        public IFlashStorageDevice CreateFlashStorageDevice()
        {
            ThrowIfDisposed();
            ThrowIfException();

            return new FlashStorageDevice(this);
        }

        public INrf24Device CreateNrf24Device()
        {
            ThrowIfDisposed();
            ThrowIfException();

            return new Nrf24Device(this);
        }


        private void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(Device));
            }
        }

        private bool _disposed;

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _connector.ReadCompleted -= Connector_ReadCompleted;
                    _connector.ReadError -= Connector_ReadError;
                    _connector.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~Device()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
