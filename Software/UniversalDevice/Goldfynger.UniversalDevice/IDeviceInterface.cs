﻿namespace Goldfynger.UniversalDevice
{
    /// <summary>
    /// Universal device interface.
    /// </summary>
    public interface IDeviceInterface
    {
        /// <summary>
        /// Gets status of device interface.
        /// </summary>
        bool IsCorrupted { get; }
    }
}
