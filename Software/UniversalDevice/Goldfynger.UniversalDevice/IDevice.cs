﻿using System;

using Goldfynger.UniversalDevice.Connector;
using Goldfynger.UniversalDevice.FlashStorage;
using Goldfynger.UniversalDevice.I2C;
using Goldfynger.UniversalDevice.Info;
using Goldfynger.UniversalDevice.Nrf24;

namespace Goldfynger.UniversalDevice
{
    /// <summary>
    /// Universal device.
    /// </summary>
    public interface IDevice : IDisposable
    {
        /// <summary>
        /// Device is corrupted.
        /// </summary>
        event EventHandler? Corrupted;


        /// <summary>
        /// Gets device connector.
        /// </summary>
        IDeviceConnector Connector { get; }

        /// <summary>
        /// Gets status of device.
        /// </summary>
        bool IsCorrupted { get; }


        /// <summary>
        /// Creates <see cref="IInfoDevice"/> interface.
        /// </summary>
        /// <returns><see cref="IInfoDevice"/> interface</returns>
        IInfoDevice CreateDeviceInfo();

        /// <summary>
        /// Creates <see cref="II2CDevice"/> interface.
        /// </summary>
        /// <param name="portNumber">Port number.</param>
        /// <returns><see cref="II2CDevice"/> interface</returns>
        II2CDevice CreateI2CDevice(I2CPortNumbers portNumber);

        /// <summary>
        /// Creates <see cref="IFlashStorageDevice"/> interface.
        /// </summary>
        /// <returns><see cref="IFlashStorageDevice"/> interface</returns>
        IFlashStorageDevice CreateFlashStorageDevice();

        /// <summary>
        /// Creates <see cref="Nrf24Device"/> interface.
        /// </summary>
        /// <returns><see cref="Nrf24Device"/> interface</returns>
        INrf24Device CreateNrf24Device();
    }
}
