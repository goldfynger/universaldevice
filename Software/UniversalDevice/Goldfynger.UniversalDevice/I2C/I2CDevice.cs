﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice.I2C
{
    internal sealed class I2CDevice : II2CDevice
    {
        private static readonly int __readWriteMaxDataSize = Message.BodyMaxSize - ReadWriteInfo.Size;
        private static readonly int __readWriteMemoryMaxDataSize = Message.BodyMaxSize - ReadWriteInfo.Size - ReadWriteMemoryInfo.Size;

        private static readonly TimeSpan __timeout = TimeSpan.FromSeconds(2);


        private readonly Device _device;

        private readonly I2CPortNumbers _portNumber;


        static I2CDevice() => Message.MessageHeader.AddCommandsToResolver<Commands>(DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_BASE_I2C);

        internal I2CDevice(Device device, I2CPortNumbers portNumber)
        {
            _device = device;

            portNumber.ThrowIfNotDefined();
            _portNumber = portNumber;
        }


        public bool IsCorrupted => _device.IsCorrupted;


        public async Task<bool> IsAcquiredAsync() => new IsAcquiredResponse(await Exchange(new IsAcquiredRequest(_device.NextMessageId(), _portNumber)).ConfigureAwait(false)).IsAcquired;

        public async Task AcquireAsync() => _ = new AcquireResponse(await Exchange(new AcquireRequest(_device.NextMessageId(), _portNumber)).ConfigureAwait(false));

        public async Task ReleaseAsync() => _ = new ReleaseResponse(await Exchange(new ReleaseRequest(_device.NextMessageId(), _portNumber)).ConfigureAwait(false));

        public async Task<IList<byte>> ReadAsync(I2CSpeeds speed, I2CAddress address, ushort size)
        {
            speed.ThrowIfNotDefined();

            if (size == 0 || size.IncreaseToMultipleOfFour() > __readWriteMaxDataSize)
            {
                throw new ArgumentOutOfRangeException(nameof(size));
            }

            return new ReadResponse(await Exchange(new ReadRequest(_device.NextMessageId(), _portNumber, new ReadWriteInfo(speed, address, size))).ConfigureAwait(false)).Data;
        }

        public async Task WriteAsync(I2CSpeeds speed, I2CAddress address, IList<byte> data)
        {
            speed.ThrowIfNotDefined();

            if (data.Count == 0 || data.Count.IncreaseToMultipleOfFour() > __readWriteMaxDataSize)
            {
                throw new ArgumentOutOfRangeException(nameof(data));
            }

            _ = new WriteResponse(await Exchange(new WriteRequest(_device.NextMessageId(), _portNumber, new ReadWriteInfo(speed, address, (ushort)data.Count), data)).ConfigureAwait(false));
        }

        public async Task<IList<byte>> ReadMemoryAsync(I2CSpeeds speed, I2CAddress address, I2CMemoryAddress memoryAddress, ushort size)
        {
            speed.ThrowIfNotDefined();

            if (size == 0 || size.IncreaseToMultipleOfFour() > __readWriteMemoryMaxDataSize)
            {
                throw new ArgumentOutOfRangeException(nameof(size));
            }

            return new ReadMemoryResponse(await Exchange(
                new ReadMemoryRequest(_device.NextMessageId(), _portNumber, new ReadWriteInfo(speed, address, size), new ReadWriteMemoryInfo(memoryAddress))).ConfigureAwait(false)).Data;
        }

        public async Task WriteMemoryAsync(I2CSpeeds speed, I2CAddress address, I2CMemoryAddress memoryAddress, IList<byte> data)
        {
            speed.ThrowIfNotDefined();

            if (data.Count == 0 || data.Count.IncreaseToMultipleOfFour() > __readWriteMemoryMaxDataSize)
            {
                throw new ArgumentOutOfRangeException(nameof(data));
            }

            _ = new WriteMemoryResponse(await Exchange(
                new WriteMemoryRequest(_device.NextMessageId(), _portNumber, new ReadWriteInfo(speed, address, (ushort)data.Count), new ReadWriteMemoryInfo(memoryAddress), data)).ConfigureAwait(false));
        }

        private async Task<Message> Exchange(Message request)
        {
            using var listener = _device.CreateListener(m =>
                m.Header.MessageId == request.Header.MessageId &&
                m.Header.InterfaceBase == request.Header.InterfaceBase &&
                m.Header.InterfacePort == request.Header.InterfacePort &&
                m.Header.InterfaceCommand == request.Header.InterfaceCommand);

            await _device.WriteAsync(request).ConfigureAwait(false);

            var response = await listener.ReadAsync(__timeout).ConfigureAwait(false);

            response.ThrowIfError(m =>
            {
                var errorResponse = new ErrorResponse(m);
                return new I2CHalException(errorResponse, errorResponse.I2CHalError);
            });

            return response;
        }


        private enum Commands : byte
        {
            /// <summary>
            /// Request: <see cref="IsAcquiredRequest"/>
            /// Response: <see cref="IsAcquiredResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_I2C_CMD_IS_ACQUIRED = 0x20,
            /// <summary>
            /// Request: <see cref="AcquireRequest"/>
            /// Response: <see cref="AcquireResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_I2C_CMD_ACQUIRE = 0x21,
            /// <summary>
            /// Request: <see cref="ReleaseRequest"/>
            /// Response: <see cref="ReleaseResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_I2C_CMD_RELEASE = 0x22,

            /// <summary>
            /// Request: <see cref="WriteRequest"/>
            /// Response: <see cref="ReadResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_I2C_CMD_READ = 0x31,
            /// <summary>
            /// Request: <see cref="WriteRequest"/>
            /// Response: <see cref="WriteResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_I2C_CMD_WRITE = 0x32,

            /// <summary>
            /// Request: <see cref="ReadMemoryRequest"/>
            /// Response: <see cref="ReadMemoryResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_I2C_CMD_READ_MEMORY = 0x33,
            /// <summary>
            /// Request: <see cref="WriteMemoryRequest"/>
            /// Response: <see cref="WriteMemoryResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_I2C_CMD_WRITE_MEMORY = 0x34,
        }


        private abstract class Request : Message
        {
            internal Request(ushort messageId, I2CPortNumbers port, Commands command, IList<byte> body) : base(new MessageHeader(
                messageId, messageBodySize: (ushort)body.Count,
                fragmentBodyOffset: 0, fragmentBodySize: (ushort)body.Count,
                DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_BASE_I2C, (byte)port, (byte)command, DeviceErrors.UNIVERSAL_DEVICE_ERR_OK),
                body)
            {
            }
        }

        private class ErrorResponse : Message
        {
            private const ushort __bodySize = 4;


            internal ErrorResponse(Message message) : base(message)
            {
                if (message.Body.Count != __bodySize)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }

                I2CHalError = message.Body.ToArray().ToStruct<UnmanagedBody>().I2CHalError;
            }


            internal uint I2CHalError { get; }


            [StructLayout(LayoutKind.Explicit, Size = __bodySize)]
            private struct UnmanagedBody
            {
                [FieldOffset(0)] [MarshalAs(UnmanagedType.U4)] internal uint I2CHalError;
            }
        }

        private class IsAcquiredRequest : Request
        {
            internal IsAcquiredRequest(ushort messageId, I2CPortNumbers port) : base(messageId, port, Commands.UNIVERSAL_DEVICE_I2C_CMD_IS_ACQUIRED, EmptyList)
            {
            }
        }

        private class IsAcquiredResponse : Message
        {
            private const ushort __bodySize = 4;


            internal IsAcquiredResponse(Message message) : base(message)
            {
                if (message.Body.Count != __bodySize)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }

                IsAcquired = message.Body.ToArray().ToStruct<UnmanagedBody>().IsAcquired;
            }


            internal bool IsAcquired { get; }


            [StructLayout(LayoutKind.Explicit, Size = __bodySize)]
            private struct UnmanagedBody
            {
                [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] internal bool IsAcquired;

                /* __Reserved byte[3] */
            }
        }

        private class AcquireRequest : Request
        {
            internal AcquireRequest(ushort messageId, I2CPortNumbers port) : base(messageId, port, Commands.UNIVERSAL_DEVICE_I2C_CMD_ACQUIRE, EmptyList)
            {
            }
        }

        private class AcquireResponse : Message
        {
            internal AcquireResponse(Message message) : base(message)
            {
                if (message.Body.Count != 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }
            }
        }

        private class ReleaseRequest : Request
        {
            internal ReleaseRequest(ushort messageId, I2CPortNumbers port) : base(messageId, port, Commands.UNIVERSAL_DEVICE_I2C_CMD_RELEASE, EmptyList)
            {
            }
        }

        private class ReleaseResponse : Message
        {
            internal ReleaseResponse(Message message) : base(message)
            {
                if (message.Body.Count != 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }
            }
        }

        private class ReadRequest : Request
        {
            internal ReadRequest(ushort messageId, I2CPortNumbers port, ReadWriteInfo info) : base(messageId, port, Commands.UNIVERSAL_DEVICE_I2C_CMD_READ, info.Raw) => Info = info;


            internal ReadWriteInfo Info { get; }
        }

        private class ReadResponse : Message
        {
            internal ReadResponse(Message message) : base(message)
            {
                if (message.Body.Count <= ReadWriteInfo.Size)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }

                var info = new ReadWriteInfo(message.Body.Copy(0, ReadWriteInfo.Size));

                if (info.DataSize > message.Body.Count)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }

                var data = message.Body.Copy(ReadWriteInfo.Size, info.DataSize);                                

                Info = info;
                Data = new ReadOnlyCollection<byte>(data);
            }


            internal ReadWriteInfo Info { get; }
            internal ReadOnlyCollection<byte> Data { get; }
        }

        private class WriteRequest : Request
        {
            internal WriteRequest(ushort messageId, I2CPortNumbers port, ReadWriteInfo info, IList<byte> data) : base(messageId, port, Commands.UNIVERSAL_DEVICE_I2C_CMD_WRITE, CreateBody(info, data))
            {
                Info = info;
                Data = new ReadOnlyCollection<byte>(data);
            }


            internal ReadWriteInfo Info { get; }
            internal ReadOnlyCollection<byte> Data { get; }


            private static IList<byte> CreateBody(ReadWriteInfo info, IList<byte> data)
            {
                var body = new byte[ReadWriteInfo.Size + info.DataSize.IncreaseToMultipleOfFour()];

                info.Raw.CopyTo(body, 0);
                data.CopyTo(body, ReadWriteInfo.Size);

                return body;
            }
        }

        private class WriteResponse : Message
        {
            internal WriteResponse(Message message) : base(message)
            {
                if (message.Body.Count != ReadWriteInfo.Size)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }

                Info = new ReadWriteInfo(message.Body);
            }


            internal ReadWriteInfo Info { get; }
        }

        private class ReadMemoryRequest : Request
        {
            internal ReadMemoryRequest(ushort messageId, I2CPortNumbers port, ReadWriteInfo info, ReadWriteMemoryInfo memoryInfo) :
                base(messageId, port, Commands.UNIVERSAL_DEVICE_I2C_CMD_READ_MEMORY, info.Raw.Concat(memoryInfo.Raw).ToList())
            {
                Info = info;
                MemoryInfo = memoryInfo;
            }


            internal ReadWriteInfo Info { get; }
            internal ReadWriteMemoryInfo MemoryInfo { get; }
        }

        private class ReadMemoryResponse : Message
        {
            internal ReadMemoryResponse(Message message) : base(message)
            {
                if (message.Body.Count <= ReadWriteInfo.Size + ReadWriteMemoryInfo.Size)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }

                var info = new ReadWriteInfo(message.Body.Copy(0, ReadWriteInfo.Size));
                var memoryInfo = new ReadWriteMemoryInfo(message.Body.Copy(ReadWriteInfo.Size, ReadWriteMemoryInfo.Size));

                if (info.DataSize > message.Body.Count)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }

                var data = message.Body.Copy(ReadWriteInfo.Size + ReadWriteMemoryInfo.Size, info.DataSize);

                Info = info;
                MemoryInfo = memoryInfo;
                Data = new ReadOnlyCollection<byte>(data);
            }


            internal ReadWriteInfo Info { get; }
            internal ReadWriteMemoryInfo MemoryInfo { get; }
            internal ReadOnlyCollection<byte> Data { get; }
        }

        private class WriteMemoryRequest : Request
        {
            internal WriteMemoryRequest(ushort messageId, I2CPortNumbers port, ReadWriteInfo info, ReadWriteMemoryInfo memoryInfo, IList<byte> data) :
                base(messageId, port, Commands.UNIVERSAL_DEVICE_I2C_CMD_WRITE_MEMORY, CreateBody(info, memoryInfo, data))
            {
                Info = info;
                MemoryInfo = memoryInfo;
                Data = new ReadOnlyCollection<byte>(data);
            }


            internal ReadWriteInfo Info { get; }
            internal ReadWriteMemoryInfo MemoryInfo { get; }
            internal ReadOnlyCollection<byte> Data { get; }


            private static IList<byte> CreateBody(ReadWriteInfo info, ReadWriteMemoryInfo memoryInfo, IList<byte> data)
            {
                var body = new byte[ReadWriteInfo.Size + ReadWriteMemoryInfo.Size + info.DataSize.IncreaseToMultipleOfFour()];

                info.Raw.CopyTo(body, 0);
                memoryInfo.Raw.CopyTo(body, ReadWriteInfo.Size);
                data.CopyTo(body, ReadWriteInfo.Size + ReadWriteMemoryInfo.Size);

                return body;
            }
        }

        private class WriteMemoryResponse : Message
        {
            internal WriteMemoryResponse(Message message) : base(message)
            {
                if (message.Body.Count != ReadWriteInfo.Size + ReadWriteMemoryInfo.Size)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }

                Info = new ReadWriteInfo(message.Body.Copy(0, ReadWriteInfo.Size));
                MemoryInfo = new ReadWriteMemoryInfo(message.Body.Copy(ReadWriteInfo.Size, ReadWriteMemoryInfo.Size));
            }


            internal ReadWriteInfo Info { get; }
            internal ReadWriteMemoryInfo MemoryInfo { get; }
        }


        private class ReadWriteInfo
        {
            internal const ushort Size = 8;


            internal ReadWriteInfo(I2CSpeeds busSpeed, I2CAddress deviceAddress, ushort dataSize)
            {
                BusSpeed = busSpeed;
                DeviceAddress = deviceAddress;
                DataSize = dataSize;

                var unmanagedReadWriteInfo = new UnmanagedReadWriteInfo
                {
                    BusSpeed = (uint)busSpeed,
                    MainAddress = deviceAddress.RawMainAddress,
                    Wide10Address = deviceAddress.RawWide10Address,
                    DataSize = dataSize,
                };

                Raw = new ReadOnlyCollection<byte>(unmanagedReadWriteInfo.ToByteArray());
            }

            internal ReadWriteInfo(IList<byte> raw)
            {
                if (raw.Count != Size)
                {
                    throw new ArgumentOutOfRangeException(nameof(raw));
                }

                var unmanagedReadWriteInfo = raw.ToArray().ToStruct<UnmanagedReadWriteInfo>();

                var busSpeed = (I2CSpeeds)unmanagedReadWriteInfo.BusSpeed;
                var deviceAddress = new I2CAddress(unmanagedReadWriteInfo.MainAddress, unmanagedReadWriteInfo.Wide10Address);
                var dataSize = unmanagedReadWriteInfo.DataSize;

                busSpeed.ThrowIfNotDefined();

                if (dataSize == 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(raw));
                }

                BusSpeed = busSpeed;
                DeviceAddress = deviceAddress;
                DataSize = dataSize;

                Raw = new ReadOnlyCollection<byte>(raw);
            }


            internal I2CSpeeds BusSpeed { get; }
            internal I2CAddress DeviceAddress { get; }
            internal ushort DataSize { get; }

            internal ReadOnlyCollection<byte> Raw { get; }


            [StructLayout(LayoutKind.Explicit, Size = Size)]
            private struct UnmanagedReadWriteInfo
            {
                [FieldOffset(0)] [MarshalAs(UnmanagedType.U4)] internal uint BusSpeed;
                [FieldOffset(4)] [MarshalAs(UnmanagedType.U1)] internal byte MainAddress;
                [FieldOffset(5)] [MarshalAs(UnmanagedType.U1)] internal byte Wide10Address;
                [FieldOffset(6)] [MarshalAs(UnmanagedType.U2)] internal ushort DataSize;
            }
        }

        private class ReadWriteMemoryInfo
        {
            internal const ushort Size = 4;


            internal ReadWriteMemoryInfo(I2CMemoryAddress memoryAddress)
            {
                MemoryAddress = memoryAddress;

                var unmanagedReadWriteMemoryInfo = new UnmanagedReadWriteMemoryInfo
                {
                    MemoryAddress = memoryAddress.Value,
                    MemoryAddressSize = (ushort)memoryAddress.Size,
                };

                Raw = new ReadOnlyCollection<byte>(unmanagedReadWriteMemoryInfo.ToByteArray());
            }

            internal ReadWriteMemoryInfo(IList<byte> raw)
            {
                if (raw.Count != Size)
                {
                    throw new ArgumentOutOfRangeException(nameof(raw));
                }

                var unmanagedReadWriteMemoryInfo = raw.ToArray().ToStruct<UnmanagedReadWriteMemoryInfo>();

                var memoryAddressSize = (I2CMemoryAddress.Sizes)unmanagedReadWriteMemoryInfo.MemoryAddressSize;

                memoryAddressSize.ThrowIfNotDefined();

                if (memoryAddressSize == I2CMemoryAddress.Sizes.Size8)
                {
                    if (unmanagedReadWriteMemoryInfo.MemoryAddress > byte.MaxValue)
                    {
                        throw new ArgumentOutOfRangeException(nameof(raw));
                    }

                    MemoryAddress = I2CMemoryAddress.Create8((byte)unmanagedReadWriteMemoryInfo.MemoryAddress);
                }
                else
                {
                    MemoryAddress = I2CMemoryAddress.Create16(unmanagedReadWriteMemoryInfo.MemoryAddress);
                }

                Raw = new ReadOnlyCollection<byte>(raw);
            }


            internal I2CMemoryAddress MemoryAddress { get; }

            internal ReadOnlyCollection<byte> Raw { get; }


            [StructLayout(LayoutKind.Explicit, Size = Size)]
            private struct UnmanagedReadWriteMemoryInfo
            {
                [FieldOffset(0)] [MarshalAs(UnmanagedType.U2)] internal ushort MemoryAddress;
                [FieldOffset(2)] [MarshalAs(UnmanagedType.U2)] internal ushort MemoryAddressSize;
            }
        }
    }
}
