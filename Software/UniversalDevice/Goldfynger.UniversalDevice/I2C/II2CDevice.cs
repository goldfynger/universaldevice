﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Goldfynger.UniversalDevice.I2C
{
    /// <summary>
    /// Interface to communicate with universal device I2C ports.
    /// </summary>
    public interface II2CDevice : IDeviceInterface
    {
        /// <summary>
        /// Check is access to I2C device acquired in async mode.
        /// </summary>
        /// <returns><see cref="Task{TResult}"/> of <see cref="bool"/>.</returns>
        Task<bool> IsAcquiredAsync();
        /// <summary>
        /// Acquire access to I2C device in async mode.
        /// </summary>
        /// <returns><see cref="Task"/>.</returns>
        Task AcquireAsync();
        /// <summary>
        /// Release access to I2C device in async mode.
        /// </summary>
        /// <returns><see cref="Task"/>.</returns>
        Task ReleaseAsync();

        /// <summary>
        /// Read data from I2C device in async mode.
        /// </summary>
        /// <param name="speed">I2C bus speed.</param>
        /// <param name="address">I2C device address.</param>
        /// <param name="size">Size of data to read.</param>
        /// <returns><see cref="Task{TResult}"/> of <see cref="IList{T}"/> of <see cref="byte"/>.</returns>
        Task<IList<byte>> ReadAsync(I2CSpeeds speed, I2CAddress address, ushort size);
        /// <summary>
        /// Write data to I2C device in async mode.
        /// </summary>
        /// <param name="speed">I2C bus speed.</param>
        /// <param name="address">I2C device address.</param>
        /// <param name="data">Data to write.</param>
        /// <returns><see cref="Task"/>.</returns>
        Task WriteAsync(I2CSpeeds speed, I2CAddress address, IList<byte> data);

        /// <summary>
        /// Read data at specified memory address from I2C device in async mode.
        /// </summary>
        /// <param name="speed">I2C bus speed.</param>
        /// <param name="address">I2C device address.</param>
        /// <param name="memoryAddress">Read memory address.</param>
        /// <param name="size">Size of data to read.</param>
        /// <returns><see cref="Task{TResult}"/> of <see cref="IList{T}"/> of <see cref="byte"/>.</returns>
        Task<IList<byte>> ReadMemoryAsync(I2CSpeeds speed, I2CAddress address, I2CMemoryAddress memoryAddress, ushort size);
        /// <summary>
        /// Write data at specified memory address to I2C device in async mode.
        /// </summary>
        /// <param name="speed">I2C bus speed.</param>
        /// <param name="address">I2C device address.</param>
        /// <param name="memoryAddress">Write memory address.</param>
        /// <param name="data">Data to write.</param>
        /// <returns><see cref="Task"/>.</returns>
        Task WriteMemoryAsync(I2CSpeeds speed, I2CAddress address, I2CMemoryAddress memoryAddress, IList<byte> data);
    }
}
