﻿namespace Goldfynger.UniversalDevice.I2C
{
    /// <summary>
    /// I2C bus available speed types.
    /// </summary>
    public enum I2CSpeeds : uint
    {
        /// <summary>Standart speed up to 100 kbit/s.</summary>
        Standart = 100000,
    }
}
