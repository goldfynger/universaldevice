﻿using System;
using System.Diagnostics;

namespace Goldfynger.UniversalDevice.I2C
{
    /// <summary>
    /// I2C interface HAL exception.
    /// </summary>
    [DebuggerDisplay("DeviceError = {DeviceError} I2CHalError = {I2CHalError}")]
    public sealed class I2CHalException : UniversalDeviceException
    {
        internal I2CHalException(Message errorResponse, uint i2cHalError) : base(errorResponse) => I2CHalError = i2cHalError;

        internal I2CHalException(Message errorResponse, uint i2cHalError, string message) : base(errorResponse, message) => I2CHalError = i2cHalError;

        internal I2CHalException(Message errorResponse, uint i2cHalError, string message, Exception innerException) : base(errorResponse, message, innerException) => I2CHalError = i2cHalError;


        /// <summary>
        /// I2C HAL error that caused exception.
        /// </summary>
        public uint I2CHalError { get; }
    }
}
