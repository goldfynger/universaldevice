﻿
using System;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice.I2C
{
    /// <summary>
    /// Represents memory address of I2C device.
    /// </summary>
    public sealed record I2CMemoryAddress
    {
        private I2CMemoryAddress(ushort value, Sizes size)
        {
            Value = value;
            size.ThrowIfNotDefined();
            Size = size;
        }


        /// <summary>
        /// Address value.
        /// </summary>
        public ushort Value { get; }

        /// <summary>
        /// Address size.
        /// </summary>
        public Sizes Size { get; }


        /// <summary>
        /// Creates 8-bit wide memory address of I2C device.
        /// </summary>
        /// <param name="value">Address value.</param>
        /// <returns>8-bit wide memory address of I2C device.</returns>
        public static I2CMemoryAddress Create8(byte value) => new(value, Sizes.Size8);

        /// <summary>
        /// Creates 16-bit wide memory address of I2C device.
        /// </summary>
        /// <param name="value">Address value.</param>
        /// <returns>16-bit wide memory address of I2C device.</returns>
        public static I2CMemoryAddress Create16(ushort value) => new(value, Sizes.Size16);

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return Size switch
            {
                Sizes.Size8 => $"0x{Value:X2}",
                Sizes.Size16 => $"0x{Value:X4}",
                _ => throw new ApplicationException($"Invalid value of {nameof(Size)} property."),
            };
        }


        /// <summary>
        /// I2C device memory address types.
        /// </summary>
        public enum Sizes : ushort
        {
            /// <summary>8-bit wide memory address.</summary>
            Size8 = 8,
            /// <summary>16-bit wide memory address.</summary>
            Size16 = 16,
        }
    }
}
