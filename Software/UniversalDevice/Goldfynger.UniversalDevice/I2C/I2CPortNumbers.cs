﻿namespace Goldfynger.UniversalDevice.I2C
{
    /// <summary>
    /// Universal device I2C port numbers.
    /// </summary>
    public enum I2CPortNumbers : byte
    {
        /// <summary>Control port number.</summary>
        Control,
        /// <summary>I2C port 1.</summary>
        Port1,
        /// <summary>I2C port 2.</summary>
        Port2,
        /// <summary>I2C port 3.</summary>
        Port3,
        /// <summary>I2C port 4.</summary>
        Port4,
        /// <summary>I2C port 5.</summary>
        Port5,
        /// <summary>I2C port 6.</summary>
        Port6,
        /// <summary>I2C port 7.</summary>
        Port7,
    }
}
