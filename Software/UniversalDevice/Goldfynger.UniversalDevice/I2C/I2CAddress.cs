﻿using System;

namespace Goldfynger.UniversalDevice.I2C
{
    /// <summary>
    /// Represents I2C device address.
    /// </summary>
    public sealed record I2CAddress
    {
        /* Little endian representation of reserved addresses. In all cases except Wide-10 address high byte is ignored. */
        /* https://www.i2c-bus.org/addressing/ */

        private const int __generalCallAddressMarker = 0b_0000_0000;
        private const int __generalCallAddressMarkerMask = 0b_1111_1111;

        private const int __startByteAddressMarker = 0b_0000_0001;
        private const int __startByteAddressMarkerMask = 0b_1111_1111;

        private const int __cbusAddressMarker = 0b_0000_0010;
        private const int __cbusAddressMarkerMask = 0b_1111_1110;

        private const int __differentBusAddressMarker = 0b_0000_0100;
        private const int __differentBusAddressMarkerMask = 0b_1111_1110;

        private const int __future1AddressMarker = 0b_0000_0110;
        private const int __future1AddressMarkerMask = 0b_1111_1110;

        private const int __highSpeedAddressMarker = 0b_0000_1000;
        private const int __highSpeedAddressMarkerMask = 0b_1111_1000;

        private const int __wide10AddressMarker = 0b_1111_0000;
        private const int __wide10AddressMarkerMask = 0b_1111_1000;

        private const int __future2AddressMarker = 0b_1111_1000;
        private const int __future2AddressMarkerMask = 0b_1111_1000;

        private const int __wide7AddressMax = 0x7F;
        private const int __wide10AddressMax = 0x03FF;


        private byte _wide7 = 0;
        private ushort _wide10 = 0;


        private I2CAddress()
        {
        }

        internal I2CAddress(byte rawMainAddress, byte rawWide10Address)
        {
            var type = Types.Wide7;

            if (IsGeneralCall(rawMainAddress))
            {
                type = Types.GeneralCall;
            }

            if (IsStartByte(rawMainAddress))
            {
                type = Types.StartByte;
            }

            if (IsCbus(rawMainAddress))
            {
                type = Types.Cbus;
            }

            if (IsDifferentBus(rawMainAddress))
            {
                type = Types.DifferentBus;
            }

            if (IsFuture1(rawMainAddress) || IsFuture2(rawMainAddress))
            {
                type = Types.Future2;
            }

            if (IsHighSpeed(rawMainAddress))
            {
                type = Types.HighSpeed;
            }

            if (IsWide10(rawMainAddress))
            {
                type = Types.Wide10;
            }

            if (type == Types.Wide7)
            {
                RawToWide7(rawMainAddress, out byte wide7);

                _wide7 = wide7;
            }
            else if (type == Types.Wide10)
            {
                RawToWide10(rawMainAddress, rawWide10Address, out ushort wide10);

                _wide10 = wide10;
            }

            Type = type;
            RawMainAddress = rawMainAddress;
            RawWide10Address = rawWide10Address;
        }


        /// <summary>
        /// 7-bit wide address of I2C device.
        /// </summary>
        /// <returns><see cref="byte"/> with right-shifted 7-bit address if address type is <see cref="Types.Wide7"/>; otherwise <see langword="null"/>.</returns>
        public byte? Wide7
        {
            get
            {
                if (Type != Types.Wide7)
                {
                    return null;
                }

                return _wide7;
            }
        }

        /// <summary>
        /// 10-bit wide address of I2C device.
        /// </summary>
        /// <returns><see cref="ushort"/> with right-shifted 10-bit address if address type is <see cref="Types.Wide10"/>; otherwise <see langword="null"/>.</returns>
        public ushort? Wide10
        {
            get
            {
                if (Type != Types.Wide10)
                {
                    return null;
                }

                return _wide10;
            }
        }

        /// <summary>
        /// Address type of current instance.
        /// </summary>
        public Types Type { get; private init; } = Types.GeneralCall;

        internal byte RawMainAddress { get; private init; } = 0;

        internal byte RawWide10Address { get; private init; } = 0;


        /// <summary>
        /// Creates 7-bit wide I2C device address.
        /// </summary>
        /// <param name="wide7">Right-shifted <see cref="byte"/> with 7-bit wide address.</param>
        /// <returns>7-bit wide I2C device address.</returns>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="wide7"/> has invalid value.</exception>
        public static I2CAddress Create7(byte wide7)
        {
            if (!Wide7ToRaw(wide7, out byte rawMainAddress))
            {
                throw new ArgumentOutOfRangeException(nameof(wide7));
            }

            return new I2CAddress
            {
                _wide7 = wide7,
                Type = Types.Wide7,
                RawMainAddress = rawMainAddress,
                RawWide10Address = 0,                
            };
        }

        /// <summary>
        /// Creates 10-bit wide I2C device address.
        /// </summary>
        /// <param name="wide10">Right-shifted <see cref="ushort"/> with 10-bit wide address.</param>
        /// <returns>10-bit wide I2C device address.</returns>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="wide10"/> has invalid value.</exception>
        public static I2CAddress Create10(ushort wide10)
        {
            if (!Wide10ToRaw(wide10, out byte rawMainAddress, out byte rawWide10Address))
            {
                throw new ArgumentOutOfRangeException(nameof(wide10));
            }

            return new I2CAddress
            {
                _wide10 = wide10,
                Type = Types.Wide10,
                RawMainAddress = rawMainAddress,
                RawWide10Address = rawWide10Address,                
            };
        }


        private static bool Wide7ToRaw(byte wide7, out byte rawMainAddress)
        {
            rawMainAddress = 0;

            if (wide7 > __wide7AddressMax)
            {
                return false;
            }

            rawMainAddress = (byte)(wide7 << 1);

            return IsWide7(rawMainAddress);
        }

        private static bool RawToWide7(byte rawMainAddress, out byte wide7)
        {
            wide7 = 0;

            if (!IsWide7(rawMainAddress))
            {
                return false;
            }

            wide7 = (byte)(rawMainAddress >> 1);

            return true;
        }

        private static bool Wide10ToRaw(ushort wide10, out byte rawMainAddress, out byte rawWide10Address)
        {
            rawMainAddress = 0;
            rawWide10Address = 0;

            if (wide10 > __wide10AddressMax)
            {
                return false;
            }

            rawMainAddress = (byte)(((wide10 >> 7) & 0x00000006) | __wide10AddressMarker);
            rawWide10Address = (byte)(wide10 & byte.MaxValue);

            return true;
        }

        private static bool RawToWide10(byte rawMainAddress, byte rawWide10Address, out ushort wide10)
        {
            wide10 = 0;

            if (!IsWide10(rawMainAddress))
            {
                return false;
            }

            wide10 = (ushort)(((rawMainAddress << 7) & 0x00000003) | rawWide10Address);

            return true;
        }


        private static bool IsGeneralCall(byte rawMainAddress) => (rawMainAddress & __generalCallAddressMarkerMask) == __generalCallAddressMarker;
        private static bool IsStartByte(byte rawMainAddress) => (rawMainAddress & __startByteAddressMarkerMask) == __startByteAddressMarker;
        private static bool IsCbus(byte rawMainAddress) => (rawMainAddress & __cbusAddressMarkerMask) == __cbusAddressMarker;
        private static bool IsDifferentBus(byte rawMainAddress) => (rawMainAddress & __differentBusAddressMarkerMask) == __differentBusAddressMarker;
        private static bool IsFuture1(byte rawMainAddress) => (rawMainAddress & __future1AddressMarkerMask) == __future1AddressMarker;
        private static bool IsHighSpeed(byte rawMainAddress) => (rawMainAddress & __highSpeedAddressMarkerMask) == __highSpeedAddressMarker;
        private static bool IsWide10(byte rawMainAddress) => (rawMainAddress & __wide10AddressMarkerMask) == __wide10AddressMarker;
        private static bool IsFuture2(byte rawMainAddress) => (rawMainAddress & __future2AddressMarkerMask) == __future2AddressMarker;
        private static bool IsWide7(byte rawMainAddress) =>
            !IsGeneralCall(rawMainAddress) &&
            !IsStartByte(rawMainAddress) &&
            !IsCbus(rawMainAddress) &&
            !IsDifferentBus(rawMainAddress) &&
            !IsFuture1(rawMainAddress) &&
            !IsHighSpeed(rawMainAddress) &&
            !IsWide10(rawMainAddress) &&
            !IsFuture2(rawMainAddress);


        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return Type switch
            {
                Types.Wide7 => $"0x{_wide7:X2}",
                Types.Wide10 => $"0x{_wide10:X4}",
                _ => throw new ApplicationException($"Unsupported value of {nameof(Type)} property."),
            };
        }


        /// <summary>
        /// I2C device address types.
        /// </summary>
        public enum Types: ushort
        {
            /// <summary>7-bit Slave Addressing</summary>
            Wide7,
            /// <summary>General Call.</summary>
            GeneralCall,
            /// <summary>Start Byte.</summary>
            StartByte,
            /// <summary>CBUS Addresses.</summary>
            Cbus,
            /// <summary>Reserved for Different Bus Formats.</summary>
            DifferentBus,
            /// <summary>Reserved for future purposes.</summary>
            Future1,
            /// <summary>High-Speed Master Code.</summary>
            HighSpeed,
            /// <summary>10-bit Slave Addressing.</summary>
            Wide10,
            /// <summary>Reserved for future purposes.</summary>
            Future2,
        }
    }
}
