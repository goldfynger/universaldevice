﻿using Goldfynger.UniversalDevice.FlashStorage;
using Goldfynger.UniversalDevice.I2C;
using Goldfynger.UniversalDevice.Info;
using Goldfynger.UniversalDevice.Nrf24;

namespace Goldfynger.UniversalDevice
{
    /// <summary>Universal device interfaces.</summary>
    public enum DeviceInterfaces : byte
    {
        /// <summary>Device control interface.</summary>
        UNIVERSAL_DEVICE_INTERFACE_BASE_CONTROL = 0x00,

        /// <summary>Device info interface. See <see cref="IInfoDevice"/>.</summary>
        UNIVERSAL_DEVICE_INTERFACE_BASE_INFO = 0x10,

        /// <summary>USART interface.</summary>
        UNIVERSAL_DEVICE_INTERFACE_BASE_USART = 0x20,

        /// <summary>I2C interface. See <see cref="II2CDevice"/>.</summary>
        UNIVERSAL_DEVICE_INTERFACE_BASE_I2C = 0x28,

        /// <summary>Flash storage interface. See <see cref="IFlashStorageDevice"/>.</summary>
        UNIVERSAL_DEVICE_INTERFACE_BASE_FLASH_STORAGE = 0x40,

        /// <summary>Nrf24 interface. See <see cref="INrf24Device"/>.</summary>
        UNIVERSAL_DEVICE_INTERFACE_BASE_NRF24 = 0x60,

        /* Interfaces 0x7F..0xFE reserved for future use. */

        /// <summary>Unknown interface. Schould not be used directly.</summary>
        __UNIVERSAL_DEVICE_INTERFACE_BASE_UNKNOWN = 0xFF,
    }
}
