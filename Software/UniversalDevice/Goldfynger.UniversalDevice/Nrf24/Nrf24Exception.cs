﻿using System;
using System.Diagnostics;

namespace Goldfynger.UniversalDevice.Nrf24
{
    /// <summary>
    /// Nrf24 interface exception.
    /// </summary>
    [DebuggerDisplay("DeviceError = {DeviceError} Nrf24Error = {Nrf24Error}")]
    public sealed class Nrf24Exception : UniversalDeviceException
    {
        internal Nrf24Exception(Message errorResponse, Nrf24Errors nrf24Error) : base(errorResponse) => Nrf24Error = nrf24Error;

        internal Nrf24Exception(Message errorResponse, Nrf24Errors nrf24Error, string message) : base(errorResponse, message) => Nrf24Error = nrf24Error;

        internal Nrf24Exception(Message errorResponse, Nrf24Errors nrf24Error, string message, Exception innerException) : base(errorResponse, message, innerException) => Nrf24Error = nrf24Error;


        /// <summary>
        /// Nrf24 error that caused exception.
        /// </summary>
        public Nrf24Errors Nrf24Error { get; }
    }
}
