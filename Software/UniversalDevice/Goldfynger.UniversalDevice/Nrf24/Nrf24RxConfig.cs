﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice.Nrf24
{
    /// <summary>Represents RX configuration of Nrf24 device.</summary>
    public sealed record Nrf24RxConfig : IEquatable<Nrf24RxConfig>
    {
        /// <summary>Default TxRx0 address.</summary>
        public static readonly Nrf24Address DefaultRx0Address = new(0xE7, 0xE7, 0xE7, 0xE7, 0xE7);
        /// <summary>Default Rx1 address.</summary>
        public static readonly Nrf24Address DefaultRx1Address = new(0xC2, 0xC2, 0xC2, 0xC2, 0xC2);
        /// <summary>Default Rx2 subaddress.</summary>
        public const byte DefaultRx2Subaddress = 0xC3;
        /// <summary>Default Rx3 subaddress.</summary>
        public const byte DefaultRx3Subaddress = 0xC4;
        /// <summary>Default Rx4 subaddress.</summary>
        public const byte DefaultRx4Subaddress = 0xC5;
        /// <summary>Default Rx5 subaddress.</summary>
        public const byte DefaultRx5Subaddress = 0xC6;
        /// <summary>Default Rx data pipes.</summary>
        public const RxDataPipes DefaultRxDataPipe = RxDataPipes.NRF24_RX_DATA_PIPE_1;

        internal const ushort Size = 16;

        /// <summary>Default config instance.</summary>
        public static readonly Nrf24RxConfig Default = new(DefaultRx0Address, DefaultRx1Address, DefaultRx2Subaddress, DefaultRx3Subaddress, DefaultRx4Subaddress, DefaultRx5Subaddress, DefaultRxDataPipe);


        /// <summary>Creates new instance of <see cref="Nrf24RxConfig"/> using RX configuration parameters.</summary>
        /// <param name="rx0Address">Rx0 address.</param>
        /// <param name="rx1Address">Rx1 address.</param>
        /// <param name="rx2Subaddress">Rx2 subaddress.</param>
        /// <param name="rx3Subaddress">Rx3 subaddress.</param>
        /// <param name="rx4Subaddress">Rx4 subaddress.</param>
        /// <param name="rx5Subaddress">Rx5 subaddress.</param>
        /// <param name="rxPipes">Rx data pipes.</param>
        public Nrf24RxConfig(Nrf24Address rx0Address, Nrf24Address rx1Address, byte rx2Subaddress, byte rx3Subaddress, byte rx4Subaddress, byte rx5Subaddress, RxDataPipes rxPipes)
        {
            Rx0Address = rx0Address;

            Rx1Address = rx1Address;

            Rx2Subaddress = rx2Subaddress;
            Rx2Address = rx1Address.CreateNewForSubaddress(rx2Subaddress);

            Rx3Subaddress = rx3Subaddress;
            Rx3Address = rx1Address.CreateNewForSubaddress(rx3Subaddress);

            Rx4Subaddress = rx4Subaddress;
            Rx4Address = rx1Address.CreateNewForSubaddress(rx4Subaddress);

            Rx5Subaddress = rx5Subaddress;
            Rx5Address = rx1Address.CreateNewForSubaddress(rx5Subaddress);

            rxPipes.ThrowIfUnknownFlags();
            RxPipes = rxPipes;

            var unmanagedRxConfig = new UnmanagedRxConfig
            {
                Rx0AddressB0 = rx0Address.B0,
                Rx0AddressB1 = rx0Address.B1,
                Rx0AddressB2 = rx0Address.B2,
                Rx0AddressB3 = rx0Address.B3,
                Rx0AddressB4 = rx0Address.B4,
                Rx1AddressB0 = rx1Address.B0,
                Rx1AddressB1 = rx1Address.B1,
                Rx1AddressB2 = rx1Address.B2,
                Rx1AddressB3 = rx1Address.B3,
                Rx1AddressB4 = rx1Address.B4,
                Rx2Subaddress = rx2Subaddress,
                Rx3Subaddress = rx3Subaddress,
                Rx4Subaddress = rx4Subaddress,
                Rx5Subaddress = rx5Subaddress,
                RxPipes = rxPipes,
            };

            Raw = new ReadOnlyCollection<byte>(unmanagedRxConfig.ToByteArray());
        }

        internal Nrf24RxConfig(IList<byte> raw)
        {
            if (raw.Count != Size)
            {
                throw new ArgumentOutOfRangeException(nameof(raw));
            }

            var unmanagedRxConfig = raw.ToArray().ToStruct<UnmanagedRxConfig>();

            var rx0Address = new Nrf24Address(unmanagedRxConfig.Rx0AddressB0, unmanagedRxConfig.Rx0AddressB1, unmanagedRxConfig.Rx0AddressB2, unmanagedRxConfig.Rx0AddressB3, unmanagedRxConfig.Rx0AddressB4);
            var rx1Address = new Nrf24Address(unmanagedRxConfig.Rx1AddressB0, unmanagedRxConfig.Rx1AddressB1, unmanagedRxConfig.Rx1AddressB2, unmanagedRxConfig.Rx1AddressB3, unmanagedRxConfig.Rx1AddressB4);
            var rx2SubAddress = unmanagedRxConfig.Rx2Subaddress;
            var rx3SubAddress = unmanagedRxConfig.Rx3Subaddress;
            var rx4SubAddress = unmanagedRxConfig.Rx4Subaddress;
            var rx5SubAddress = unmanagedRxConfig.Rx5Subaddress;
            var rxPipes = unmanagedRxConfig.RxPipes;

            Rx0Address = rx0Address;

            Rx1Address = rx1Address;

            Rx2Subaddress = rx2SubAddress;

            Rx3Subaddress = rx3SubAddress;

            Rx4Subaddress = rx4SubAddress;

            Rx5Subaddress = rx5SubAddress;

            RxPipes = rxPipes;

            Rx2Address = rx1Address.CreateNewForSubaddress(rx2SubAddress);

            Rx3Address = rx1Address.CreateNewForSubaddress(rx3SubAddress);

            Rx4Address = rx1Address.CreateNewForSubaddress(rx4SubAddress);

            Rx5Address = rx1Address.CreateNewForSubaddress(rx5SubAddress);

            Raw = new ReadOnlyCollection<byte>(raw);
        }


        /// <summary>Rx0 address.</summary>
        public Nrf24Address Rx0Address { get; }

        /// <summary>Rx1 address.</summary>
        public Nrf24Address Rx1Address { get; }
        /// <summary>Rx2 subaddress.</summary>
        public byte Rx2Subaddress { get; }
        /// <summary>Rx3 subaddress.</summary>
        public byte Rx3Subaddress { get; }
        /// <summary>Rx4 subaddress.</summary>
        public byte Rx4Subaddress { get; }
        /// <summary>Rx5 subaddress.</summary>
        public byte Rx5Subaddress { get; }

        /// <summary>Rx data pipes.</summary>
        public RxDataPipes RxPipes { get; }

        /// <summary>Rx2 address.</summary>
        public Nrf24Address Rx2Address { get; }
        /// <summary>Rx3 address.</summary>
        public Nrf24Address Rx3Address { get; }
        /// <summary>Rx4 address.</summary>
        public Nrf24Address Rx4Address { get; }
        /// <summary>Rx5 address.</summary>
        public Nrf24Address Rx5Address { get; }

        internal ReadOnlyCollection<byte> Raw { get; }


        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns><see langword="true"/> if the current object is equal to the other parameter; otherwise, <see langword="false"/>.</returns>
        public bool Equals(Nrf24RxConfig? other) => other != null && other.Rx0Address == Rx0Address && other.Rx1Address == Rx1Address &&
            other.Rx2Subaddress == Rx2Subaddress && other.Rx3Subaddress == Rx3Subaddress && other.Rx4Subaddress == Rx4Subaddress && other.Rx5Subaddress == Rx5Subaddress &&
            other.RxPipes == RxPipes && other.Rx2Address == Rx2Address && other.Rx3Address == Rx3Address && other.Rx4Address == Rx4Address && other.Rx5Address == Rx5Address;

        /// <summary>Serves as the default hash function.</summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode() => HashCode.Combine(Rx0Address, Rx1Address, Rx2Subaddress, Rx3Subaddress, Rx4Subaddress, Rx5Subaddress, RxPipes,
            HashCode.Combine(Rx2Address, Rx3Address, Rx4Address, Rx5Address));

        /// <summary>Configuration representation.</summary>
        /// <returns><see cref="string"/> configuration.</returns>
        public override string ToString() => $"{Rx0Address}|{Rx1Address}|{Rx2Subaddress:X2}|{Rx3Subaddress:X2}|{Rx4Subaddress:X2}|{Rx5Subaddress:X2}|{RxPipes}";


        [StructLayout(LayoutKind.Explicit, Size = Size)]
        private struct UnmanagedRxConfig
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] internal byte Rx0AddressB0;
            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] internal byte Rx0AddressB1;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U1)] internal byte Rx0AddressB2;
            [FieldOffset(3)] [MarshalAs(UnmanagedType.U1)] internal byte Rx0AddressB3;
            [FieldOffset(4)] [MarshalAs(UnmanagedType.U1)] internal byte Rx0AddressB4;

            [FieldOffset(5)] [MarshalAs(UnmanagedType.U1)] internal byte Rx1AddressB0;
            [FieldOffset(6)] [MarshalAs(UnmanagedType.U1)] internal byte Rx1AddressB1;
            [FieldOffset(7)] [MarshalAs(UnmanagedType.U1)] internal byte Rx1AddressB2;
            [FieldOffset(8)] [MarshalAs(UnmanagedType.U1)] internal byte Rx1AddressB3;
            [FieldOffset(9)] [MarshalAs(UnmanagedType.U1)] internal byte Rx1AddressB4;
            [FieldOffset(10)] [MarshalAs(UnmanagedType.U1)] internal byte Rx2Subaddress;
            [FieldOffset(11)] [MarshalAs(UnmanagedType.U1)] internal byte Rx3Subaddress;
            [FieldOffset(12)] [MarshalAs(UnmanagedType.U1)] internal byte Rx4Subaddress;
            [FieldOffset(13)] [MarshalAs(UnmanagedType.U1)] internal byte Rx5Subaddress;

            [FieldOffset(14)] [MarshalAs(UnmanagedType.U1)] internal RxDataPipes RxPipes;

            /* __Reserved byte[1] */
        }


        /// <summary>Rx data pipes.</summary>
        [Flags]
        public enum RxDataPipes : byte
        {
            /// <summary>No Rx pipes.</summary>
            NRF24_RX_DATA_PIPE_NONE = 0x00,

            /// <summary>Rx pipe 0 flag.</summary>
            NRF24_RX_DATA_PIPE_0 = 0x01,
            /// <summary>Rx pipe 1 flag.</summary>
            NRF24_RX_DATA_PIPE_1 = 0x02,
            /// <summary>Rx pipe 2 flag.</summary>
            NRF24_RX_DATA_PIPE_2 = 0x04,
            /// <summary>Rx pipe 3 flag.</summary>
            NRF24_RX_DATA_PIPE_3 = 0x08,
            /// <summary>Rx pipe 4 flag.</summary>
            NRF24_RX_DATA_PIPE_4 = 0x10,
            /// <summary>Rx pipe 5 flag.</summary>
            NRF24_RX_DATA_PIPE_5 = 0x20,

            /// <summary>All Rx flags while Tx active.</summary>
            NRF24_RX_DATA_PIPE_ALL_RX_TX = NRF24_RX_DATA_PIPE_1 | NRF24_RX_DATA_PIPE_2 | NRF24_RX_DATA_PIPE_3 | NRF24_RX_DATA_PIPE_4 | NRF24_RX_DATA_PIPE_5,
            /// <summary>All Rx flags while Tx inactive.</summary>
            NRF24_RX_DATA_PIPE_ALL_RX_ONLY = NRF24_RX_DATA_PIPE_0 | NRF24_RX_DATA_PIPE_1 | NRF24_RX_DATA_PIPE_2 | NRF24_RX_DATA_PIPE_3 | NRF24_RX_DATA_PIPE_4 | NRF24_RX_DATA_PIPE_5,
        }
    }
}
