﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice.Nrf24
{
    /// <summary>Represents configuration of Nrf24 device.</summary>
    public sealed record Nrf24Config : IEquatable<Nrf24Config>
    {
        /// <summary>Default frequency channel.</summary>
        public const byte DefaultFrequencyChannel = 2;
        /// <summary>Default data rate.</summary>
        public const DataRates DefaultDataRate = DataRates.NRF24_DATA_RATE_2MBPS;
        /// <summary>Default amplifier power.</summary>
        public const AmpPowers DefaultAmpPower = AmpPowers.NRF24_AMP_POWER_0DBM;
        /// <summary>Default RxTx mode.</summary>
        public const RxTxModes DefaultRxTxMode = RxTxModes.NRF24_RX_TX_MODE_RX_TX;

        /// <summary>Max frequency channel.</summary>
        public const byte MaxFrequencyChannel = 125;

        internal const ushort Size = 4;

        /// <summary>Default config instance.</summary>
        public static readonly Nrf24Config Default = new(DefaultFrequencyChannel, DefaultAmpPower, DefaultDataRate, DefaultRxTxMode);


        /// <summary>Creates new instance of <see cref="Nrf24Config"/> using configuration parameters.</summary>
        /// <param name="frequencyChannel">Frequency channel.</param>
        /// <param name="ampPower">Amplifier power.</param>
        /// <param name="dataRate">Data rate.</param>
        /// <param name="rxTxMode">RxTx mode.</param>
        public Nrf24Config(byte frequencyChannel, AmpPowers ampPower, DataRates dataRate, RxTxModes rxTxMode)
        {
            FrequencyChannel = frequencyChannel <= MaxFrequencyChannel ? frequencyChannel : throw new ArgumentOutOfRangeException(nameof(frequencyChannel));

            ampPower.ThrowIfNotDefined();
            AmpPower = ampPower;

            dataRate.ThrowIfNotDefined();
            DataRate = dataRate;

            rxTxMode.ThrowIfUnknownFlags();
            RxTxMode = rxTxMode;

            var unmanagedConfig = new UnmanagedConfig
            {
                FrequencyChannel = frequencyChannel,
                AmpPower = ampPower,
                DataRate = dataRate,
                RxTxMode = rxTxMode,
            };

            Raw = new ReadOnlyCollection<byte>(unmanagedConfig.ToByteArray());
        }

        internal Nrf24Config(IList<byte> raw)
        {
            if (raw.Count != Size)
            {
                throw new ArgumentOutOfRangeException(nameof(raw));
            }

            var unmanagedConfig = raw.ToArray().ToStruct<UnmanagedConfig>();

            var frequencyChannel = unmanagedConfig.FrequencyChannel;
            var ampPower = unmanagedConfig.AmpPower;
            var dataRate = unmanagedConfig.DataRate;
            var rxTxMode = unmanagedConfig.RxTxMode;

            FrequencyChannel = frequencyChannel <= MaxFrequencyChannel ? frequencyChannel : throw new ArgumentOutOfRangeException(nameof(raw));

            ampPower.ThrowIfNotDefined();
            AmpPower = ampPower;

            dataRate.ThrowIfNotDefined();
            DataRate = dataRate;

            rxTxMode.ThrowIfUnknownFlags();
            RxTxMode = rxTxMode;

            Raw = new ReadOnlyCollection<byte>(raw);
        }


        /// <summary>Frequency channel.</summary>
        public byte FrequencyChannel { get; }

        /// <summary>Amplifier power.</summary>
        public AmpPowers AmpPower { get; }
        /// <summary>Data rate.</summary>
        public DataRates DataRate { get; }

        /// <summary>RxTx mode.</summary>
        public RxTxModes RxTxMode { get; }

        internal ReadOnlyCollection<byte> Raw { get; }


        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns><see langword="true"/> if the current object is equal to the other parameter; otherwise, <see langword="false"/>.</returns>
        public bool Equals(Nrf24Config? other) => other != null && other.FrequencyChannel == FrequencyChannel && other.AmpPower == AmpPower && other.DataRate == DataRate && other.RxTxMode == RxTxMode;

        /// <summary>Serves as the default hash function.</summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode() => HashCode.Combine(FrequencyChannel, AmpPower, DataRate, RxTxMode);

        /// <summary>Configuration representation.</summary>
        /// <returns><see cref="string"/> configuration.</returns>
        public override string ToString() => $"{FrequencyChannel}|{AmpPower}|{DataRate}|{RxTxMode}";


        [StructLayout(LayoutKind.Explicit, Size = Size)]
        private struct UnmanagedConfig
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] internal byte FrequencyChannel;

            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] internal AmpPowers AmpPower;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U1)] internal DataRates DataRate;

            [FieldOffset(3)] [MarshalAs(UnmanagedType.U1)] internal RxTxModes RxTxMode;
        }


        /// <summary>Amplifier powers.</summary>
        public enum AmpPowers : byte
        {
            /// <summary>-18 dBm amplifier power.</summary>
            NRF24_AMP_POWER_M18DBM = 0 << 1,
            /// <summary>-12 dBm amplifier power.</summary>
            NRF24_AMP_POWER_M12DBM = 1 << 1,
            /// <summary>-6 dBm amplifier power.</summary>
            NRF24_AMP_POWER_M6DBM = 2 << 1,
            /// <summary>0 dBm amplifier power.</summary>
            NRF24_AMP_POWER_0DBM = 3 << 1,
        }

        /// <summary>Data rates.</summary>
        public enum DataRates : byte
        {
            /// <summary>1 Mbps data rate.</summary>
            NRF24_DATA_RATE_1MBPS = 0 << 3 | 0 << 5,
            /// <summary>2 Mbps data rate.</summary>
            NRF24_DATA_RATE_2MBPS = 1 << 3 | 0 << 5,
            /// <summary>250 kbps data rate.</summary>
            NRF24_DATA_RATE_250KBPS = 0 << 3 | 1 << 5,
        }

        /// <summary>RxTx modes.</summary>
        [Flags]
        public enum RxTxModes : byte
        {
            /// <summary>No Rx and no Tx.</summary>
            NRF24_RX_TX_MODE_NONE = 0x00,

            /// <summary>Rx mode flag.</summary>
            NRF24_RX_TX_MODE_RX = 0x01,
            /// <summary>Tx mode flag.</summary>
            NRF24_RX_TX_MODE_TX = 0x02,

            /// <summary>Rx only flag.</summary>
            NRF24_RX_TX_MODE_RX_ONLY = NRF24_RX_TX_MODE_RX,
            /// <summary>Tx only flag.</summary>
            NRF24_RX_TX_MODE_TX_ONLY = NRF24_RX_TX_MODE_TX,
            /// <summary>Rx and Tx flag.</summary>
            NRF24_RX_TX_MODE_RX_TX = NRF24_RX_TX_MODE_RX | NRF24_RX_TX_MODE_TX,
        }
    }
}
