﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice.Nrf24
{
    internal sealed class Nrf24Device : INrf24Device
    {
        private static readonly TimeSpan __timeout = TimeSpan.FromSeconds(2);


        private readonly Device _device;


        static Nrf24Device() => Message.MessageHeader.AddCommandsToResolver<Commands>(DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_BASE_NRF24);

        internal Nrf24Device(Device device) => _device = device;


        public bool IsCorrupted => _device.IsCorrupted;


        public async Task ResetAsync() => _ = new ResetResponse(await Exchange(new ResetRequest(_device.NextMessageId())).ConfigureAwait(false));

        public async Task SetConfigAsync(Nrf24Config config) => _ = new SetConfigResponse(await Exchange(new SetConfigRequest(_device.NextMessageId(), config)).ConfigureAwait(false));

        public async Task SendAsync(Nrf24TxConfig txConfig, IList<byte> data)
        {
            if (data.Count == 0 || data.Count.IncreaseToMultipleOfFour() > Nrf24Const.MaxDataLength)
            {
                throw new ArgumentOutOfRangeException(nameof(data));
            }

            _ = new SendResponse(await Exchange(new SendRequest(_device.NextMessageId(), txConfig, data)).ConfigureAwait(false));
        }


        public async Task StartListenAsync(Nrf24RxConfig rxConfig) => _ = new StartListenResponse(await Exchange(new StartListenRequest(_device.NextMessageId(), rxConfig)).ConfigureAwait(false));

        public async Task StopListenAsync() => _ = new StopListenResponse(await Exchange(new StopListenRequest(_device.NextMessageId())).ConfigureAwait(false));

        public Listener<Nrf24RxData> GetListener()
        {
            var listener = _device.CreateListener(m => Device.IsInitiativeMessageId(m.Header.MessageId) &&
            m.Header.InterfaceBase == DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_BASE_NRF24 && m.Header.InterfaceCommand == (byte)Commands.UNIVERSAL_DEVICE_NRF24_CMD_LISTEN);

            return listener.Wrap(m =>
            {
                m.ThrowIfError(m =>
                {
                    var errorResponse = new ErrorResponse(m);
                    return new Nrf24Exception(errorResponse, errorResponse.NRF24Error);
                });

                return new ListenResponse(m).RxData;
            });
        }

        private async Task<Message> Exchange(Message request)
        {
            using var listener = _device.CreateListener(m =>
                m.Header.MessageId == request.Header.MessageId &&
                m.Header.InterfaceBase == request.Header.InterfaceBase &&
                m.Header.InterfaceCommand == request.Header.InterfaceCommand);

            await _device.WriteAsync(request).ConfigureAwait(false);

            var response = await listener.ReadAsync(__timeout).ConfigureAwait(false);

            response.ThrowIfError(m =>
            {
                var errorResponse = new ErrorResponse(m);
                return new Nrf24Exception(errorResponse, errorResponse.NRF24Error);
            });

            return response;
        }


        private enum Commands : byte
        {
            /// <summary>
            /// Request: <see cref="ResetRequest"/>
            /// Response: <see cref="ResetResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_NRF24_CMD_RESET = 0x08,

            /// <summary>
            /// Request: <see cref="SetConfigRequest"/>
            /// Response: <see cref="SetConfigResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_NRF24_CMD_SET_CONFIG = 0x30,

            /// <summary>
            /// Request: <see cref="SendRequest"/>
            /// Response: <see cref="SendResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_NRF24_CMD_SEND = 0x40,

            /// <summary>
            /// Request: <see cref="StartListenRequest"/>
            /// Response: <see cref="StartListenResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_NRF24_CMD_START_LISTEN = 0x50,
            /// <summary>
            /// Request: <see cref="StopListenRequest"/>
            /// Response: <see cref="StopListenResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_NRF24_CMD_STOP_LISTEN = 0x51,
            /// <summary>
            /// Initiative response: <see cref="ListenResponse"/>
            /// Error: <see cref="ErrorResponse"/> or <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_NRF24_CMD_LISTEN = 0x52,
        }


        private abstract class Request : Message
        {
            internal Request(ushort messageId, Commands command, IList<byte> body) : base(new MessageHeader(
                messageId, messageBodySize: (ushort)body.Count,
                fragmentBodyOffset: 0, fragmentBodySize: (ushort)body.Count,
                DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_BASE_NRF24, interfacePort: 0, (byte)command, DeviceErrors.UNIVERSAL_DEVICE_ERR_OK),
                body)
                => command.ThrowIfNotDefined();
        }

        private class ErrorResponse : Message
        {
            private const ushort __bodySize = 4;


            internal ErrorResponse(Message message) : base(message)
            {
                if (message.Body.Count != __bodySize)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }

                NRF24Error = message.Body.ToArray().ToStruct<UnmanagedBody>().NRF24Error;
            }


            internal Nrf24Errors NRF24Error { get; }


            [StructLayout(LayoutKind.Explicit, Size = __bodySize)]
            private struct UnmanagedBody
            {
                [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] internal Nrf24Errors NRF24Error;
            }
        }

        private class ResetRequest : Request
        {
            internal ResetRequest(ushort messageId) : base(messageId, Commands.UNIVERSAL_DEVICE_NRF24_CMD_RESET, EmptyList)
            {
            }
        }

        private class ResetResponse : Message
        {
            internal ResetResponse(Message message) : base(message)
            {
                if (message.Body.Count != 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }
            }
        }

        private class SetConfigRequest : Request
        {
            internal SetConfigRequest(ushort messageId, Nrf24Config config) : base(messageId, Commands.UNIVERSAL_DEVICE_NRF24_CMD_SET_CONFIG, config.Raw) => Config = config;


            internal Nrf24Config Config { get; }
        }

        private class SetConfigResponse : Message
        {
            internal SetConfigResponse(Message message) : base(message)
            {
                if (message.Body.Count != 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }
            }
        }

        private class SendRequest : Request
        {
            private const ushort __dataInfoSize = 4;


            internal SendRequest(ushort messageId, Nrf24TxConfig txConfig, IList<byte> data) : base(messageId, Commands.UNIVERSAL_DEVICE_NRF24_CMD_SEND, CreateBody(txConfig, data))
            {
                TxConfig = txConfig;
                Data = new ReadOnlyCollection<byte>(data);
            }


            internal Nrf24TxConfig TxConfig { get; }

            internal ReadOnlyCollection<byte> Data { get; }


            private static IList<byte> CreateBody(Nrf24TxConfig txConfig, IList<byte> data)
            {
                var body = new byte[Nrf24TxConfig.Size + __dataInfoSize + data.Count.IncreaseToMultipleOfFour()];

                txConfig.Raw.CopyTo(body, 0);
                new UnmanagedTxDataInfo { Length = (byte)data.Count }.ToByteArray().CopyTo(body, Nrf24TxConfig.Size);
                data.CopyTo(body, Nrf24TxConfig.Size + __dataInfoSize);

                return body;
            }


            [StructLayout(LayoutKind.Explicit, Size = __dataInfoSize)]
            private struct UnmanagedTxDataInfo
            {
                [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] internal byte Length;

                /* __Reserved byte[3] */
            }
        }

        private class SendResponse : Message
        {
            internal SendResponse(Message message) : base(message)
            {
                if (message.Body.Count != 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }
            }
        }

        private class StartListenRequest : Request
        {
            internal StartListenRequest(ushort messageId, Nrf24RxConfig rxConfig) : base(messageId, Commands.UNIVERSAL_DEVICE_NRF24_CMD_START_LISTEN, rxConfig.Raw) => RxConfig = rxConfig;


            internal Nrf24RxConfig RxConfig { get; }
        }

        private class StartListenResponse : Message
        {
            internal StartListenResponse(Message message) : base(message)
            {
                if (message.Body.Count != 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }
            }
        }

        private class StopListenRequest : Request
        {
            internal StopListenRequest(ushort messageId) : base(messageId, Commands.UNIVERSAL_DEVICE_NRF24_CMD_STOP_LISTEN, EmptyList)
            {
            }
        }

        private class StopListenResponse : Message
        {
            internal StopListenResponse(Message message) : base(message)
            {
                if (message.Body.Count != 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }
            }
        }

        private class ListenResponse : Message
        {
            internal ListenResponse(Message message) : base(message) => RxData = new Nrf24RxData(message.Body);


            internal Nrf24RxData RxData { get; }
        }
    }
}
