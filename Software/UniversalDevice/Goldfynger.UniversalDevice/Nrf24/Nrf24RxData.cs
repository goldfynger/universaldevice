﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice.Nrf24
{
    /// <summary>Represents Nrf24 Rx data.</summary>
    public sealed record Nrf24RxData : IEquatable<Nrf24RxData>
    {
        internal const ushort Size = 8;


        internal Nrf24RxData(IList<byte> raw)
        {
            if (raw.Count <= Size)
            {
                throw new ArgumentOutOfRangeException(nameof(raw));
            }

            var unmanagedRxDataInfo = raw.ToArray().ToStruct<UnmanagedRxDataInfo>();

            var rxAddress = new Nrf24Address(unmanagedRxDataInfo.RxAddressB0, unmanagedRxDataInfo.RxAddressB1, unmanagedRxDataInfo.RxAddressB2, unmanagedRxDataInfo.RxAddressB3, unmanagedRxDataInfo.RxAddressB4);
            var length = unmanagedRxDataInfo.Length;

            var rxPipe = unmanagedRxDataInfo.RxPipe;
            rxPipe.ThrowIfNotDefined();

            if (raw.Count - Size < length)
            {
                throw new ArgumentOutOfRangeException(nameof(raw));
            }

            var data = raw.Copy(Size, length);

            Data = new ReadOnlyCollection<byte>(data);
            Length = length;
            RxAddress = rxAddress;
            RxPipe = rxPipe;

            Raw = new ReadOnlyCollection<byte>(raw);
        }


        /// <summary>Rx data.</summary>
        public ReadOnlyCollection<byte> Data { get; }

        /// <summary>Rx data length.</summary>
        public byte Length { get; }

        /// <summary>Rx pipe address.</summary>
        public Nrf24Address RxAddress { get; }

        /// <summary>Rx pipe number.</summary>
        public Nrf24RxConfig.RxDataPipes RxPipe { get; }

        internal ReadOnlyCollection<byte> Raw { get; }


        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns><see langword="true"/> if the current object is equal to the other parameter; otherwise, <see langword="false"/>.</returns>
        public bool Equals(Nrf24RxData? other) => other != null && other.Data.SequenceEqual(Data) && other.Length == Length && other.RxAddress == RxAddress && other.RxPipe == RxPipe;

        /// <summary>Serves as the default hash function.</summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode() => HashCode.Combine(Data, Length, RxAddress, RxPipe);


        [StructLayout(LayoutKind.Explicit, Size = Size)]
        private struct UnmanagedRxDataInfo
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] internal byte RxAddressB0;
            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] internal byte RxAddressB1;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U1)] internal byte RxAddressB2;
            [FieldOffset(3)] [MarshalAs(UnmanagedType.U1)] internal byte RxAddressB3;
            [FieldOffset(4)] [MarshalAs(UnmanagedType.U1)] internal byte RxAddressB4;

            [FieldOffset(5)] [MarshalAs(UnmanagedType.U1)] internal byte Length;

            [FieldOffset(6)] [MarshalAs(UnmanagedType.U1)] internal Nrf24RxConfig.RxDataPipes RxPipe;

            /* __Reserved byte[1] */
        }
    }
}
