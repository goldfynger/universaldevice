﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Goldfynger.UniversalDevice.Nrf24
{
    /// <summary>
    /// Interface to communicate with universal device Nrf24.
    /// </summary>
    public interface INrf24Device : IDeviceInterface
    {
        /// <summary>
        /// Reset interface in async mode.
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        Task ResetAsync();

        /// <summary>
        /// Sets nRF24 config.
        /// </summary>
        /// <param name="config"><see cref="Nrf24Config"/></param>
        /// <returns><see cref="Task"/></returns>
        Task SetConfigAsync(Nrf24Config config);

        /// <summary>
        /// Sends data in async mode.
        /// </summary>
        /// <param name="txConfig"><see cref="Nrf24TxConfig"/></param>
        /// <param name="data"><see cref="IList{T}"/> of <see cref="byte"/> with data to sent.</param>
        /// <returns><see cref="Task"/></returns>
        Task SendAsync(Nrf24TxConfig txConfig, IList<byte> data);

        /// <summary>
        /// Start listening for new Rx data in async mode.
        /// </summary>
        /// <param name="rxConfig"><see cref="Nrf24RxConfig"/></param>
        /// <returns><see cref="Task"/></returns>
        Task StartListenAsync(Nrf24RxConfig rxConfig);
        /// <summary>
        /// Stop listening for new Rx data in async mode.
        /// </summary>
        /// <returns><see cref="Task"/></returns>
        Task StopListenAsync();
        /// <summary>
        /// Gets Nrf24 data listener.
        /// </summary>
        /// <returns><see cref="Listener{T}"/> of <see cref="Nrf24RxData"/>.</returns>
        Listener<Nrf24RxData> GetListener();
    }
}
