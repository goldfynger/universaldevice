﻿namespace Goldfynger.UniversalDevice.Nrf24
{
    /// <summary>Contains NRF24 constant values.</summary>
    public static class Nrf24Const
    {
        /// <summary>Maximum receive and transmit data length.</summary>
        public const int MaxDataLength = 32;
    }
}
