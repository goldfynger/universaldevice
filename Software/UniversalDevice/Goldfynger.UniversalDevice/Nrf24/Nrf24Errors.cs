﻿namespace Goldfynger.UniversalDevice.Nrf24
{
    /// <summary>
    /// Nrf24 interface errors.
    /// </summary>
    public enum Nrf24Errors : byte
    {
        /// <summary>No error.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        NRF24_ERR_OK = 0x00,

        /// <summary>Function parameter has NULL pointer.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        NRF24_ERR_PARAM_NULLPTR = 0x01,
        /// <summary>Function parameter has invalid value.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        NRF24_ERR_PARAM_INVALID = 0x02,
        /// <summary>Operation not allowed.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        NRF24_ERR_NOT_ALLOWED = 0x03,
        /// <summary>Buffer length is not enough for operation.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        NRF24_ERR_LEN_NOT_ENOUGH = 0x04,

        /// <summary>HAL function error.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        NRF24_ERR_HAL = 0x20,
        /// <summary>HAL function not implemented.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        NRF24_ERR_NOT_IMPLEMENTED = 0x21,

        /// <summary>nRF24 configuration error.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        NRF24_ERR_CONFIGURATION = 0x40,

        /* NRF24_ERR_TX_COMPLETE = NRF24_ERR_OK, */
        /* NRF24_ERR_RX_COMPLETE = NRF24_ERR_OK, */
        /// <summary>Tx not complete yet.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        NRF24_ERR_TX_NOT_COMPLETE = 0x80,
        /// <summary>Tx no ACK.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        NRF24_ERR_TX_NO_ACK = 0x81,
        /// <summary>Rx not complete yet.</summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        NRF24_ERR_RX_NOT_COMPLETE = 0x82,
    }
}
