﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice.Nrf24
{
    /// <summary>Represents TX configuration of Nrf24 device.</summary>
    public sealed record Nrf24TxConfig : IEquatable<Nrf24TxConfig>
    {
        /// <summary>Default TxRx0 address.</summary>
        public static readonly Nrf24Address DefaultTxAddress = new(0xE7, 0xE7, 0xE7, 0xE7, 0xE7);
        /// <summary>Default retransmissions count.</summary>
        public const RetrCounts DefaultRetrCount = RetrCounts.NRF24_RETR_COUNT_UP_TO_3;
        /// <summary>Default retransmissions delay.</summary>
        public const RetrDelays DefaultRetrDelay = RetrDelays.NRF24_RETR_DELAY_250;

        internal const ushort Size = 8;

        /// <summary>Default config instance.</summary>
        public static readonly Nrf24TxConfig Default = new(DefaultTxAddress, DefaultRetrCount, DefaultRetrDelay);


        /// <summary>Creates new instance of <see cref="Nrf24TxConfig"/> using TX configuration parameters.</summary>
        /// <param name="txAddress">Tx address.</param>
        /// <param name="retrCount">Retransmissions count.</param>
        /// <param name="retrDelay">Retransmissions delay.</param>
        public Nrf24TxConfig(Nrf24Address txAddress, RetrCounts retrCount, RetrDelays retrDelay)
        {
            TxAddress = txAddress;

            retrCount.ThrowIfNotDefined();
            RetrCount = retrCount;

            retrDelay.ThrowIfNotDefined();
            RetrDelay = retrDelay;

            var unmanagedTxConfig = new UnmanagedTxConfig
            {
                TxAddressB0 = txAddress.B0,
                TxAddressB1 = txAddress.B1,
                TxAddressB2 = txAddress.B2,
                TxAddressB3 = txAddress.B3,
                TxAddressB4 = txAddress.B4,
                RetrCount = retrCount,
                RetrDelay = retrDelay,
            };

            Raw = new ReadOnlyCollection<byte>(unmanagedTxConfig.ToByteArray());
        }

        internal Nrf24TxConfig(IList<byte> raw)
        {
            if (raw.Count != Size)
            {
                throw new ArgumentOutOfRangeException(nameof(raw));
            }

            var unmanagedTxConfig = raw.ToArray().ToStruct<UnmanagedTxConfig>();

            var txAddress = new Nrf24Address(unmanagedTxConfig.TxAddressB0, unmanagedTxConfig.TxAddressB1, unmanagedTxConfig.TxAddressB2, unmanagedTxConfig.TxAddressB3, unmanagedTxConfig.TxAddressB4);
            var retrCount = unmanagedTxConfig.RetrCount;
            var retrDelay = unmanagedTxConfig.RetrDelay;

            TxAddress = txAddress;

            retrCount.ThrowIfNotDefined();
            RetrCount = retrCount;

            retrDelay.ThrowIfNotDefined();
            RetrDelay = retrDelay;

            Raw = new ReadOnlyCollection<byte>(raw);
        }


        /// <summary>Tx address.</summary>
        public Nrf24Address TxAddress { get; }

        /// <summary>Retransmissions count.</summary>
        public RetrCounts RetrCount { get; }
        /// <summary>Retransmissions delay.</summary>
        public RetrDelays RetrDelay { get; }


        internal ReadOnlyCollection<byte> Raw { get; }


        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns><see langword="true"/> if the current object is equal to the other parameter; otherwise, <see langword="false"/>.</returns>
        public bool Equals(Nrf24TxConfig? other) => other != null && other.TxAddress == TxAddress && other.RetrCount == RetrCount && other.RetrDelay == RetrDelay;

        /// <summary>Serves as the default hash function.</summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode() => HashCode.Combine(TxAddress, RetrCount, RetrDelay);

        /// <summary>Configuration representation.</summary>
        /// <returns><see cref="string"/> configuration.</returns>
        public override string ToString() => $"{TxAddress}|{RetrCount}|{RetrDelay}";


        [StructLayout(LayoutKind.Explicit, Size = Size)]
        private struct UnmanagedTxConfig
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] internal byte TxAddressB0;
            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] internal byte TxAddressB1;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U1)] internal byte TxAddressB2;
            [FieldOffset(3)] [MarshalAs(UnmanagedType.U1)] internal byte TxAddressB3;
            [FieldOffset(4)] [MarshalAs(UnmanagedType.U1)] internal byte TxAddressB4;

            [FieldOffset(5)] [MarshalAs(UnmanagedType.U1)] internal RetrCounts RetrCount;
            [FieldOffset(6)] [MarshalAs(UnmanagedType.U1)] internal RetrDelays RetrDelay;

            /* __Reserved byte[1] */
        }


        /// <summary>Retransmissions counts.</summary>
        public enum RetrCounts : byte
        {
            /// <summary>No retransmissions.</summary>
            NRF24_RETR_COUNT_NO = 0 << 0,
            /// <summary>Up to 1 retransmission.</summary>
            NRF24_RETR_COUNT_UP_TO_1 = 1 << 0,
            /// <summary>Up to 2 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_2 = 2 << 0,
            /// <summary>Up to 3 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_3 = 3 << 0,
            /// <summary>Up to 4 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_4 = 4 << 0,
            /// <summary>Up to 5 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_5 = 5 << 0,
            /// <summary>Up to 6 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_6 = 6 << 0,
            /// <summary>Up to 7 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_7 = 7 << 0,
            /// <summary>Up to 8 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_8 = 8 << 0,
            /// <summary>Up to 9 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_9 = 9 << 0,
            /// <summary>Up to 10 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_10 = 10 << 0,
            /// <summary>Up to 11 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_11 = 11 << 0,
            /// <summary>Up to 12 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_12 = 12 << 0,
            /// <summary>Up to 13 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_13 = 13 << 0,
            /// <summary>Up to 14 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_14 = 14 << 0,
            /// <summary>Up to 15 retransmissions.</summary>
            NRF24_RETR_COUNT_UP_TO_15 = 15 << 0,
        }

        /// <summary>Retransmissions delays.</summary>
        public enum RetrDelays : byte
        {
            /// <summary>Delay 250 uS.</summary>
            NRF24_RETR_DELAY_250 = 0 << 4,
            /// <summary>Delay 500 uS.</summary>
            NRF24_RETR_DELAY_500 = 1 << 4,
            /// <summary>Delay 750 uS.</summary>
            NRF24_RETR_DELAY_750 = 2 << 4,
            /// <summary>Delay 1000 uS.</summary>
            NRF24_RETR_DELAY_1000 = 3 << 4,
            /// <summary>Delay 1250 uS.</summary>
            NRF24_RETR_DELAY_1250 = 4 << 4,
            /// <summary>Delay 1500 uS.</summary>
            NRF24_RETR_DELAY_1500 = 5 << 4,
            /// <summary>Delay 1750 uS.</summary>
            NRF24_RETR_DELAY_1750 = 6 << 4,
            /// <summary>Delay 2000 uS.</summary>
            NRF24_RETR_DELAY_2000 = 7 << 4,
            /// <summary>Delay 2250 uS.</summary>
            NRF24_RETR_DELAY_2250 = 8 << 4,
            /// <summary>Delay 2500 uS.</summary>
            NRF24_RETR_DELAY_2500 = 9 << 4,
            /// <summary>Delay 2750 uS.</summary>
            NRF24_RETR_DELAY_2750 = 10 << 4,
            /// <summary>Delay 3000 uS.</summary>
            NRF24_RETR_DELAY_3000 = 11 << 4,
            /// <summary>Delay 3250 uS.</summary>
            NRF24_RETR_DELAY_3250 = 12 << 4,
            /// <summary>Delay 3500 uS.</summary>
            NRF24_RETR_DELAY_3500 = 13 << 4,
            /// <summary>Delay 3750 uS.</summary>
            NRF24_RETR_DELAY_3750 = 14 << 4,
            /// <summary>Delay 4000 uS.</summary>
            NRF24_RETR_DELAY_4000 = 15 << 4,
        }
    }
}
