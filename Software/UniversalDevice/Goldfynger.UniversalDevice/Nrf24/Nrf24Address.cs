﻿using System;
using System.Collections.ObjectModel;

namespace Goldfynger.UniversalDevice.Nrf24
{
    /// <summary>Nrf24 pipe address.</summary>
    public sealed record Nrf24Address : IEquatable<Nrf24Address>
    {
        /// <summary>Creates new <see cref="Nrf24Address"/> using address bytes.</summary>
        /// <param name="b0">Address byte 0.</param>
        /// <param name="b1">Address byte 1.</param>
        /// <param name="b2">Address byte 2.</param>
        /// <param name="b3">Address byte 3.</param>
        /// <param name="b4">Address byte 4.</param>
        public Nrf24Address(byte b0, byte b1, byte b2, byte b3, byte b4) => Raw = new ReadOnlyCollection<byte>(new byte[] { b0, b1, b2, b3, b4 });


        /// <summary>Address byte 0.</summary>
        public byte B0 => this[0];

        /// <summary>Address byte 1.</summary>
        public byte B1 => this[1];

        /// <summary>Address byte 2.</summary>
        public byte B2 => this[2];

        /// <summary>Address byte 3.</summary>
        public byte B3 => this[3];

        /// <summary>Address byte 4.</summary>
        public byte B4 => this[4];

        /// <summary>Gets the element at the specified index.</summary>
        /// <param name="byte">The zero-based index of the element to get.</param>
        /// <returns>The element at the specified index.</returns>
        /// <exception cref="ArgumentOutOfRangeException"/>
        public byte this[int @byte] => Raw[@byte];

        internal ReadOnlyCollection<byte> Raw { get; }


        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns><see langword="true"/> if the current object is equal to the other parameter; otherwise, <see langword="false"/>.</returns>
        public bool Equals(Nrf24Address? other) => other != null && other.B0 == B0 && other.B1 == B1 && other.B2 == B2 && other.B3 == B3 && other.B4 == B4;

        /// <summary>Serves as the default hash function.</summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode() => HashCode.Combine(B0, B1, B2, B3, B4);

        /// <summary>Address representation.</summary>
        /// <returns><see cref="string"/> address.</returns>
        public override string ToString() => $"{B0:X2}:{B1:X2}:{B2:X2}:{B3:X2}:{B4:X2}";

        /// <summary>Creates new address based on this instance for specified subaddress.</summary>
        /// <param name="subaddress">Subaddress - B0 of new address.</param>
        /// <returns>New address.</returns>
        public Nrf24Address CreateNewForSubaddress(byte subaddress) => new(subaddress, B1, B2, B3, B4);
    }
}
