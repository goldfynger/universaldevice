﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice
{
    internal class Message
    {
        internal const ushort HeaderSize = MessageHeader.Size;
        internal const ushort MaxSize = 65532;
        internal const ushort BodyMaxSize = MaxSize - HeaderSize;

        protected static readonly List<byte> EmptyList = Array.Empty<byte>().ToList();
        protected static readonly ReadOnlyCollection<byte> EmptyCollection = new(EmptyList);


        internal Message(MessageHeader header, IList<byte> body)
        {
            if (body.Count != header.FragmentBodySize)
            {
                throw new ArgumentOutOfRangeException(nameof(body));
            }

            Header = header;
            Body = new ReadOnlyCollection<byte>(body);
            Raw = new ReadOnlyCollection<byte>(header.Raw.Concat(body).ToArray());
        }

        internal Message(IList<byte> raw)
        {
            if (raw.Count > MaxSize || raw.Count % 4 != 0)
            {
                throw new ArgumentOutOfRangeException(nameof(raw));
            }

            var header = new MessageHeader(raw.Copy(0, HeaderSize));

            if (raw.Count - HeaderSize < header.FragmentBodySize)
            {
                throw new ArgumentOutOfRangeException(nameof(raw));
            }

            var body = header.FragmentBodySize == 0 ? EmptyCollection : new ReadOnlyCollection<byte>(raw.Copy(HeaderSize, header.FragmentBodySize));            

            Header = header;
            Body = body;
            Raw = new ReadOnlyCollection<byte>(raw);
        }

        internal Message(Message message)
        {
            Header = message.Header;
            Body = new ReadOnlyCollection<byte>(message.Body.ToArray());
            Raw = new ReadOnlyCollection<byte>(message.Raw.ToArray());
        }


        internal MessageHeader Header { get; }

        internal ReadOnlyCollection<byte> Body { get; }

        internal ReadOnlyCollection<byte> Raw { get; }

        internal ushort Size => (ushort)Raw.Count;

        internal ushort BodySize => (ushort)Body.Count;


        internal Message Combine(Message message)
        {
            if (Header.MessageId != message.Header.MessageId)
            {
                throw new ArgumentException($"Combined messages must have same {nameof(MessageHeader.MessageId)}.", nameof(message));
            }

            if (Header.MessageBodySize != message.Header.MessageBodySize)
            {
                throw new ArgumentException($"Combined messages must have same {nameof(MessageHeader.MessageBodySize)}.", nameof(message));
            }

            if (Header.FragmentBodySize != message.Header.FragmentBodyOffset)
            {
                throw new ArgumentException($"{nameof(MessageHeader.FragmentBodySize)} of first combined message and {nameof(MessageHeader.FragmentBodyOffset)} of second must be equal.",
                    nameof(message));
            }

            if (Header.FragmentBodySize + message.Header.FragmentBodySize > message.Header.MessageBodySize)
            {
                throw new ArgumentException($"Sum of {nameof(MessageHeader.FragmentBodySize)} of two messages must be less or equal than {nameof(MessageHeader.MessageBodySize)}.", nameof(message));
            }

            var header = Header.UpdateFragmentBodyInfo(Header.FragmentBodyOffset, (ushort)(Header.FragmentBodySize + message.Header.FragmentBodySize));
            var body = Body.Concat(message.Body).ToArray();

            return new Message(header, body);
        }

        internal IList<Message> Split(ushort fragmentMaxSize)
        {
            if (fragmentMaxSize <= HeaderSize || fragmentMaxSize > MaxSize || fragmentMaxSize % 4 != 0)
            {
                throw new ArgumentOutOfRangeException(nameof(fragmentMaxSize));
            }

            var list = new List<Message>();

            if (fragmentMaxSize >= Size)
            {
                list.Add(new Message(this));
            }
            else
            {
                var bodyOffset = Header.FragmentBodyOffset;

                while (bodyOffset < Header.MessageBodySize)
                {
                    var size = (ushort)Math.Min(HeaderSize + (Header.MessageBodySize - bodyOffset), fragmentMaxSize);
                    var bodySize = (ushort)(size - HeaderSize);

                    var header = Header.UpdateFragmentBodyInfo(bodyOffset, bodySize);
                    var body = new ReadOnlyCollection<byte>(Body.Copy(bodyOffset, bodySize));

                    list.Add(new Message(header, body));

                    bodyOffset += bodySize;
                }
            }

            return list;
        }

        internal void ThrowIfError(Func<Message, Exception>? interfaceSpecificExceptionActivator = null)
        {
            if (Header.Error != DeviceErrors.UNIVERSAL_DEVICE_ERR_OK)
            {
                if (Header.Error == DeviceErrors.UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC && interfaceSpecificExceptionActivator != null)
                {
                    throw interfaceSpecificExceptionActivator(this);
                }
                else
                {
                    throw new UniversalDeviceException(this);
                }
            }
        }


        internal sealed class MessageHeader
        {
            internal const int Size = 12;            

            private static readonly Dictionary<DeviceInterfaces, Dictionary<byte, string>> __commandToStringResolverDictionary = new();

            private readonly UnmanagedMessageHeader _unmanagedHeader;


            internal MessageHeader(
                ushort messageId, ushort messageBodySize,
                ushort fragmentBodyOffset, ushort fragmentBodySize,
                DeviceInterfaces interfaceBase, byte interfacePort, byte interfaceCommand, DeviceErrors error)
            {
                var unmanagedHeader = new UnmanagedMessageHeader
                {
                    MessageId = messageId,
                    MessageBodySize = messageBodySize,
                    FragmentBodyOffset = fragmentBodyOffset,
                    FragmentBodySize = fragmentBodySize,
                    InterfaceBase = interfaceBase,
                    InterfacePort = interfacePort,
                    InterfaceCommand = interfaceCommand,
                    Error = error,
                };

                ThrowIfInvalid(unmanagedHeader);

                _unmanagedHeader = unmanagedHeader;

                Raw = new ReadOnlyCollection<byte>(_unmanagedHeader.ToByteArray());
            }

            internal MessageHeader(IList<byte> raw)
            {
                if (raw.Count != HeaderSize)
                {
                    throw new ArgumentOutOfRangeException(nameof(raw));
                }

                var unmanagedHeader = raw.ToArray().ToStruct<UnmanagedMessageHeader>();

                ThrowIfInvalid(unmanagedHeader);

                _unmanagedHeader = unmanagedHeader;

                Raw = new ReadOnlyCollection<byte>(raw);
            }

            internal MessageHeader(MessageHeader header)
            {
                _unmanagedHeader = header._unmanagedHeader;

                Raw = new ReadOnlyCollection<byte>(_unmanagedHeader.ToByteArray());
            }

            private MessageHeader(UnmanagedMessageHeader unmanagedHeader)
            {
                _unmanagedHeader = unmanagedHeader;

                Raw = new ReadOnlyCollection<byte>(_unmanagedHeader.ToByteArray());
            }

            internal ushort MessageId => _unmanagedHeader.MessageId;
            internal ushort MessageBodySize => _unmanagedHeader.MessageBodySize;

            internal ushort FragmentBodyOffset => _unmanagedHeader.FragmentBodyOffset;
            internal ushort FragmentBodySize => _unmanagedHeader.FragmentBodySize;

            internal DeviceInterfaces InterfaceBase => _unmanagedHeader.InterfaceBase;
            internal byte InterfacePort => _unmanagedHeader.InterfacePort;
            internal byte InterfaceCommand => _unmanagedHeader.InterfaceCommand;
            internal DeviceErrors Error => _unmanagedHeader.Error;

            internal bool IsInitiative => _unmanagedHeader.MessageId >= Device.InitiativeMessageIdMin;
            internal bool IsWhole => _unmanagedHeader.MessageBodySize == _unmanagedHeader.FragmentBodySize;
            internal bool HasZeroOffset => _unmanagedHeader.FragmentBodyOffset == 0;

            internal ReadOnlyCollection<byte> Raw { get; }


            private static void ThrowIfInvalid(in UnmanagedMessageHeader unmanagedHeader)
            {
                if (unmanagedHeader.MessageBodySize > BodyMaxSize || unmanagedHeader.MessageBodySize % 4 != 0)
                {
                    throw new InvalidOperationException($"{nameof(UnmanagedMessageHeader.MessageBodySize)} has invalid value.");
                }

                if (unmanagedHeader.FragmentBodySize > BodyMaxSize || unmanagedHeader.FragmentBodySize % 4 != 0)
                {
                    throw new InvalidOperationException($"{nameof(UnmanagedMessageHeader.FragmentBodySize)} has invalid value.");
                }

                if (unmanagedHeader.FragmentBodyOffset + unmanagedHeader.FragmentBodySize > unmanagedHeader.MessageBodySize || unmanagedHeader.FragmentBodyOffset % 4 != 0)
                {
                    throw new InvalidOperationException($"{nameof(UnmanagedMessageHeader.FragmentBodyOffset)} or {nameof(UnmanagedMessageHeader.FragmentBodySize)} have invalid values.");
                }

                unmanagedHeader.InterfaceBase.ThrowIfNotDefined();
                unmanagedHeader.Error.ThrowIfNotDefined();
            }

            internal static void AddCommandsToResolver<TEnum>(DeviceInterfaces @interface) where TEnum : Enum
            {
                var dictionary = new Dictionary<byte, string>();
                var values = Enum.GetValues(typeof(TEnum));

                foreach (byte value in values)
                {
                    dictionary.Add(value, Enum.GetName(typeof(TEnum), value)!);
                }

                __commandToStringResolverDictionary.Add(@interface, dictionary);
            }

            internal MessageHeader UpdateFragmentBodyInfo(ushort fragmentBodyOffset, ushort fragmentBodySize)
            {
                var unmanagedHeader = _unmanagedHeader;
                unmanagedHeader.FragmentBodyOffset = fragmentBodyOffset;
                unmanagedHeader.FragmentBodySize = fragmentBodySize;

                ThrowIfInvalid(unmanagedHeader);

                return new MessageHeader(unmanagedHeader);
            }

            public string ToString(string? format = null)
            {
                var result = new StringBuilder();

                result.Append($"{nameof(Header.MessageId)}:0x{MessageId:X4}");
                result.Append($" {nameof(Header.MessageBodySize)}:0x{MessageBodySize:X4}");
                result.Append($" {nameof(Header.FragmentBodyOffset)}:0x{FragmentBodyOffset:X4}");
                result.Append($" {nameof(Header.FragmentBodySize)}:0x{FragmentBodySize:X4}");
                result.Append($" {nameof(Header.InterfaceBase)}:{InterfaceBase}");
                result.Append($" {nameof(Header.InterfacePort)}:0x{InterfacePort:X2}");

                if (__commandToStringResolverDictionary.TryGetValue(InterfaceBase, out Dictionary<byte, string>? commands) && commands.TryGetValue(InterfaceCommand, out string? command))
                {
                    result.Append($" {nameof(Header.InterfaceCommand)}:{command}");
                }
                else
                {
                    result.Append($" {nameof(Header.InterfaceCommand)}:unknown(0x{InterfaceCommand:X2})");
                }

                if (format == "DebugRead")
                {
                    result.Append($" {nameof(Header.Error)}:{Error}");
                }

                return result.ToString();
            }


            [StructLayout(LayoutKind.Explicit, Size = Size)]
            private struct UnmanagedMessageHeader
            {
                [FieldOffset(0)] [MarshalAs(UnmanagedType.U2)] internal ushort MessageId;
                [FieldOffset(2)] [MarshalAs(UnmanagedType.U2)] internal ushort MessageBodySize;

                [FieldOffset(4)] [MarshalAs(UnmanagedType.U2)] internal ushort FragmentBodyOffset;
                [FieldOffset(6)] [MarshalAs(UnmanagedType.U2)] internal ushort FragmentBodySize;

                [FieldOffset(8)] [MarshalAs(UnmanagedType.U1)] internal DeviceInterfaces InterfaceBase;
                [FieldOffset(9)] [MarshalAs(UnmanagedType.U1)] internal byte InterfacePort;
                [FieldOffset(10)] [MarshalAs(UnmanagedType.U1)] internal byte InterfaceCommand;
                [FieldOffset(11)] [MarshalAs(UnmanagedType.U1)] internal DeviceErrors Error;
            }
        }
    }
}
