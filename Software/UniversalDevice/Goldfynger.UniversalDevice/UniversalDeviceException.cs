﻿using System;
using System.Diagnostics;

namespace Goldfynger.UniversalDevice
{
    /// <summary>
    /// Base class of universal device exceptions.
    /// </summary>
    [DebuggerDisplay("DeviceError = {DeviceError}")]
    public class UniversalDeviceException : Exception
    {
        internal UniversalDeviceException(Message errorResponse)
        {
            DeviceError = errorResponse.Header.Error;
            ErrorResponse = errorResponse;
        }

        internal UniversalDeviceException(Message errorResponse, string message) : base(message)
        {
            DeviceError = errorResponse.Header.Error;
            ErrorResponse = errorResponse;
        }

        internal UniversalDeviceException(Message errorResponse, string message, Exception innerException) : base(message, innerException)
        {
            DeviceError = errorResponse.Header.Error;
            ErrorResponse = errorResponse;
        }


        /// <summary>
        /// Universal device error that caused exception.
        /// </summary>
        public DeviceErrors DeviceError { get; }

        internal Message ErrorResponse { get; }
    }
}
