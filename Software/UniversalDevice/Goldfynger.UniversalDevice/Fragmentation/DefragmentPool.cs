﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Goldfynger.UniversalDevice.Fragmentation
{
    internal sealed class DefragmentPool
    {
        private readonly Dictionary<Tuple<DeviceInterfaces, byte>, Message> _defragmentPool = new();


        internal bool Defragment(Message fragment, [NotNullWhen(true)] out Message? whole)
        {
            var key = new Tuple<DeviceInterfaces, byte>(fragment.Header.InterfaceBase, fragment.Header.InterfacePort);

            if (_defragmentPool.TryGetValue(key, out Message? messageInPool))
            {
                if (fragment.Header.MessageId != messageInPool.Header.MessageId)
                {
                    if (!fragment.Header.HasZeroOffset)
                    {
                        throw new ArgumentException($"{nameof(Message.MessageHeader.FragmentBodyOffset)} of first fragment must be 0.", nameof(fragment));
                    }

                    messageInPool = fragment;
                    _defragmentPool[key] = messageInPool;
                }
                else if (messageInPool.Header.FragmentBodySize != fragment.Header.FragmentBodyOffset)
                {
                    throw new ArgumentException($"Input fragment is not a continue of previously saved fragment.", nameof(fragment));
                }
                else if (messageInPool.Header.MessageBodySize != fragment.Header.MessageBodySize)
                {
                    throw new ArgumentException($"Input fragment and previously saved fragment have different {nameof(Message.MessageHeader.MessageBodySize)}.", nameof(fragment));
                }
                else
                {
                    messageInPool = messageInPool.Combine(fragment);
                    _defragmentPool[key] = messageInPool;
                }
            }
            else
            {
                if (!fragment.Header.HasZeroOffset)
                {
                    throw new ArgumentException($"{nameof(Message.MessageHeader.FragmentBodyOffset)} of first fragment must be 0.", nameof(fragment));
                }

                messageInPool = fragment;
                _defragmentPool.Add(key, messageInPool);
            }

            if (messageInPool.Header.IsWhole)
            {
                whole = messageInPool;
                return true;
            }
            else
            {
                whole = null;
                return false;
            }
        }
    }
}
