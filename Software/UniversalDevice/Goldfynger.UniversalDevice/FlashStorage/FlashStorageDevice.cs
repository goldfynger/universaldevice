﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice.FlashStorage
{
    internal sealed class FlashStorageDevice : IFlashStorageDevice
    {
        //private const int __maxNamesInMessage = (Device.MessageBodySize - 8) / FlashStorageRecordName.Size;
        //private const int __maxContentSizeInMessage = Device.MessageBodySize - FlashStorageRecordName.Size - 8;
        private const int __maxContentSizeInRecord = 254;

        private static readonly TimeSpan __timeout = TimeSpan.FromSeconds(2);


        private readonly Device _device;


        internal FlashStorageDevice(Device device)
        {
            _device = device ?? throw new ArgumentNullException(nameof(device));
        }


        public bool IsCorrupted => _device.IsCorrupted;


#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task OpenAsync()

        {
            //var request = new OpenCloseRequestResponse
            //{
            //    Header =
            //    {
            //        Error = DeviceErrors.UNIVERSAL_DEVICE_ERR_OK,
            //        Interface = DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_FLASH_STORAGE,
            //        Command = Commands.UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_OPEN,

            //        MessageId = _device.NextMessageId()
            //    }
            //};

            //await Exchange<OpenCloseRequestResponse, OpenCloseRequestResponse>(request);
        }

        public async Task CloseAsync()
        {
            //var request = new OpenCloseRequestResponse
            //{
            //    Header =
            //    {
            //        Error = DeviceErrors.UNIVERSAL_DEVICE_ERR_OK,
            //        Interface = DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_FLASH_STORAGE,
            //        Command = Commands.UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_CLOSE,

            //        MessageId = _device.NextMessageId()
            //    }
            //};

            //await Exchange<OpenCloseRequestResponse, OpenCloseRequestResponse>(request);
        }

        public async Task<byte> GetRecordsCountAsync()
        {
            //var request = new RecordsCountRequest
            //{
            //    Header =
            //    {
            //        Error = DeviceErrors.UNIVERSAL_DEVICE_ERR_OK,
            //        Interface = DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_FLASH_STORAGE,
            //        Command = Commands.UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORDS_COUNT,

            //        MessageId = _device.NextMessageId()
            //    }
            //};

            //return (await Exchange<RecordsCountRequest, RecordsCountResponse>(request)).RecordsCount;

            return 0;
        }

        public async Task<ReadOnlyCollection<FlashStorageRecordName>> GetRecordsNamesAsync()
        {
            //var request = new RecordsNamesRequest
            //{
            //    Header =
            //    {
            //        Error = DeviceErrors.UNIVERSAL_DEVICE_ERR_OK,
            //        Interface = DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_FLASH_STORAGE,
            //        Command = Commands.UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORDS_NAMES,

            //        MessageId = _device.NextMessageId()
            //    }
            //};

            //var requestMessage = request.ToByteArray().ToStruct<Device.Message>();

            //using var listener = _device.CreateListener(m => m.Header.Interface == requestMessage.Header.Interface &&
            //    m.Header.Command == requestMessage.Header.Command && m.Header.MessageId == requestMessage.Header.MessageId);

            //await _device.WriteAsync(requestMessage);

            //var names = new List<FlashStorageRecordName>();

            //while (true)
            //{
            //    var responseMessage = await listener.ReadAsync(__timeout);

            //    if (responseMessage.Header.Error != DeviceErrors.UNIVERSAL_DEVICE_ERR_OK)
            //    {
            //        var errorResponse = responseMessage.ToByteArray().ToStruct<ErrorResponse>();

            //        throw new FlashStorageException(responseMessage, errorResponse.FlashStorageError);
            //    }

            //    var response = responseMessage.ToByteArray().ToStruct<RecordsNamesResponse>();

            //    var namesToCopyFull = response.NamesCount - response.NamesOffset;
            //    var namesToCopyNow = Math.Min(namesToCopyFull, __maxNamesInMessage);

            //    for (var nameIdx = 0; nameIdx < namesToCopyNow; nameIdx++)
            //    {
            //        names.Add(new FlashStorageRecordName(response.Names[nameIdx]));
            //    }

            //    if (namesToCopyFull <= __maxNamesInMessage)
            //    {
            //        break;
            //    }
            //}

            //return names.AsReadOnly();

#pragma warning disable CS8603 // Possible null reference return.
            return null;
#pragma warning restore CS8603 // Possible null reference return.
        }

        public async Task<byte> GetRecordSizeAsync(FlashStorageRecordName name)
        {
            //if (name == null)
            //{
            //    throw new ArgumentNullException(nameof(name));
            //}

            //var request = new RecordSizeRequest
            //{
            //    Header =
            //    {
            //        Error = DeviceErrors.UNIVERSAL_DEVICE_ERR_OK,
            //        Interface = DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_FLASH_STORAGE,
            //        Command = Commands.UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORD_SIZE,

            //        MessageId = _device.NextMessageId()
            //    },

            //    Name = name.Raw
            //};

            //return (await Exchange<RecordSizeRequest, RecordSizeResponse>(request)).Size;

            return 0;
        }

        public async Task<ReadOnlyCollection<byte>> ReadRecordAsync(FlashStorageRecordName name)
        {
            //if (name == null)
            //{
            //    throw new ArgumentNullException(nameof(name));
            //}

            //var request = new ReadRecordRequest
            //{
            //    Header =
            //    {
            //        Error = DeviceErrors.UNIVERSAL_DEVICE_ERR_OK,
            //        Interface = DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_FLASH_STORAGE,
            //        Command = Commands.UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_READ_RECORD,

            //        MessageId = _device.NextMessageId()
            //    },

            //    Name = name.Raw
            //};

            //var requestMessage = request.ToByteArray().ToStruct<Device.Message>();

            //using var listener = _device.CreateListener(m => m.Header.Interface == requestMessage.Header.Interface &&
            //    m.Header.Command == requestMessage.Header.Command && m.Header.MessageId == requestMessage.Header.MessageId);

            //await _device.WriteAsync(requestMessage);

            //byte[] array = null;
            //var arrayIdx = 0;

            //while (true)
            //{
            //    var responseMessage = await listener.ReadAsync(__timeout);

            //    if (responseMessage.Header.Error != DeviceErrors.UNIVERSAL_DEVICE_ERR_OK)
            //    {
            //        var errorResponse = responseMessage.ToByteArray().ToStruct<ErrorResponse>();

            //        throw new FlashStorageException(responseMessage, errorResponse.FlashStorageError);
            //    }

            //    var response = responseMessage.ToByteArray().ToStruct<ReadRecordResponse>();

            //    var contentToCopyFull = response.ContentSize - response.ContentReadOffset;
            //    var contentToCopyNow = Math.Min(contentToCopyFull, __maxContentSizeInMessage);

            //    if (array == null)
            //    {
            //        array = new byte[contentToCopyFull];
            //    }

            //    Array.Copy(response.Content, 0, array, arrayIdx, contentToCopyNow);

            //    arrayIdx += contentToCopyNow;

            //    if (contentToCopyFull <= __maxContentSizeInMessage)
            //    {
            //        break;
            //    }
            //}

            //return new ReadOnlyCollection<byte>(array);

#pragma warning disable CS8603 // Possible null reference return.
            return null;
#pragma warning restore CS8603 // Possible null reference return.
        }

        public async Task WriteRecordAsync(FlashStorageRecordName name, ReadOnlyCollection<byte> content)
        {
            //if (name == null)
            //{
            //    throw new ArgumentNullException(nameof(name));
            //}

            //if (content == null)
            //{
            //    throw new ArgumentNullException(nameof(content));
            //}

            //if (content.Count == 0 || content.Count % 2 != 0 || content.Count > __maxContentSizeInRecord)
            //{
            //    throw new ArgumentOutOfRangeException(nameof(content));
            //}

            //var request = new WriteRecordRequest
            //{
            //    Header =
            //    {
            //        Error = DeviceErrors.UNIVERSAL_DEVICE_ERR_OK,
            //        Interface = DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_FLASH_STORAGE,
            //        Command = Commands.UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_WRITE_RECORD,

            //        MessageId = _device.NextMessageId()
            //    },

            //    Name = name.Raw,
            //    ContentSize = (byte)content.Count,
            //    ContentWriteOffset = 0,
            //    Content = new byte[__maxContentSizeInMessage],
            //};

            //var writeTimes = content.Count / __maxContentSizeInMessage;

            //if (content.Count % __maxContentSizeInMessage != 0)
            //{
            //    writeTimes += 1;
            //}

            //var array = content.ToArray();
            //var offset = 0;

            //using var listener = _device.CreateListener(m => m.Header.Interface == request.Header.Interface &&
            //    m.Header.Command == (byte)request.Header.Command && m.Header.MessageId == request.Header.MessageId);

            //for (var writeIdx = 0; writeIdx < writeTimes; writeIdx++)
            //{
            //    var contentToCopy = Math.Min(content.Count - offset, __maxContentSizeInMessage);

            //    request.ContentWriteOffset = (byte)offset;

            //    Array.Copy(array, offset, request.Content, 0, contentToCopy);

            //    offset += contentToCopy;

            //    await _device.WriteAsync(request.ToByteArray().ToStruct<Device.Message>());
            //}

            //var responseMessage = await listener.ReadAsync(__timeout);

            //if (responseMessage.Header.Error != DeviceErrors.UNIVERSAL_DEVICE_ERR_OK)
            //{
            //    var errorResponse = responseMessage.ToByteArray().ToStruct<ErrorResponse>();

            //    throw new FlashStorageException(responseMessage, errorResponse.FlashStorageError);
            //}
        }

        public async Task RemoveRecordAsync(FlashStorageRecordName name)
        {
            //if (name == null)
            //{
            //    throw new ArgumentNullException(nameof(name));
            //}

            //var request = new RemoveRecordRequest
            //{
            //    Header =
            //    {
            //        Error = DeviceErrors.UNIVERSAL_DEVICE_ERR_OK,
            //        Interface = DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_FLASH_STORAGE,
            //        Command = Commands.UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_REMOVE_RECORD,

            //        MessageId = _device.NextMessageId()
            //    },

            //    Name = name.Raw
            //};

            //await Exchange<RemoveRecordRequest, RemoveRecordResponse>(request);
        }

        private async Task<TResponse> Exchange<TRequest, TResponse>(TRequest request) where TRequest : struct where TResponse : struct
        {
            //var requestMessage = request.ToByteArray().ToStruct<Device.Message>();

            //using var listener = _device.CreateListener(m => m.Header.Interface == requestMessage.Header.Interface &&
            //    m.Header.Command == requestMessage.Header.Command && m.Header.MessageId == requestMessage.Header.MessageId);

            //await _device.WriteAsync(requestMessage).ConfigureAwait(false);

            //var responseMessage = await listener.ReadAsync(__timeout).ConfigureAwait(false);

            //if (responseMessage.Header.Error != DeviceErrors.UNIVERSAL_DEVICE_ERR_OK)
            //{
            //    var errorResponse = responseMessage.ToByteArray().ToStruct<ErrorResponse>();

            //    throw new FlashStorageException(responseMessage, errorResponse.FlashStorageError);
            //}

            //return responseMessage.ToByteArray().ToStruct<TResponse>();

            return default(TResponse);
        }


        private enum Commands : byte
        {
            /* Error: ErrorResponse */


#pragma warning disable CS1574 // XML comment has cref attribute that could not be resolved
            /// <summary>
            /// Request and response: <see cref="OpenCloseRequestResponse"/>
            /// Error: <see cref="ErrorResponse"/>
            /// </summary>
            UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_OPEN = 0x20,
            /// <summary>
            /// Request and response: <see cref="OpenCloseRequestResponse"/>
            /// Error: <see cref="ErrorResponse"/>
            /// </summary>
            UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_CLOSE = 0x21,

            /// <summary>
            /// Request: <see cref="RecordsCountRequest"/>
            /// Response: <see cref="RecordsCountResponse"/>
            /// Error: <see cref="ErrorResponse"/>
            /// </summary>
            UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORDS_COUNT = 0x30,
            /// <summary>
            /// Request: <see cref="RecordsNamesRequest"/>
            /// Response: <see cref="RecordsNamesResponse"/>
            /// Error: <see cref="ErrorResponse"/>
            /// </summary>
            UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORDS_NAMES = 0x31,
            /// <summary>
            /// Request: <see cref="RecordSizeRequest"/>
            /// Response: <see cref="RecordSizeResponse"/>
            /// Error: <see cref="ErrorResponse"/>
            /// </summary>
            UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORD_SIZE = 0x32,

            /// <summary>
            /// Request: <see cref="ReadRecordRequest"/>
            /// Response: <see cref="ReadRecordResponse"/>
            /// Error: <see cref="ErrorResponse"/>
            /// </summary>
            UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_READ_RECORD = 0x40,
            /// <summary>
            /// Request: <see cref="WriteRecordRequest"/>
            /// Response: <see cref="WriteRecordResponse"/>
            /// Error: <see cref="ErrorResponse"/>
            /// </summary>
            UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_WRITE_RECORD = 0x41,
            /// <summary>
            /// Request: <see cref="RemoveRecordRequest"/>
            /// Response: <see cref="RemoveRecordResponse"/>
            /// Error: <see cref="ErrorResponse"/>
            /// </summary>
            UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_REMOVE_RECORD = 0x42,

#pragma warning restore CS1574 // XML comment has cref attribute that could not be resolved
        }

        /*
        [StructLayout(LayoutKind.Explicit, Size = FlashStorageRecordName.Size)]
        internal struct RawRecordName
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.ByValArray, SizeConst = FlashStorageRecordName.Size)] internal byte[] Array;
        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageHeaderSize)]
        private struct MessageHeader
        {
            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] internal DeviceErrors Error;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U1)] internal DeviceInterfaces Interface;
            [FieldOffset(3)] [MarshalAs(UnmanagedType.U1)] internal Commands Command;

            [FieldOffset(4)] [MarshalAs(UnmanagedType.U4)] internal uint MessageId;
        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct ErrorResponse
        {
            [FieldOffset(0)] internal MessageHeader Header;

            [FieldOffset(Device.MessageHeaderSize)] [MarshalAs(UnmanagedType.U1)] internal FlashStorageErrors FlashStorageError;

        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct OpenCloseRequestResponse
        {
            [FieldOffset(0)] internal MessageHeader Header;

        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct RecordsCountRequest
        {
            [FieldOffset(0)] internal MessageHeader Header;

        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct RecordsCountResponse
        {
            [FieldOffset(0)] internal MessageHeader Header;

            [FieldOffset(Device.MessageHeaderSize)] [MarshalAs(UnmanagedType.U1)] internal byte RecordsCount;

        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct RecordsNamesRequest
        {
            [FieldOffset(0)] internal MessageHeader Header;

        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct RecordsNamesResponse
        {
            [FieldOffset(0)] internal MessageHeader Header;

            [FieldOffset(Device.MessageHeaderSize)] [MarshalAs(UnmanagedType.U1)] internal byte NamesCount;
            [FieldOffset(Device.MessageHeaderSize + 1)] [MarshalAs(UnmanagedType.U1)] internal byte NamesOffset;
     

            [FieldOffset(Device.MessageHeaderSize + 8)] [MarshalAs(UnmanagedType.ByValArray, SizeConst = __maxNamesInMessage * FlashStorageRecordName.Size)] internal RawRecordName[] Names;
        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct RecordSizeRequest
        {
            [FieldOffset(0)] internal MessageHeader Header;

            [FieldOffset(Device.MessageHeaderSize)] internal RawRecordName Name;

        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct RecordSizeResponse
        {
            [FieldOffset(0)] internal MessageHeader Header;

            [FieldOffset(Device.MessageHeaderSize)] internal RawRecordName Name;
            [FieldOffset(Device.MessageHeaderSize + FlashStorageRecordName.Size)] [MarshalAs(UnmanagedType.U1)] internal byte Size;

        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct ReadRecordRequest
        {
            [FieldOffset(0)] internal MessageHeader Header;

            [FieldOffset(Device.MessageHeaderSize)] internal RawRecordName Name;

        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct ReadRecordResponse
        {
            [FieldOffset(0)] internal MessageHeader Header;

            [FieldOffset(Device.MessageHeaderSize)] internal RawRecordName Name;

            [FieldOffset(Device.MessageHeaderSize + FlashStorageRecordName.Size)] [MarshalAs(UnmanagedType.U1)] internal byte ContentSize;
            [FieldOffset(Device.MessageHeaderSize + FlashStorageRecordName.Size + 1)] [MarshalAs(UnmanagedType.U1)] internal byte ContentReadOffset;
        

            [FieldOffset(Device.MessageHeaderSize + FlashStorageRecordName.Size + 8)] [MarshalAs(UnmanagedType.ByValArray, SizeConst = __maxContentSizeInMessage)] internal byte[] Content;
        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct WriteRecordRequest
        {
            [FieldOffset(0)] internal MessageHeader Header;

            [FieldOffset(Device.MessageHeaderSize)] internal RawRecordName Name;

            [FieldOffset(Device.MessageHeaderSize + FlashStorageRecordName.Size)] [MarshalAs(UnmanagedType.U1)] internal byte ContentSize;
            [FieldOffset(Device.MessageHeaderSize + FlashStorageRecordName.Size + 1)] [MarshalAs(UnmanagedType.U1)] internal byte ContentWriteOffset;
       

            [FieldOffset(Device.MessageHeaderSize + FlashStorageRecordName.Size + 8)] [MarshalAs(UnmanagedType.ByValArray, SizeConst = __maxContentSizeInMessage)] internal byte[] Content;
        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct WriteRecordResponse
        {
            [FieldOffset(0)] internal MessageHeader Header;

            [FieldOffset(Device.MessageHeaderSize)] internal RawRecordName Name;

            [FieldOffset(Device.MessageHeaderSize + FlashStorageRecordName.Size)] [MarshalAs(UnmanagedType.U1)] internal byte ContentSize;

        
        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct RemoveRecordRequest
        {
            [FieldOffset(0)] internal MessageHeader Header;

            [FieldOffset(Device.MessageHeaderSize)] internal RawRecordName Name;

        }

        [StructLayout(LayoutKind.Explicit, Size = Device.MessageSize)]
        private struct RemoveRecordResponse
        {
            [FieldOffset(0)] internal MessageHeader Header;

            [FieldOffset(Device.MessageHeaderSize)] internal RawRecordName Name;

        }*/
    }
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
}
