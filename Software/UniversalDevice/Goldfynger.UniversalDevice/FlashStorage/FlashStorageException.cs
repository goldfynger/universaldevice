﻿using System;
using System.Diagnostics;

namespace Goldfynger.UniversalDevice.FlashStorage
{
    /// <summary>
    /// Flash storage interface exception.
    /// </summary>
    [DebuggerDisplay("DeviceError = {DeviceError} FlashStorageError = {FlashStorageError}")]
    public sealed class FlashStorageException : UniversalDeviceException
    {
        internal FlashStorageException(Message errorResponse, FlashStorageErrors flashStorageError) : base(errorResponse)
        {
            FlashStorageError = flashStorageError;
        }

        internal FlashStorageException(Message errorResponse, FlashStorageErrors flashStorageError, string message) : base(errorResponse, message)
        {
            FlashStorageError = flashStorageError;
        }

        internal FlashStorageException(Message errorResponse, FlashStorageErrors flashStorageError, string message, Exception innerException) : base(errorResponse, message, innerException)
        {
            FlashStorageError = flashStorageError;
        }


        /// <summary>
        /// Flash storage error that caused exception.
        /// </summary>
        public FlashStorageErrors FlashStorageError { get; }
    }
}
