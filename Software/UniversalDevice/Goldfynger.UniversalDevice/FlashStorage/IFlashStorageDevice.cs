﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Goldfynger.UniversalDevice.FlashStorage
{
    /// <summary>
    /// Interface to communicate with universal device flash storage.
    /// </summary>
    public interface IFlashStorageDevice : IDeviceInterface
    {
        /// <summary>
        /// Open flash storage in async mode.
        /// </summary>
        /// <returns><see cref="Task"/>.</returns>
        Task OpenAsync();
        /// <summary>
        /// Close flash storage in async mode.
        /// </summary>
        /// <returns><see cref="Task"/>.</returns>
        Task CloseAsync();

        /// <summary>
        /// Gets count of existing flash storage records in async mode.
        /// </summary>
        /// <returns><see cref="Task{TResult}"/> of <see cref="byte"/>.</returns>
        Task<byte> GetRecordsCountAsync();
        /// <summary>
        /// Gets existing records names that stored in flash storage in async mode.
        /// </summary>
        /// <returns><see cref="Task{TResult}"/> of <see cref="ReadOnlyCollection{T}"/> of <see cref="FlashStorageRecordName"/>.</returns>
        Task<ReadOnlyCollection<FlashStorageRecordName>> GetRecordsNamesAsync();
        /// <summary>
        /// Gets size of record in async mode.
        /// </summary>
        /// <param name="name"><see cref="FlashStorageRecordName"/>.</param>
        /// <returns><see cref="Task{TResult}"/> of <see cref="byte"/>.</returns>
        Task<byte> GetRecordSizeAsync(FlashStorageRecordName name);

        /// <summary>
        /// Read record in async mode.
        /// </summary>
        /// <param name="name"><see cref="FlashStorageRecordName"/>.</param>
        /// <returns><see cref="Task{TResult}"/> of <see cref="ReadOnlyCollection{T}"/> of <see cref="byte"/>.</returns>
        Task<ReadOnlyCollection<byte>> ReadRecordAsync(FlashStorageRecordName name);
        /// <summary>
        /// Write record in async mode.
        /// </summary>
        /// <param name="name"><see cref="FlashStorageRecordName"/>.</param>
        /// <param name="content"><see cref="ReadOnlyCollection{T}"/> of <see cref="byte"/>.</param>
        /// <returns><see cref="Task"/></returns>
        Task WriteRecordAsync(FlashStorageRecordName name, ReadOnlyCollection<byte> content);
        /// <summary>
        /// Remove record in async mode.
        /// </summary>
        /// <param name="name"><see cref="FlashStorageRecordName"/>.</param>
        /// <returns><see cref="Task"/>.</returns>
        Task RemoveRecordAsync(FlashStorageRecordName name);
    }
}
