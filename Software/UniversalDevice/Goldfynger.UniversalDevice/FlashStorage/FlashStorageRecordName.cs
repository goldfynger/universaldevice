﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Goldfynger.UniversalDevice.FlashStorage
{
    /// <summary>
    /// Representation of flash storage record name.
    /// </summary>
    [DebuggerDisplay("Name = {Name}")]
    public sealed class FlashStorageRecordName
    {
        /// <summary>
        /// Size of record name.
        /// </summary>
        public const int Size = 8;

        /// <summary>
        /// Creates new <see cref="FlashStorageRecordName"/> using <see cref="string"/> name.
        /// </summary>
        /// <param name="name"><see cref="string"/> name.</param>
        public FlashStorageRecordName(string name)
        {
            if (name.Length == 0 || name.Length > Size)
            {
                throw new ArgumentOutOfRangeException(nameof(name));
            }

            /* First char must not be '\0'. */
            if (name[0] == '\0')
            {
                throw new ArgumentOutOfRangeException(nameof(name));
            }

            /* Only printable ASCII characters allowed without Space and Delete; '\0' allowed for non-first chars. */
            if (name.All(c => !(c >= '!' && c <= '~') && c != '\0'))
            {
                throw new ArgumentOutOfRangeException(nameof(name));
            }

            var array = Encoding.ASCII.GetBytes(name);

            Name = name;
            //Raw = new FlashStorageDevice.RawRecordName { Array = array };
        }

        //internal FlashStorageRecordName(FlashStorageDevice.RawRecordName raw)
        //{
        //    if (raw.Array == null)
        //    {
        //        throw new ArgumentNullException(nameof(raw));
        //    }

        //    if (raw.Array[0] == 0x00)
        //    {
        //        throw new ArgumentOutOfRangeException(nameof(raw));
        //    }

        //    /* Only printable ASCII characters allowed without Space and Delete; '\0' allowed too. */
        //    if (raw.Array.All(b => !(b >= 0x21 && b <= 0x7E) && b != 0x00))
        //    {
        //        throw new ArgumentOutOfRangeException(nameof(raw));
        //    }

        //    Name = Encoding.ASCII.GetString(raw.Array);
        //    Raw = raw;
        //}


        /// <summary>
        /// Record name.
        /// </summary>
        public string Name { get; }

        //internal FlashStorageDevice.RawRecordName Raw { get; }


        /// <summary>
        /// Retirns record name.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => Name;
    }
}
