﻿namespace Goldfynger.UniversalDevice.FlashStorage
{
    /// <summary>
    /// Flash storage interface errors.
    /// </summary>
    public enum FlashStorageErrors : byte
    {
        /// <summary>No error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_OK = 0x00,

        /// <summary>Function parameter has invalid value.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_INVALID_PARAMETER,
        /// <summary>Flash storage configuration is invalid.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_INVALID_CONFIGURATION,
        /// <summary>Flash storage invalid init status.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_INVALID_INIT_STATUS,

        /// <summary>Flash storage is empty.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_EMPTY,
        /// <summary>Requested record not found.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_RECORD_NOT_FOUND,
        /// <summary>Not enough space to store new record.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_NOT_ENOUGH_SPACE,
        /// <summary>Not enough IDs to store new record.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_NOT_ENOUGH_ID,

        /// <summary>HAL function not implemented.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_HAL_NOT_IMPLEMENTED,
        /// <summary>HAL write error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_HAL_WRITE,
        /// <summary>HAL CRC32 compute error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_HAL_CRC32,
        /// <summary>HAL erase error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        FLASH_STORAGE_ERR_HAL_ERASE,

        /// <summary>Unknown error. Schould not be used directly.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        __FLASH_STORAGE_ERR_UNKNOWN,
    }
}
