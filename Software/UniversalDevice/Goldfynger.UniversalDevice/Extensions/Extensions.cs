﻿using System;
using System.Collections.Generic;

namespace Goldfynger.UniversalDevice
{
    internal static class Extensions
    {
        public static byte GetFirstByte(this ushort value)
        {
            return (byte)(value & 0xFF);
        }

        public static byte GetSecondByte(this ushort value)
        {
            return (byte)(value >> 8);
        }

        public static byte GetFirstByte(this DeviceErrors cmd)
        {
            return ((ushort)cmd).GetFirstByte();
        }

        public static byte GetSecondByte(this DeviceErrors cmd)
        {
            return ((ushort)cmd).GetSecondByte();
        }

        public static ushort GetUshort(this IList<byte> list, int idx)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            if (list.Count < 2)
            {
                throw new ArgumentOutOfRangeException(nameof(list));
            }

            if (idx < 0 || idx > list.Count - 2)
            {
                throw new ArgumentOutOfRangeException(nameof(idx));
            }

            return (ushort)((list[idx + 1] << 8) + list[idx]);
        }

        public static void AddUshort(this IList<byte> list, ushort value)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            list.Add(value.GetFirstByte());
            list.Add(value.GetSecondByte());
        }

        public static DeviceErrors GetCommand(this IList<byte> list, int idx)
        {
            return (DeviceErrors)list.GetUshort(idx);
        }

        public static void AddCommand(this IList<byte> list, DeviceErrors cmd)
        {
            list.AddUshort((ushort)cmd);
        }

        public static int IncreaseToMultipleOfFour(this int value)
        {
            var remainder = value % 4;
            if (remainder != 0)
            {
                value += 4 - remainder;
            }
            return value;
        }

        public static int IncreaseToMultipleOfFour(this ushort value) => IncreaseToMultipleOfFour(value);
    }
}
