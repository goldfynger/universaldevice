﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Goldfynger.UniversalDevice
{
    internal class BitField
    {
        private readonly byte[] _raw;

        public BitField(IEnumerable<byte> raw)
        {
            if (raw == null)
            {
                throw new ArgumentNullException(nameof(raw));
            }

            if (!raw.Any())
            {
                throw new ArgumentOutOfRangeException(nameof(raw));
            }

            _raw = raw.ToArray();

            RawCollection = new ReadOnlyCollection<byte>(_raw);

            BitCount = _raw.Length * 8;
        }


        public bool this[int index]
        {
            get
            {
                if (index < 0 || index > BitCount - 1)
                {
                    throw new ArgumentOutOfRangeException(nameof(index));
                }

                var rawByte = _raw[index >> 3];

                return (rawByte & (index & 3)) != 0;
            }
        }

        public ReadOnlyCollection<byte> RawCollection { get; }

        public int BitCount { get; }
    }

    internal sealed class SupportedInterfaces : BitField
    {
        public SupportedInterfaces(IEnumerable<byte> raw) : base(raw)
        {
            if (BitCount != 256)
            {
                throw new ArgumentOutOfRangeException(nameof(raw));
            }
        }


        public bool HasSupport(DeviceInterfaces iface)
        {
            return this[(int)iface];
        }
    }
}
