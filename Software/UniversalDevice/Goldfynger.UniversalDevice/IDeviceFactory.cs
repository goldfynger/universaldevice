﻿
using Goldfynger.UniversalDevice.Connector;

namespace Goldfynger.UniversalDevice
{
    /// <summary>
    /// Universal device factory.
    /// </summary>
    public interface IDeviceFactory
    {
        /// <summary>
        /// Creates new device using connector.
        /// </summary>
        /// <param name="connector">Connector.</param>
        /// <returns>Universal device.</returns>
        IDevice Create(IDeviceConnector connector);
    }
}
