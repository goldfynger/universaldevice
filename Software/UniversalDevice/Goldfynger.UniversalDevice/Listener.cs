﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Goldfynger.UniversalDevice
{
    /// <summary>
    /// Represents object for data listening. For external code it is look like just async read. Needed to send each readed message to many async listeners with exceptions support.
    /// </summary>
    /// <typeparam name="T">Listener data type.</typeparam>
    public abstract class Listener<T> : IDisposable
    {
        private readonly object _wrapStateLocker = new();


        private Listener()
        {
            IsWrapped = false;
            CanBeWrapped = true;
        }


        /// <summary>
        /// Can be wrapped before first read operation.
        /// </summary>
        internal bool CanBeWrapped { get; private set; }

        private protected abstract Exception? Exception { get; }

        /// <summary>
        /// Gets status of listener.
        /// </summary>
        public abstract bool IsCorrupted { get; }

        internal bool IsWrapped { get; private set; }


        static internal Listener<T> Create(Func<T, bool>? validator = null)
        {
            return new Worker(validator);
        }

        /// <summary>
        /// Reads data in async mode.
        /// </summary>
        /// <param name="cancellationToken"><see cref="CancellationToken"/></param>
        /// <returns><see cref="Task{T}"/>.</returns>
        public async Task<T> ReadAsync(CancellationToken cancellationToken = default)
        {
            lock (_wrapStateLocker)
            {
                if (IsWrapped)
                {
                    throw new InvalidOperationException("Listener was wrapped and direct read is not available.");
                }

                CanBeWrapped = false;
            }

            return await ReadAsyncPrivateProtected(Timeout.InfiniteTimeSpan, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Reads data in async mode.
        /// </summary>
        /// <param name="timeout"><see cref="TimeSpan"/></param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/></param>
        /// <returns><see cref="Task{T}"/>.</returns>
        public async Task<T> ReadAsync(TimeSpan timeout, CancellationToken cancellationToken = default)
        {
            lock (_wrapStateLocker)
            {
                if (IsWrapped)
                {
                    throw new InvalidOperationException("Listener was wrapped and direct read is not available.");
                }

                CanBeWrapped = false;
            }

            return await ReadAsyncPrivateProtected(timeout, cancellationToken).ConfigureAwait(false);
        }

        private protected abstract Task<T> ReadAsyncPrivateProtected(TimeSpan timeout, CancellationToken cancellationToken = default);

        internal abstract void SetException(Exception ex);

        private void ThrowIfExceptionSet()
        {
            if (Exception != null)
            {
                throw Exception;
            }
        }

        internal Listener<TNew> Wrap<TNew>(Func<T, TNew> converter)
        {
            ThrowIfDisposed();
            ThrowIfExceptionSet();

            lock (_wrapStateLocker)
            {
                if (IsWrapped)
                {
                    throw new InvalidOperationException("Listener already wrapped.");
                }

                if (!CanBeWrapped)
                {
                    throw new InvalidOperationException("Listener can not be wrapped.");
                }

                IsWrapped = true;
                CanBeWrapped = false;
            }

            return new Wrapper<TNew>(this, converter);
        }

        internal abstract void Write(T data);


        private void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(Listener<T>));
            }
        }

        private bool _disposed;

        /// <summary>
        /// Dispose all internal disposable resources.
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~Listener()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        /// <summary>
        /// Dispose all internal disposable resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }


        private sealed class Worker : Listener<T>
        {
            private Exception? _listenerException;
            private readonly object _listenerExceptionLocker = new();

            private readonly ConcurrentQueue<T> _queue = new();

            private readonly SemaphoreSlim _readMutex = new(0);

            private readonly Func<T, bool>? _validator;
                        

            internal Worker(Func<T, bool>? validator)
            {
                _validator = validator;
            }


            private protected override Exception? Exception
            {
                get
                {
                    lock (_listenerExceptionLocker)
                    {
                        return _listenerException;
                    }
                }
            }

            public override bool IsCorrupted => Exception != null;


            private protected override async Task<T> ReadAsyncPrivateProtected(TimeSpan timeout, CancellationToken cancellationToken = default)
            {
                ThrowIfDisposed();

                cancellationToken.ThrowIfCancellationRequested();
                ThrowIfExceptionSet();

                using var readTokenSource = cancellationToken.CanBeCanceled ? CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, default) : new CancellationTokenSource();

                var readTask = Task.Run(async () =>
                {
                    if (!await _readMutex.WaitAsync(timeout, readTokenSource.Token).ConfigureAwait(false))
                    {
                        readTokenSource.Token.ThrowIfCancellationRequested();
                        ThrowIfExceptionSet();

                        throw new TimeoutException();
                    }

                    if (!_queue.TryDequeue(out T? data))
                    {
                        throw new ApplicationException("Mutex was taken but cannot dequeue data.");
                    }

                    /* Return data if dequeued even token is cancelled to prevent data loss. */

                    return data;
                }, readTokenSource.Token);

                while (true)
                {
                    using var timeoutTokenSource = new CancellationTokenSource();

                    var timeoutTask = await Task.WhenAny(readTask, Task.Delay(250, timeoutTokenSource.Token)).ConfigureAwait(false);

                    if (timeoutTask == readTask)
                    {
                        timeoutTokenSource.Cancel();

                        return await readTask.ConfigureAwait(false);
                    }
                    else if (Exception != null)
                    {
                        readTokenSource.Cancel();

                        throw Exception;
                    }
                }
            }

            internal override void SetException(Exception ex)
            {
                lock (_listenerExceptionLocker)
                {
                    _listenerException = _listenerException != null ? new AggregateException(_listenerException, ex) : ex;
                }
            }

            internal override void Write(T data)
            {
                ThrowIfDisposed();
                ThrowIfExceptionSet();

                if (_validator == null || _validator(data))
                {
                    _queue.Enqueue(data);
                    _readMutex.Release();
                }
            }


            protected override void Dispose(bool disposing)
            {
                if (!_disposed)
                {
                    if (disposing)
                    {
                        // TODO: dispose managed state (managed objects)
                        _readMutex?.Dispose();
                    }

                    // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                    // TODO: set large fields to null
                    _disposed = true;
                }

                base.Dispose(disposing);
            }
        }

        private sealed class Wrapper<TNew> : Listener<TNew>
        {
            private readonly Listener<T> _listenerToWrap;

            private readonly Func<T, TNew> _converter;
                        

            internal Wrapper(Listener<T> listenerToWrap, Func<T, TNew> converter)
            {
                _listenerToWrap = listenerToWrap;
                _converter = converter;
            }


            private protected override Exception? Exception => _listenerToWrap.Exception;

            public override bool IsCorrupted => Exception != null;


            private protected override async Task<TNew> ReadAsyncPrivateProtected(TimeSpan timeout, CancellationToken cancellationToken = default)
            {
                return _converter(await _listenerToWrap.ReadAsyncPrivateProtected(timeout, cancellationToken).ConfigureAwait(false));
            }

            internal override void SetException(Exception ex)
            {
                throw new NotSupportedException("Set exception to listener wrapper is not supported.");
            }

            internal override void Write(TNew data)
            {
                throw new NotSupportedException("Write to listener wrapper is not supported.");
            }
        }
    }
}
