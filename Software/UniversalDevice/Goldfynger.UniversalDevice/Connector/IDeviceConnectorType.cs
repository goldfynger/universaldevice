﻿using System;

namespace Goldfynger.UniversalDevice.Connector
{
    /// <summary>
    /// Universal device connector type.
    /// </summary>
    public interface IDeviceConnectorType : IEquatable<IDeviceConnectorType>
    {
        /// <summary>
        /// <see cref="string"/> representation of <see cref="IDeviceConnector"/> type.
        /// </summary>
        string Type { get; }
    }
}
