﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;

using Goldfynger.Usb.CustomHid.Win32;

namespace Goldfynger.UniversalDevice.Connector
{
    /// <summary>
    /// Universal device connector factory.
    /// </summary>
    public sealed class DeviceConnectorFactory : IDeviceConnectorFactory
    {
        /// <summary>
        /// Creates new connector factory.
        /// </summary>
        public DeviceConnectorFactory()
        {
            var list = new List<IDeviceConnectorType>
            {
                SerialConnector.ConnectorType,
            };

            if (CustomHidConnector.IsAccessible)
            {
                list.Add(CustomHidConnector.ConnectorType);
            }

            AvailableTypes = list.AsReadOnly();
        }


        /// <summary>
        /// Gets available connector types.
        /// </summary>
        public ReadOnlyCollection<IDeviceConnectorType> AvailableTypes { get; }


        /// <summary>
        /// Gets available identifiers for specified connector type.
        /// </summary>
        /// <param name="type">Connector type.</param>
        /// <returns>List of available identifiers for specified connector type.</returns>
        public ReadOnlyCollection<IDeviceConnectorIdentifier> GetAvailableIdentifiers(IDeviceConnectorType type)
        {
            switch (type.Type)
            {
                case nameof(SerialConnector):
                    {
                        return SerialPort.GetPortNames().
                            Select(name => new SerialConnectorIdentifier(name) as IDeviceConnectorIdentifier).ToList().AsReadOnly();
                    }

                case nameof(CustomHidConnector):
                    {
                        return DevicePath.FindDevicePaths(CustomHidConnector.TargetVid, CustomHidConnector.TargetPid).
                            Select(path => new CustomHidConnectorIdentifier(path) as IDeviceConnectorIdentifier).ToList().AsReadOnly();
                    }

                default:
                    throw new ArgumentException($"Value of {nameof(type)} must be one of available connector types.");
            }
        }

        /// <summary>
        /// Creates new <see cref="IDeviceConnector"/> using connector type and identifier.
        /// </summary>
        /// <param name="type">Connector type.</param>
        /// <param name="identifier">Connector identifier.</param>
        /// <returns>Universal device connector.</returns>
        public IDeviceConnector Create(IDeviceConnectorType type, IDeviceConnectorIdentifier identifier)
        {
            switch (type.Type)
            {
                case nameof(SerialConnector):
                    {
                        return new SerialConnector((identifier as SerialConnectorIdentifier)!.PortName, identifier);
                    }

                case nameof(CustomHidConnector):
                    {
                        return new CustomHidConnector((identifier as CustomHidConnectorIdentifier)!.DevicePath, identifier);
                    }

                default:
                    throw new ArgumentException($"Value of {nameof(type)} must be one of available connector types.");
            }
        }


        [DebuggerDisplay("SerialConnectorIdentifier: {Identifier}")]
        private sealed record SerialConnectorIdentifier : IDeviceConnectorIdentifier, IEquatable<SerialConnectorIdentifier>
        {
            private static readonly ReadOnlyDictionary<string, string> __emptyDictionary = new(new Dictionary<string, string>());


            internal SerialConnectorIdentifier(string portName)
            {
                Identifier = portName;
                PortName = portName;
                Info = new ReadOnlyDictionary<string, string>(new Dictionary<string, string>
                {
                    { "PortName", portName },
                });
            }


            public string Identifier { get; }

            internal string PortName { get; }

            public ReadOnlyDictionary<string, string> Info { get; } = __emptyDictionary;


            public bool Equals(IDeviceConnectorIdentifier? other) => other is SerialConnectorIdentifier identifier && identifier.PortName == PortName;

            public override string ToString() => PortName;
        }

        [DebuggerDisplay("CustomHidConnectorIdentifier: {Identifier}")]
        private sealed record CustomHidConnectorIdentifier : IDeviceConnectorIdentifier, IEquatable<CustomHidConnectorIdentifier>
        {
            internal CustomHidConnectorIdentifier(DevicePath devicePath)
            {
                Identifier = devicePath.Path;
                DevicePath = devicePath;                
                Info = new ReadOnlyDictionary<string, string>(new Dictionary<string, string>
                {
                    { nameof(devicePath.Path), devicePath.Path },
                    { nameof(devicePath.Vid), devicePath.Vid.ToString() },
                    { nameof(devicePath.Pid), devicePath.Pid.ToString() }
                });
            }


            public string Identifier { get; }

            internal DevicePath DevicePath { get; }

            public ReadOnlyDictionary<string, string> Info { get; }            


            public bool Equals(IDeviceConnectorIdentifier? other) => other is CustomHidConnectorIdentifier identifier && identifier.DevicePath == DevicePath;

            public override string ToString() => DevicePath.ToString();
        }
    }
}
