﻿using System;
using System.Collections.ObjectModel;

namespace Goldfynger.UniversalDevice.Connector
{
    /// <summary>
    /// Universal device connector identifier.
    /// </summary>
    public interface IDeviceConnectorIdentifier : IEquatable<IDeviceConnectorIdentifier>
    {
        /// <summary>
        /// <see cref="string"/> represenatation of <see cref="IDeviceConnector"/> identifier.
        /// </summary>
        string Identifier { get; }

        /// <summary>
        /// Connector information as Property:Value pairs.
        /// </summary>
        ReadOnlyDictionary<string, string> Info { get; }
    }
}
