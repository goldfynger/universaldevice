﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;

using Goldfynger.Utils.DataContainers;

namespace Goldfynger.UniversalDevice.Connector
{
    internal abstract class DeviceConnector : IDeviceConnector
    {
        internal abstract event EventHandler<DataEventArgs<Message>>? ReadCompleted;

        internal abstract event EventHandler<DataEventArgs<Exception>>? ReadError;

        public abstract event EventHandler? Corrupted;


        private protected DeviceConnector()
        {
        }


        public abstract IDeviceConnectorType Type { get; }

        public abstract IDeviceConnectorIdentifier Identifier { get; }

        public abstract ReadOnlyDictionary<string, string> Info { get; }

        public abstract bool IsCorrupted { get; }

        internal abstract ushort WriteMessageMaxSize { get; }
                

        internal async Task WriteAsync(Message message, CancellationToken cancellationToken = default)
        {
            await WriteAsyncPrivateProtected(message, Timeout.InfiniteTimeSpan, cancellationToken).ConfigureAwait(false);
        }

        internal async Task WriteAsync(Message message, TimeSpan timeout, CancellationToken cancellationToken = default)
        {
            await WriteAsyncPrivateProtected(message, timeout, cancellationToken).ConfigureAwait(false);
        }

        private protected abstract Task WriteAsyncPrivateProtected(Message message, TimeSpan timeout, CancellationToken cancellationToken);


        private bool _disposed;

        private protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~DeviceConnector()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
