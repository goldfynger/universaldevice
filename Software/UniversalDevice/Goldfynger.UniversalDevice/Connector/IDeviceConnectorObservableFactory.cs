﻿using System;
using System.Collections.ObjectModel;

namespace Goldfynger.UniversalDevice.Connector
{
    /// <summary>
    /// Universal device connector observable factory.
    /// </summary>
    public interface IDeviceConnectorObservableFactory
    {
        /// <summary>
        /// Gets available connector types.
        /// </summary>
        ReadOnlyCollection<IDeviceConnectorType> AvailableTypes { get; }


        /// <summary>
        /// Gets available identifiers for specified connector type.
        /// </summary>
        /// <param name="type">Connector type.</param>
        /// <returns>Observable list of available identifiers for specified connector type.</returns>
        ReadOnlyObservableCollection<IDeviceConnectorIdentifier> GetAvailableIdentifiers(IDeviceConnectorType type);

        /// <summary>
        /// Creates new <see cref="IDeviceConnector"/> using connector type and identifier.
        /// </summary>
        /// <param name="type">Connector type.</param>
        /// <param name="identifier">Connector identifier.</param>
        /// <returns>Universal device connector.</returns>
        IDeviceConnector Create(IDeviceConnectorType type, IDeviceConnectorIdentifier identifier);

        /// <summary>
        /// Call hooks to watch for devices lists changes. This function should be called from windows WndProc.
        /// </summary>
        /// <param name="msg">WndProc msg parameter.</param>
        /// <param name="wParam">WndProc wParam parameter.</param>
        void WndProc(int msg, IntPtr wParam);
    }
}
