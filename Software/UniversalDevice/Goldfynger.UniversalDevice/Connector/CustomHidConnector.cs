﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

using Goldfynger.Usb.CustomHid.Win32;
using Goldfynger.Utils.DataContainers;

namespace Goldfynger.UniversalDevice.Connector
{
    [DebuggerDisplay("CustomHidConnector: {ConnectorDescription}")]
    internal sealed class CustomHidConnector : DeviceConnector
    {
        internal static readonly DeviceConnectorType ConnectorType = new(nameof(CustomHidConnector));

        internal const ushort TargetVid = 1155;
        internal const ushort TargetPid = 22352;

        private readonly ConcurrentUsbHid _usbDevice;

        private readonly object _exceptionLocker = new();
        private Exception? _exception;


        internal override event EventHandler<DataEventArgs<Message>>? ReadCompleted;

        internal override event EventHandler<DataEventArgs<Exception>>? ReadError;

        public override event EventHandler? Corrupted;


        internal CustomHidConnector(DevicePath devicePath, IDeviceConnectorIdentifier identifier)
        {
            CheckAccessibility();

            Identifier = identifier;

            try
            {
                _usbDevice = new ConcurrentUsbHid(devicePath, GenericUsbHidReadModes.Single);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Can not open USB HID for specified device path.", ex);
            }


            _usbDevice.ReadExceptionOccurred += UsbDevice_ReadExceptionOccurred;
            _usbDevice.WriteExceptionOccurred += UsbDevice_WriteExceptionOccurred;


            Task.Run(async () =>
            {
                while (true)
                {
                    var inputReport = await _usbDevice.ReadAsync().ConfigureAwait(false);

                    var message = new Message(inputReport);

                    Debug.WriteLine($"{nameof(CustomHidConnector)}:{"ReadAsync"}:{message.Header.ToString("DebugRead")}.");

                    ReadCompleted?.Invoke(this, new DataEventArgs<Message>(message));
                }
            }).ContinueWith(task =>
            {
                try
                {
                    task.Wait();
                }
                catch (Exception ex)
                {
                    var readException = new InvalidOperationException($"{nameof(SerialConnector)}: data reading error.{Environment.NewLine}{ex.Message}", ex);

                    AddException(readException);

                    ReadError?.Invoke(this, new DataEventArgs<Exception>(readException));
                }
            },
            TaskContinuationOptions.OnlyOnFaulted);
        }


        private void UsbDevice_ReadExceptionOccurred(object? sender, ExchangeExceptionEventArgs e)
        {
            Debug.WriteLine($"{nameof(CustomHidConnector)}:{"ReadExceptionOccurred"}:{nameof(ExchangeExceptionEventArgs.Exception.Message)}: {e.Exception.Message}");
        }

        private void UsbDevice_WriteExceptionOccurred(object? sender, ExchangeExceptionEventArgs e)
        {
            Debug.WriteLine($"{nameof(CustomHidConnector)}:{"WriteExceptionOccurred"}:{nameof(ExchangeExceptionEventArgs.Exception.Message)}: {e.Exception.Message}");
        }        


        internal static bool IsAccessible { get; } = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        public override IDeviceConnectorType Type => ConnectorType;

        public override IDeviceConnectorIdentifier Identifier { get; }

        public override ReadOnlyDictionary<string, string> Info
        {
            get
            {
                CheckAccessibility();
                ThrowIfDisposed();
                ThrowIfException();

                var deviceInfo = _usbDevice.Info;

                return new ReadOnlyDictionary<string, string>(new Dictionary<string, string>
                {
                    { nameof(deviceInfo.Vid), deviceInfo.Vid.ToString() },
                    { nameof(deviceInfo.Pid), deviceInfo.Pid.ToString() },
                    { nameof(deviceInfo.ManufacturerDescription), deviceInfo.ManufacturerDescription },
                    { nameof(deviceInfo.ProductDescription), deviceInfo.ProductDescription },
                    { nameof(deviceInfo.SerialNumber), deviceInfo.SerialNumber },
                });
            }
        }

        public override bool IsCorrupted
        {
            get
            {
                lock (_exceptionLocker)
                {
                    return _exception != null;
                }
            }
        }

        internal override ushort WriteMessageMaxSize => (ushort)(_usbDevice.Info.OutputReportLength - 1);


        private void AddException(Exception ex)
        {
            bool wasCorrupted = false;

            lock (_exceptionLocker)
            {
                wasCorrupted = _exception != null;

                _exception = _exception == null ? ex : new AggregateException(_exception, ex);
            }

            if (!wasCorrupted)
            {
                Corrupted?.Invoke(this, EventArgs.Empty);
            }
        }

        private void ThrowIfException()
        {
            lock (_exceptionLocker)
            {
                if (_exception != null)
                {
                    throw new InvalidOperationException($"Connector state is corrupted.{Environment.NewLine}{_exception.Message}", _exception);
                }
            }
        }

        private protected override Task WriteAsyncPrivateProtected(Message message, TimeSpan timeout, CancellationToken cancellationToken)
        {
            var referenceDateTime = DateTime.UtcNow;

            CheckAccessibility();
            ThrowIfDisposed();
            ThrowIfException();

            Debug.WriteLine($"{nameof(CustomHidConnector)}:{"WriteAsync"}:{message.Header.ToString("DebugWrite")}.");

            if (message.Size > WriteMessageMaxSize)
            {
                throw new ArgumentOutOfRangeException(nameof(message), $"{nameof(Message.Size)} must be less or equal than {nameof(WriteMessageMaxSize)}.");
            }

            if (!(timeout == Timeout.InfiniteTimeSpan || timeout > TimeSpan.Zero))
            {
                throw new ArgumentOutOfRangeException(nameof(timeout), $"Timeout must be greater than {TimeSpan.Zero} or {Timeout.InfiniteTimeSpan}.");
            }

            cancellationToken.ThrowIfCancellationRequested();


            var referenceTimeSpan = DateTime.UtcNow - referenceDateTime;

            if (timeout != Timeout.InfiniteTimeSpan)
            {
                if (referenceTimeSpan >= timeout)
                {
                    throw new TimeoutException();
                }
                else
                {
                    timeout -= referenceTimeSpan;
                }
            }


            try
            {
                var outputReportData = new byte[WriteMessageMaxSize];
                message.Raw.CopyTo(outputReportData, 0);
                _usbDevice.Enqueue(new GenericUsbOutputReport(new ReadOnlyCollection<byte>(outputReportData)), timeout, cancellationToken);
            }
            catch (OperationCanceledException) { throw; }
            catch (TimeoutException) { throw; }
            catch (Exception ex)
            {
                var writeException = new InvalidOperationException($"{nameof(SerialConnector)}: data writing error.{Environment.NewLine}{ex.Message}", ex);

                AddException(writeException);

                throw writeException;
            }

            return Task.CompletedTask;
        }

        private static void CheckAccessibility()
        {
            if (!IsAccessible)
            {
                throw new ApplicationException("Connector not accessible.");
            }
        }


        private void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(CustomHidConnector));
            }
        }

        private bool _disposed;

        private protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _usbDevice.Dispose();
                    _usbDevice.ReadExceptionOccurred -= UsbDevice_ReadExceptionOccurred;
                    _usbDevice.WriteExceptionOccurred -= UsbDevice_WriteExceptionOccurred;
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }

            base.Dispose(disposing);
        }
    }
}
