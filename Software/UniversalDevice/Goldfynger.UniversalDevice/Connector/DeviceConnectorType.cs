﻿using System;
using System.Diagnostics;

namespace Goldfynger.UniversalDevice.Connector
{
    [DebuggerDisplay("DeviceConnectorType: {Type}")]
    internal sealed record DeviceConnectorType : IDeviceConnectorType, IEquatable<DeviceConnectorType>
    {
        internal DeviceConnectorType(string type)
        {
            Type = type;
        }


        public string Type { get; }


        public bool Equals(IDeviceConnectorType? other) => other is DeviceConnectorType type && type.Type == Type;
    }
}
