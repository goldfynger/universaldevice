﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.InteropServices;

using Goldfynger.Usb.Cdc.Win32;
using Goldfynger.Usb.CustomHid.Win32;

namespace Goldfynger.UniversalDevice.Connector
{
    /// <summary>
    /// Universal device connector observable factory. This factory only available in OS Windows.
    /// </summary>
    public sealed class DeviceConnectorObservableFactory : IDeviceConnectorObservableFactory, IDisposable
    {
        private Action<int, IntPtr>? _addSerialHook = null;
        private Action<int, IntPtr>? _addHidHook = null;

        private readonly Win32SerialWatcher _win32SerialWatcher;
        private readonly Win32HidWatcher _win32HidWatcher;

        private readonly List<Win32Hid> _win32Hids;

        private readonly ObservableCollection<IDeviceConnectorIdentifier> _serialIdentifiers;
        private readonly ObservableCollection<IDeviceConnectorIdentifier> _hidIdentifiers;

        private readonly ReadOnlyObservableCollection<IDeviceConnectorIdentifier> _serialReadOnlyIdentifiers;
        private readonly ReadOnlyObservableCollection<IDeviceConnectorIdentifier> _hidReadOnlyIdentifiers;
        

        /// <summary>
        /// Creates new connector observable factory.
        /// </summary>
        /// <param name="wndHandle">Windows handle.</param>
        public DeviceConnectorObservableFactory(IntPtr wndHandle)
        {
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                throw new PlatformNotSupportedException($"{nameof(DeviceConnectorObservableFactory)}: type available only on OS Windows.");
            }

            _win32SerialWatcher = new Win32SerialWatcher(wndHandle, addHook => _addSerialHook = addHook);
            _win32HidWatcher = new Win32HidWatcher(wndHandle, addHook => _addHidHook = addHook);

            _win32SerialWatcher.SerialPortArrived += Win32SerialWatcher_SerialPortArrived;
            _win32SerialWatcher.SerialPortRemoved += Win32SerialWatcher_SerialPortRemoved;

            _win32HidWatcher.DeviceArrived += Win32HidWatcher_DeviceArrived;
            _win32HidWatcher.DeviceRemoved += Win32HidWatcher_DeviceRemoved;

            _win32Hids = new List<Win32Hid>();

            foreach (var devicePath in DevicePath.FindDevicePaths(CustomHidConnector.TargetVid, CustomHidConnector.TargetPid))
            {
                if (TryOpenHid(devicePath, out Win32Hid? hid))
                {
                    _win32Hids.Add(hid);
                }
            }

            _serialIdentifiers = new ObservableCollection<IDeviceConnectorIdentifier>(SerialPortInfo.FindSerialPorts().Select(i => new SerialConnectorIdentifier(i)));
            _hidIdentifiers = new ObservableCollection<IDeviceConnectorIdentifier>(_win32Hids.Select(h => new CustomHidConnectorIdentifier(h.Info)));

            _serialReadOnlyIdentifiers = new ReadOnlyObservableCollection<IDeviceConnectorIdentifier>(_serialIdentifiers);
            _hidReadOnlyIdentifiers = new ReadOnlyObservableCollection<IDeviceConnectorIdentifier>(_hidIdentifiers);            

            var list = new List<IDeviceConnectorType>
            {
                SerialConnector.ConnectorType,
            };

            if (CustomHidConnector.IsAccessible)
            {
                list.Add(CustomHidConnector.ConnectorType);
            }

            AvailableTypes = list.AsReadOnly();
        }

        private void Win32SerialWatcher_SerialPortArrived(object? sender, EventArgs e) => UpdateSerialCollection();

        private void Win32SerialWatcher_SerialPortRemoved(object? sender, EventArgs e) => UpdateSerialCollection();

        private void Win32HidWatcher_DeviceArrived(object? sender, EventArgs e) => UpdateHidCollection();

        private void Win32HidWatcher_DeviceRemoved(object? sender, EventArgs e) => UpdateHidCollection();


        /// <summary>
        /// Gets available connector types.
        /// </summary>
        public ReadOnlyCollection<IDeviceConnectorType> AvailableTypes { get; }


        /// <summary>
        /// Gets available identifiers for specified connector type.
        /// </summary>
        /// <param name="type">Connector type.</param>
        /// <returns>Observable list of available identifiers for specified connector type.</returns>
        public ReadOnlyObservableCollection<IDeviceConnectorIdentifier> GetAvailableIdentifiers(IDeviceConnectorType type)
        {
            switch (type.Type)
            {
                case nameof(SerialConnector):
                    {
                        return _serialReadOnlyIdentifiers;
                    }

                case nameof(CustomHidConnector):
                    {
                        return _hidReadOnlyIdentifiers;
                    }

                default:
                    throw new ArgumentException($"Value of {nameof(type)} must be one of available connector types.");
            }
        }

        /// <summary>
        /// Creates new <see cref="IDeviceConnector"/> using connector type and identifier.
        /// </summary>
        /// <param name="type">Connector type.</param>
        /// <param name="identifier">Connector identifier.</param>
        /// <returns>Universal device connector.</returns>
        public IDeviceConnector Create(IDeviceConnectorType type, IDeviceConnectorIdentifier identifier)
        {
            switch (type.Type)
            {
                case nameof(SerialConnector):
                    {
                        return new SerialConnector((identifier as SerialConnectorIdentifier)!.SerialPortInfo.PortName, identifier);
                    }

                case nameof(CustomHidConnector):
                    {
                        return new CustomHidConnector((identifier as CustomHidConnectorIdentifier)!.HidInfo.DevicePath, identifier);
                    }

                default:
                    throw new ArgumentException($"Value of {nameof(type)} must be one of available connector types.");
            }
        }


        /// <summary>
        /// Call hooks to watch for devices lists changes. This function should be called from windows WndProc.
        /// </summary>
        /// <param name="msg">WndProc msg parameter.</param>
        /// <param name="wParam">WndProc wParam parameter.</param>
        public void WndProc(int msg, IntPtr wParam)
        {
            _addSerialHook?.Invoke(msg, wParam);
            _addHidHook?.Invoke(msg, wParam);
        }

        private bool TryOpenHid(DevicePath path, [NotNullWhen(true)] out Win32Hid? hid)
        {
            try
            {
                hid = new GenericUsbHid(path, GenericUsbHidReadModes.Single);
                return true;
            }
            catch (Exception firstEx)
            {
                Debug.WriteLine($"{nameof(DeviceConnectorObservableFactory)}: {nameof(TryOpenHid)}: HID open first try exception: {firstEx.Message}");

                /* Try again to resolve known problem when first try is unsuccessful. */

                try
                {
                    hid = new GenericUsbHid(path, GenericUsbHidReadModes.Single);
                    return true;
                }
                catch (Exception secondEx)
                {
                    Debug.WriteLine($"{nameof(DeviceConnectorObservableFactory)}: {nameof(TryOpenHid)}: HID open second try exception: {secondEx.Message}");
                }
            }

            hid = null;
            return false;
        }

        private void UpdateSerialCollection()
        {
            var actualPorts = SerialPortInfo.FindSerialPorts();

            var removeIdx = 0;

            while (true) /* Remove all unavailable identifiers. */
            {
                if (removeIdx >= _serialIdentifiers.Count) /* End of collection. */
                {
                    break;
                }

                if (actualPorts.IndexOf(((SerialConnectorIdentifier)_serialIdentifiers[removeIdx]).SerialPortInfo) < 0) /* Not found in actual collection - port removed. */
                {
                    _serialIdentifiers.RemoveAt(removeIdx);
                }
                else /* Else increment index. */
                {
                    removeIdx++;
                }
            }

            /* Here we have old collection and new actual collection that can have more items than old. */
            /* We can save order returned by FindSerialPorts and insert new ports in right places. */

            for (var addIdx = 0; addIdx < actualPorts.Count; addIdx++) /* Add. */
            {
                if (_serialIdentifiers.Select(i => ((SerialConnectorIdentifier)i).SerialPortInfo).ToList().IndexOf(actualPorts[addIdx]) < 0) /* Not found in exisiting collection - new port. */
                {
                    _serialIdentifiers.Insert(addIdx, new SerialConnectorIdentifier(actualPorts[addIdx]));
                }
            }
        }

        private void UpdateHidCollection()
        {
            var actualDevices = DevicePath.FindDevicePaths(CustomHidConnector.TargetVid, CustomHidConnector.TargetPid);

            var removeIdx = 0;

            while (true) /* Remove all unavailable identifiers. */
            {
                if (removeIdx >= _hidIdentifiers.Count) /* End of collection. */
                {
                    break;
                }

                if (actualDevices.IndexOf(((CustomHidConnectorIdentifier)_hidIdentifiers[removeIdx]).HidInfo.DevicePath) < 0) /* Not found in actual collection - device removed. */
                {
                    /* Opened HID contained in another collection and we scould dispose it. */

                    var hidIdentifier = (CustomHidConnectorIdentifier)_hidIdentifiers[removeIdx];
                    var hidIdx = _win32Hids.IndexOf(_win32Hids.Where(h => h.Info.DevicePath == hidIdentifier.HidInfo.DevicePath).First());
                    _win32Hids[hidIdx].Dispose();
                    _win32Hids.RemoveAt(hidIdx);
                    _hidIdentifiers.RemoveAt(removeIdx);
                }
                else /* Else increment index. */
                {
                    removeIdx++;
                }
            }

            /* Here we have old collection and new actual collection that can have more items than old. */
            /* We can save order returned by FindDevicePaths and insert new devices in right places. */

            for (var addIdx = 0; addIdx < actualDevices.Count; addIdx++) /* Add. */
            {
                if (_hidIdentifiers.Select(i => ((CustomHidConnectorIdentifier)i).HidInfo.DevicePath).ToList().IndexOf(actualDevices[addIdx]) < 0) /* Not found in exisiting collection - new device. */
                {
                    if (TryOpenHid(actualDevices[addIdx], out Win32Hid? hid))
                    {
                        _win32Hids.Add(hid);
                        _hidIdentifiers.Insert(addIdx, new CustomHidConnectorIdentifier(hid.Info));
                    }
                }
            }
        }


        private bool _disposed;

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _win32SerialWatcher?.Dispose();
                    _win32HidWatcher?.Dispose();
                    _win32Hids.ForEach(h => h.Dispose());
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~DeviceConnectorObservableFactory()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }


        [DebuggerDisplay("SerialConnectorIdentifier: {Identifier}")]
        private sealed record SerialConnectorIdentifier : IDeviceConnectorIdentifier, IEquatable<SerialConnectorIdentifier>
        {
            internal SerialConnectorIdentifier(SerialPortInfo serialPortInfo)
            {
                Identifier = serialPortInfo.PortName;
                SerialPortInfo = serialPortInfo;

                var dictionary = new Dictionary<string, string>
                {
                    { nameof(serialPortInfo.PortNumber), serialPortInfo.PortNumber.ToString() },
                    { nameof(serialPortInfo.PortName), serialPortInfo.PortName }
                };
                if (serialPortInfo.FriendlyName != null)
                {
                    dictionary.Add(nameof(serialPortInfo.FriendlyName), serialPortInfo.FriendlyName);
                }
                if (serialPortInfo.HardwareIds != null)
                {
                    for (var idx = 0; idx < serialPortInfo.HardwareIds.Count; idx++)
                    {
                        dictionary.Add($"{nameof(serialPortInfo.HardwareIds)} ({idx + 1})", serialPortInfo.HardwareIds[idx]);
                    }
                }
                if (serialPortInfo.Service != null)
                {
                    dictionary.Add(nameof(serialPortInfo.Service), serialPortInfo.Service);
                }
                if (serialPortInfo.Description != null)
                {
                    dictionary.Add(nameof(serialPortInfo.Description), serialPortInfo.Description);
                }
                if (serialPortInfo.Vid != null)
                {
                    dictionary.Add(nameof(serialPortInfo.Vid), serialPortInfo.Vid.Value.ToString());
                }
                if (serialPortInfo.Pid != null)
                {
                    dictionary.Add(nameof(serialPortInfo.Pid), serialPortInfo.Pid.Value.ToString());
                }

                Info = new ReadOnlyDictionary<string, string>(dictionary);
            }


            public string Identifier { get; }

            internal SerialPortInfo SerialPortInfo { get; }

            public ReadOnlyDictionary<string, string> Info { get; }


            public bool Equals(IDeviceConnectorIdentifier? other) => other is SerialConnectorIdentifier identifier && identifier.SerialPortInfo == SerialPortInfo;

            public override string ToString() => SerialPortInfo.ToString();
        }

        [DebuggerDisplay("CustomHidConnectorIdentifier: {Identifier}")]
        private sealed record CustomHidConnectorIdentifier : IDeviceConnectorIdentifier, IEquatable<CustomHidConnectorIdentifier>
        {
            internal CustomHidConnectorIdentifier(HidInfo hidInfo)
            {
                Identifier = hidInfo.DevicePath.Path;
                HidInfo = hidInfo;

                var dictionary = new Dictionary<string, string>
                {
                    { nameof(hidInfo.DevicePath.Path), hidInfo.DevicePath.Path },
                    { nameof(hidInfo.DevicePath.Vid), hidInfo.DevicePath.Vid.ToString() },
                    { nameof(hidInfo.DevicePath.Pid), hidInfo.DevicePath.Pid.ToString() }
                };
                if (hidInfo.ManufacturerDescription != null)
                {
                    dictionary.Add(nameof(hidInfo.ManufacturerDescription), hidInfo.ManufacturerDescription);
                }
                if (hidInfo.ProductDescription != null)
                {
                    dictionary.Add(nameof(hidInfo.ProductDescription), hidInfo.ProductDescription);
                }
                if (hidInfo.SerialNumber != null)
                {
                    dictionary.Add(nameof(hidInfo.SerialNumber), hidInfo.SerialNumber);
                }

                Info = new ReadOnlyDictionary<string, string>(dictionary);
            }


            public string Identifier { get; }

            internal HidInfo HidInfo { get; }

            public ReadOnlyDictionary<string, string> Info { get; }


            public bool Equals(IDeviceConnectorIdentifier? other) => other is CustomHidConnectorIdentifier identifier && identifier.HidInfo == HidInfo;

            public override string ToString() => HidInfo.ToString();
        }
    }
}
