﻿using System;
using System.Collections.ObjectModel;

namespace Goldfynger.UniversalDevice.Connector
{
    /// <summary>
    /// Universal device connector.
    /// </summary>
    public interface IDeviceConnector : IDisposable
    {
        /// <summary>
        /// Device connector is corrupted.
        /// </summary>
        event EventHandler? Corrupted;


        /// <summary>
        /// Gets connector type.
        /// </summary>
        IDeviceConnectorType Type { get; }

        /// <summary>
        /// Gets connector identifier.
        /// </summary>
        IDeviceConnectorIdentifier Identifier { get; }

        /// <summary>
        /// Connector information as Property:Value pairs.
        /// </summary>
        ReadOnlyDictionary<string, string> Info { get; }

        /// <summary>
        /// Gets status of connector.
        /// </summary>
        bool IsCorrupted { get; }
    }
}
