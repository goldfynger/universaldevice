﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Goldfynger.Utils.DataContainers;
using Goldfynger.Utils.Stm32.StreamToPacket;

namespace Goldfynger.UniversalDevice.Connector
{
    [DebuggerDisplay("SerialConnector: {ConnectorDescription}")]
    internal sealed class SerialConnector : DeviceConnector
    {
        internal static readonly DeviceConnectorType ConnectorType = new(nameof(SerialConnector));

        private static readonly TimeSpan __checkSerialPortTimeout = TimeSpan.FromMilliseconds(250);

        private readonly SerialPort _serialDevice;

        private readonly SemaphoreSlim _writeMutex = new(1);

        private readonly object _exceptionLocker = new();
        private Exception? _exception;


        internal override event EventHandler<DataEventArgs<Message>>? ReadCompleted;

        internal override event EventHandler<DataEventArgs<Exception>>? ReadError;

        public override event EventHandler? Corrupted;


        internal SerialConnector(string portName, IDeviceConnectorIdentifier identifier)
        {
            Identifier = identifier;

            try
            {
                _serialDevice = new SerialPort(portName);
                _serialDevice.Open();
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Can not open serial port for specified port name.", ex);
            }


            Task.Run(async () =>
            {
                using var readTokenSource = new CancellationTokenSource();
                var readToken = readTokenSource.Token;

                var readTask = Task.Run(async () =>
                {
                    while (true)
                    {
                        readToken.ThrowIfCancellationRequested();

                        var packet = await StreamToPacketMaker.MakePacketAsync(_serialDevice.BaseStream, readTokenSource.Token);

                        readToken.ThrowIfCancellationRequested();

                        var message = new Message(packet);

                        Debug.WriteLine($"{nameof(SerialConnector)}:{"ReadAsync"}:{message.Header.ToString("DebugRead")}.");

                        ReadCompleted?.Invoke(this, new DataEventArgs<Message>(message));
                    }
                }, readToken);

                while (true)
                {
                    using var timeoutTokenSource = new CancellationTokenSource();

                    var timeoutTask = await Task.WhenAny(readTask, Task.Delay(__checkSerialPortTimeout, timeoutTokenSource.Token)).ConfigureAwait(false);

                    if (timeoutTask == readTask)
                    {
                        timeoutTokenSource.Cancel();

                        await readTask.ConfigureAwait(false);
                    }
                    else if (!_serialDevice.BaseStream.CanRead)
                    {
                        readTokenSource.Cancel();

                        throw new InvalidOperationException("Read stream is closed.");
                    }
                }

            }).ContinueWith(task =>
            {
                try
                {
                    task.Wait();
                }
                catch (Exception ex)
                {
                    var readException = new InvalidOperationException($"{nameof(SerialConnector)}: data reading error.{Environment.NewLine}{ex.Message}", ex);

                    AddException(readException);

                    ReadError?.Invoke(this, new DataEventArgs<Exception>(readException));
                }
            }, TaskContinuationOptions.OnlyOnFaulted);
        }


        public override IDeviceConnectorType Type => ConnectorType;

        public override IDeviceConnectorIdentifier Identifier { get; }

        public override ReadOnlyDictionary<string, string> Info
        {
            get
            {
                ThrowIfDisposed();
                ThrowIfException();

                return new ReadOnlyDictionary<string, string>(new Dictionary<string, string>());
            }
        }

        public override bool IsCorrupted
        {
            get
            {
                lock (_exceptionLocker)
                {
                    return _exception != null;
                }
            }
        }

        internal override ushort WriteMessageMaxSize => Message.MaxSize;


        private void AddException(Exception ex)
        {
            bool wasCorrupted = false;

            lock (_exceptionLocker)
            {
                wasCorrupted = _exception != null;

                _exception = _exception == null ? ex : new AggregateException(_exception, ex);
            }

            if (!wasCorrupted)
            {
                Corrupted?.Invoke(this, EventArgs.Empty);
            }
        }

        private void ThrowIfException()
        {
            lock (_exceptionLocker)
            {
                if (_exception != null)
                {
                    throw new InvalidOperationException($"Connector state is corrupted.{Environment.NewLine}{_exception.Message}", _exception);
                }
            }
        }

        private protected override async Task WriteAsyncPrivateProtected(Message message, TimeSpan timeout, CancellationToken cancellationToken)
        {
            var referenceDateTime = DateTime.UtcNow;

            ThrowIfDisposed();
            ThrowIfException();

            Debug.WriteLine($"{nameof(SerialConnector)}:{"WriteAsync"}:{message.Header.ToString("DebugWrite")}.");

            if (message.Size > WriteMessageMaxSize)
            {
                throw new ArgumentOutOfRangeException(nameof(message), $"{nameof(Message.Size)} must be less or equal than {nameof(WriteMessageMaxSize)}.");
            }

            if (timeout.TotalMilliseconds < -1)
            {
                throw new ArgumentOutOfRangeException(nameof(timeout), "Timeout must be infinite (-1 ms) or greater then 0.");
            }

            cancellationToken.ThrowIfCancellationRequested();


            var stream = StreamToPacketMaker.MakeStream(message.Raw).ToArray().AsMemory();

            if (!await _writeMutex.WaitAsync(timeout, cancellationToken).ConfigureAwait(false))
            {
                cancellationToken.ThrowIfCancellationRequested();

                throw new TimeoutException();
            }

            cancellationToken.ThrowIfCancellationRequested();


            var referenceTimeSpan = DateTime.UtcNow - referenceDateTime;

            if (timeout != Timeout.InfiniteTimeSpan)
            {
                if (referenceTimeSpan >= timeout)
                {
                    throw new TimeoutException();
                }
                else
                {
                    timeout -= referenceTimeSpan;
                }
            }


            try
            {                                
                if (timeout != Timeout.InfiniteTimeSpan)
                {
                    using var writeTokenSource = cancellationToken.CanBeCanceled ? CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, default) : new CancellationTokenSource();

                    var writeTask = _serialDevice.BaseStream.WriteAsync(stream, writeTokenSource.Token).AsTask();

                    using var timeoutTokenSource = cancellationToken.CanBeCanceled ? CancellationTokenSource.CreateLinkedTokenSource(cancellationToken, default) : new CancellationTokenSource();

                    var timeoutTask = await Task.WhenAny(writeTask, Task.Delay(timeout, timeoutTokenSource.Token)).ConfigureAwait(false);

                    if (timeoutTask == writeTask)
                    {
                        timeoutTokenSource.Cancel();

                        try
                        {
                            await writeTask.ConfigureAwait(false);
                        }
                        catch (Exception ex)
                        {
                            var writeException = new InvalidOperationException($"{nameof(SerialConnector)}: data writing error.{Environment.NewLine}{ex.Message}", ex);

                            AddException(writeException);

                            throw writeException;
                        }
                    }
                    else
                    {
                        writeTokenSource.Cancel();

                        throw new TimeoutException();
                    }
                }
                else
                {
                    try
                    {
                        await _serialDevice.BaseStream.WriteAsync(stream, cancellationToken).ConfigureAwait(false);
                    }
                    catch (Exception ex)
                    {
                        var writeException = new InvalidOperationException($"{nameof(SerialConnector)}: data writing error.{Environment.NewLine}{ex.Message}", ex);

                        AddException(writeException);

                        throw writeException;
                    }
                }
            }
            finally
            {
                _writeMutex.Release();
            }

            cancellationToken.ThrowIfCancellationRequested();
        }


        private void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(SerialConnector));
            }
        }

        private bool _disposed;

        private protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _serialDevice?.Dispose();
                    _writeMutex?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }

            base.Dispose(disposing);
        }
    }
}
