﻿using System.Collections.ObjectModel;

namespace Goldfynger.UniversalDevice.Connector
{
    /// <summary>
    /// Universal device connector factory.
    /// </summary>
    public interface IDeviceConnectorFactory
    {
        /// <summary>
        /// Gets available connector types.
        /// </summary>
        ReadOnlyCollection<IDeviceConnectorType> AvailableTypes { get; }


        /// <summary>
        /// Gets available identifiers for specified connector type.
        /// </summary>
        /// <param name="type">Connector type.</param>
        /// <returns>List of available identifiers for specified connector type.</returns>
        ReadOnlyCollection<IDeviceConnectorIdentifier> GetAvailableIdentifiers(IDeviceConnectorType type);

        /// <summary>
        /// Creates new <see cref="IDeviceConnector"/> using connector type and identifier.
        /// </summary>
        /// <param name="type">Connector type.</param>
        /// <param name="identifier">Connector identifier.</param>
        /// <returns>Universal device connector.</returns>
        IDeviceConnector Create(IDeviceConnectorType type, IDeviceConnectorIdentifier identifier);
    }
}
