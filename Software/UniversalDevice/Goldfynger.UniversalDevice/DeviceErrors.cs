﻿namespace Goldfynger.UniversalDevice
{
    /// <summary>Universal device errors.</summary>
    public enum DeviceErrors : byte
    {
        /// <summary>No error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_OK = 0x00,

        /// <summary>Invalid request parameter.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_INVALID_PARAMETER,

        /// <summary>Request message has initiative message ID.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_MESSAGE_ID,
        /// <summary>Invalid message size or body offset or body size.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_MESSAGE_SIZE_OR_OFFSET,
        /// <summary>Fragmentation error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_MESSAGE_FRAGMENTATION,

        /// <summary>Interface not initialised.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_INTERFACE_NOT_INITIALISED,
        /// <summary>Interface initialisation error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_INTERFACE_INITIALISATION,
        /// <summary>Interface unknown or invalid.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_INTERFACE_UNKNOWN_OR_INVALID,
        /// <summary>Interface not available in current device.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_INTERFACE_NOT_AVAILABLE,
        /// <summary>Interface busy and cannot process message.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_INTERFACE_BUSY,

        /// <summary>Internal interface error must be specified in interface error response.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC,

        /// <summary>Port unknown or invalid.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_PORT_UNKNOWN_OR_INVALID,
        /// <summary>Port not available in current device.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_PORT_NOT_AVAILABLE,

        /// <summary>Command unknown or invalid.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_COMMAND_UNKNOWN_OR_INVALID,
        /// <summary>Command cannot be executed in current context.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_COMMAND_INVALID_IN_CURRENT_CONTEXT,

        /// <summary>Resource not acquired and cannot be used or released.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED,
        /// <summary>Resource already acquired and cannot be aquired again.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_ALREADY_ACQUIRED,
        /// <summary>Resource busy or inaccesible and cannot be aquired.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_CANNOT_BE_ACQUIRED,
        /// <summary>Resource release error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_CANNOT_BE_RELEASED,

        /// <summary>Not enough memory for allocation.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED,
        /// <summary>OS kernel object creation error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UINVERSAL_DEVICE_ERR_OS_OBJECT_CANNOT_BE_CREATED,
        /// <summary>OS function executed with unexpected result.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_OS_FUNCTION_FAIL,

        /// <summary>External library specific error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_EXTERNAL_LIBRARY_SPECIFIC,

        /// <summary>Hardware abstraction level error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_HAL,

        /// <summary>Feature not supported or not implemented yet.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED,

        /// <summary>Timeout error.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        UNIVERSAL_DEVICE_ERR_TIMEOUT,

        /// <summary>Unknown error. Schould not be used directly.</summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707")]
        __UNIVERSAL_DEVICE_ERR_UNKNOWN,
    }
}
