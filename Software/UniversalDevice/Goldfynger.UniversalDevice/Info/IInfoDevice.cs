﻿using System.Threading.Tasks;

using Goldfynger.Utils.Stm32.DeviceInfo;

namespace Goldfynger.UniversalDevice.Info
{
    /// <summary>
    /// Interface to communicate with universal device information.
    /// </summary>
    public interface IInfoDevice : IDeviceInterface
    {
        /// <summary>
        /// Gets information about connected device in async mode.
        /// </summary>
        /// <returns><see cref="Task{TResult}"/> of <see cref="Information"/></returns>
        Task<Information> GetInformationAsync();
        /// <summary>
        /// Gets UID of connected device in async mode.
        /// </summary>
        /// <returns><see cref="Task{TResult}"/> of <see cref="Uid"/></returns>
        Task<Uid> GetUidAsync();
    }
}
