﻿using System;
using System.Threading.Tasks;

using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Stm32.DeviceInfo;

namespace Goldfynger.UniversalDevice.Info
{
    internal sealed class InfoDevice : IInfoDevice
    {
        private static readonly TimeSpan __timeout = TimeSpan.FromSeconds(2);


        private readonly Device _device;


        static InfoDevice() => Message.MessageHeader.AddCommandsToResolver<Commands>(DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_BASE_INFO);

        internal InfoDevice(Device device) => _device = device;


        public bool IsCorrupted => _device.IsCorrupted;


        public async Task<Information> GetInformationAsync() => new InformationResponse(await Exchange(new InformationRequest(_device.NextMessageId())).ConfigureAwait(false)).Information;

        public async Task<Uid> GetUidAsync() => new UidResponse(await Exchange(new UidRequest(_device.NextMessageId())).ConfigureAwait(false)).Uid;

        private async Task<Message> Exchange(Message request)
        {
            using var listener = _device.CreateListener(m =>
                m.Header.MessageId == request.Header.MessageId &&
                m.Header.InterfaceBase == request.Header.InterfaceBase &&
                m.Header.InterfaceCommand == request.Header.InterfaceCommand);

            await _device.WriteAsync(request).ConfigureAwait(false);

            var response = await listener.ReadAsync(__timeout).ConfigureAwait(false);

            response.ThrowIfError();

            return response;
        }


        private enum Commands : byte
        {
            /// <summary>
            /// Request: <see cref="InformationRequest"/>
            /// Response: <see cref="InformationResponse"/>
            /// Error: <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_INFO_CMD_GET_INFORMATION = 0x20,
            /// <summary>
            /// Request: <see cref="UidRequest"/>
            /// Response: <see cref="UidResponse"/>
            /// Error: <see cref="Message"/>
            /// </summary>
            UNIVERSAL_DEVICE_INFO_CMD_GET_UID = 0x21,
        }


        private abstract class Request : Message
        {
            internal Request(ushort messageId, Commands command) : base(new MessageHeader(
                messageId, messageBodySize: 0,
                fragmentBodyOffset: 0, fragmentBodySize: 0,
                DeviceInterfaces.UNIVERSAL_DEVICE_INTERFACE_BASE_INFO, interfacePort: 0, (byte)command, DeviceErrors.UNIVERSAL_DEVICE_ERR_OK),
                EmptyList)
                => command.ThrowIfNotDefined();
        }

        private class InformationRequest : Request
        {
            internal InformationRequest(ushort messageId) : base(messageId, Commands.UNIVERSAL_DEVICE_INFO_CMD_GET_INFORMATION)
            {
            }
        }

        private class InformationResponse : Message
        {
            internal InformationResponse(Message message) : base(message)
            {
                if (message.Body.Count != Information.Size)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }

                Information = new Information(message.Body);
            }


            internal Information Information { get; }
        }

        private class UidRequest : Request
        {
            internal UidRequest(ushort messageId) : base(messageId, Commands.UNIVERSAL_DEVICE_INFO_CMD_GET_UID)
            {
            }
        }

        private class UidResponse : Message
        {
            internal UidResponse(Message message) : base(message)
            {
                if (message.Body.Count != Uid.Size)
                {
                    throw new ArgumentOutOfRangeException(nameof(message));
                }

                Uid = new Uid(message.Body);
            }


            internal Uid Uid { get; }
        }
    }
}
