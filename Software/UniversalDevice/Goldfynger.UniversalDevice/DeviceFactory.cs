﻿using System;

using Goldfynger.UniversalDevice.Connector;

namespace Goldfynger.UniversalDevice
{
    /// <summary>
    /// Universal device factory.
    /// </summary>
    public sealed class DeviceFactory : IDeviceFactory
    {
        /// <summary>
        /// Creates new device using connector.
        /// </summary>
        /// <param name="connector">Connector.</param>
        /// <returns>Universal device.</returns>
        public IDevice Create(IDeviceConnector connector) => new Device((connector as DeviceConnector)!);
    }
}
