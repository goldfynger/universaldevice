﻿using System.Diagnostics;
using System.Windows;

namespace Goldfynger.UniversalDevice.Viewer.Models
{
    [DebuggerDisplay("DeviceInterfaceModel: {UniqueIdentifier}")]
    public abstract class DeviceInterfaceModel : DependencyObject
    {
        protected readonly IDevice _device;


        protected DeviceInterfaceModel(IDevice device) => _device = device;


        public abstract string UniqueIdentifier { get; }
    }
}
