﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;

using Goldfynger.UniversalDevice.Connector;
using Goldfynger.UniversalDevice.Viewer.Services;
using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice.Viewer.Models
{
    [DebuggerDisplay("DeviceSelectorModel: {SelectedDevice.UniqueIdentifier}")]
    public sealed class DeviceSelectorModel : DependencyObject
    {
        private static readonly Lazy<DeviceSelectorModel> __lazyInstance = new(() => new DeviceSelectorModel());

        private static readonly DependencyPropertyKey DevicesCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DevicesCollection), typeof(ReadOnlyObservableCollection<UniversalDeviceModel>), typeof(DeviceSelectorModel),
                new PropertyMetadata());
        public static readonly DependencyProperty DevicesCollectionProperty = DevicesCollectionPropertyKey.DependencyProperty;

        public static readonly DependencyProperty SelectedDeviceProperty =
            DependencyProperty.Register(nameof(SelectedDevice), typeof(UniversalDeviceModel), typeof(DeviceSelectorModel),
                new PropertyMetadata(defaultValue: null));

        public static readonly StorageService.InstanceStorage __storage = new(nameof(DeviceSelectorModel));

        private readonly ObservableCollection<UniversalDeviceModel> _devicesCollection = new();


        private DeviceSelectorModel()
        {
            DevicesCollection = new ReadOnlyObservableCollection<UniversalDeviceModel>(_devicesCollection);

            ObserveDevicesCollection();

            LoadSelectedDeviceFromStorage();
        }


        public static DeviceSelectorModel Instance => __lazyInstance.Value;

        public ReadOnlyObservableCollection<UniversalDeviceModel> DevicesCollection
        {
            get => (ReadOnlyObservableCollection<UniversalDeviceModel>)GetValue(DevicesCollectionProperty);
            private set => SetValue(DevicesCollectionPropertyKey, value);
        }

        public UniversalDeviceModel? SelectedDevice
        {
            get => (UniversalDeviceModel?)GetValue(SelectedDeviceProperty);
            set => SetValue(SelectedDeviceProperty, value);
        }


        private void ObserveDevicesCollection()
        {
            if (DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }

            DeviceFactoryService.Instance.DeviceConnectorObservableFactory.AvailableTypes.ForEach(t =>
            {
                var availableIdentifiers = DeviceFactoryService.Instance.DeviceConnectorObservableFactory.GetAvailableIdentifiers(t);                

                /* Subscribe for observable collection events. */
                ((INotifyCollectionChanged)availableIdentifiers).CollectionChanged += (sender, e) =>
                {
                    e.OldItems?.Cast<IDeviceConnectorIdentifier>().ForEach(i =>
                    {
                        var deviceToRemove = _devicesCollection.FirstOrDefault(d => d.ConnectorType.Equals(t) && d.ConnectorIdentifier.Equals(i));

                        if (deviceToRemove != null)
                        {
                            _devicesCollection.Remove(deviceToRemove);
                            deviceToRemove.Dispose();
                        }
                    });

                    e.NewItems?.Cast<IDeviceConnectorIdentifier>().ForEach(i =>
                    {
                        if (_devicesCollection.FirstOrDefault(d => d.ConnectorType.Equals(t) && d.ConnectorIdentifier.Equals(i)) == null)
                        {
                            _devicesCollection.Add(new UniversalDeviceModel(t, i));
                        }
                    });

                    if (SelectedDevice != null && !IsCollectionHasDevice(SelectedDevice))
                    {
                        SelectedDevice = null;
                    }
                };

                /* Add current devices. */
                availableIdentifiers.ForEach(i =>
                {
                    if (_devicesCollection.FirstOrDefault(d => d.ConnectorType.Equals(t) && d.ConnectorIdentifier.Equals(i)) == null)
                    {
                        _devicesCollection.Add(new UniversalDeviceModel(t, i));
                    }
                });
            });
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            switch (e.Property.Name)
            {
                case nameof(SelectedDevice):
                    ThrowIfSelectedTypeInvalid(e);
                    __storage[nameof(SelectedDevice)] = SelectedDevice?.UniqueIdentifier;
                    break;
            }

            base.OnPropertyChanged(e);
        }

        private void ThrowIfSelectedTypeInvalid(DependencyPropertyChangedEventArgs e)
        {
            var newSelectedDevice = (UniversalDeviceModel)e.NewValue;

            if (newSelectedDevice != null && !IsCollectionHasDevice(newSelectedDevice))
            {
                throw new InvalidOperationException($"Value of {nameof(e.Property.Name)} ({newSelectedDevice}) is not a part of {nameof(DevicesCollection)}.");
            }
        }

        private bool IsCollectionHasDevice(UniversalDeviceModel device) => device != null && !DevicesCollection.All(d => !ReferenceEquals(d, device));

        private void LoadSelectedDeviceFromStorage()
        {
            var selectedDeviceStorageValue = __storage[nameof(SelectedDevice)];

            if (selectedDeviceStorageValue != null)
            {
                var device = DevicesCollection.FirstOrDefault(d => d.UniqueIdentifier == selectedDeviceStorageValue);

                if (device != null)
                {
                    SelectedDevice = device;
                }
            }
        }
    }
}
