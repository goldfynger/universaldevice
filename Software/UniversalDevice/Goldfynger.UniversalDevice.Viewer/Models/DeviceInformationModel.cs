﻿using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;

using Goldfynger.UniversalDevice.Info;
using Goldfynger.Utils.Stm32.DeviceInfo;

namespace Goldfynger.UniversalDevice.Viewer.Models
{
    [DebuggerDisplay("DeviceInformationModel: {UniqueIdentifier}|{Information}|{Uid}")]
    public sealed class DeviceInformationModel : DeviceInterfaceModel
    {
        private static readonly DependencyPropertyKey InformationPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Information), typeof(Information), typeof(DeviceInformationModel), new PropertyMetadata());
        public static readonly DependencyProperty InformationProperty = InformationPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey UidPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Uid), typeof(Uid), typeof(DeviceInformationModel), new PropertyMetadata());
        public static readonly DependencyProperty UidProperty = UidPropertyKey.DependencyProperty;

        private readonly IInfoDevice _deviceInfo;


        public DeviceInformationModel(IDevice device) : base(device)
        {
            _deviceInfo = _device.CreateDeviceInfo();

            UniqueIdentifier = $"{_device.Connector.Type.Type}#{_device.Connector.Identifier.Identifier}#{nameof(IInfoDevice)}";
        }


        public override string UniqueIdentifier { get; }

        public Information? Information
        {
            get => (Information?)GetValue(InformationProperty);
            private set => SetValue(InformationPropertyKey, value);
        }

        public Uid? Uid
        {
            get => (Uid?)GetValue(UidProperty);
            private set => SetValue(UidPropertyKey, value);
        }


        public async Task ReadInformation() => Information = await _deviceInfo.GetInformationAsync();

        public async Task ReadUid() => Uid = await _deviceInfo.GetUidAsync();
    }
}
