﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;

using Goldfynger.UniversalDevice.I2C;

namespace Goldfynger.UniversalDevice.Viewer.Models
{
    [DebuggerDisplay("I2CDeviceModel: {UniqueIdentifier}")]
    public sealed class I2CDeviceModel : DeviceInterfaceModel
    {
        private static readonly DependencyPropertyKey PortNumberPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(PortNumber), typeof(I2CPortNumbers), typeof(I2CDeviceModel),
                new PropertyMetadata());
        public static readonly DependencyProperty PortNumberProperty = PortNumberPropertyKey.DependencyProperty;

        public static readonly DependencyProperty AddressProperty =
            DependencyProperty.Register(nameof(Address), typeof(I2CAddress), typeof(I2CDeviceModel),
                new PropertyMetadata(defaultValue: null));

        public static readonly DependencyProperty SpeedProperty =
            DependencyProperty.Register(nameof(Speed), typeof(I2CSpeeds?), typeof(I2CDeviceModel),
                new PropertyMetadata(defaultValue: null));

        public static readonly DependencyProperty ReadDataProperty =
            DependencyProperty.Register(nameof(ReadData), typeof(ReadOnlyCollection<byte>), typeof(I2CDeviceModel),
                new PropertyMetadata(defaultValue: null));

        public static readonly DependencyProperty ReadLengthProperty =
            DependencyProperty.Register(nameof(ReadLength), typeof(ushort?), typeof(I2CDeviceModel),
                new PropertyMetadata(defaultValue: null));

        public static readonly DependencyProperty ReadMemoryAddressInUseProperty =
            DependencyProperty.Register(nameof(ReadMemoryAddressInUse), typeof(bool), typeof(I2CDeviceModel),
                new PropertyMetadata(defaultValue: false));

        public static readonly DependencyProperty ReadMemoryAddressProperty =
            DependencyProperty.Register(nameof(ReadMemoryAddress), typeof(I2CMemoryAddress), typeof(I2CDeviceModel),
                new PropertyMetadata(defaultValue: null));

        public static readonly DependencyProperty WriteDataProperty =
            DependencyProperty.Register(nameof(WriteData), typeof(ReadOnlyCollection<byte>), typeof(I2CDeviceModel),
                new PropertyMetadata(defaultValue: null));

        public static readonly DependencyProperty WriteMemoryAddressInUseProperty =
            DependencyProperty.Register(nameof(WriteMemoryAddressInUse), typeof(bool), typeof(I2CDeviceModel),
                new PropertyMetadata(defaultValue: false));

        public static readonly DependencyProperty WriteMemoryAddressProperty =
            DependencyProperty.Register(nameof(WriteMemoryAddress), typeof(I2CMemoryAddress), typeof(I2CDeviceModel),
                new PropertyMetadata(defaultValue: null));

        private readonly II2CDevice _i2cDevice;


        public I2CDeviceModel(IDevice device, I2CPortNumbers portNumber) : base(device)
        {
            _i2cDevice = _device.CreateI2CDevice(portNumber);

            UniqueIdentifier = $"{_device.Connector.Type.Type}#{_device.Connector.Identifier.Identifier}#{nameof(II2CDevice)}#{portNumber}";
            PortNumber = portNumber;
        }


        public override string UniqueIdentifier { get; }

        public I2CPortNumbers PortNumber
        {
            get => (I2CPortNumbers)GetValue(PortNumberProperty);
            private set => SetValue(PortNumberPropertyKey, value);
        }

        public I2CAddress? Address
        {
            get => (I2CAddress?)GetValue(AddressProperty);
            set => SetValue(AddressProperty, value);
        }

        public I2CSpeeds? Speed
        {
            get => (I2CSpeeds?)GetValue(SpeedProperty);
            set => SetValue(SpeedProperty, value);
        }

        public ReadOnlyCollection<byte>? ReadData
        {
            get => (ReadOnlyCollection<byte>?)GetValue(ReadDataProperty);
            set => SetValue(ReadDataProperty, value);
        }

        public ushort? ReadLength
        {
            get => (ushort?)GetValue(ReadLengthProperty);
            set => SetValue(ReadLengthProperty, value);
        }

        public bool ReadMemoryAddressInUse
        {
            get => (bool)GetValue(ReadMemoryAddressInUseProperty);
            set => SetValue(ReadMemoryAddressInUseProperty, value);
        }

        public I2CMemoryAddress? ReadMemoryAddress
        {
            get => (I2CMemoryAddress?)GetValue(ReadMemoryAddressProperty);
            set => SetValue(ReadMemoryAddressProperty, value);
        }

        public ReadOnlyCollection<byte>? WriteData
        {
            get => (ReadOnlyCollection<byte>?)GetValue(WriteDataProperty);
            set => SetValue(WriteDataProperty, value);
        }

        public bool WriteMemoryAddressInUse
        {
            get => (bool)GetValue(WriteMemoryAddressInUseProperty);
            set => SetValue(WriteMemoryAddressInUseProperty, value);
        }

        public I2CMemoryAddress? WriteMemoryAddress
        {
            get => (I2CMemoryAddress?)GetValue(WriteMemoryAddressProperty);
            set => SetValue(WriteMemoryAddressProperty, value);
        }


        public async Task Read()
        {
            if (Address != null && Speed != null && ReadLength != null && (!ReadMemoryAddressInUse || ReadMemoryAddress != null))
            {
                try
                {
                    if (!await _i2cDevice.IsAcquiredAsync())
                    {
                        await _i2cDevice.AcquireAsync();
                    }

                    try
                    {
                        if (ReadMemoryAddressInUse)
                        {
                            ReadData = new ReadOnlyCollection<byte>(await _i2cDevice.ReadMemoryAsync(Speed.Value, Address, ReadMemoryAddress!, ReadLength.Value));
                        }
                        else
                        {
                            ReadData = new ReadOnlyCollection<byte>(await _i2cDevice.ReadAsync(Speed.Value, Address, ReadLength.Value));
                        }
                    }
                    finally
                    {
                        if (!_i2cDevice.IsCorrupted)
                        {
                            await _i2cDevice.ReleaseAsync();
                        }
                    }
                }
                catch
                {
                    ReadData = null;
                    throw;
                }
            }
            else
            {
                ReadData = null;
            }
        }
        
        public async Task Write()
        {
            if (Address != null && Speed != null && WriteData != null && (!WriteMemoryAddressInUse || WriteMemoryAddress != null))
            {
                if (!await _i2cDevice.IsAcquiredAsync())
                {
                    await _i2cDevice.AcquireAsync();
                }

                try
                {
                    if (WriteMemoryAddressInUse)
                    {
                        await _i2cDevice.WriteMemoryAsync(Speed.Value, Address, WriteMemoryAddress!, WriteData);
                    }
                    else
                    {
                        await _i2cDevice.WriteAsync(Speed.Value, Address, WriteData);
                    }
                }
                finally
                {
                    if (!_i2cDevice.IsCorrupted)
                    {
                        await _i2cDevice.ReleaseAsync();
                    }
                }
            }
        }
    }
}
