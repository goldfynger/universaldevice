﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;

using Goldfynger.UniversalDevice.Connector;
using Goldfynger.UniversalDevice.I2C;
using Goldfynger.UniversalDevice.Viewer.Extensions;
using Goldfynger.UniversalDevice.Viewer.Services;

namespace Goldfynger.UniversalDevice.Viewer.Models
{
    [DebuggerDisplay("UniversalDeviceModel: {UniqueIdentifier}|{State}")]
    public sealed class UniversalDeviceModel : DependencyObject, IDisposable
    {
        private static readonly DependencyPropertyKey StatePropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(State), typeof(UniversalDeviceModelStates), typeof(UniversalDeviceModel),
                new PropertyMetadata(defaultValue: UniversalDeviceModelStates.NotConnected));
        public static readonly DependencyProperty StateProperty = StatePropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey DeviceInfoPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DeviceInfo), typeof(DeviceInformationModel), typeof(UniversalDeviceModel),
                new PropertyMetadata(defaultValue: null));
        public static readonly DependencyProperty DeviceInfoProperty = DeviceInfoPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey InterfacesCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(InterfacesCollection), typeof(ReadOnlyObservableCollection<DeviceInterfaceModel>), typeof(UniversalDeviceModel),
                new PropertyMetadata());
        public static readonly DependencyProperty InterfacesCollectionProperty = InterfacesCollectionPropertyKey.DependencyProperty;

        public static readonly DependencyProperty SelectedInterfaceProperty =
            DependencyProperty.Register(nameof(SelectedInterface), typeof(DeviceInterfaceModel), typeof(UniversalDeviceModel),
                new PropertyMetadata(defaultValue: null));

        private readonly ObservableCollection<DeviceInterfaceModel> _interfacesCollection = new();

        private readonly Lazy<DeviceWindow> _deviceWindow;

        public readonly StorageService.InstanceStorage _storage;

        private IDeviceConnector? _deviceConnector;
        private IDevice? _device;
        

        public UniversalDeviceModel(IDeviceConnectorType connectorType, IDeviceConnectorIdentifier connectorIdentifier)
        {
            ConnectorType = connectorType ?? throw new ArgumentNullException(nameof(connectorType));
            ConnectorIdentifier = connectorIdentifier ?? throw new ArgumentNullException(nameof(connectorIdentifier));

            UniqueIdentifier = $"{ConnectorType.Type}#{ConnectorIdentifier.Identifier}";

            _deviceWindow = new Lazy<DeviceWindow>(() => new DeviceWindow(this));

            InterfacesCollection = new ReadOnlyObservableCollection<DeviceInterfaceModel>(_interfacesCollection);

            _storage = new(nameof(UniversalDeviceModel), UniqueIdentifier);

            if (_storage[nameof(State)] == UniversalDeviceModelStates.Connected.ToString())
            {
                Connect();
            }            
        }


        private void Device_Corrupted(object? sender, EventArgs e) => State = UniversalDeviceModelStates.Corrupted;


        public IDeviceConnectorType ConnectorType { get; }

        public IDeviceConnectorIdentifier ConnectorIdentifier { get; }

        public string UniqueIdentifier { get; }

        public DeviceWindow DeviceWindow => _deviceWindow.Value;

        public UniversalDeviceModelStates State
        {
            get => (UniversalDeviceModelStates)GetValue(StateProperty);
            private set => SetValue(StatePropertyKey, value);
        }

        public DeviceInformationModel? DeviceInfo
        {
            get => (DeviceInformationModel?)GetValue(DeviceInfoProperty);
            private set => SetValue(DeviceInfoPropertyKey, value);
        }

        public ReadOnlyObservableCollection<DeviceInterfaceModel> InterfacesCollection
        {
            get => (ReadOnlyObservableCollection<DeviceInterfaceModel>)GetValue(InterfacesCollectionProperty);
            private set => SetValue(InterfacesCollectionPropertyKey, value);
        }

        public DeviceInterfaceModel? SelectedInterface
        {
            get => (DeviceInterfaceModel?)GetValue(SelectedInterfaceProperty);
            set => SetValue(SelectedInterfaceProperty, value);
        }


        public void Connect()
        {
            ThrowIfDisposed();

            var savedState = State;

            State = UniversalDeviceModelStates.Transition;

            if (savedState != UniversalDeviceModelStates.NotConnected)
            {
                DisconnectPrivate();
            }

            try
            {
                ConnectPrivate();
                State = _device!.IsCorrupted ? UniversalDeviceModelStates.Corrupted : UniversalDeviceModelStates.Connected;
            }
            catch
            {
                DisconnectPrivate();
                State = UniversalDeviceModelStates.NotConnected;
            }
        }

        public void Disconnect()
        {
            ThrowIfDisposed();

            State = UniversalDeviceModelStates.Transition;

            DisconnectPrivate();
            State = UniversalDeviceModelStates.NotConnected;
        }

        private void ConnectPrivate()
        {
            try
            {
                _deviceConnector = DeviceFactoryService.Instance.DeviceConnectorObservableFactory.Create(ConnectorType, ConnectorIdentifier);
            }
            catch (Exception ex)
            {
                ShowErrorService.Instance.ShowError(ex, "Connector creation error");

                throw;
            }

            try
            {
                _device = DeviceFactoryService.Instance.DeviceFactory.Create(_deviceConnector);

                _device.Corrupted += Device_Corrupted;
            }
            catch (Exception ex)
            {
                ShowErrorService.Instance.ShowError(ex, "Device creation error");

                throw;
            }

            try
            {
                DeviceInfo = new DeviceInformationModel(_device);

                _interfacesCollection.Add(new FlashStorageDeviceModel(_device));
                _interfacesCollection.Add(new I2CDeviceModel(_device, I2CPortNumbers.Port1));
                _interfacesCollection.Add(new I2CDeviceModel(_device, I2CPortNumbers.Port2));
                _interfacesCollection.Add(new Nrf24DeviceModel(_device));

                var selectedInterfaceStorageValue = _storage[nameof(SelectedInterface)];

                if (selectedInterfaceStorageValue != null)
                {
                    var @interface = InterfacesCollection.FirstOrDefault(i => i.UniqueIdentifier == selectedInterfaceStorageValue);

                    if (@interface != null)
                    {
                        SelectedInterface = @interface;
                    }
                    else
                    {
                        SelectedInterface = null;
                    }
                }
                else
                {
                    SelectedInterface = null;
                }
            }
            catch (Exception ex)
            {
                ShowErrorService.Instance.ShowError(ex, "Interface creation error");

                throw;
            }
        }

        private void DisconnectPrivate()
        {
            DeviceInfo = null;

            SelectedInterface = null;
            _interfacesCollection.RemoveAll();

            if (_device != null)
            {
                _device.Corrupted -= Device_Corrupted;
                _device.Dispose();
            }
            _device = null;

            if (_deviceConnector != null)
            {
                _deviceConnector.Dispose();
            }
            _deviceConnector = null;
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            switch (e.Property.Name)
            {
                case nameof(State):
                    if (State == UniversalDeviceModelStates.NotConnected || State == UniversalDeviceModelStates.Connected)
                    {
                        _storage[nameof(State)] = State.ToString();
                    }
                    break;

                case nameof(SelectedInterface):
                    ThrowIfSelectedInterfaceInvalid(e);
                    if (SelectedInterface != null)
                    {
                        _storage[nameof(SelectedInterface)] = SelectedInterface.UniqueIdentifier;
                    }                    
                    break;
            }

            base.OnPropertyChanged(e);
        }

        private void ThrowIfSelectedInterfaceInvalid(DependencyPropertyChangedEventArgs e)
        {
            var newSelectedInterface = (DeviceInterfaceModel)e.NewValue;

            if (newSelectedInterface != null && !IsCollectionHasInterface(newSelectedInterface))
            {
                throw new InvalidOperationException($"Value of {nameof(e.Property.Name)} ({newSelectedInterface}) is not a part of {nameof(InterfacesCollection)}.");
            }
        }

        private bool IsCollectionHasInterface(DeviceInterfaceModel @interface) => InterfacesCollection != null && @interface != null && !InterfacesCollection.All(i => !ReferenceEquals(i, @interface));


        public enum UniversalDeviceModelStates
        {
            NotConnected,
            Connected,
            Transition,
            Corrupted,
        }


        private void ThrowIfDisposed()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(nameof(UniversalDeviceModel));
            }
        }

        private bool _disposed;

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    _device?.Dispose();
                    _deviceConnector?.Dispose();
                    if (_deviceWindow.IsValueCreated)
                    {
                        _deviceWindow.Value.Dispose();
                    }
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~UniversalDeviceModel()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
