﻿using System.Diagnostics;

using Goldfynger.UniversalDevice.FlashStorage;

namespace Goldfynger.UniversalDevice.Viewer.Models
{
    [DebuggerDisplay("FlashStorageDeviceModel: {UniqueIdentifier}")]
    public sealed class FlashStorageDeviceModel : DeviceInterfaceModel
    {
        private readonly IFlashStorageDevice _flashStorageDevice;


        public FlashStorageDeviceModel(IDevice device) : base(device)
        {
            _flashStorageDevice = _device.CreateFlashStorageDevice();

            UniqueIdentifier = $"{_device.Connector.Type.Type}#{_device.Connector.Identifier.Identifier}#{nameof(IFlashStorageDevice)}";
        }


        public override string UniqueIdentifier { get; }
    }
}
