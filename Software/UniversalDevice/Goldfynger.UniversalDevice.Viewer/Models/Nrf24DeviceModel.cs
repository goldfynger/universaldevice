﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

using Goldfynger.UniversalDevice.Nrf24;

namespace Goldfynger.UniversalDevice.Viewer.Models
{
    [DebuggerDisplay("Nrf24DeviceModel: {UniqueIdentifier}")]
    public sealed class Nrf24DeviceModel : DeviceInterfaceModel
    {
        private readonly INrf24Device _nrf24Device;


        public Nrf24DeviceModel(IDevice device) : base(device)
        {
            _nrf24Device = _device.CreateNrf24Device();

            UniqueIdentifier = $"{_device.Connector.Type.Type}#{_device.Connector.Identifier.Identifier}#{nameof(INrf24Device)}";
        }


        public override string UniqueIdentifier { get; }


        public async Task Reset() => await _nrf24Device.ResetAsync();

        public async Task SetConfig(Nrf24Config config) => await _nrf24Device.SetConfigAsync(config);

        public async Task Send(Nrf24TxConfig txConfig, IList<byte> data) => await _nrf24Device.SendAsync(txConfig, data);

        public async Task<Listener<Nrf24RxData>> StartListen(Nrf24RxConfig rxConfig)
        {
            await _nrf24Device.StartListenAsync(rxConfig);

            return _nrf24Device.GetListener();
        }

        public async Task StopListen() => await _nrf24Device.StopListenAsync();
    }
}
