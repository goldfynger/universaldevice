﻿using System;
using System.Runtime.InteropServices;

namespace Goldfynger.UniversalDevice.Viewer.WindowPlacement
{
    /// <summary>
    /// Internal class with WinAPI infrastructure.
    /// </summary>
    internal static class Win32Native
    {
        /// <summary>
        /// Windows messages.
        /// </summary>
        internal enum WM : uint
        {
            /// <summary>Sent after a window has been moved.</summary>
            WM_MOVE = 0x00000003,
            /// <summary>Sent to a window after its size has changed.</summary>
            WM_SIZE = 0x00000005,
            /// <summary>Sent one time to a window after it enters the moving or sizing modal loop.</summary>
            WM_ENTERSIZEMOVE = 0x00000231,
            /// <summary>Sent one time to a window, after it has exited the moving or sizing modal loop.</summary>
            WM_EXITSIZEMOVE = 0x00000232,
        }


        /// <summary>
        /// Contains information about the placement of a window on the screen.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct WINDOWPLACEMENT
        {
            public uint length;
            public uint flags;
            public uint showCmd;
            public POINT ptMinPosition;
            public POINT ptMaxPosition;
            public RECT rcNormalPosition;
        }

        /// <summary>
        /// The <see cref="POINT"/> structure defines the x- and y- coordinates of a point.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct POINT
        {
            public uint x;
            public uint y;
        }

        /// <summary>
        /// The <see cref="RECT"/> structure defines a rectangle by the coordinates of its upper-left and lower-right corners.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct RECT
        {
            public uint left;
            public uint top;
            public uint right;
            public uint bottom;
        }


        /// <summary>
        /// Retrieves the show state and the restored, minimized, and maximized positions of the specified window.
        /// </summary>
        /// <param name="hWnd">A handle to the window.</param>
        /// <param name="lpwndpl">A pointer to the <see cref="WINDOWPLACEMENT"/> structure that receives the show state and position information.</param>
        /// <returns>True if successful.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool GetWindowPlacement(
            IntPtr hWnd,
            ref WINDOWPLACEMENT lpwndpl);

        /// <summary>
        /// Sets the show state and the restored, minimized, and maximized positions of the specified window.
        /// </summary>
        /// <param name="hWnd">A handle to the window.</param>
        /// <param name="lpwndpl">A pointer to a <see cref="WINDOWPLACEMENT"/> structure that specifies the new show state and window positions.</param>
        /// <returns>True if successful.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool SetWindowPlacement(
            IntPtr hWnd,
            [In] ref WINDOWPLACEMENT lpwndpl);
    }
}
