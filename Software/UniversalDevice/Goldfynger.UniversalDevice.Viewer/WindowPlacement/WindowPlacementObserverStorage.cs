﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Interop;

using Goldfynger.UniversalDevice.Viewer.Services;

namespace Goldfynger.UniversalDevice.Viewer.WindowPlacement
{
    public sealed class WindowPlacementObserverStorage
    {
        /* Current implementation does not save window placement when window is docked (snapped) to left or right edge of screen.
         * WINDOWPLACEMENT structure saves placement of window normal state and does not reflects placement of docked state.
         * There is no easy way to restore docked state with resolwing posible problems with screen resolution and multiple monitor configuration.
         * We can save placement in docked state as normal state of window, but this way we can not normalize window placement with double click on title bar after restoring. */

        private const char __splitter = '#';

        private static readonly string __flagsPropertyKey = nameof(Win32Native.WINDOWPLACEMENT.flags);
        private static readonly string __showCmdPropertyKey = nameof(Win32Native.WINDOWPLACEMENT.showCmd);
        private static readonly string __ptMinPositionXPropertyKey = nameof(Win32Native.WINDOWPLACEMENT.ptMinPosition) + __splitter + nameof(Win32Native.POINT.x);
        private static readonly string __ptMinPositionYPropertyKey = nameof(Win32Native.WINDOWPLACEMENT.ptMinPosition) + __splitter + nameof(Win32Native.POINT.y);
        private static readonly string __ptMaxPositionXPropertyKey = nameof(Win32Native.WINDOWPLACEMENT.ptMaxPosition) + __splitter + nameof(Win32Native.POINT.x);
        private static readonly string __ptMaxPositionYPropertyKey = nameof(Win32Native.WINDOWPLACEMENT.ptMaxPosition) + __splitter + nameof(Win32Native.POINT.y);
        private static readonly string __rcNormalPositionLeftPropertyKey = nameof(Win32Native.WINDOWPLACEMENT.rcNormalPosition) + __splitter + nameof(Win32Native.RECT.left);
        private static readonly string __rcNormalPositionTopPropertyKey = nameof(Win32Native.WINDOWPLACEMENT.rcNormalPosition) + __splitter + nameof(Win32Native.RECT.top);
        private static readonly string __rcNormalPositionRightPropertyKey = nameof(Win32Native.WINDOWPLACEMENT.rcNormalPosition) + __splitter + nameof(Win32Native.RECT.right);
        private static readonly string __rcNormalPositionBottomPropertyKey = nameof(Win32Native.WINDOWPLACEMENT.rcNormalPosition) + __splitter + nameof(Win32Native.RECT.bottom);

        private readonly string _flagsKey;
        private readonly string _showCmdKey;
        private readonly string _ptMinPositionXKey;
        private readonly string _ptMinPositionYKey;
        private readonly string _ptMaxPositionXKey;
        private readonly string _ptMaxPositionYKey;
        private readonly string _rcNormalPositionLeftKey;
        private readonly string _rcNormalPositionTopKey;
        private readonly string _rcNormalPositionRightKey;
        private readonly string _rcNormalPositionBottomKey;

        private readonly IntPtr _hWnd;

        private readonly WindowMoveSizeObserver _observer;

        private Win32Native.WINDOWPLACEMENT? _savedPlacement;


        public WindowPlacementObserverStorage(Window window, string typeName, params string[] instanceKeys)
        {
            _hWnd = new WindowInteropHelper(window).Handle;

            _observer = new WindowMoveSizeObserver(window);
            _observer.WindowMovedOrResized += (sender, e) => ForceSavePlacement();

            var builder = new StringBuilder();

            builder.Append(typeName);
            builder.Append(__splitter);

            foreach (var instanceKey in instanceKeys)
            {
                builder.Append(instanceKey);
                builder.Append(__splitter);
            }

            builder.Append(nameof(Win32Native.WINDOWPLACEMENT));
            builder.Append(__splitter);

            var commonKey = builder.ToString();

            _flagsKey = commonKey + __flagsPropertyKey;
            _showCmdKey = commonKey + __showCmdPropertyKey;
            _ptMinPositionXKey = commonKey + __ptMinPositionXPropertyKey;
            _ptMinPositionYKey = commonKey + __ptMinPositionYPropertyKey;
            _ptMaxPositionXKey = commonKey + __ptMaxPositionXPropertyKey;
            _ptMaxPositionYKey = commonKey + __ptMaxPositionYPropertyKey;
            _rcNormalPositionLeftKey = commonKey + __rcNormalPositionLeftPropertyKey;
            _rcNormalPositionTopKey = commonKey + __rcNormalPositionTopPropertyKey;
            _rcNormalPositionRightKey = commonKey + __rcNormalPositionRightPropertyKey;
            _rcNormalPositionBottomKey = commonKey + __rcNormalPositionBottomPropertyKey;
        }


        public void ForceSavePlacement()
        {
            var windowPlacement = new Win32Native.WINDOWPLACEMENT();
            windowPlacement.length = (uint)Marshal.SizeOf(windowPlacement);

            if (!Win32Native.GetWindowPlacement(
                _hWnd,
                ref windowPlacement))
            {
                var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                throw new ApplicationException($"Failed to get window placement: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}", win32Exception);
            }

            if (_savedPlacement == null || windowPlacement.flags != _savedPlacement.Value.flags)
            {
                StorageService.Instance.SaveValue(_flagsKey, windowPlacement.flags.ToString());
            }

            if (_savedPlacement == null || windowPlacement.showCmd != _savedPlacement.Value.showCmd)
            {
                StorageService.Instance.SaveValue(_showCmdKey, windowPlacement.showCmd.ToString());
            }

            if (_savedPlacement == null || windowPlacement.ptMinPosition.x != _savedPlacement.Value.ptMinPosition.x)
            {
                StorageService.Instance.SaveValue(_ptMinPositionXKey, windowPlacement.ptMinPosition.x.ToString());
            }

            if (_savedPlacement == null || windowPlacement.ptMinPosition.y != _savedPlacement.Value.ptMinPosition.y)
            {
                StorageService.Instance.SaveValue(_ptMinPositionYKey, windowPlacement.ptMinPosition.y.ToString());
            }

            if (_savedPlacement == null || windowPlacement.ptMaxPosition.x != _savedPlacement.Value.ptMaxPosition.x)
            {
                StorageService.Instance.SaveValue(_ptMaxPositionXKey, windowPlacement.ptMaxPosition.x.ToString());
            }

            if (_savedPlacement == null || windowPlacement.ptMaxPosition.y != _savedPlacement.Value.ptMaxPosition.y)
            {
                StorageService.Instance.SaveValue(_ptMaxPositionYKey, windowPlacement.ptMaxPosition.y.ToString());
            }

            if (_savedPlacement == null || windowPlacement.rcNormalPosition.left != _savedPlacement.Value.rcNormalPosition.left)
            {
                StorageService.Instance.SaveValue(_rcNormalPositionLeftKey, windowPlacement.rcNormalPosition.left.ToString());
            }

            if (_savedPlacement == null || windowPlacement.rcNormalPosition.top != _savedPlacement.Value.rcNormalPosition.top)
            {
                StorageService.Instance.SaveValue(_rcNormalPositionTopKey, windowPlacement.rcNormalPosition.top.ToString());
            }

            if (_savedPlacement == null || windowPlacement.rcNormalPosition.right != _savedPlacement.Value.rcNormalPosition.right)
            {
                StorageService.Instance.SaveValue(_rcNormalPositionRightKey, windowPlacement.rcNormalPosition.right.ToString());
            }

            if (_savedPlacement == null || windowPlacement.rcNormalPosition.bottom != _savedPlacement.Value.rcNormalPosition.bottom)
            {
                StorageService.Instance.SaveValue(_rcNormalPositionBottomKey, windowPlacement.rcNormalPosition.bottom.ToString());
            }

            _savedPlacement = windowPlacement;
        }

        public void LoadPlacement()
        {
            var flagsValue = StorageService.Instance.LoadValue(_flagsKey);
            var showCmdValue = StorageService.Instance.LoadValue(_showCmdKey);
            var ptMinPositionXValue = StorageService.Instance.LoadValue(_ptMinPositionXKey);
            var ptMinPositionYValue = StorageService.Instance.LoadValue(_ptMinPositionYKey);
            var ptMaxPositionXValue = StorageService.Instance.LoadValue(_ptMaxPositionXKey);
            var ptMaxPositionYValue = StorageService.Instance.LoadValue(_ptMaxPositionYKey);
            var rcNormalPositionLeftValue = StorageService.Instance.LoadValue(_rcNormalPositionLeftKey);
            var rcNormalPositionTopValue = StorageService.Instance.LoadValue(_rcNormalPositionTopKey);
            var rcNormalPositionRightValue = StorageService.Instance.LoadValue(_rcNormalPositionRightKey);
            var rcNormalPositionBottomValue = StorageService.Instance.LoadValue(_rcNormalPositionBottomKey);

            if (flagsValue != null && showCmdValue != null && ptMinPositionXValue != null && ptMinPositionYValue != null && ptMaxPositionXValue != null && ptMaxPositionYValue != null &&
                rcNormalPositionLeftValue != null && rcNormalPositionTopValue != null && rcNormalPositionRightValue != null && rcNormalPositionBottomValue != null)
            {
                var windowPlacement = new Win32Native.WINDOWPLACEMENT();
                windowPlacement.length = (uint)Marshal.SizeOf(windowPlacement);

                windowPlacement.flags = uint.Parse(flagsValue);
                windowPlacement.showCmd = uint.Parse(showCmdValue);
                windowPlacement.ptMinPosition.x = uint.Parse(ptMinPositionXValue);
                windowPlacement.ptMinPosition.y = uint.Parse(ptMinPositionYValue);
                windowPlacement.ptMaxPosition.x = uint.Parse(ptMaxPositionXValue);
                windowPlacement.ptMaxPosition.y = uint.Parse(ptMaxPositionYValue);
                windowPlacement.rcNormalPosition.left = uint.Parse(rcNormalPositionLeftValue);
                windowPlacement.rcNormalPosition.top = uint.Parse(rcNormalPositionTopValue);
                windowPlacement.rcNormalPosition.right = uint.Parse(rcNormalPositionRightValue);
                windowPlacement.rcNormalPosition.bottom = uint.Parse(rcNormalPositionBottomValue);

                if (!Win32Native.SetWindowPlacement(
                    _hWnd,
                    ref windowPlacement))
                {
                    var win32Exception = new Win32Exception(Marshal.GetLastWin32Error());
                    throw new ApplicationException($"Failed to set window placement: ({win32Exception.NativeErrorCode:X8}) {win32Exception.Message}", win32Exception);
                }
            }
        }
    }
}
