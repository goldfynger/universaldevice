﻿using System;
using System.Windows;
using System.Windows.Interop;

namespace Goldfynger.UniversalDevice.Viewer.WindowPlacement
{
    public sealed class WindowMoveSizeObserver
    {
        private bool _enterSizeMove = false;


        public event EventHandler? WindowMovedOrResized;


        public WindowMoveSizeObserver(Window window) => HwndSource.FromHwnd(new WindowInteropHelper(window).Handle).AddHook(new HwndSourceHook(WndProc));


        private IntPtr WndProc(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            /* User moves window using title bar:
             * WM_ENTERSIZEMOVE
             * WM_MOVE
             * ...
             * WM_MOVE
             * WM_EXITSIZEMOVE
             * 
             * User resizes window using sizing border:
             * WM_ENTERSIZEMOVE
             * WM_SIZE
             * ...
             * WM_SIZE
             * WM_EXITSIZEMOVE
             * 
             * User sets new window location in code:
             * WM_MOVE
             * 
             * User sets new window size in code:
             * WM_SIZE
             * 
             * User changes window state (normal, maximized or minimized):
             * WM_MOVE
             * WM_SIZE
             * 
             * User docks window with keyboard (for example Win + Left to dock to left edge of screen):
             * WM_MOVE
             * WM_SIZE
             * 
             * User docks or maximizes window with mouse using title bar:
             * WM_ENTERSIZEMOVE
             * WM_MOVE
             * ...
             * WM_MOVE
             * WM_SIZE
             * WM_EXITSIZEMOVE
             * 
             * User undocks or normalizes window with mouse using title bar:
             * WM_ENTERSIZEMOVE
             * WM_MOVE
             * ...
             * WM_MOVE
             * WM_SIZE
             * WM_MOVE
             * ...
             * WM_MOVE
             * WM_EXITSIZEMOVE 
             * 
             * So, in some cases there is two events (one for move and on for size) after one action. */

            switch ((Win32Native.WM)msg)
            {
                case Win32Native.WM.WM_MOVE:
                case Win32Native.WM.WM_SIZE:
                    if (!_enterSizeMove)
                    {
                        OnWindowMovedOrResized();
                    }
                    break;

                case Win32Native.WM.WM_ENTERSIZEMOVE:
                    _enterSizeMove = true;
                    break;

                case Win32Native.WM.WM_EXITSIZEMOVE:
                    if (_enterSizeMove)
                    {
                        _enterSizeMove = false;
                        OnWindowMovedOrResized();
                    }
                    break;
            }

            return IntPtr.Zero;
        }


        private void OnWindowMovedOrResized() => WindowMovedOrResized?.Invoke(this, EventArgs.Empty);
    }
}
