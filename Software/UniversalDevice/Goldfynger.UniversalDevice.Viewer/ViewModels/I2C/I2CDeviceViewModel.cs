﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

using Goldfynger.UniversalDevice.I2C;
using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.Services;
using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Parser;
using Goldfynger.Utils.Wpf.Commands;
using Goldfynger.Utils.Wpf.Converters;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels.I2C
{
    [DebuggerDisplay("I2CDeviceViewModel: {Model.UniqueIdentifier}")]
    public class I2CDeviceViewModel : DeviceInterfaceViewModel<I2CDeviceModel>
    {
        /// <summary>Collection of <see cref="string"/> representations of <see cref="I2CAddress.Types"/>.</summary>
        public static readonly ReadOnlyCollection<string> __addressTypes = new(((I2CAddress.Types[])Enum.GetValues(typeof(I2CAddress.Types))).Select(t => t.ToString()).ToList());

        /// <summary>Collection of <see cref="string"/> representations of <see cref="I2CSpeeds"/>.</summary>
        public static readonly ReadOnlyCollection<string> __speeds = new(((I2CSpeeds[])Enum.GetValues(typeof(I2CSpeeds))).Select(t => t.ToString()).ToList());

        /// <summary>Collection of <see cref="string"/> representations of <see cref="I2CMemoryAddress.Sizes"/>.</summary>
        public static readonly ReadOnlyCollection<string> __memoryAddressSizes = new(((I2CMemoryAddress.Sizes[])Enum.GetValues(typeof(I2CMemoryAddress.Sizes))).Select(t => t.ToString()).ToList());


        private static readonly DependencyPropertyKey AddressTypesCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(AddressTypesCollection), typeof(ReadOnlyObservableCollection<string>), typeof(I2CDeviceViewModel),
                new PropertyMetadata(defaultValue: new ReadOnlyObservableCollection<string>(new ObservableCollection<string>(__addressTypes))));
        public static readonly DependencyProperty AddressTypesCollectionProperty = AddressTypesCollectionPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey SpeedsCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(SpeedsCollection), typeof(ReadOnlyObservableCollection<string>), typeof(I2CDeviceViewModel),
                new PropertyMetadata(defaultValue: new ReadOnlyObservableCollection<string>(new ObservableCollection<string>(__speeds))));
        public static readonly DependencyProperty SpeedsCollectionProperty = SpeedsCollectionPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey MemoryAddressSizesCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(MemoryAddressSizesCollection), typeof(ReadOnlyObservableCollection<string>), typeof(I2CDeviceViewModel),
                new PropertyMetadata(defaultValue: new ReadOnlyObservableCollection<string>(new ObservableCollection<string>(__memoryAddressSizes))));
        public static readonly DependencyProperty MemoryAddressSizesCollectionProperty = MemoryAddressSizesCollectionPropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawAddressProperty =
            DependencyProperty.Register(nameof(RawAddress), typeof(string), typeof(I2CDeviceViewModel));

        public static readonly DependencyProperty RawAddressTypeProperty =
            DependencyProperty.Register(nameof(RawAddressType), typeof(string), typeof(I2CDeviceViewModel));

        private static readonly DependencyPropertyKey AddressPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Address), typeof(I2CAddress), typeof(I2CDeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty AddressProperty = AddressPropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawSpeedProperty =
            DependencyProperty.Register(nameof(RawSpeed), typeof(string), typeof(I2CDeviceViewModel));

        private static readonly DependencyPropertyKey SpeedPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Speed), typeof(I2CSpeeds?), typeof(I2CDeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty SpeedProperty = SpeedPropertyKey.DependencyProperty;


        public static readonly DependencyProperty ReadDataProperty =
            DependencyProperty.Register(nameof(ReadData), typeof(ReadOnlyCollection<byte>), typeof(I2CDeviceViewModel));

        private static readonly DependencyPropertyKey PreviewReadDataPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(PreviewReadData), typeof(string), typeof(I2CDeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty PreviewReadDataProperty = PreviewReadDataPropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawReadLengthProperty =
            DependencyProperty.Register(nameof(RawReadLength), typeof(string), typeof(I2CDeviceViewModel));

        private static readonly DependencyPropertyKey ReadLengthPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ReadLength), typeof(ushort?), typeof(I2CDeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ReadLengthProperty = ReadLengthPropertyKey.DependencyProperty;


        public static readonly DependencyProperty ReadMemoryAddressInUseProperty =
            DependencyProperty.Register(nameof(ReadMemoryAddressInUse), typeof(bool), typeof(I2CDeviceViewModel));


        public static readonly DependencyProperty RawReadMemoryAddressProperty =
            DependencyProperty.Register(nameof(RawReadMemoryAddress), typeof(string), typeof(I2CDeviceViewModel));

        public static readonly DependencyProperty RawReadMemoryAddressSizeProperty =
            DependencyProperty.Register(nameof(RawReadMemoryAddressSize), typeof(string), typeof(I2CDeviceViewModel));

        private static readonly DependencyPropertyKey ReadMemoryAddressPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ReadMemoryAddress), typeof(I2CMemoryAddress), typeof(I2CDeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ReadMemoryAddressProperty = ReadMemoryAddressPropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawWriteDataProperty =
            DependencyProperty.Register(nameof(RawWriteData), typeof(string), typeof(I2CDeviceViewModel));

        private static readonly DependencyPropertyKey PreviewWriteDataPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(PreviewWriteData), typeof(string), typeof(I2CDeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty PreviewWriteDataProperty = PreviewWriteDataPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey WriteDataPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(WriteData), typeof(ReadOnlyCollection<byte>), typeof(I2CDeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty WriteDataProperty = WriteDataPropertyKey.DependencyProperty;


        public static readonly DependencyProperty WriteMemoryAddressInUseProperty =
            DependencyProperty.Register(nameof(WriteMemoryAddressInUse), typeof(bool), typeof(I2CDeviceViewModel));


        public static readonly DependencyProperty RawWriteMemoryAddressProperty =
            DependencyProperty.Register(nameof(RawWriteMemoryAddress), typeof(string), typeof(I2CDeviceViewModel));

        public static readonly DependencyProperty RawWriteMemoryAddressSizeProperty =
            DependencyProperty.Register(nameof(RawWriteMemoryAddressSize), typeof(string), typeof(I2CDeviceViewModel));

        private static readonly DependencyPropertyKey WriteMemoryAddressPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(WriteMemoryAddress), typeof(I2CMemoryAddress), typeof(I2CDeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty WriteMemoryAddressProperty = WriteMemoryAddressPropertyKey.DependencyProperty;


        public static readonly DependencyProperty ReadCommandProperty =
            DependencyProperty.Register(nameof(ReadCommand), typeof(AsyncDependencyCommand), typeof(DeviceViewModel));

        public static readonly DependencyProperty WriteCommandProperty =
            DependencyProperty.Register(nameof(WriteCommand), typeof(AsyncDependencyCommand), typeof(DeviceViewModel));


        private static readonly StringParser __addressParser = new(StringParser.AllowedInputs.Binary | StringParser.AllowedInputs.Decimal | StringParser.AllowedInputs.Hexadecimal);
        private static readonly StringParser __readLengthParser = new(StringParser.AllowedInputs.Decimal);
        private static readonly StringParser __memoryAddressParser = new(StringParser.AllowedInputs.Decimal | StringParser.AllowedInputs.Hexadecimal);
        private static readonly StringParser __writeDataParser = new();

        public readonly StorageService.InstanceStorage _storage;


        public I2CDeviceViewModel(I2CDeviceModel model) : base(model)
        {
            Description = $"{DescriptionService.Instance.DeviceInterfacesDescriptions[nameof(II2CDevice)]} - {DescriptionService.Instance.I2CDevicePortsDescriptions[Model.PortNumber]}";

            _storage = new(nameof(I2CDeviceViewModel), model.UniqueIdentifier);

            LoadPropertyStates();

            BindModel();

            InitializeCommands();
        }


        public ReadOnlyObservableCollection<string> AddressTypesCollection
        {
            get => (ReadOnlyObservableCollection<string>)GetValue(AddressTypesCollectionProperty);
            private set => SetValue(AddressTypesCollectionPropertyKey, value);
        }

        public ReadOnlyObservableCollection<string> SpeedsCollection
        {
            get => (ReadOnlyObservableCollection<string>)GetValue(SpeedsCollectionProperty);
            private set => SetValue(SpeedsCollectionPropertyKey, value);
        }

        public ReadOnlyObservableCollection<string> MemoryAddressSizesCollection
        {
            get => (ReadOnlyObservableCollection<string>)GetValue(MemoryAddressSizesCollectionProperty);
            private set => SetValue(MemoryAddressSizesCollectionPropertyKey, value);
        }


        public string? RawAddress
        {
            get => (string?)GetValue(RawAddressProperty);
            set => SetValue(RawAddressProperty, value);
        }

        public string? RawAddressType
        {
            get => (string?)GetValue(RawAddressTypeProperty);
            set => SetValue(RawAddressTypeProperty, value);
        }

        public I2CAddress? Address
        {
            get => (I2CAddress?)GetValue(AddressProperty);
            private set => SetValue(AddressPropertyKey, value);
        }


        public string? RawSpeed
        {
            get => (string?)GetValue(RawSpeedProperty);
            set => SetValue(RawSpeedProperty, value);
        }

        public I2CSpeeds? Speed
        {
            get => (I2CSpeeds?)GetValue(SpeedProperty);
            private set => SetValue(SpeedPropertyKey, value);
        }


        public ReadOnlyCollection<byte>? ReadData
        {
            get => (ReadOnlyCollection<byte>?)GetValue(ReadDataProperty);
            set => SetValue(ReadDataProperty, value);
        }

        public string? PreviewReadData
        {
            get => (string?)GetValue(PreviewReadDataProperty);
            private set => SetValue(PreviewReadDataPropertyKey, value);
        }        


        public string? RawReadLength
        {
            get => (string?)GetValue(RawReadLengthProperty);
            set => SetValue(RawReadLengthProperty, value);
        }

        public ushort? ReadLength
        {
            get => (ushort?)GetValue(ReadLengthProperty);
            private set => SetValue(ReadLengthPropertyKey, value);
        }


        public bool ReadMemoryAddressInUse
        {
            get => (bool)GetValue(ReadMemoryAddressInUseProperty);
            set => SetValue(ReadMemoryAddressInUseProperty, value);
        }


        public string? RawReadMemoryAddress
        {
            get => (string?)GetValue(RawReadMemoryAddressProperty);
            set => SetValue(RawReadMemoryAddressProperty, value);
        }

        public string? RawReadMemoryAddressSize
        {
            get => (string?)GetValue(RawReadMemoryAddressSizeProperty);
            set => SetValue(RawReadMemoryAddressSizeProperty, value);
        }

        public I2CMemoryAddress? ReadMemoryAddress
        {
            get => (I2CMemoryAddress?)GetValue(ReadMemoryAddressProperty);
            private set => SetValue(ReadMemoryAddressPropertyKey, value);
        }


        public string? RawWriteData
        {
            get => (string?)GetValue(RawWriteDataProperty);
            set => SetValue(RawWriteDataProperty, value);
        }

        public string? PreviewWriteData
        {
            get => (string?)GetValue(PreviewWriteDataProperty);
            private set => SetValue(PreviewWriteDataPropertyKey, value);
        }

        public ReadOnlyCollection<byte>? WriteData
        {
            get => (ReadOnlyCollection<byte>?)GetValue(WriteDataProperty);
            private set => SetValue(WriteDataPropertyKey, value);
        }


        public bool WriteMemoryAddressInUse
        {
            get => (bool)GetValue(WriteMemoryAddressInUseProperty);
            set => SetValue(WriteMemoryAddressInUseProperty, value);
        }


        public string? RawWriteMemoryAddress
        {
            get => (string?)GetValue(RawWriteMemoryAddressProperty);
            set => SetValue(RawWriteMemoryAddressProperty, value);
        }

        public string? RawWriteMemoryAddressSize
        {
            get => (string?)GetValue(RawWriteMemoryAddressSizeProperty);
            set => SetValue(RawWriteMemoryAddressSizeProperty, value);
        }

        public I2CMemoryAddress? WriteMemoryAddress
        {
            get => (I2CMemoryAddress?)GetValue(WriteMemoryAddressProperty);
            private set => SetValue(WriteMemoryAddressPropertyKey, value);
        }


        public AsyncDependencyCommand ReadCommand
        {
            get => (AsyncDependencyCommand)GetValue(ReadCommandProperty);
            set => SetValue(ReadCommandProperty, value);
        }

        public AsyncDependencyCommand WriteCommand
        {
            get => (AsyncDependencyCommand)GetValue(WriteCommandProperty);
            set => SetValue(WriteCommandProperty, value);
        }


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            switch (e.Property.Name)
            {
                case nameof(RawAddress):
                    _storage[nameof(RawAddress)] = RawAddress;
                    UpdateAddress();
                    break;

                case nameof(RawAddressType):
                    ThrowIfAddressTypeInvalid(e);
                    _storage[nameof(RawAddressType)] = RawAddressType;
                    UpdateAddress();
                    break;


                case nameof(RawSpeed):
                    ThrowIfSpeedInvalid(e);
                    _storage[nameof(RawSpeed)] = RawSpeed;
                    Speed = RawSpeed == null ? null : (I2CSpeeds?)Enum.Parse(typeof(I2CSpeeds), RawSpeed);
                    break;


                case nameof(ReadData):
                    PreviewReadData = ReadData?.ConvertToStringWithSeparator(converter: b => b.ToString("X2").AddHexHeader());
                    break;


                case nameof(RawReadLength):
                    _storage[nameof(RawReadLength)] = RawReadLength;
                    UpdateReadLength();
                    break;


                case nameof(ReadMemoryAddressInUse):
                    _storage[nameof(ReadMemoryAddressInUse)] = ReadMemoryAddressInUse.ToString();
                    break;

                case nameof(RawReadMemoryAddress):
                    _storage[nameof(RawReadMemoryAddress)] = RawReadMemoryAddress;
                    UpdateReadMemoryAddress();
                    break;

                case nameof(RawReadMemoryAddressSize):
                    ThrowIfMemoryAddressSizeInvalid(e);
                    _storage[nameof(RawReadMemoryAddressSize)] = RawReadMemoryAddressSize;
                    UpdateReadMemoryAddress();
                    break;


                case nameof(RawWriteData):
                    _storage[nameof(RawWriteData)] = RawWriteData;
                    UpdateWriteData();
                    break;


                case nameof(WriteMemoryAddressInUse):
                    _storage[nameof(WriteMemoryAddressInUse)] = WriteMemoryAddressInUse.ToString();
                    break;

                case nameof(RawWriteMemoryAddress):
                    _storage[nameof(RawWriteMemoryAddress)] = RawWriteMemoryAddress;
                    UpdateWriteMemoryAddress();
                    break;

                case nameof(RawWriteMemoryAddressSize):
                    ThrowIfMemoryAddressSizeInvalid(e);
                    _storage[nameof(RawWriteMemoryAddressSize)] = RawWriteMemoryAddressSize;
                    UpdateWriteMemoryAddress();
                    break;
            }

            base.OnPropertyChanged(e);
        }

        private void ThrowIfAddressTypeInvalid(DependencyPropertyChangedEventArgs e)
        {
            var newAddressType = (string?)e.NewValue;

            if (newAddressType != null && AddressTypesCollection.All(t => !t.Equals(newAddressType)))
            {
                throw new InvalidOperationException($"Value of {e.Property.Name} ({newAddressType}) is not a part of {nameof(AddressTypesCollection)}.");
            }
        }

        private void ThrowIfSpeedInvalid(DependencyPropertyChangedEventArgs e)
        {
            var newSpeed = (string?)e.NewValue;

            if (newSpeed != null && SpeedsCollection.All(t => !t.Equals(newSpeed)))
            {
                throw new InvalidOperationException($"Value of {e.Property.Name} ({newSpeed}) is not a part of {nameof(SpeedsCollection)}.");
            }
        }

        private void ThrowIfMemoryAddressSizeInvalid(DependencyPropertyChangedEventArgs e)
        {
            var newMemoryAddressSizeType = (string?)e.NewValue;

            if (newMemoryAddressSizeType != null && MemoryAddressSizesCollection.All(t => !t.Equals(newMemoryAddressSizeType)))
            {
                throw new InvalidOperationException($"Value of {e.Property.Name} ({newMemoryAddressSizeType}) is not a part of {nameof(MemoryAddressSizesCollection)}.");
            }
        }

        private void UpdateAddress()
        {
            if (string.IsNullOrWhiteSpace(RawAddress) || RawAddressType == null)
            {
                Address = null;
            }
            else
            {
                var parseResults = __addressParser.Parse(RawAddress);

                switch ((I2CAddress.Types)Enum.Parse(typeof(I2CAddress.Types), RawAddressType))
                {
                    case I2CAddress.Types.Wide7:
                        {
                            if (parseResults.Count != 1 || !parseResults[0].U8Value.HasValue)
                            {
                                goto default;
                            }

                            try
                            {
                                Address = I2CAddress.Create7(parseResults[0].U8Value!.Value);
                            }
                            catch
                            {
                                goto default;
                            }
                        }
                        break;

                    case I2CAddress.Types.Wide10:
                        {
                            if (parseResults.Count != 1 || !parseResults[0].U16Value.HasValue)
                            {
                                goto default;
                            }

                            try
                            {
                                Address = I2CAddress.Create10(parseResults[0].U16Value!.Value);
                            }
                            catch
                            {
                                goto default;
                            }
                        }
                        break;

                    default:
                        Address = null;
                        break;
                }
            }
        }

        private void UpdateReadLength()
        {
            if (string.IsNullOrWhiteSpace(RawReadLength))
            {
                ReadLength = null;
            }
            else
            {
                var parseResults = __readLengthParser.Parse(RawReadLength);

                if (parseResults.Count == 1 && parseResults[0].U16Value.HasValue && parseResults[0].U16Value!.Value > 0)
                {
                    ReadLength = parseResults[0].U16Value;
                }
                else
                {
                    ReadLength = null;
                }
            }
        }

        private void UpdateReadMemoryAddress()
        {
            if (string.IsNullOrWhiteSpace(RawReadMemoryAddress) || RawReadMemoryAddressSize == null)
            {
                ReadMemoryAddress = null;
            }
            else
            {
                var parseResults = __memoryAddressParser.Parse(RawReadMemoryAddress);

                switch ((I2CMemoryAddress.Sizes)Enum.Parse(typeof(I2CMemoryAddress.Sizes), RawReadMemoryAddressSize))
                {
                    case I2CMemoryAddress.Sizes.Size8:
                        {
                            if (parseResults.Count != 1 || !parseResults[0].U8Value.HasValue)
                            {
                                goto default;
                            }

                            try
                            {
                                ReadMemoryAddress = I2CMemoryAddress.Create8(parseResults[0].U8Value!.Value);
                            }
                            catch
                            {
                                goto default;
                            }
                        }
                        break;

                    case I2CMemoryAddress.Sizes.Size16:
                        {
                            if (parseResults.Count != 1 || !parseResults[0].U16Value.HasValue)
                            {
                                goto default;
                            }

                            try
                            {
                                ReadMemoryAddress = I2CMemoryAddress.Create16(parseResults[0].U16Value!.Value);
                            }
                            catch
                            {
                                goto default;
                            }
                        }
                        break;

                    default:
                        ReadMemoryAddress = null;
                        break;
                }
            }
        }

        private void UpdateWriteData()
        {
            if (string.IsNullOrWhiteSpace(RawWriteData))
            {
                WriteData = null;
                PreviewWriteData = null;
            }
            else
            {
                var parseResults = __writeDataParser.Parse(RawWriteData);

                if (parseResults.Count == 0)
                {
                    WriteData = null;
                    PreviewWriteData = null;
                }
                else
                {
                    WriteData = parseResults.Select(container => ConvertParseValueContainer(container)).SelectMany(i => i).ToList().AsReadOnly();
                    PreviewWriteData = parseResults.ConvertToStringWithSeparator();
                }
            }
        }

        private void UpdateWriteMemoryAddress()
        {
            if (string.IsNullOrWhiteSpace(RawWriteMemoryAddress) || RawWriteMemoryAddressSize == null)
            {
                WriteMemoryAddress = null;
            }
            else
            {
                var parseResults = __memoryAddressParser.Parse(RawWriteMemoryAddress);

                switch ((I2CMemoryAddress.Sizes)Enum.Parse(typeof(I2CMemoryAddress.Sizes), RawWriteMemoryAddressSize))
                {
                    case I2CMemoryAddress.Sizes.Size8:
                        {
                            if (parseResults.Count != 1 || !parseResults[0].U8Value.HasValue)
                            {
                                goto default;
                            }

                            try
                            {
                                WriteMemoryAddress = I2CMemoryAddress.Create8(parseResults[0].U8Value!.Value);
                            }
                            catch
                            {
                                goto default;
                            }
                        }
                        break;

                    case I2CMemoryAddress.Sizes.Size16:
                        {
                            if (parseResults.Count != 1 || !parseResults[0].U16Value.HasValue)
                            {
                                goto default;
                            }

                            try
                            {
                                WriteMemoryAddress = I2CMemoryAddress.Create16(parseResults[0].U16Value!.Value);
                            }
                            catch
                            {
                                goto default;
                            }
                        }
                        break;

                    default:
                        WriteMemoryAddress = null;
                        break;
                }
            }
        }

        private void LoadPropertyStates()
        {
            var rawAddressStorageValue = _storage[nameof(RawAddress)];
            var rawAddressTypeStorageValue = _storage[nameof(RawAddressType)];
            var rawSpeedStorageValue = _storage[nameof(RawSpeed)];
            var rawReadLengthStorageValue = _storage[nameof(RawReadLength)];
            var readMemoryAddressInUseStorageValue = _storage[nameof(ReadMemoryAddressInUse)];
            var rawReadMemoryAddressStorageValue = _storage[nameof(RawReadMemoryAddress)];
            var rawReadMemoryAddressSizeStorageValue = _storage[nameof(RawReadMemoryAddressSize)];
            var rawWriteDataStorageValue = _storage[nameof(RawWriteData)];
            var writeMemoryAddressInUseStorageValue = _storage[nameof(WriteMemoryAddressInUse)];
            var rawWriteMemoryAddressStorageValue = _storage[nameof(RawWriteMemoryAddress)];
            var rawWriteMemoryAddressSizeStorageValue = _storage[nameof(RawWriteMemoryAddressSize)];

            RawAddress = rawAddressStorageValue;
            RawAddressType = rawAddressTypeStorageValue;
            RawSpeed = rawSpeedStorageValue;
            RawReadLength = rawReadLengthStorageValue;
            ReadMemoryAddressInUse = readMemoryAddressInUseStorageValue != null && readMemoryAddressInUseStorageValue == true.ToString();
            RawReadMemoryAddress = rawReadMemoryAddressStorageValue;
            RawReadMemoryAddressSize = rawReadMemoryAddressSizeStorageValue;
            RawWriteData = rawWriteDataStorageValue;
            WriteMemoryAddressInUse = writeMemoryAddressInUseStorageValue != null && writeMemoryAddressInUseStorageValue == true.ToString();
            RawWriteMemoryAddress = rawWriteMemoryAddressStorageValue;
            RawWriteMemoryAddressSize = rawWriteMemoryAddressSizeStorageValue;
        }

        private void BindModel()
        {
            BindingOperations.SetBinding(Model, I2CDeviceModel.AddressProperty, new Binding($"{nameof(Address)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            BindingOperations.SetBinding(Model, I2CDeviceModel.SpeedProperty, new Binding($"{nameof(Speed)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            BindingOperations.SetBinding(this, ReadDataProperty, new Binding($"{nameof(I2CDeviceModel.ReadData)}")
            {
                Source = Model,
                Mode = BindingMode.OneWay,
            });

            BindingOperations.SetBinding(Model, I2CDeviceModel.ReadLengthProperty, new Binding($"{nameof(I2CDeviceModel.ReadLength)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            BindingOperations.SetBinding(Model, I2CDeviceModel.ReadMemoryAddressInUseProperty, new Binding($"{nameof(I2CDeviceModel.ReadMemoryAddressInUse)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            BindingOperations.SetBinding(Model, I2CDeviceModel.ReadMemoryAddressProperty, new Binding($"{nameof(I2CDeviceModel.ReadMemoryAddress)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            BindingOperations.SetBinding(Model, I2CDeviceModel.WriteDataProperty, new Binding($"{nameof(WriteData)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            BindingOperations.SetBinding(Model, I2CDeviceModel.WriteMemoryAddressInUseProperty, new Binding($"{nameof(WriteMemoryAddressInUse)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            BindingOperations.SetBinding(Model, I2CDeviceModel.WriteMemoryAddressProperty, new Binding($"{nameof(WriteMemoryAddress)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });
        }

        private void InitializeCommands()
        {
            ReadCommand = new AsyncDependencyCommand(OnReadCommand, e => ShowErrorService.Instance.ShowError(e, "I2C data reading error"));
            WriteCommand = new AsyncDependencyCommand(OnWriteCommand, e => ShowErrorService.Instance.ShowError(e, "I2C data writing error"));

            var readMultiBinding = new MultiBinding
            {
                Mode = BindingMode.OneWay,
                Converter = ReadCommandConverter.Instance,                
            };

            readMultiBinding.Bindings.Add(new Binding($"{nameof(Address)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            readMultiBinding.Bindings.Add(new Binding($"{nameof(Speed)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            readMultiBinding.Bindings.Add(new Binding($"{nameof(ReadLength)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            readMultiBinding.Bindings.Add(new Binding($"{nameof(ReadMemoryAddressInUse)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            readMultiBinding.Bindings.Add(new Binding($"{nameof(ReadMemoryAddress)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            BindingOperations.SetBinding(ReadCommand, DependencyCommand.AllowExecuteProperty, readMultiBinding);

            var writeMultiBinding = new MultiBinding
            {
                Mode = BindingMode.OneWay,
                Converter = WriteCommandConverter.Instance,
            };

            writeMultiBinding.Bindings.Add(new Binding($"{nameof(Address)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            writeMultiBinding.Bindings.Add(new Binding($"{nameof(Speed)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            writeMultiBinding.Bindings.Add(new Binding($"{nameof(WriteData)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            writeMultiBinding.Bindings.Add(new Binding($"{nameof(WriteMemoryAddressInUse)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            writeMultiBinding.Bindings.Add(new Binding($"{nameof(WriteMemoryAddress)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
            });

            BindingOperations.SetBinding(WriteCommand, DependencyCommand.AllowExecuteProperty, writeMultiBinding);
        }

        private async Task OnReadCommand() => await Model.Read();
        private async Task OnWriteCommand() => await Model.Write();

        private static IEnumerable<byte> ConvertParseValueContainer(ParseValueContainer container)
        {
            switch (container.ValueType)
            {
                case ParseValueContainer.ValueTypes.U8:
                    {
                        yield return container.U8Value ?? throw new ApplicationException();
                    }
                    break;

                case ParseValueContainer.ValueTypes.U16:
                    {
                        var bytes = BitConverter.GetBytes(container.U16Value ?? throw new ApplicationException());
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                case ParseValueContainer.ValueTypes.U32:
                    {
                        var bytes = BitConverter.GetBytes(container.U32Value ?? throw new ApplicationException());
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                case ParseValueContainer.ValueTypes.U64:
                    {
                        var bytes = BitConverter.GetBytes(container.U64Value ?? throw new ApplicationException());
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                case ParseValueContainer.ValueTypes.String:
                    {
                        var bytes = Encoding.ASCII.GetBytes(container.String!);
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                default:
                    throw new ApplicationException("Unknown container value type.");
            }
        }


        private sealed class ReadCommandConverter : SingletonMultiConverterBase<ReadCommandConverter>
        {
            public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
            {
                var address = (I2CAddress?)values[0];
                var speed = (I2CSpeeds?)values[1];
                var readLength = (ushort?)values[2];
                var readMemoryAddressInUse = (bool)values[3];
                var readMemoryAddress = (I2CMemoryAddress?)values[4];

                return address != null && speed != null && readLength != null && (!readMemoryAddressInUse || (readMemoryAddress != null));
            }

            public override object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) => throw new NotImplementedException();
        }

        private sealed class WriteCommandConverter : SingletonMultiConverterBase<WriteCommandConverter>
        {
            public override object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
            {
                var address = (I2CAddress?)values[0];
                var speed = (I2CSpeeds?)values[1];
                var writeData = (ReadOnlyCollection<byte>?)values[2];
                var writeMemoryAddressInUse = (bool)values[3];
                var writeMemoryAddress = (I2CMemoryAddress?)values[4];                

                return address != null && speed != null && writeData != null && (!writeMemoryAddressInUse || (writeMemoryAddress != null));
            }

            public override object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) => throw new NotImplementedException();
        }
    }
}
