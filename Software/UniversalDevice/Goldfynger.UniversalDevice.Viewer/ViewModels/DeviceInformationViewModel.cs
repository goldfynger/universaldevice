﻿using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

using Goldfynger.UniversalDevice.Info;
using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.Services;
using Goldfynger.Utils.Wpf.Commands;
using Goldfynger.Utils.Wpf.Converters;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels
{
    [DebuggerDisplay("DeviceInformationViewModel: {Model.UniqueIdentifier}|{Model.Information}|{Model.Uid}")]
    public class DeviceInformationViewModel : DeviceInterfaceViewModel<DeviceInformationModel>
    {
        public static readonly DependencyProperty ReadInformationCommandProperty =
            DependencyProperty.Register(nameof(ReadInformationCommand), typeof(AsyncDependencyCommand), typeof(DeviceInformationViewModel));

        public static readonly DependencyProperty ReadUidCommandProperty =
            DependencyProperty.Register(nameof(ReadUidCommand), typeof(AsyncDependencyCommand), typeof(DeviceInformationViewModel));


        public DeviceInformationViewModel(DeviceInformationModel model) : base(model)
        {
            Description = DescriptionService.Instance.DeviceInterfacesDescriptions[nameof(IInfoDevice)];

            InitializeCommands();
        }


        public AsyncDependencyCommand ReadInformationCommand
        {
            get => (AsyncDependencyCommand)GetValue(ReadInformationCommandProperty);
            set => SetValue(ReadInformationCommandProperty, value);
        }

        public AsyncDependencyCommand ReadUidCommand
        {
            get => (AsyncDependencyCommand)GetValue(ReadUidCommandProperty);
            set => SetValue(ReadUidCommandProperty, value);
        }


        private void InitializeCommands()
        {
            ReadInformationCommand = new AsyncDependencyCommand(OnReadInformationCommand, e => ShowErrorService.Instance.ShowError(e, "Info reading error"));
            ReadUidCommand = new AsyncDependencyCommand(OnReadUidCommand, e => ShowErrorService.Instance.ShowError(e, "UID reading error"));

            BindingOperations.SetBinding(ReadInformationCommand, DependencyCommand.AllowExecuteProperty, new Binding($"{nameof(Model)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
                Converter = IsValueNotNullConverter.Instance,
            });
            BindingOperations.SetBinding(ReadUidCommand, DependencyCommand.AllowExecuteProperty, new Binding($"{nameof(Model)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
                Converter = IsValueNotNullConverter.Instance,
            });
        }

        private async Task OnReadInformationCommand() => await Model.ReadInformation();
        private async Task OnReadUidCommand() => await Model.ReadUid();
    }
}
