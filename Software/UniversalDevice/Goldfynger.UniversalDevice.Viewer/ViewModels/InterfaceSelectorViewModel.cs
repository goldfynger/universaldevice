﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;

using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.ViewModels.FlashStorage;
using Goldfynger.UniversalDevice.Viewer.ViewModels.I2C;
using Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24;
using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels
{
    [DebuggerDisplay("InterfaceSelectorViewModel: {SelectedInterface.Description}|Count:{InterfacesCollection.Count}")]
    public class InterfaceSelectorViewModel : DependencyObject
    {
        private static readonly DependencyPropertyKey ModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Model), typeof(UniversalDeviceModel), typeof(InterfaceSelectorViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ModelProperty = ModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey ConnectControlViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ConnectControlViewModel), typeof(ConnectControlViewModel), typeof(InterfaceSelectorViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ConnectControlViewModelProperty = ConnectControlViewModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey InterfacesCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(InterfacesCollection), typeof(ReadOnlyObservableCollection<DeviceInterfaceViewModel>), typeof(InterfaceSelectorViewModel), new PropertyMetadata());
        public static readonly DependencyProperty InterfacesCollectionProperty = InterfacesCollectionPropertyKey.DependencyProperty;

        public static readonly DependencyProperty SelectedInterfaceProperty =
            DependencyProperty.Register(nameof(SelectedInterface), typeof(DeviceInterfaceViewModel), typeof(InterfaceSelectorViewModel));

        private readonly ObservableCollection<DeviceInterfaceViewModel> _interfacesCollection = new();

        /* Own ViewModel instance for each Model. */
        private readonly Dictionary<DeviceInterfaceModel, DeviceInterfaceViewModel> _deviceInterfaceViewModelPool = new();

        private bool _initiatedByModel = false;


        public InterfaceSelectorViewModel(UniversalDeviceModel model)
        {
            Model = model;

            ConnectControlViewModel = new ConnectControlViewModel(model);

            InterfacesCollection = new ReadOnlyObservableCollection<DeviceInterfaceViewModel>(_interfacesCollection);

            ObserveInterfacesCollection();

            ObserveSelectedInterface();
        }


        public UniversalDeviceModel Model
        {
            get => (UniversalDeviceModel)GetValue(ModelProperty);
            private set => SetValue(ModelPropertyKey, value);
        }

        public ConnectControlViewModel ConnectControlViewModel
        {
            get => (ConnectControlViewModel)GetValue(ConnectControlViewModelProperty);
            private set => SetValue(ConnectControlViewModelPropertyKey, value);
        }

        public ReadOnlyObservableCollection<DeviceInterfaceViewModel> InterfacesCollection
        {
            get => (ReadOnlyObservableCollection<DeviceInterfaceViewModel>)GetValue(InterfacesCollectionProperty);
            private set => SetValue(InterfacesCollectionPropertyKey, value);
        }

        public DeviceInterfaceViewModel? SelectedInterface
        {
            get => (DeviceInterfaceViewModel?)GetValue(SelectedInterfaceProperty);
            set => SetValue(SelectedInterfaceProperty, value);
        }


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == SelectedInterfaceProperty)
            {
                if (!_initiatedByModel)
                {
                    Model.SelectedInterface = SelectedInterface == null ? null : GetModelFromPool(SelectedInterface);
                }
            }

            base.OnPropertyChanged(e);
        }

        private void ObserveInterfacesCollection()
        {
            void addViewModel(DeviceInterfaceModel m)
            {
                DeviceInterfaceViewModel vm = m switch
                {
                    FlashStorageDeviceModel flashStorageDeviceModel => new FlashStorageDeviceViewModel(flashStorageDeviceModel),
                    I2CDeviceModel i2CDeviceModel => new I2CDeviceViewModel(i2CDeviceModel),
                    Nrf24DeviceModel nrf24DeviceModel => new Nrf24DeviceViewModel(nrf24DeviceModel),
                    _ => throw new ArgumentException($"Unknown model {m}."),
                };

                _deviceInterfaceViewModelPool.Add(m, vm);
                _interfacesCollection.Add(vm);
            }

            ((INotifyCollectionChanged)Model.InterfacesCollection).CollectionChanged += (sender, e) =>
            {
                e.OldItems?.Cast<DeviceInterfaceModel>().ForEach(m =>
                {
                    if (_deviceInterfaceViewModelPool.Remove(m, out DeviceInterfaceViewModel? vm))
                    {
                        _interfacesCollection.Remove(vm);
                    }
                });

                e.NewItems?.Cast<DeviceInterfaceModel>().ForEach(m => addViewModel(m));
            };

            Model.InterfacesCollection.ForEach(m => addViewModel(m));
        }

        private void ObserveSelectedInterface()
        {
            void updateSelectedInterfaceViewModel()
            {
                _initiatedByModel = true;

                if (Model.SelectedInterface != null)
                {
                    SelectedInterface = _deviceInterfaceViewModelPool[Model.SelectedInterface];
                }
                else
                {
                    SelectedInterface = null;
                }

                _initiatedByModel = false;
            }

            DependencyPropertyDescriptor.FromProperty(UniversalDeviceModel.SelectedInterfaceProperty, typeof(UniversalDeviceModel))
                .AddValueChanged(Model, (sender, e) => updateSelectedInterfaceViewModel());

            updateSelectedInterfaceViewModel();
        }

        private DeviceInterfaceModel GetModelFromPool(DeviceInterfaceViewModel viewModel) => _deviceInterfaceViewModelPool.First(p => ReferenceEquals(p.Value, viewModel)).Key;
    }
}
