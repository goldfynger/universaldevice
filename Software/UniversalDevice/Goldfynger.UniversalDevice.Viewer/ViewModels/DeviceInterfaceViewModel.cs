﻿using System.Diagnostics;
using System.Windows;

using Goldfynger.UniversalDevice.Viewer.Models;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels
{
    [DebuggerDisplay("DeviceInterfaceViewModel: {Description}")]
    public abstract class DeviceInterfaceViewModel : DependencyObject
    {
        protected static readonly DependencyPropertyKey DescriptionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Description), typeof(string), typeof(DeviceInterfaceViewModel), new PropertyMetadata("Empty description"));
        private static readonly DependencyProperty DescriptionProperty = DescriptionPropertyKey.DependencyProperty;


        protected DeviceInterfaceViewModel()
        {
        }


        public string Description
        {
            get => (string)GetValue(DescriptionProperty);
            protected set => SetValue(DescriptionPropertyKey, value);
        }
    }

    [DebuggerDisplay("DeviceInterfaceViewModel<TModel>: {Model.UniqueIdentifier}")]
    public abstract class DeviceInterfaceViewModel<TModel> : DeviceInterfaceViewModel where TModel : notnull, DeviceInterfaceModel
    {
        private static readonly DependencyPropertyKey ModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Model), typeof(TModel), typeof(DeviceInterfaceViewModel<TModel>), new PropertyMetadata());
        private static readonly DependencyProperty ModelProperty = ModelPropertyKey.DependencyProperty;


        protected DeviceInterfaceViewModel(TModel model) => Model = model;


        public TModel Model
        {
            get => (TModel)GetValue(ModelProperty);
            private set => SetValue(ModelPropertyKey, value);
        }
    }
}
