﻿using System.Diagnostics;
using System.Windows;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels
{
    [DebuggerDisplay("DeviceSelectorItemViewModel: {Identifier}")]
    public sealed class DeviceSelectorItemViewModel : DependencyObject
    {
        public static readonly DependencyProperty IdentifierProperty =
            DependencyProperty.Register(nameof(Identifier), typeof(string), typeof(DeviceSelectorItemViewModel));

        public static readonly DependencyProperty DisplayNameProperty =
            DependencyProperty.Register(nameof(DisplayName), typeof(string), typeof(DeviceSelectorItemViewModel));


        public string? Identifier
        {
            get => (string?)GetValue(IdentifierProperty);
            set => SetValue(IdentifierProperty, value);
        }

        public string? DisplayName
        {
            get => (string?)GetValue(DisplayNameProperty);
            set => SetValue(DisplayNameProperty, value);
        }
    }
}
