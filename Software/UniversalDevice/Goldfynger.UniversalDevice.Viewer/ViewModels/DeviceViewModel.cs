﻿using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Data;

using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.Utils.Wpf.Commands;
using Goldfynger.Utils.Wpf.Converters;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels
{
    [DebuggerDisplay("DeviceViewModel: {Model.UniqueIdentifier}|{Model.State}")]
    public class DeviceViewModel : DependencyObject
    {
        private static readonly DependencyPropertyKey ModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Model), typeof(UniversalDeviceModel), typeof(DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ModelProperty = ModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey ConnectControlViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ConnectControlViewModel), typeof(ConnectControlViewModel), typeof(DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ConnectControlViewModelProperty = ConnectControlViewModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey DeviceInfoPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DeviceInfo), typeof(DeviceInformationViewModel), typeof(DeviceViewModel), new PropertyMetadata(defaultValue: null));
        public static readonly DependencyProperty DeviceInfoProperty = DeviceInfoPropertyKey.DependencyProperty;

        public static readonly DependencyProperty ShowWindowCommandProperty =
            DependencyProperty.Register(nameof(ShowWindowCommand), typeof(DependencyCommand), typeof(DeviceViewModel));


        public DeviceViewModel(UniversalDeviceModel model)
        {
            Model = model;

            ConnectControlViewModel = new ConnectControlViewModel(model);

            ObserveDeviceInfo();

            InitializeCommands();
        }


        public UniversalDeviceModel Model
        {
            get => (UniversalDeviceModel)GetValue(ModelProperty);
            private set => SetValue(ModelPropertyKey, value);
        }

        public ConnectControlViewModel ConnectControlViewModel
        {
            get => (ConnectControlViewModel)GetValue(ConnectControlViewModelProperty);
            private set => SetValue(ConnectControlViewModelPropertyKey, value);
        }

        public DeviceInformationViewModel? DeviceInfo
        {
            get => (DeviceInformationViewModel?)GetValue(DeviceInfoProperty);
            private set => SetValue(DeviceInfoPropertyKey, value);
        }

        public DependencyCommand ShowWindowCommand
        {
            get => (DependencyCommand)GetValue(ShowWindowCommandProperty);
            set => SetValue(ShowWindowCommandProperty, value);
        }


        private void ObserveDeviceInfo()
        {
            void updateDeviceInfoViewModel()
            {
                DeviceInfo = Model.DeviceInfo != null ? new DeviceInformationViewModel(Model.DeviceInfo) : null;
            }

            DependencyPropertyDescriptor.FromProperty(UniversalDeviceModel.DeviceInfoProperty, typeof(UniversalDeviceModel))
                .AddValueChanged(Model, (sender, e) => updateDeviceInfoViewModel());

            updateDeviceInfoViewModel();
        }

        private void InitializeCommands()
        {
            ShowWindowCommand = new DependencyCommand(OnShowWindowCommand);

            BindingOperations.SetBinding(ShowWindowCommand, DependencyCommand.AllowExecuteProperty, new Binding($"{nameof(UniversalDeviceModel.State)}")
            {
                Source = Model,
                Mode = BindingMode.OneWay,
                Converter = new IsValueEqualsToComparableConverter<UniversalDeviceModel.UniversalDeviceModelStates>(UniversalDeviceModel.UniversalDeviceModelStates.Connected),
            });
        }

        private void OnShowWindowCommand()
        {
            if (Model.DeviceWindow.Visibility != Visibility.Visible)
            {
                Model.DeviceWindow.Show();
            }

            Model.DeviceWindow.Activate();
        }
    }
}
