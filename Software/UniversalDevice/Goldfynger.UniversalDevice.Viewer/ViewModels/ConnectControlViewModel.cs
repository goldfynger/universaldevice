﻿using System.ComponentModel;
using System.Windows;

using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.Utils.Wpf.Commands;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels
{
    public class ConnectControlViewModel : DependencyObject
    {
        private static readonly DependencyPropertyKey ModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Model), typeof(UniversalDeviceModel), typeof(ConnectControlViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ModelProperty = ModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey ConnectDisplayNamePropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ConnectDisplayName), typeof(string), typeof(ConnectControlViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ConnectDisplayNameProperty = ConnectDisplayNamePropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey ConnectCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ConnectCommand), typeof(DependencyCommand), typeof(ConnectControlViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ConnectCommandProperty = ConnectCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey DisconnectCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DisconnectCommand), typeof(DependencyCommand), typeof(ConnectControlViewModel), new PropertyMetadata());
        public static readonly DependencyProperty DisconnectCommandProperty = DisconnectCommandPropertyKey.DependencyProperty;


        public ConnectControlViewModel(UniversalDeviceModel model)
        {
            Model = model;

            InitializeCommands();

            ObserveModelState();
        }


        public UniversalDeviceModel Model
        {
            get => (UniversalDeviceModel)GetValue(ModelProperty);
            private set => SetValue(ModelPropertyKey, value);
        }

        public string ConnectDisplayName
        {
            get => (string)GetValue(ConnectDisplayNameProperty);
            private set => SetValue(ConnectDisplayNamePropertyKey, value);
        }

        public DependencyCommand ConnectCommand
        {
            get => (DependencyCommand)GetValue(ConnectCommandProperty);
            private set => SetValue(ConnectCommandPropertyKey, value);
        }

        public DependencyCommand DisconnectCommand
        {
            get => (DependencyCommand)GetValue(DisconnectCommandProperty);
            private set => SetValue(DisconnectCommandPropertyKey, value);
        }


        private void ObserveModelState()
        {
            void update()
            {
                switch (Model.State)
                {
                    case UniversalDeviceModel.UniversalDeviceModelStates.NotConnected:
                        ConnectDisplayName = "Connect";
                        ConnectCommand.AllowExecute = true;
                        DisconnectCommand.AllowExecute = true;
                        break;

                    case UniversalDeviceModel.UniversalDeviceModelStates.Connected:
                    case UniversalDeviceModel.UniversalDeviceModelStates.Corrupted:                    
                        ConnectDisplayName = "Reconnect";
                        ConnectCommand.AllowExecute = true;
                        DisconnectCommand.AllowExecute = true;
                        break;

                    case UniversalDeviceModel.UniversalDeviceModelStates.Transition:
                        ConnectCommand.AllowExecute = false;
                        DisconnectCommand.AllowExecute = false;
                        break;
                }
            }

            DependencyPropertyDescriptor.FromProperty(UniversalDeviceModel.StateProperty, typeof(UniversalDeviceModel)).AddValueChanged(Model, (sender, e) => update());

            update();
        }

        private void InitializeCommands()
        {
            ConnectCommand = new DependencyCommand(OnConnectCommand);
            DisconnectCommand = new DependencyCommand(OnDisconnectCommand);
        }

        private void OnConnectCommand() => Model?.Connect();
        private void OnDisconnectCommand() => Model?.Disconnect();
    }
}
