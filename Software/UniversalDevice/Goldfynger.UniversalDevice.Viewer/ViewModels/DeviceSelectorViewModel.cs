﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Data;

using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.Services;
using Goldfynger.Utils.Extensions;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels
{
    [DebuggerDisplay("DeviceSelectorModel: {SelectedDevice.ConnectorType.Type}|{SelectedDevice.ConnectorIdentifier.Identifier}")]
    public class DeviceSelectorViewModel : DependencyObject
    {
        private static readonly DependencyPropertyKey ModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Model), typeof(DeviceSelectorModel), typeof(DeviceSelectorViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ModelProperty = ModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey GroupsCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(GroupsCollection), typeof(ReadOnlyObservableCollection<DeviceSelectorGroupViewModel>), typeof(DeviceSelectorViewModel), new PropertyMetadata());
        public static readonly DependencyProperty GroupsCollectionProperty = GroupsCollectionPropertyKey.DependencyProperty;

        public static readonly DependencyProperty SelectedDeviceProperty =
            DependencyProperty.Register(nameof(SelectedDevice), typeof(UniversalDeviceModel), typeof(DeviceSelectorViewModel), new PropertyMetadata(propertyChangedCallback: SelectedDevice_PropertyChangedCallback));

        private readonly ObservableCollection<DeviceSelectorGroupViewModel> _groupsCollection = new();

        private bool _initiatedByView = false;
        private bool _initiatedByModel = false;


        public DeviceSelectorViewModel(DeviceSelectorModel model)
        {
            Model = model ?? throw new ArgumentNullException(nameof(model));

            GroupsCollection = new ReadOnlyObservableCollection<DeviceSelectorGroupViewModel>(_groupsCollection);

            ObserveDevicesCollection();

            ObserveSelectedDevice();
        }


        private static void SelectedDevice_PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var deviceSelectorViewModel = (DeviceSelectorViewModel)d;
            var newSelectedValue = deviceSelectorViewModel.SelectedDevice;

            if (!deviceSelectorViewModel._initiatedByView)
            {
                if (newSelectedValue == null)
                {
                    deviceSelectorViewModel.GroupsCollection.ForEach(g => g.SelectedItem = null);
                }
                else
                {
                    deviceSelectorViewModel.GroupsCollection.ForEach(g =>
                    {
                        if (g.Type == newSelectedValue.ConnectorType.Type)
                        {
                            foreach (var item in g.ItemsCollection)
                            {
                                if (item.Identifier == newSelectedValue.ConnectorIdentifier.Identifier)
                                {
                                    deviceSelectorViewModel._initiatedByModel = true;
                                    g.SelectedItem = item;
                                    deviceSelectorViewModel._initiatedByModel = false;
                                }
                            }
                        }
                    });
                }
            }
        }

        private void DeviceSelectorGroupViewModel_SelectedItemPropertyChanged(object? sender, EventArgs e)
        {
            var deviceSelectorGroupViewModel = (DeviceSelectorGroupViewModel)sender!;
            var newSelectedValue = deviceSelectorGroupViewModel.SelectedItem;

            if (newSelectedValue != null)
            {
                GroupsCollection.Where(g => !ReferenceEquals(g, deviceSelectorGroupViewModel)).ForEach(g => g.SelectedItem = null);

                if (!_initiatedByModel)
                {
                    _initiatedByView = true;
                    SelectedDevice = Model.DevicesCollection.First(d => d.ConnectorType.Type == deviceSelectorGroupViewModel.Type && d.ConnectorIdentifier.Identifier == newSelectedValue.Identifier);
                    _initiatedByView = false;
                }
            }
        }


        public DeviceSelectorModel Model
        {
            get => (DeviceSelectorModel)GetValue(ModelProperty);
            private set => SetValue(ModelPropertyKey, value);
        }

        public ReadOnlyObservableCollection<DeviceSelectorGroupViewModel> GroupsCollection
        {
            get => (ReadOnlyObservableCollection<DeviceSelectorGroupViewModel>)GetValue(GroupsCollectionProperty);
            private set => SetValue(GroupsCollectionPropertyKey, value);
        }

        public UniversalDeviceModel? SelectedDevice
        {
            get => (UniversalDeviceModel?)GetValue(SelectedDeviceProperty);
            set => SetValue(SelectedDeviceProperty, value);
        }


        private void ObserveDevicesCollection()
        {
            var selectedDevicePropertyDescriptor = DependencyPropertyDescriptor.FromProperty(DeviceSelectorGroupViewModel.SelectedItemProperty, typeof(DeviceSelectorGroupViewModel));

            void addItemToGroupCollection(UniversalDeviceModel d)
            {
                var group = _groupsCollection.FirstOrDefault(g => g.Type == d.ConnectorType.Type);

                if (group == null) /* Create new group if group with same type not found in groups collection. */
                {
                    group = new DeviceSelectorGroupViewModel { Type = d.ConnectorType.Type, DisplayName = DescriptionService.Instance.ConnectorTypesDescriptions[d.ConnectorType.Type] };
                    _groupsCollection.Add(group);

                    selectedDevicePropertyDescriptor.AddValueChanged(group, DeviceSelectorGroupViewModel_SelectedItemPropertyChanged);
                }

                group.ItemsCollection.Add(new DeviceSelectorItemViewModel { Identifier = d.ConnectorIdentifier.Identifier, DisplayName = d.ConnectorIdentifier.ToString() });
            }

            ((INotifyCollectionChanged)Model.DevicesCollection).CollectionChanged += (sender, e) =>
            {
                e.OldItems?.Cast<UniversalDeviceModel>().ForEach(d =>
                {
                    var group = _groupsCollection.First(g => g.Type == d.ConnectorType.Type);
                    group.ItemsCollection.Remove(group.ItemsCollection.First(i => i.Identifier == d.ConnectorIdentifier.Identifier));

                    if (group.ItemsCollection.Count == 0)
                    {
                        selectedDevicePropertyDescriptor.RemoveValueChanged(group, DeviceSelectorGroupViewModel_SelectedItemPropertyChanged);
                        _groupsCollection.Remove(group);
                    }
                });

                e.NewItems?.Cast<UniversalDeviceModel>().ForEach(d => addItemToGroupCollection(d));
            };

            Model.DevicesCollection.ForEach(d => addItemToGroupCollection(d));
        }

        private void ObserveSelectedDevice()
        {
            BindingOperations.SetBinding(this, SelectedDeviceProperty, new Binding($"{nameof(Model)}.{nameof(DeviceSelectorModel.SelectedDevice)}")
            {
                Source = this,
                Mode = BindingMode.TwoWay,
            });
        }
    }
}
