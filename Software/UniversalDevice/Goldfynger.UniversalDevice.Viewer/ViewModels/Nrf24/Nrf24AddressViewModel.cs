﻿using System.Diagnostics;
using System.Globalization;
using System.Windows;

using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Parser;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24
{
    [DebuggerDisplay("Nrf24AddressViewModel: Address:{Address}")]
    public class Nrf24AddressViewModel : DependencyObject
    {
        public static readonly DependencyProperty RawB0Property =
            DependencyProperty.Register(nameof(RawB0), typeof(string), typeof(Nrf24AddressViewModel));

        private static readonly DependencyPropertyKey B0PropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(B0), typeof(byte?), typeof(Nrf24AddressViewModel), new PropertyMetadata());
        public static readonly DependencyProperty B0Property = B0PropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawB1Property =
            DependencyProperty.Register(nameof(RawB1), typeof(string), typeof(Nrf24AddressViewModel));

        private static readonly DependencyPropertyKey B1PropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(B1), typeof(byte?), typeof(Nrf24AddressViewModel), new PropertyMetadata());
        public static readonly DependencyProperty B1Property = B1PropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawB2Property =
            DependencyProperty.Register(nameof(RawB2), typeof(string), typeof(Nrf24AddressViewModel));

        private static readonly DependencyPropertyKey B2PropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(B2), typeof(byte?), typeof(Nrf24AddressViewModel), new PropertyMetadata());
        public static readonly DependencyProperty B2Property = B2PropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawB3Property =
            DependencyProperty.Register(nameof(RawB3), typeof(string), typeof(Nrf24AddressViewModel));

        private static readonly DependencyPropertyKey B3PropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(B3), typeof(byte?), typeof(Nrf24AddressViewModel), new PropertyMetadata());
        public static readonly DependencyProperty B3Property = B3PropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawB4Property =
            DependencyProperty.Register(nameof(RawB4), typeof(string), typeof(Nrf24AddressViewModel));

        private static readonly DependencyPropertyKey B4PropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(B4), typeof(byte?), typeof(Nrf24AddressViewModel), new PropertyMetadata());
        public static readonly DependencyProperty B4Property = B4PropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey AddressPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Address), typeof(Nrf24Address), typeof(Nrf24AddressViewModel), new PropertyMetadata());
        public static readonly DependencyProperty AddressProperty = AddressPropertyKey.DependencyProperty;


        private static readonly StringParser __addressParser = new(StringParser.AllowedInputs.Hexadecimal);

        private bool _applyingAddress;


        public event DependencyPropertyChangedEventHandler? PropertyChanged;


        public string? RawB0
        {
            get => (string?)GetValue(RawB0Property);
            set => SetValue(RawB0Property, value);
        }

        public byte? B0
        {
            get => (byte?)GetValue(B0Property);
            private set => SetValue(B0PropertyKey, value);
        }


        public string? RawB1
        {
            get => (string?)GetValue(RawB1Property);
            set => SetValue(RawB1Property, value);
        }

        public byte? B1
        {
            get => (byte?)GetValue(B1Property);
            private set => SetValue(B1PropertyKey, value);
        }


        public string? RawB2
        {
            get => (string?)GetValue(RawB2Property);
            set => SetValue(RawB2Property, value);
        }

        public byte? B2
        {
            get => (byte?)GetValue(B2Property);
            private set => SetValue(B2PropertyKey, value);
        }


        public string? RawB3
        {
            get => (string?)GetValue(RawB3Property);
            set => SetValue(RawB3Property, value);
        }

        public byte? B3
        {
            get => (byte?)GetValue(B3Property);
            private set => SetValue(B3PropertyKey, value);
        }


        public string? RawB4
        {
            get => (string?)GetValue(RawB4Property);
            set => SetValue(RawB4Property, value);
        }

        public byte? B4
        {
            get => (byte?)GetValue(B4Property);
            private set => SetValue(B4PropertyKey, value);
        }


        public Nrf24Address? Address
        {
            get => (Nrf24Address?)GetValue(AddressProperty);
            private set => SetValue(AddressPropertyKey, value);
        }


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (!_applyingAddress)
            {
                switch (e.Property.Name)
                {
                    case nameof(RawB0):
                        B0 = ParseByte(RawB0);
                        UpdateAddress();
                        break;

                    case nameof(RawB1):
                        B1 = ParseByte(RawB1);
                        UpdateAddress();
                        break;

                    case nameof(RawB2):
                        B2 = ParseByte(RawB2);
                        UpdateAddress();
                        break;

                    case nameof(RawB3):
                        B3 = ParseByte(RawB3);
                        UpdateAddress();
                        break;

                    case nameof(RawB4):
                        B4 = ParseByte(RawB4);
                        UpdateAddress();
                        break;
                }
            }

            PropertyChanged?.Invoke(this, e);

            base.OnPropertyChanged(e);
        }

        private void UpdateAddress() => Address = B0 != null && B1 != null && B2 != null && B3 != null && B4 != null ? new Nrf24Address(B0.Value, B1.Value, B2.Value, B3.Value, B4.Value) : null;

        public void SetAddress(Nrf24Address? address)
        {
            _applyingAddress = true;

            B0 = address?.B0;
            RawB0 = address?.B0.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader();            

            B1 = address?.B1;
            RawB1 = address?.B1.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader();            

            B2 = address?.B2;
            RawB2 = address?.B2.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader();            

            B3 = address?.B3;
            RawB3 = address?.B3.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader();            

            B4 = address?.B4;
            RawB4 = address?.B4.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader();            

            Address = address;

            _applyingAddress = false;
        }

        private static byte? ParseByte(string? input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return null;
            }

            var parseResults = __addressParser.Parse(input);

            return parseResults.Count != 1 ? null : parseResults[0].U8Value;
        }
    }
}
