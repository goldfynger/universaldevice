﻿using System.Diagnostics;
using System.Globalization;
using System.Windows;

using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Parser;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24
{
    [DebuggerDisplay("Nrf24SubaddressViewModel: B4:{B4}")]
    public class Nrf24SubaddressViewModel : DependencyObject
    {
        public static readonly DependencyProperty RawB0Property =
            DependencyProperty.Register(nameof(RawB0), typeof(string), typeof(Nrf24SubaddressViewModel));

        private static readonly DependencyPropertyKey B0PropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(B0), typeof(byte?), typeof(Nrf24SubaddressViewModel), new PropertyMetadata());
        public static readonly DependencyProperty B0Property = B0PropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey PreviewB1PropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(PreviewB1), typeof(string), typeof(Nrf24SubaddressViewModel), new PropertyMetadata());
        public static readonly DependencyProperty PreviewB1Property = PreviewB1PropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey PreviewB2PropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(PreviewB2), typeof(string), typeof(Nrf24SubaddressViewModel), new PropertyMetadata());
        public static readonly DependencyProperty PreviewB2Property = PreviewB2PropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey PreviewB3PropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(PreviewB3), typeof(string), typeof(Nrf24SubaddressViewModel), new PropertyMetadata());
        public static readonly DependencyProperty PreviewB3Property = PreviewB3PropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey PreviewB4PropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(PreviewB4), typeof(string), typeof(Nrf24SubaddressViewModel), new PropertyMetadata());
        public static readonly DependencyProperty PreviewB4Property = PreviewB4PropertyKey.DependencyProperty;


        private static readonly StringParser __addressParser = new(StringParser.AllowedInputs.Hexadecimal);

        private bool _applyingSubaddress;


        public event DependencyPropertyChangedEventHandler? PropertyChanged;


        public string? RawB0
        {
            get => (string?)GetValue(RawB0Property);
            set => SetValue(RawB0Property, value);
        }

        public byte? B0
        {
            get => (byte?)GetValue(B0Property);
            private set => SetValue(B0PropertyKey, value);
        }


        public string? PreviewB1
        {
            get => (string?)GetValue(PreviewB1Property);
            private set => SetValue(PreviewB1PropertyKey, value);
        }

        public string? PreviewB2
        {
            get => (string?)GetValue(PreviewB2Property);
            private set => SetValue(PreviewB2PropertyKey, value);
        }

        public string? PreviewB3
        {
            get => (string?)GetValue(PreviewB3Property);
            private set => SetValue(PreviewB3PropertyKey, value);
        }

        public string? PreviewB4
        {
            get => (string?)GetValue(PreviewB4Property);
            private set => SetValue(PreviewB4PropertyKey, value);
        }


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (!_applyingSubaddress)
            {
                switch (e.Property.Name)
                {
                    case nameof(RawB0):
                        B0 = ParseByte(RawB0);
                        break;
                }
            }

            PropertyChanged?.Invoke(this, e);

            base.OnPropertyChanged(e);
        }

        public void SetBaseAddress(Nrf24Address? baseAddress)
        {            
            PreviewB1 = baseAddress?.B1.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader();
            PreviewB2 = baseAddress?.B2.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader();
            PreviewB3 = baseAddress?.B3.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader();
            PreviewB4 = baseAddress?.B4.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader();
        }

        public void SetSubaddress(byte? subaddress)
        {
            _applyingSubaddress = true;

            B0 = subaddress;
            RawB0 = subaddress?.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader();

            _applyingSubaddress = false;
        }

        private static byte? ParseByte(string? input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return null;
            }

            var parseResults = __addressParser.Parse(input);

            return parseResults.Count != 1 ? null : parseResults[0].U8Value;
        }
    }
}
