﻿using System.Diagnostics;
using System.Windows;

using Goldfynger.UniversalDevice.Nrf24;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24
{
    [DebuggerDisplay("Nrf24RxConfigViewModel: RxConfig:{RxConfig}")]
    public class Nrf24RxConfigViewModel : DependencyObject
    {
        private static readonly DependencyPropertyKey Rx0AddressViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Rx0AddressViewModel), typeof(Nrf24AddressViewModel), typeof(Nrf24RxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty Rx0AddressViewModelProperty = Rx0AddressViewModelPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey Rx1AddressViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Rx1AddressViewModel), typeof(Nrf24AddressViewModel), typeof(Nrf24RxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty Rx1AddressViewModelProperty = Rx1AddressViewModelPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey Rx2SubaddressViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Rx2SubaddressViewModel), typeof(Nrf24SubaddressViewModel), typeof(Nrf24RxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty Rx2SubaddressViewModelProperty = Rx2SubaddressViewModelPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey Rx3SubaddressViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Rx3SubaddressViewModel), typeof(Nrf24SubaddressViewModel), typeof(Nrf24RxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty Rx3SubaddressViewModelProperty = Rx3SubaddressViewModelPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey Rx4SubaddressViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Rx4SubaddressViewModel), typeof(Nrf24SubaddressViewModel), typeof(Nrf24RxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty Rx4SubaddressViewModelProperty = Rx4SubaddressViewModelPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey Rx5SubaddressViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Rx5SubaddressViewModel), typeof(Nrf24SubaddressViewModel), typeof(Nrf24RxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty Rx5SubaddressViewModelProperty = Rx5SubaddressViewModelPropertyKey.DependencyProperty;


        public static readonly DependencyProperty IsRx0CheckedProperty =
            DependencyProperty.Register(nameof(IsRx0Checked), typeof(bool?), typeof(Nrf24RxConfigViewModel));

        public static readonly DependencyProperty IsRx1CheckedProperty =
            DependencyProperty.Register(nameof(IsRx1Checked), typeof(bool?), typeof(Nrf24RxConfigViewModel));

        public static readonly DependencyProperty IsRx2CheckedProperty =
            DependencyProperty.Register(nameof(IsRx2Checked), typeof(bool?), typeof(Nrf24RxConfigViewModel));

        public static readonly DependencyProperty IsRx3CheckedProperty =
            DependencyProperty.Register(nameof(IsRx3Checked), typeof(bool?), typeof(Nrf24RxConfigViewModel));

        public static readonly DependencyProperty IsRx4CheckedProperty =
            DependencyProperty.Register(nameof(IsRx4Checked), typeof(bool?), typeof(Nrf24RxConfigViewModel));

        public static readonly DependencyProperty IsRx5CheckedProperty =
            DependencyProperty.Register(nameof(IsRx5Checked), typeof(bool?), typeof(Nrf24RxConfigViewModel));

        private static readonly DependencyPropertyKey RxPipesPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(RxPipes), typeof(Nrf24RxConfig.RxDataPipes?), typeof(Nrf24RxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty RxPipesProperty = RxPipesPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey RxConfigPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(RxConfig), typeof(Nrf24RxConfig), typeof(Nrf24RxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty RxConfigProperty = RxConfigPropertyKey.DependencyProperty;


        private bool _applyingRxConfig;


        public event DependencyPropertyChangedEventHandler? PropertyChanged;


        public Nrf24RxConfigViewModel()
        {
            Rx0AddressViewModel = new Nrf24AddressViewModel();
            Rx1AddressViewModel = new Nrf24AddressViewModel();
            Rx2SubaddressViewModel = new Nrf24SubaddressViewModel();
            Rx3SubaddressViewModel = new Nrf24SubaddressViewModel();
            Rx4SubaddressViewModel = new Nrf24SubaddressViewModel();
            Rx5SubaddressViewModel = new Nrf24SubaddressViewModel();

            Rx0AddressViewModel.PropertyChanged += Rx0AddressViewModel_PropertyChanged;
            Rx1AddressViewModel.PropertyChanged += Rx1AddressViewModel_PropertyChanged;
            Rx2SubaddressViewModel.PropertyChanged += RxSubaddressViewModel_PropertyChanged;
            Rx3SubaddressViewModel.PropertyChanged += RxSubaddressViewModel_PropertyChanged;
            Rx4SubaddressViewModel.PropertyChanged += RxSubaddressViewModel_PropertyChanged;
            Rx5SubaddressViewModel.PropertyChanged += RxSubaddressViewModel_PropertyChanged;
        }


        private void Rx0AddressViewModel_PropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!_applyingRxConfig)
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24AddressViewModel.Address):
                        UpdateRxConfig();
                        break;
                }
            }
        }

        private void Rx1AddressViewModel_PropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!_applyingRxConfig)
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24AddressViewModel.Address):
                        Rx2SubaddressViewModel.SetBaseAddress(Rx1AddressViewModel.Address);
                        Rx3SubaddressViewModel.SetBaseAddress(Rx1AddressViewModel.Address);
                        Rx4SubaddressViewModel.SetBaseAddress(Rx1AddressViewModel.Address);
                        Rx5SubaddressViewModel.SetBaseAddress(Rx1AddressViewModel.Address);
                        UpdateRxConfig();
                        break;
                }
            }
        }

        private void RxSubaddressViewModel_PropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!_applyingRxConfig)
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24SubaddressViewModel.B0):
                        UpdateRxConfig();
                        break;
                }
            }
        }


        public Nrf24AddressViewModel Rx0AddressViewModel
        {
            get => (Nrf24AddressViewModel)GetValue(Rx0AddressViewModelProperty);
            private set => SetValue(Rx0AddressViewModelPropertyKey, value);
        }


        public Nrf24AddressViewModel Rx1AddressViewModel
        {
            get => (Nrf24AddressViewModel)GetValue(Rx1AddressViewModelProperty);
            private set => SetValue(Rx1AddressViewModelPropertyKey, value);
        }


        public Nrf24SubaddressViewModel Rx2SubaddressViewModel
        {
            get => (Nrf24SubaddressViewModel)GetValue(Rx2SubaddressViewModelProperty);
            private set => SetValue(Rx2SubaddressViewModelPropertyKey, value);
        }


        public Nrf24SubaddressViewModel Rx3SubaddressViewModel
        {
            get => (Nrf24SubaddressViewModel)GetValue(Rx3SubaddressViewModelProperty);
            private set => SetValue(Rx3SubaddressViewModelPropertyKey, value);
        }


        public Nrf24SubaddressViewModel Rx4SubaddressViewModel
        {
            get => (Nrf24SubaddressViewModel)GetValue(Rx4SubaddressViewModelProperty);
            private set => SetValue(Rx4SubaddressViewModelPropertyKey, value);
        }


        public Nrf24SubaddressViewModel Rx5SubaddressViewModel
        {
            get => (Nrf24SubaddressViewModel)GetValue(Rx5SubaddressViewModelProperty);
            private set => SetValue(Rx5SubaddressViewModelPropertyKey, value);
        }


        public bool? IsRx0Checked
        {
            get => (bool?)GetValue(IsRx0CheckedProperty);
            set => SetValue(IsRx0CheckedProperty, value);
        }

        public bool? IsRx1Checked
        {
            get => (bool?)GetValue(IsRx1CheckedProperty);
            set => SetValue(IsRx1CheckedProperty, value);
        }

        public bool? IsRx2Checked
        {
            get => (bool?)GetValue(IsRx2CheckedProperty);
            set => SetValue(IsRx2CheckedProperty, value);
        }

        public bool? IsRx3Checked
        {
            get => (bool?)GetValue(IsRx3CheckedProperty);
            set => SetValue(IsRx3CheckedProperty, value);
        }

        public bool? IsRx4Checked
        {
            get => (bool?)GetValue(IsRx4CheckedProperty);
            set => SetValue(IsRx4CheckedProperty, value);
        }

        public bool? IsRx5Checked
        {
            get => (bool?)GetValue(IsRx5CheckedProperty);
            set => SetValue(IsRx5CheckedProperty, value);
        }

        public Nrf24RxConfig.RxDataPipes? RxPipes
        {
            get => (Nrf24RxConfig.RxDataPipes?)GetValue(RxPipesProperty);
            private set => SetValue(RxPipesPropertyKey, value);
        }


        public Nrf24RxConfig? RxConfig
        {
            get => (Nrf24RxConfig?)GetValue(RxConfigProperty);
            private set => SetValue(RxConfigPropertyKey, value);
        }


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (!_applyingRxConfig)
            {
                switch (e.Property.Name)
                {
                    case nameof(IsRx0Checked):
                    case nameof(IsRx1Checked):
                    case nameof(IsRx2Checked):
                    case nameof(IsRx3Checked):
                    case nameof(IsRx4Checked):
                    case nameof(IsRx5Checked):
                        UpdateRxPipes();
                        UpdateRxConfig();
                        break;
                }
            }

            if (e.Property.Name == nameof(RxConfig))
            {
                Debug.WriteLine($"{nameof(Nrf24RxConfigViewModel)}: {nameof(RxConfig)}: {(RxConfig != null ? RxConfig.ToString() : "null")}.");
            }

            PropertyChanged?.Invoke(this, e);

            base.OnPropertyChanged(e);
        }

        private void UpdateRxPipes()
        {
            if (IsRx0Checked == null || IsRx1Checked == null || IsRx2Checked == null || IsRx3Checked == null || IsRx4Checked == null || IsRx5Checked == null)
            {
                RxPipes = null;
            }
            else
            {
                var rxPipes = Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_NONE;

                if (IsRx0Checked.Value)
                {
                    rxPipes |= Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_0;
                }

                if (IsRx1Checked.Value)
                {
                    rxPipes |= Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_1;
                }

                if (IsRx2Checked.Value)
                {
                    rxPipes |= Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_2;
                }

                if (IsRx3Checked.Value)
                {
                    rxPipes |= Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_3;
                }

                if (IsRx4Checked.Value)
                {
                    rxPipes |= Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_4;
                }

                if (IsRx5Checked.Value)
                {
                    rxPipes |= Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_5;
                }

                RxPipes = rxPipes;
            }
        }

        private void UpdateRxConfig()
        {
            if (Rx0AddressViewModel.Address != null && Rx1AddressViewModel.Address != null &&
                Rx2SubaddressViewModel.B0 != null && Rx3SubaddressViewModel.B0 != null && Rx4SubaddressViewModel.B0 != null && Rx5SubaddressViewModel.B0 != null && RxPipes != null)
            {
                RxConfig = new Nrf24RxConfig(Rx0AddressViewModel.Address, Rx1AddressViewModel.Address,
                    Rx2SubaddressViewModel.B0.Value, Rx3SubaddressViewModel.B0.Value, Rx4SubaddressViewModel.B0.Value, Rx5SubaddressViewModel.B0.Value, RxPipes.Value);
            }
            else
            {
                RxConfig = null;
            }
        }

        public void SetRxConfig(Nrf24RxConfig? rxConfig)
        {
            if (rxConfig != RxConfig)
            {
                _applyingRxConfig = true;

                Rx0AddressViewModel.SetAddress(rxConfig?.Rx0Address);

                Rx1AddressViewModel.SetAddress(rxConfig?.Rx1Address);

                Rx2SubaddressViewModel.SetBaseAddress(rxConfig?.Rx1Address);
                Rx2SubaddressViewModel.SetSubaddress(rxConfig?.Rx2Subaddress);

                Rx3SubaddressViewModel.SetBaseAddress(rxConfig?.Rx1Address);
                Rx3SubaddressViewModel.SetSubaddress(rxConfig?.Rx3Subaddress);

                Rx4SubaddressViewModel.SetBaseAddress(rxConfig?.Rx1Address);
                Rx4SubaddressViewModel.SetSubaddress(rxConfig?.Rx4Subaddress);

                Rx5SubaddressViewModel.SetBaseAddress(rxConfig?.Rx1Address);
                Rx5SubaddressViewModel.SetSubaddress(rxConfig?.Rx5Subaddress);

                RxPipes = rxConfig?.RxPipes;
                IsRx0Checked = rxConfig?.RxPipes.HasFlag(Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_0);
                IsRx1Checked = rxConfig?.RxPipes.HasFlag(Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_1);
                IsRx2Checked = rxConfig?.RxPipes.HasFlag(Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_2);
                IsRx3Checked = rxConfig?.RxPipes.HasFlag(Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_3);
                IsRx4Checked = rxConfig?.RxPipes.HasFlag(Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_4);
                IsRx5Checked = rxConfig?.RxPipes.HasFlag(Nrf24RxConfig.RxDataPipes.NRF24_RX_DATA_PIPE_5);

                RxConfig = rxConfig;

                _applyingRxConfig = false;
            }
        }
    }
}
