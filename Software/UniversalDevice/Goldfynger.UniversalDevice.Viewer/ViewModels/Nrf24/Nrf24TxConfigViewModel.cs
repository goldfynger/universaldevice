﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;

using Goldfynger.UniversalDevice.Nrf24;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24
{
    [DebuggerDisplay("Nrf24TxConfigViewModel: TxConfig:{TxConfig}")]
    public class Nrf24TxConfigViewModel : DependencyObject
    {
        /// <summary>Collection of <see cref="string"/> representations of <see cref="Nrf24TxConfig.RetrCounts"/>.</summary>
        public static readonly ReadOnlyCollection<string> __retrCounts = new(((Nrf24TxConfig.RetrCounts[])Enum.GetValues(typeof(Nrf24TxConfig.RetrCounts))).Select(t => t.ToString()).ToList());

        /// <summary>Collection of <see cref="string"/> representations of <see cref="Nrf24TxConfig.RetrDelays"/>.</summary>
        public static readonly ReadOnlyCollection<string> __retrDelays = new(((Nrf24TxConfig.RetrDelays[])Enum.GetValues(typeof(Nrf24TxConfig.RetrDelays))).Select(t => t.ToString()).ToList());


        private static readonly DependencyPropertyKey RetrCountsCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(RetrCountsCollection), typeof(ReadOnlyObservableCollection<string>), typeof(Nrf24TxConfigViewModel),
                new PropertyMetadata(defaultValue: new ReadOnlyObservableCollection<string>(new ObservableCollection<string>(__retrCounts))));
        public static readonly DependencyProperty RetrCountsCollectionProperty = RetrCountsCollectionPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey RetrDelaysCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(RetrDelaysCollection), typeof(ReadOnlyObservableCollection<string>), typeof(Nrf24TxConfigViewModel),
                new PropertyMetadata(defaultValue: new ReadOnlyObservableCollection<string>(new ObservableCollection<string>(__retrDelays))));
        public static readonly DependencyProperty RetrDelaysCollectionProperty = RetrDelaysCollectionPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey TxAddressViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(TxAddressViewModel), typeof(Nrf24AddressViewModel), typeof(Nrf24TxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty TxAddressViewModelProperty = TxAddressViewModelPropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawRetrCountProperty =
            DependencyProperty.Register(nameof(RawRetrCount), typeof(string), typeof(Nrf24TxConfigViewModel));

        private static readonly DependencyPropertyKey RetrCountPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(RetrCount), typeof(Nrf24TxConfig.RetrCounts?), typeof(Nrf24TxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty RetrCountProperty = RetrCountPropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawRetrDelayProperty =
            DependencyProperty.Register(nameof(RawRetrDelay), typeof(string), typeof(Nrf24TxConfigViewModel));

        private static readonly DependencyPropertyKey RetrDelayPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(RetrDelay), typeof(Nrf24TxConfig.RetrDelays?), typeof(Nrf24TxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty RetrDelayProperty = RetrDelayPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey TxConfigPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(TxConfig), typeof(Nrf24TxConfig), typeof(Nrf24TxConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty TxConfigProperty = TxConfigPropertyKey.DependencyProperty;


        private bool _applyingTxConfig = false;


        public event DependencyPropertyChangedEventHandler? PropertyChanged;


        public Nrf24TxConfigViewModel()
        {
            TxAddressViewModel = new Nrf24AddressViewModel();

            TxAddressViewModel.PropertyChanged += TxAddressViewModel_PropertyChanged;
        }

        private void TxAddressViewModel_PropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!_applyingTxConfig)
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24AddressViewModel.Address):
                        UpdateTxConfig();
                        break;
                }
            }
        }


        public ReadOnlyObservableCollection<string> RetrCountsCollection
        {
            get => (ReadOnlyObservableCollection<string>)GetValue(RetrCountsCollectionProperty);
            private set => SetValue(RetrCountsCollectionPropertyKey, value);
        }

        public ReadOnlyObservableCollection<string> RetrDelaysCollection
        {
            get => (ReadOnlyObservableCollection<string>)GetValue(RetrDelaysCollectionProperty);
            private set => SetValue(RetrDelaysCollectionPropertyKey, value);
        }


        public Nrf24AddressViewModel TxAddressViewModel
        {
            get => (Nrf24AddressViewModel)GetValue(TxAddressViewModelProperty);
            private set => SetValue(TxAddressViewModelPropertyKey, value);
        }


        public string? RawRetrCount
        {
            get => (string?)GetValue(RawRetrCountProperty);
            set => SetValue(RawRetrCountProperty, value);
        }

        public Nrf24TxConfig.RetrCounts? RetrCount
        {
            get => (Nrf24TxConfig.RetrCounts?)GetValue(RetrCountProperty);
            private set => SetValue(RetrCountPropertyKey, value);
        }


        public string? RawRetrDelay
        {
            get => (string?)GetValue(RawRetrDelayProperty);
            set => SetValue(RawRetrDelayProperty, value);
        }

        public Nrf24TxConfig.RetrDelays? RetrDelay
        {
            get => (Nrf24TxConfig.RetrDelays?)GetValue(RetrDelayProperty);
            private set => SetValue(RetrDelayPropertyKey, value);
        }


        public Nrf24TxConfig? TxConfig
        {
            get => (Nrf24TxConfig?)GetValue(TxConfigProperty);
            private set => SetValue(TxConfigPropertyKey, value);
        }


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (!_applyingTxConfig)
            {
                switch (e.Property.Name)
                {
                    case nameof(RawRetrCount):
                        ThrowIfRetrCountInvalid(e);
                        RetrCount = RawRetrCount == null ? null : (Nrf24TxConfig.RetrCounts)Enum.Parse(typeof(Nrf24TxConfig.RetrCounts), RawRetrCount);
                        UpdateTxConfig();
                        break;

                    case nameof(RawRetrDelay):
                        ThrowIfRetrDelayInvalid(e);
                        RetrDelay = RawRetrDelay == null ? null : (Nrf24TxConfig.RetrDelays)Enum.Parse(typeof(Nrf24TxConfig.RetrDelays), RawRetrDelay);
                        UpdateTxConfig();
                        break;
                }
            }

            if (e.Property.Name == nameof(TxConfig))
            {
                Debug.WriteLine($"{nameof(Nrf24TxConfigViewModel)}: {nameof(TxConfig)}: {(TxConfig != null ? TxConfig.ToString() : "null")}.");
            }

            PropertyChanged?.Invoke(this, e);

            base.OnPropertyChanged(e);
        }

        private void ThrowIfRetrCountInvalid(DependencyPropertyChangedEventArgs e)
        {
            var newRetrCount = (string?)e.NewValue;

            if (newRetrCount != null && RetrCountsCollection.All(t => !t.Equals(newRetrCount)))
            {
                throw new InvalidOperationException($"Value of {e.Property.Name} ({newRetrCount}) is not a part of {nameof(RetrCountsCollection)}.");
            }
        }

        private void ThrowIfRetrDelayInvalid(DependencyPropertyChangedEventArgs e)
        {
            var newRetrDelay = (string?)e.NewValue;

            if (newRetrDelay != null && RetrDelaysCollection.All(t => !t.Equals(newRetrDelay)))
            {
                throw new InvalidOperationException($"Value of {e.Property.Name} ({newRetrDelay}) is not a part of {nameof(RetrDelaysCollection)}.");
            }
        }

        private void UpdateTxConfig()
        {
            if (TxAddressViewModel.Address != null && RetrCount != null && RetrDelay != null)
            {
                TxConfig = new Nrf24TxConfig(TxAddressViewModel.Address, RetrCount.Value, RetrDelay.Value);
            }
            else
            {
                TxConfig = null;
            }
        }

        public void SetTxConfig(Nrf24TxConfig? txConfig)
        {
            if (txConfig != TxConfig)
            {
                _applyingTxConfig = true;

                TxAddressViewModel.SetAddress(txConfig?.TxAddress);

                RetrCount = txConfig?.RetrCount;
                RawRetrCount = txConfig?.RetrCount.ToString();

                RetrDelay = txConfig?.RetrDelay;
                RawRetrDelay = txConfig?.RetrDelay.ToString();

                TxConfig = txConfig;

                _applyingTxConfig = false;
            }
        }
    }
}
