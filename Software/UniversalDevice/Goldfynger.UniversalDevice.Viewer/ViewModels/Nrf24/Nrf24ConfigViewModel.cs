﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;

using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.Utils.Parser;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24
{
    [DebuggerDisplay("Nrf24ConfigViewModel: Config:{Config}")]
    public class Nrf24ConfigViewModel : DependencyObject
    {
        /// <summary>Collection of <see cref="string"/> representations of <see cref="Nrf24Config.AmpPowers"/>.</summary>
        public static readonly ReadOnlyCollection<string> __ampPowers = new(((Nrf24Config.AmpPowers[])Enum.GetValues(typeof(Nrf24Config.AmpPowers))).Select(t => t.ToString()).ToList());

        /// <summary>Collection of <see cref="string"/> representations of <see cref="Nrf24Config.DataRates"/>.</summary>
        public static readonly ReadOnlyCollection<string> __dataRates = new(((Nrf24Config.DataRates[])Enum.GetValues(typeof(Nrf24Config.DataRates))).Select(t => t.ToString()).ToList());


        private static readonly DependencyPropertyKey AmpPowersCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(AmpPowersCollection), typeof(ReadOnlyObservableCollection<string>), typeof(Nrf24ConfigViewModel),
                new PropertyMetadata(defaultValue: new ReadOnlyObservableCollection<string>(new ObservableCollection<string>(__ampPowers))));
        public static readonly DependencyProperty AmpPowersCollectionProperty = AmpPowersCollectionPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey DataRatesCollectionPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DataRatesCollection), typeof(ReadOnlyObservableCollection<string>), typeof(Nrf24ConfigViewModel),
                new PropertyMetadata(defaultValue: new ReadOnlyObservableCollection<string>(new ObservableCollection<string>(__dataRates))));
        public static readonly DependencyProperty DataRatesCollectionProperty = DataRatesCollectionPropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawFrequencyChannelProperty =
            DependencyProperty.Register(nameof(RawFrequencyChannel), typeof(string), typeof(Nrf24ConfigViewModel));

        private static readonly DependencyPropertyKey FrequencyChannelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(FrequencyChannel), typeof(byte?), typeof(Nrf24ConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty FrequencyChannelProperty = FrequencyChannelPropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawAmpPowerProperty =
            DependencyProperty.Register(nameof(RawAmpPower), typeof(string), typeof(Nrf24ConfigViewModel));

        private static readonly DependencyPropertyKey AmpPowerPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(AmpPower), typeof(Nrf24Config.AmpPowers?), typeof(Nrf24ConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty AmpPowerProperty = AmpPowerPropertyKey.DependencyProperty;


        public static readonly DependencyProperty RawDataRateProperty =
            DependencyProperty.Register(nameof(RawDataRate), typeof(string), typeof(Nrf24ConfigViewModel));

        private static readonly DependencyPropertyKey DataRatePropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DataRate), typeof(Nrf24Config.DataRates?), typeof(Nrf24ConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty DataRateProperty = DataRatePropertyKey.DependencyProperty;


        public static readonly DependencyProperty IsTxCheckedProperty =
            DependencyProperty.Register(nameof(IsTxChecked), typeof(bool?), typeof(Nrf24ConfigViewModel));

        public static readonly DependencyProperty IsRxCheckedProperty =
            DependencyProperty.Register(nameof(IsRxChecked), typeof(bool?), typeof(Nrf24ConfigViewModel));

        private static readonly DependencyPropertyKey RxTxModePropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(RxTxMode), typeof(Nrf24Config.RxTxModes?), typeof(Nrf24ConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty RxTxModeProperty = RxTxModePropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey ConfigPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Config), typeof(Nrf24Config), typeof(Nrf24ConfigViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ConfigProperty = ConfigPropertyKey.DependencyProperty;


        private static readonly StringParser __frequencyChannelParser = new(StringParser.AllowedInputs.Decimal);

        private bool _applyingConfig = false;


        public event DependencyPropertyChangedEventHandler? PropertyChanged;


        public ReadOnlyObservableCollection<string> AmpPowersCollection
        {
            get => (ReadOnlyObservableCollection<string>)GetValue(AmpPowersCollectionProperty);
            private set => SetValue(AmpPowersCollectionPropertyKey, value);
        }

        public ReadOnlyObservableCollection<string> DataRatesCollection
        {
            get => (ReadOnlyObservableCollection<string>)GetValue(DataRatesCollectionProperty);
            private set => SetValue(DataRatesCollectionPropertyKey, value);
        }


        public string? RawFrequencyChannel
        {
            get => (string?)GetValue(RawFrequencyChannelProperty);
            set => SetValue(RawFrequencyChannelProperty, value);
        }

        public byte? FrequencyChannel
        {
            get => (byte?)GetValue(FrequencyChannelProperty);
            private set => SetValue(FrequencyChannelPropertyKey, value);
        }


        public string? RawAmpPower
        {
            get => (string?)GetValue(RawAmpPowerProperty);
            set => SetValue(RawAmpPowerProperty, value);
        }

        public Nrf24Config.AmpPowers? AmpPower
        {
            get => (Nrf24Config.AmpPowers?)GetValue(AmpPowerProperty);
            private set => SetValue(AmpPowerPropertyKey, value);
        }


        public string? RawDataRate
        {
            get => (string?)GetValue(RawDataRateProperty);
            set => SetValue(RawDataRateProperty, value);
        }

        public Nrf24Config.DataRates? DataRate
        {
            get => (Nrf24Config.DataRates?)GetValue(DataRateProperty);
            private set => SetValue(DataRatePropertyKey, value);
        }


        public bool? IsTxChecked
        {
            get => (bool?)GetValue(IsTxCheckedProperty);
            set => SetValue(IsTxCheckedProperty, value);
        }

        public bool? IsRxChecked
        {
            get => (bool?)GetValue(IsRxCheckedProperty);
            set => SetValue(IsRxCheckedProperty, value);
        }

        public Nrf24Config.RxTxModes? RxTxMode
        {
            get => (Nrf24Config.RxTxModes?)GetValue(RxTxModeProperty);
            private set => SetValue(RxTxModePropertyKey, value);
        }


        public Nrf24Config? Config
        {
            get => (Nrf24Config?)GetValue(ConfigProperty);
            private set => SetValue(ConfigPropertyKey, value);
        }


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (!_applyingConfig)
            {
                switch (e.Property.Name)
                {
                    case nameof(RawFrequencyChannel):
                        UpdateFrequencyChannel();
                        UpdateConfig();
                        break;

                    case nameof(RawAmpPower):
                        ThrowIfAmpPowerInvalid(e);
                        AmpPower = RawAmpPower == null ? null : (Nrf24Config.AmpPowers)Enum.Parse(typeof(Nrf24Config.AmpPowers), RawAmpPower);
                        UpdateConfig();
                        break;

                    case nameof(RawDataRate):
                        ThrowIfDataRateInvalid(e);
                        DataRate = RawDataRate == null ? null : (Nrf24Config.DataRates)Enum.Parse(typeof(Nrf24Config.DataRates), RawDataRate);
                        UpdateConfig();
                        break;

                    case nameof(IsTxChecked):
                    case nameof(IsRxChecked):
                        UpdateRxTxMode();
                        UpdateConfig();
                        break;
                }
            }

            if (e.Property.Name == nameof(Config))
            {
                Debug.WriteLine($"{nameof(Nrf24ConfigViewModel)}: {nameof(Config)}: {(Config != null ? Config.ToString() : "null")}.");
            }

            PropertyChanged?.Invoke(this, e);

            base.OnPropertyChanged(e);
        }

        private void ThrowIfAmpPowerInvalid(DependencyPropertyChangedEventArgs e)
        {
            var newAmpPower = (string?)e.NewValue;

            if (newAmpPower != null && AmpPowersCollection.All(t => !t.Equals(newAmpPower)))
            {
                throw new InvalidOperationException($"Value of {e.Property.Name} ({newAmpPower}) is not a part of {nameof(AmpPowersCollection)}.");
            }
        }

        private void ThrowIfDataRateInvalid(DependencyPropertyChangedEventArgs e)
        {
            var newDataRate = (string?)e.NewValue;

            if (newDataRate != null && DataRatesCollection.All(t => !t.Equals(newDataRate)))
            {
                throw new InvalidOperationException($"Value of {e.Property.Name} ({newDataRate}) is not a part of {nameof(DataRatesCollection)}.");
            }
        }

        private void UpdateFrequencyChannel()
        {
            if (string.IsNullOrWhiteSpace(RawFrequencyChannel))
            {
                FrequencyChannel = null;
            }
            else
            {
                var parseResults = __frequencyChannelParser.Parse(RawFrequencyChannel);

                if (parseResults.Count == 1 && parseResults[0].U8Value.HasValue && parseResults[0].U8Value!.Value <= Nrf24Config.MaxFrequencyChannel)
                {
                    FrequencyChannel = parseResults[0].U8Value;
                }
                else
                {
                    FrequencyChannel = null;
                }
            }
        }

        private void UpdateRxTxMode()
        {
            if (IsTxChecked == null || IsRxChecked == null)
            {
                RxTxMode = null;
            }
            else
            {
                var rxTxMode = Nrf24Config.RxTxModes.NRF24_RX_TX_MODE_NONE;

                if (IsTxChecked.Value)
                {
                    rxTxMode |= Nrf24Config.RxTxModes.NRF24_RX_TX_MODE_TX;
                }

                if (IsRxChecked.Value)
                {
                    rxTxMode |= Nrf24Config.RxTxModes.NRF24_RX_TX_MODE_RX;
                }

                RxTxMode = rxTxMode;
            }
        }

        private void UpdateConfig()
        {
            if (FrequencyChannel != null && AmpPower != null && DataRate != null && RxTxMode != null)
            {
                Config = new Nrf24Config(FrequencyChannel.Value, AmpPower.Value, DataRate.Value, RxTxMode.Value);
            }
            else
            {
                Config = null;
            }
        }

        public void SetConfig(Nrf24Config? config)
        {
            if (config != Config)
            {
                _applyingConfig = true;

                FrequencyChannel = config?.FrequencyChannel;
                RawFrequencyChannel = config?.FrequencyChannel.ToString();

                AmpPower = config?.AmpPower;
                RawAmpPower = config?.AmpPower.ToString();

                DataRate = config?.DataRate;
                RawDataRate = config?.DataRate.ToString();

                RxTxMode = config?.RxTxMode;
                IsTxChecked = config?.RxTxMode.HasFlag(Nrf24Config.RxTxModes.NRF24_RX_TX_MODE_TX);
                IsRxChecked = config?.RxTxMode.HasFlag(Nrf24Config.RxTxModes.NRF24_RX_TX_MODE_RX);

                Config = config;

                _applyingConfig = false;
            }
        }
    }
}
