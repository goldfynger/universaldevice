﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.Services;
using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Parser;
using Goldfynger.Utils.Wpf.Commands;
using Goldfynger.Utils.Wpf.Converters;
using Goldfynger.WirelessThermometer.TemperatureSensor.F030F4;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24
{
    [DebuggerDisplay("Nrf24DeviceViewModel: {Model.UniqueIdentifier}")]
    public class Nrf24DeviceViewModel : DeviceInterfaceViewModel<Nrf24DeviceModel>
    {
        private static readonly DependencyPropertyKey ResetCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ResetCommand), typeof(AsyncDependencyCommand), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ResetCommandProperty = ResetCommandPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey ConfigViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ConfigViewModel), typeof(Nrf24ConfigViewModel), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ConfigViewModelProperty = ConfigViewModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey DefaultConfigCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DefaultConfigCommand), typeof(DependencyCommand), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty DefaultConfigCommandProperty = DefaultConfigCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey SetConfigCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(SetConfigCommand), typeof(AsyncDependencyCommand), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty SetConfigCommandProperty = SetConfigCommandPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey RxConfigViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(RxConfigViewModel), typeof(Nrf24RxConfigViewModel), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty RxConfigViewModelProperty = RxConfigViewModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey DefaultRxConfigCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DefaultRxConfigCommand), typeof(DependencyCommand), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty DefaultRxConfigCommandProperty = DefaultRxConfigCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey StartListenCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(StartListenCommand), typeof(AsyncDependencyCommand), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty StartListenCommandProperty = StartListenCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey StopListenCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(StopListenCommand), typeof(AsyncDependencyCommand), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty StopListenCommandProperty = StopListenCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey ListenStatePropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ListenState), typeof(ListenStates), typeof(Nrf24DeviceViewModel), new PropertyMetadata(defaultValue: ListenStates.Stopped));
        public static readonly DependencyProperty ListenStateProperty = ListenStatePropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey TxConfigViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(TxConfigViewModel), typeof(Nrf24TxConfigViewModel), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty TxConfigViewModelProperty = TxConfigViewModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey DefaultTxConfigCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DefaultTxConfigCommand), typeof(DependencyCommand), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty DefaultTxConfigCommandProperty = DefaultTxConfigCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey FillSendWithRandomCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(FillSendWithRandomCommand), typeof(DependencyCommand), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty FillSendWithRandomCommandProperty = FillSendWithRandomCommandPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey SendDataCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(SendDataCommand), typeof(AsyncDependencyCommand), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty SendDataCommandProperty = SendDataCommandPropertyKey.DependencyProperty;

        public static readonly DependencyProperty RawSendDataProperty =
            DependencyProperty.Register(nameof(RawSendData), typeof(string), typeof(Nrf24DeviceViewModel));

        private static readonly DependencyPropertyKey PreviewSendDataPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(PreviewSendData), typeof(string), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty PreviewSendDataProperty = PreviewSendDataPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey SendDataPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(SendData), typeof(ReadOnlyCollection<byte>), typeof(Nrf24DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty SendDataProperty = SendDataPropertyKey.DependencyProperty;


        private static readonly DependencyPropertyKey ExchangeLogPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ExchangeLog), typeof(string), typeof(Nrf24DeviceViewModel), new PropertyMetadata(defaultValue: string.Empty));
        public static readonly DependencyProperty ExchangeLogProperty = ExchangeLogPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey ClearExchangeLogCommandPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ClearExchangeLogCommand), typeof(DependencyCommand), typeof(DeviceViewModel), new PropertyMetadata());
        public static readonly DependencyProperty ClearExchangeLogCommandProperty = ClearExchangeLogCommandPropertyKey.DependencyProperty;


        private static readonly StringParser __sendDataParser = new();

        private Listener<Nrf24RxData>? _listener;

        private CancellationTokenSource? _listenerCancellationTokenSource;

        private readonly StorageService.InstanceStorage _storage;


        public Nrf24DeviceViewModel(Nrf24DeviceModel model) : base(model)
        {
            Description = DescriptionService.Instance.DeviceInterfacesDescriptions[nameof(INrf24Device)];

            ConfigViewModel = new Nrf24ConfigViewModel();
            TxConfigViewModel = new Nrf24TxConfigViewModel();
            RxConfigViewModel = new Nrf24RxConfigViewModel();

            _storage = new(nameof(Nrf24DeviceViewModel), model.UniqueIdentifier);

            LoadPropertyStates();
            ObserveChildViewModelPropertyStates();

            InitializeCommands();
        }


        public AsyncDependencyCommand ResetCommand
        {
            get => (AsyncDependencyCommand)GetValue(ResetCommandProperty);
            private set => SetValue(ResetCommandPropertyKey, value);
        }


        public Nrf24ConfigViewModel ConfigViewModel
        {
            get => (Nrf24ConfigViewModel)GetValue(ConfigViewModelProperty);
            private set => SetValue(ConfigViewModelPropertyKey, value);
        }

        public DependencyCommand DefaultConfigCommand
        {
            get => (DependencyCommand)GetValue(DefaultConfigCommandProperty);
            private set => SetValue(DefaultConfigCommandPropertyKey, value);
        }

        public AsyncDependencyCommand SetConfigCommand
        {
            get => (AsyncDependencyCommand)GetValue(SetConfigCommandProperty);
            private set => SetValue(SetConfigCommandPropertyKey, value);
        }


        public Nrf24RxConfigViewModel RxConfigViewModel
        {
            get => (Nrf24RxConfigViewModel)GetValue(RxConfigViewModelProperty);
            private set => SetValue(RxConfigViewModelPropertyKey, value);
        }

        public DependencyCommand DefaultRxConfigCommand
        {
            get => (DependencyCommand)GetValue(DefaultRxConfigCommandProperty);
            private set => SetValue(DefaultRxConfigCommandPropertyKey, value);
        }

        public AsyncDependencyCommand StartListenCommand
        {
            get => (AsyncDependencyCommand)GetValue(StartListenCommandProperty);
            private set => SetValue(StartListenCommandPropertyKey, value);
        }

        public AsyncDependencyCommand StopListenCommand
        {
            get => (AsyncDependencyCommand)GetValue(StopListenCommandProperty);
            private set => SetValue(StopListenCommandPropertyKey, value);
        }

        public ListenStates ListenState
        {
            get => (ListenStates)GetValue(ListenStateProperty);
            private set => SetValue(ListenStatePropertyKey, value);
        }


        public Nrf24TxConfigViewModel TxConfigViewModel
        {
            get => (Nrf24TxConfigViewModel)GetValue(TxConfigViewModelProperty);
            private set => SetValue(TxConfigViewModelPropertyKey, value);
        }

        public DependencyCommand DefaultTxConfigCommand
        {
            get => (DependencyCommand)GetValue(DefaultTxConfigCommandProperty);
            private set => SetValue(DefaultTxConfigCommandPropertyKey, value);
        }

        public DependencyCommand FillSendWithRandomCommand
        {
            get => (DependencyCommand)GetValue(FillSendWithRandomCommandProperty);
            private set => SetValue(FillSendWithRandomCommandPropertyKey, value);
        }

        public AsyncDependencyCommand SendDataCommand
        {
            get => (AsyncDependencyCommand)GetValue(SendDataCommandProperty);
            private set => SetValue(SendDataCommandPropertyKey, value);
        }

        public string? RawSendData
        {
            get => (string?)GetValue(RawSendDataProperty);
            set => SetValue(RawSendDataProperty, value);
        }

        public string? PreviewSendData
        {
            get => (string?)GetValue(PreviewSendDataProperty);
            private set => SetValue(PreviewSendDataPropertyKey, value);
        }

        public ReadOnlyCollection<byte>? SendData
        {
            get => (ReadOnlyCollection<byte>?)GetValue(SendDataProperty);
            private set => SetValue(SendDataPropertyKey, value);
        }


        public string ExchangeLog
        {
            get => (string)GetValue(ExchangeLogProperty);
            private set => SetValue(ExchangeLogPropertyKey, value);
        }

        public DependencyCommand ClearExchangeLogCommand
        {
            get => (DependencyCommand)GetValue(ClearExchangeLogCommandProperty);
            private set => SetValue(ClearExchangeLogCommandPropertyKey, value);
        }


        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            switch (e.Property.Name)
            {
                case nameof(RawSendData):
                    _storage[nameof(RawSendData)] = RawSendData;
                    UpdateSendData();
                    break;
            }

            base.OnPropertyChanged(e);
        }

        private void UpdateSendData()
        {
            if (string.IsNullOrWhiteSpace(RawSendData))
            {
                SendData = null;
                PreviewSendData = null;
            }
            else
            {
                var parseResults = __sendDataParser.Parse(RawSendData);

                if (parseResults.Count is 0 or > Nrf24Const.MaxDataLength)
                {
                    SendData = null;
                    PreviewSendData = null;
                }
                else
                {
                    SendData = parseResults.Select(container => ConvertParseValueContainer(container)).SelectMany(i => i).ToList().AsReadOnly();
                    PreviewSendData = parseResults.ConvertToStringWithSeparator();
                }
            }
        }

        private void LoadPropertyStates()
        {
            var rawSendDataStorageValue = _storage[nameof(RawSendData)];
            var configRawFrequencyChannelStorageValue = _storage[$"{nameof(ConfigViewModel)}.{nameof(Nrf24ConfigViewModel.RawFrequencyChannel)}"];
            var configRawAmpPowerStorageValue = _storage[$"{nameof(ConfigViewModel)}.{nameof(Nrf24ConfigViewModel.RawAmpPower)}"];
            var configRawDataRateStorageValue = _storage[$"{nameof(ConfigViewModel)}.{nameof(Nrf24ConfigViewModel.RawDataRate)}"];
            var configIsTxCheckedStorageValue = _storage[$"{nameof(ConfigViewModel)}.{nameof(Nrf24ConfigViewModel.IsTxChecked)}"];
            var configIsRxCheckedStorageValue = _storage[$"{nameof(ConfigViewModel)}.{nameof(Nrf24ConfigViewModel.IsRxChecked)}"];
            var txConfigTxAddressRawB0StorageValue = _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.TxAddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB0)}"];
            var txConfigTxAddressRawB1StorageValue = _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.TxAddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB1)}"];
            var txConfigTxAddressRawB2StorageValue = _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.TxAddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB2)}"];
            var txConfigTxAddressRawB3StorageValue = _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.TxAddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB3)}"];
            var txConfigTxAddressRawB4StorageValue = _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.TxAddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB4)}"];
            var txConfigRawRetrCountStorageValue = _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.RawRetrCount)}"];
            var txConfigRawRetrDelayStorageValue = _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.RawRetrDelay)}"];
            var rxConfigRx0AddressRawB0StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx0AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB0)}"];
            var rxConfigRx0AddressRawB1StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx0AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB1)}"];
            var rxConfigRx0AddressRawB2StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx0AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB2)}"];
            var rxConfigRx0AddressRawB3StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx0AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB3)}"];
            var rxConfigRx0AddressRawB4StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx0AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB4)}"];
            var rxConfigRx1AddressRawB0StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx1AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB0)}"];
            var rxConfigRx1AddressRawB1StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx1AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB1)}"];
            var rxConfigRx1AddressRawB2StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx1AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB2)}"];
            var rxConfigRx1AddressRawB3StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx1AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB3)}"];
            var rxConfigRx1AddressRawB4StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx1AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB4)}"];
            var rxConfigRx2SubaddressRawB4StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx2SubaddressViewModel)}.{nameof(Nrf24SubaddressViewModel.RawB0)}"];
            var rxConfigRx3SubaddressRawB4StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx3SubaddressViewModel)}.{nameof(Nrf24SubaddressViewModel.RawB0)}"];
            var rxConfigRx4SubaddressRawB4StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx4SubaddressViewModel)}.{nameof(Nrf24SubaddressViewModel.RawB0)}"];
            var rxConfigRx5SubaddressRawB4StorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx5SubaddressViewModel)}.{nameof(Nrf24SubaddressViewModel.RawB0)}"];
            var rxConfigIsRx0CheckedStorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx0Checked)}"];
            var rxConfigIsRx1CheckedStorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx1Checked)}"];
            var rxConfigIsRx2CheckedStorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx2Checked)}"];
            var rxConfigIsRx3CheckedStorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx3Checked)}"];
            var rxConfigIsRx4CheckedStorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx4Checked)}"];
            var rxConfigIsRx5CheckedStorageValue = _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx5Checked)}"];

            RawSendData = rawSendDataStorageValue;
            ConfigViewModel.RawFrequencyChannel = configRawFrequencyChannelStorageValue;
            ConfigViewModel.RawAmpPower = configRawAmpPowerStorageValue;
            ConfigViewModel.RawDataRate = configRawDataRateStorageValue;
            ConfigViewModel.IsTxChecked = configIsTxCheckedStorageValue == null ? null : configIsTxCheckedStorageValue == true.ToString();
            ConfigViewModel.IsRxChecked = configIsRxCheckedStorageValue == null ? null : configIsRxCheckedStorageValue == true.ToString();
            TxConfigViewModel.TxAddressViewModel.RawB0 = txConfigTxAddressRawB0StorageValue;
            TxConfigViewModel.TxAddressViewModel.RawB1 = txConfigTxAddressRawB1StorageValue;
            TxConfigViewModel.TxAddressViewModel.RawB2 = txConfigTxAddressRawB2StorageValue;
            TxConfigViewModel.TxAddressViewModel.RawB3 = txConfigTxAddressRawB3StorageValue;
            TxConfigViewModel.TxAddressViewModel.RawB4 = txConfigTxAddressRawB4StorageValue;
            TxConfigViewModel.RawRetrCount = txConfigRawRetrCountStorageValue;
            TxConfigViewModel.RawRetrDelay = txConfigRawRetrDelayStorageValue;
            RxConfigViewModel.Rx0AddressViewModel.RawB0 = rxConfigRx0AddressRawB0StorageValue;
            RxConfigViewModel.Rx0AddressViewModel.RawB1 = rxConfigRx0AddressRawB1StorageValue;
            RxConfigViewModel.Rx0AddressViewModel.RawB2 = rxConfigRx0AddressRawB2StorageValue;
            RxConfigViewModel.Rx0AddressViewModel.RawB3 = rxConfigRx0AddressRawB3StorageValue;
            RxConfigViewModel.Rx0AddressViewModel.RawB4 = rxConfigRx0AddressRawB4StorageValue;
            RxConfigViewModel.Rx1AddressViewModel.RawB0 = rxConfigRx1AddressRawB0StorageValue;
            RxConfigViewModel.Rx1AddressViewModel.RawB1 = rxConfigRx1AddressRawB1StorageValue;
            RxConfigViewModel.Rx1AddressViewModel.RawB2 = rxConfigRx1AddressRawB2StorageValue;
            RxConfigViewModel.Rx1AddressViewModel.RawB3 = rxConfigRx1AddressRawB3StorageValue;
            RxConfigViewModel.Rx1AddressViewModel.RawB4 = rxConfigRx1AddressRawB4StorageValue;
            RxConfigViewModel.Rx2SubaddressViewModel.RawB0 = rxConfigRx2SubaddressRawB4StorageValue;
            RxConfigViewModel.Rx3SubaddressViewModel.RawB0 = rxConfigRx3SubaddressRawB4StorageValue;
            RxConfigViewModel.Rx4SubaddressViewModel.RawB0 = rxConfigRx4SubaddressRawB4StorageValue;
            RxConfigViewModel.Rx5SubaddressViewModel.RawB0 = rxConfigRx5SubaddressRawB4StorageValue;
            RxConfigViewModel.IsRx0Checked = rxConfigIsRx0CheckedStorageValue == null ? null : rxConfigIsRx0CheckedStorageValue == true.ToString();
            RxConfigViewModel.IsRx1Checked = rxConfigIsRx1CheckedStorageValue == null ? null : rxConfigIsRx1CheckedStorageValue == true.ToString();
            RxConfigViewModel.IsRx2Checked = rxConfigIsRx2CheckedStorageValue == null ? null : rxConfigIsRx2CheckedStorageValue == true.ToString();
            RxConfigViewModel.IsRx3Checked = rxConfigIsRx3CheckedStorageValue == null ? null : rxConfigIsRx3CheckedStorageValue == true.ToString();
            RxConfigViewModel.IsRx4Checked = rxConfigIsRx4CheckedStorageValue == null ? null : rxConfigIsRx4CheckedStorageValue == true.ToString();
            RxConfigViewModel.IsRx5Checked = rxConfigIsRx5CheckedStorageValue == null ? null : rxConfigIsRx5CheckedStorageValue == true.ToString();
        }

        private void ObserveChildViewModelPropertyStates()
        {
            ConfigViewModel.PropertyChanged += (sender, e) =>
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24ConfigViewModel.RawFrequencyChannel):
                        _storage[$"{nameof(ConfigViewModel)}.{nameof(Nrf24ConfigViewModel.RawFrequencyChannel)}"] = ConfigViewModel.RawFrequencyChannel;
                        break;

                    case nameof(Nrf24ConfigViewModel.RawAmpPower):
                        _storage[$"{nameof(ConfigViewModel)}.{nameof(Nrf24ConfigViewModel.RawAmpPower)}"] = ConfigViewModel.RawAmpPower;
                        break;

                    case nameof(Nrf24ConfigViewModel.RawDataRate):
                        _storage[$"{nameof(ConfigViewModel)}.{nameof(Nrf24ConfigViewModel.RawDataRate)}"] = ConfigViewModel.RawDataRate;
                        break;

                    case nameof(Nrf24ConfigViewModel.IsTxChecked):
                        _storage[$"{nameof(ConfigViewModel)}.{nameof(Nrf24ConfigViewModel.IsTxChecked)}"] = ConfigViewModel.IsTxChecked?.ToString();
                        break;

                    case nameof(Nrf24ConfigViewModel.IsRxChecked):
                        _storage[$"{nameof(ConfigViewModel)}.{nameof(Nrf24ConfigViewModel.IsRxChecked)}"] = ConfigViewModel.IsRxChecked?.ToString();
                        break;
                }
            };

            TxConfigViewModel.TxAddressViewModel.PropertyChanged += (sender, e) =>
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24AddressViewModel.RawB0):
                        _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.TxAddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB0)}"] = TxConfigViewModel.TxAddressViewModel.RawB0;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB1):
                        _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.TxAddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB1)}"] = TxConfigViewModel.TxAddressViewModel.RawB1;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB2):
                        _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.TxAddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB2)}"] = TxConfigViewModel.TxAddressViewModel.RawB2;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB3):
                        _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.TxAddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB3)}"] = TxConfigViewModel.TxAddressViewModel.RawB3;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB4):
                        _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.TxAddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB4)}"] = TxConfigViewModel.TxAddressViewModel.RawB4;
                        break;
                }
            };

            TxConfigViewModel.PropertyChanged += (sender, e) =>
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24TxConfigViewModel.RawRetrCount):
                        _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.RawRetrCount)}"] = TxConfigViewModel.RawRetrCount;
                        break;

                    case nameof(Nrf24TxConfigViewModel.RawRetrDelay):
                        _storage[$"{nameof(TxConfigViewModel)}.{nameof(Nrf24TxConfigViewModel.RawRetrDelay)}"] = TxConfigViewModel.RawRetrDelay;
                        break;
                }
            };

            RxConfigViewModel.Rx0AddressViewModel.PropertyChanged += (sender, e) =>
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24AddressViewModel.RawB0):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx0AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB0)}"] = RxConfigViewModel.Rx0AddressViewModel.RawB0;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB1):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx0AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB1)}"] = RxConfigViewModel.Rx0AddressViewModel.RawB1;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB2):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx0AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB2)}"] = RxConfigViewModel.Rx0AddressViewModel.RawB2;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB3):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx0AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB3)}"] = RxConfigViewModel.Rx0AddressViewModel.RawB3;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB4):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx0AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB4)}"] = RxConfigViewModel.Rx0AddressViewModel.RawB4;
                        break;
                }
            };

            RxConfigViewModel.Rx1AddressViewModel.PropertyChanged += (sender, e) =>
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24AddressViewModel.RawB0):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx1AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB0)}"] = RxConfigViewModel.Rx1AddressViewModel.RawB0;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB1):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx1AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB1)}"] = RxConfigViewModel.Rx1AddressViewModel.RawB1;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB2):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx1AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB2)}"] = RxConfigViewModel.Rx1AddressViewModel.RawB2;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB3):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx1AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB3)}"] = RxConfigViewModel.Rx1AddressViewModel.RawB3;
                        break;

                    case nameof(Nrf24AddressViewModel.RawB4):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx1AddressViewModel)}.{nameof(Nrf24AddressViewModel.RawB4)}"] = RxConfigViewModel.Rx1AddressViewModel.RawB4;
                        break;
                }
            };

            RxConfigViewModel.Rx2SubaddressViewModel.PropertyChanged += (sender, e) =>
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24SubaddressViewModel.RawB0):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx2SubaddressViewModel)}.{nameof(Nrf24SubaddressViewModel.RawB0)}"] = RxConfigViewModel.Rx2SubaddressViewModel.RawB0;
                        break;
                }
            };

            RxConfigViewModel.Rx3SubaddressViewModel.PropertyChanged += (sender, e) =>
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24SubaddressViewModel.RawB0):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx3SubaddressViewModel)}.{nameof(Nrf24SubaddressViewModel.RawB0)}"] = RxConfigViewModel.Rx3SubaddressViewModel.RawB0;
                        break;
                }
            };

            RxConfigViewModel.Rx4SubaddressViewModel.PropertyChanged += (sender, e) =>
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24SubaddressViewModel.RawB0):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx4SubaddressViewModel)}.{nameof(Nrf24SubaddressViewModel.RawB0)}"] = RxConfigViewModel.Rx4SubaddressViewModel.RawB0;
                        break;
                }
            };

            RxConfigViewModel.Rx5SubaddressViewModel.PropertyChanged += (sender, e) =>
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24SubaddressViewModel.RawB0):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.Rx5SubaddressViewModel)}.{nameof(Nrf24SubaddressViewModel.RawB0)}"] = RxConfigViewModel.Rx5SubaddressViewModel.RawB0;
                        break;
                }
            };

            RxConfigViewModel.PropertyChanged += (sender, e) =>
            {
                switch (e.Property.Name)
                {
                    case nameof(Nrf24RxConfigViewModel.IsRx0Checked):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx0Checked)}"] = RxConfigViewModel.IsRx0Checked?.ToString();
                        break;

                    case nameof(Nrf24RxConfigViewModel.IsRx1Checked):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx1Checked)}"] = RxConfigViewModel.IsRx1Checked?.ToString();
                        break;

                    case nameof(Nrf24RxConfigViewModel.IsRx2Checked):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx2Checked)}"] = RxConfigViewModel.IsRx2Checked?.ToString();
                        break;

                    case nameof(Nrf24RxConfigViewModel.IsRx3Checked):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx3Checked)}"] = RxConfigViewModel.IsRx3Checked?.ToString();
                        break;

                    case nameof(Nrf24RxConfigViewModel.IsRx4Checked):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx4Checked)}"] = RxConfigViewModel.IsRx4Checked?.ToString();
                        break;

                    case nameof(Nrf24RxConfigViewModel.IsRx5Checked):
                        _storage[$"{nameof(RxConfigViewModel)}.{nameof(Nrf24RxConfigViewModel.IsRx5Checked)}"] = RxConfigViewModel.IsRx5Checked?.ToString();
                        break;
                }
            };
        }

        private void InitializeCommands()
        {
            ResetCommand = new AsyncDependencyCommand(OnResetCommand, e => ShowErrorService.Instance.ShowError(e, "NRF24 reset error"));

            DefaultConfigCommand = new DependencyCommand(() => ConfigViewModel.SetConfig(Nrf24Config.Default));
            SetConfigCommand = new AsyncDependencyCommand(OnSetConfigCommand, e => ShowErrorService.Instance.ShowError(e, "NRF24 set config error"));

            DefaultRxConfigCommand = new DependencyCommand(() => RxConfigViewModel.SetRxConfig(Nrf24RxConfig.Default));
            StartListenCommand = new AsyncDependencyCommand(OnStartListenCommand, e => ShowErrorService.Instance.ShowError(e, "NRF24 start listen error"));
            StopListenCommand = new AsyncDependencyCommand(OnStopListenCommand, e => ShowErrorService.Instance.ShowError(e, "NRF24 stop listen error"));

            DefaultTxConfigCommand = new DependencyCommand(() => TxConfigViewModel.SetTxConfig(Nrf24TxConfig.Default));
            FillSendWithRandomCommand = new DependencyCommand(OnFillSendWithRandomCommand);
            SendDataCommand = new AsyncDependencyCommand(OnSendDataCommand, e => ShowErrorService.Instance.ShowError(e, "NRF24 send error"));

            ClearExchangeLogCommand = new DependencyCommand(() => ExchangeLog = string.Empty) { AllowExecute = true };


            _ = BindingOperations.SetBinding(DefaultConfigCommand, DependencyCommand.AllowExecuteProperty, new Binding($"{nameof(AsyncDependencyCommand.IsExecuting)}")
            {
                Source = SetConfigCommand,
                Mode = BindingMode.OneWay,
                Converter = new IsValueEqualsToComparableConverter<bool>(false), /* DefaultConfigCommand active when SetConfigCommand is not executing. */
            });


            _ = BindingOperations.SetBinding(SetConfigCommand, DependencyCommand.AllowExecuteProperty, new Binding($"{nameof(Nrf24ConfigViewModel.Config)}")
            {
                Source = ConfigViewModel,
                Mode = BindingMode.OneWay,
                Converter = IsValueNotNullConverter.Instance, /* SetConfigCommand active when Nrf24ConfigViewModel.Config is not null. */
            });


            var defaultRxMultiBinding = new MultiBinding
            {
                Mode = BindingMode.OneWay,
                Converter = new IsValuesEqualsToComparableMultiConverter<bool>(false), /* DefaultRxConfigCommand is active if all next conditions are false: */
            };

            defaultRxMultiBinding.Bindings.Add(new Binding($"{nameof(AsyncDependencyCommand.IsExecuting)}") /* StartListenCommand is executing. */
            {
                Source = StartListenCommand,
                Mode = BindingMode.OneWay,
            });

            defaultRxMultiBinding.Bindings.Add(new Binding($"{nameof(AsyncDependencyCommand.IsExecuting)}") /* And StopListenCommand is executing. */
            {
                Source = StopListenCommand,
                Mode = BindingMode.OneWay,
            });

            _ = BindingOperations.SetBinding(DefaultRxConfigCommand, DependencyCommand.AllowExecuteProperty, defaultRxMultiBinding);


            var startListenMultiBinding = new MultiBinding
            {
                Mode = BindingMode.OneWay,
                Converter = new IsValuesEqualsToComparableMultiConverter<bool>(true), /* StartListenCommand is active if all next conditions are true: */
            };

            startListenMultiBinding.Bindings.Add(new Binding($"{nameof(Nrf24RxConfigViewModel.RxConfig)}")
            {
                Source = RxConfigViewModel,
                Mode = BindingMode.OneWay,
                Converter = IsValueNotNullConverter.Instance, /* RxConfig is not null. */
            });

            startListenMultiBinding.Bindings.Add(new Binding($"{nameof(ListenState)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
                Converter = new IsValueEqualsToComparableConverter<ListenStates>(ListenStates.Stopped), /* And ListenState is Stopped. */
            });

            _ = BindingOperations.SetBinding(StartListenCommand, DependencyCommand.AllowExecuteProperty, startListenMultiBinding);


            _ = BindingOperations.SetBinding(StopListenCommand, DependencyCommand.AllowExecuteProperty, new Binding($"{nameof(ListenState)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
                Converter = new IsValueEqualsToComparableConverter<ListenStates>(ListenStates.Started), /* StopListenCommand active when ListenState is Started. */
            });


            _ = BindingOperations.SetBinding(DefaultTxConfigCommand, DependencyCommand.AllowExecuteProperty, new Binding($"{nameof(AsyncDependencyCommand.IsExecuting)}")
            {
                Source = SendDataCommand,
                Mode = BindingMode.OneWay,
                Converter = new IsValueEqualsToComparableConverter<bool>(false), /* DefaultTxConfigCommand is active when SendCommand is not executing. */
            });


            var sendMultiBinding = new MultiBinding
            {
                Mode = BindingMode.OneWay,
                Converter = new IsValuesEqualsToComparableMultiConverter<bool>(true), /* SendDataCommand is active if next all conditions are true: */
            };

            sendMultiBinding.Bindings.Add(new Binding($"{nameof(Nrf24TxConfigViewModel.TxConfig)}")
            {
                Source = TxConfigViewModel,
                Mode = BindingMode.OneWay,
                Converter = IsValueNotNullConverter.Instance, /* TxConfig is not null. */
            });

            sendMultiBinding.Bindings.Add(new Binding($"{nameof(SendData)}")
            {
                Source = this,
                Mode = BindingMode.OneWay,
                Converter = IsValueNotNullConverter.Instance, /* And SendData is not null. */
            });

            _ = BindingOperations.SetBinding(SendDataCommand, DependencyCommand.AllowExecuteProperty, sendMultiBinding);
        }

        private void OnFillSendWithRandomCommand()
        {
            var random = new Random();

            var length = random.Next(1, Nrf24Const.MaxDataLength + 1);

            var array = new byte[length];

            random.NextBytes(array);

            var raw = string.Join(" ", array.Select(x => x.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader()));

            RawSendData = raw;

            AppendLogDateTime();
            AppendLogLine($" Random data for send: {raw} Length: {length}");
        }

        private async Task OnResetCommand()
        {
            ListenState = ListenStates.Transition;

            try
            {
                AppendLogDateTime();
                AppendLog(" Reset... ");

                _listenerCancellationTokenSource?.Cancel();
                _listener?.Dispose();

                await Model.Reset();

                AppendLogLine("Success.");

                ListenState = ListenStates.Stopped;
            }
            catch (Nrf24Exception ex)
            {
                AppendLogLine($"{nameof(Nrf24Exception)}: {ex.DeviceError}|{ex.Nrf24Error}");
                ListenState = ListenStates.Corrupted;

                throw;
            }
            catch (UniversalDeviceException ex)
            {
                AppendLogLine($"{nameof(UniversalDeviceException)}: {ex.DeviceError}");
                ListenState = ListenStates.Corrupted;

                throw;
            }
            catch (Exception ex)
            {
                AppendLogLine($"{nameof(Exception)}: {ex.Message}");
                ListenState = ListenStates.Corrupted;

                throw;
            }
        }

        private async Task OnSetConfigCommand()
        {
            Debug.Assert(ConfigViewModel.Config != null, "Config should not be null.");

            try
            {
                AppendLogDateTime();
                AppendLog(" Set config... ");

                await Model.SetConfig(ConfigViewModel.Config);

                AppendLogLine("Success.");
            }
            catch (Nrf24Exception ex)
            {
                AppendLogLine($"{nameof(Nrf24Exception)}: {ex.DeviceError}|{ex.Nrf24Error}");
                throw;
            }
            catch (UniversalDeviceException ex)
            {
                AppendLogLine($"{nameof(UniversalDeviceException)}: {ex.DeviceError}");
                throw;
            }
            catch (Exception ex)
            {
                AppendLogLine($"{nameof(Exception)}: {ex.Message}");
                throw;
            }
        }

        private async Task OnStartListenCommand()
        {
            Debug.Assert(RxConfigViewModel.RxConfig != null, "RxConfig should not be null.");
            Debug.Assert(ListenState == ListenStates.Stopped, "ListenState should be Stopped.");

            ListenState = ListenStates.Transition;

            try
            {
                AppendLogDateTime();
                AppendLog(" Start listen... ");

                _listener = await Model.StartListen(RxConfigViewModel.RxConfig);
                _listenerCancellationTokenSource = new CancellationTokenSource();

                Listen();

                AppendLogLine("Success.");

                ListenState = ListenStates.Started;
            }
            catch (Nrf24Exception ex)
            {
                AppendLogLine($"{nameof(Nrf24Exception)}: {ex.DeviceError}|{ex.Nrf24Error}");
                ListenState = ListenStates.Corrupted;

                throw;
            }
            catch (UniversalDeviceException ex)
            {
                AppendLogLine($"{nameof(UniversalDeviceException)}: {ex.DeviceError}");
                ListenState = ListenStates.Corrupted;

                throw;
            }
            catch (Exception ex)
            {
                AppendLogLine($"{nameof(Exception)}: {ex.Message}");
                ListenState = ListenStates.Corrupted;

                throw;
            }
        }

        private async Task OnStopListenCommand()
        {
            Debug.Assert(ListenState == ListenStates.Started, "ListenState should be Started.");

            ListenState = ListenStates.Transition;

            try
            {
                AppendLogDateTime();
                AppendLog(" Stop listen... ");

                _listenerCancellationTokenSource?.Cancel();
                _listener?.Dispose();

                await Model.StopListen();

                AppendLogLine("Success.");

                ListenState = ListenStates.Stopped;
            }
            catch (Nrf24Exception ex)
            {
                AppendLogLine($"{nameof(Nrf24Exception)}: {ex.DeviceError}|{ex.Nrf24Error}");
                ListenState = ListenStates.Corrupted;

                throw;
            }
            catch (UniversalDeviceException ex)
            {
                AppendLogLine($"{nameof(UniversalDeviceException)}: {ex.DeviceError}");
                ListenState = ListenStates.Corrupted;

                throw;
            }
            catch (Exception ex)
            {
                AppendLogLine($"{nameof(Exception)}: {ex.Message}");
                ListenState = ListenStates.Corrupted;

                throw;
            }
        }

        private async Task OnSendDataCommand()
        {
            Debug.Assert(TxConfigViewModel.TxConfig != null, "TxConfig should not be null.");
            Debug.Assert(SendData != null, "SendData should not be null.");

            try
            {
                AppendLogDateTime();
                AppendLog(" Send data... ");

                await Model.Send(TxConfigViewModel.TxConfig, SendData);

                AppendLogLine("Success.");
            }
            catch (Nrf24Exception ex)
            {
                AppendLogLine($"{nameof(Nrf24Exception)}: {ex.DeviceError}|{ex.Nrf24Error}");
                throw;
            }
            catch (UniversalDeviceException ex)
            {
                AppendLogLine($"{nameof(UniversalDeviceException)}: {ex.DeviceError}");
                throw;
            }
            catch (Exception ex)
            {
                AppendLogLine($"{nameof(Exception)}: {ex.Message}");
                throw;
            }
        }

        private void Listen()
        {
            var token = _listenerCancellationTokenSource!.Token;

            _ = Task.Run(() =>
              {
                  while (true)
                  {
                      token.ThrowIfCancellationRequested();

                      try
                      {
                          var rxDataTask = _listener!.ReadAsync(token);
                          rxDataTask.Wait();
                          var rxData = rxDataTask.Result;
                          var rxDataParsed = RxDataParser.TryParse(rxData, out var parseResult);

                          Application.Current.Dispatcher.Invoke(() =>
                          {
                              AppendLogDateTime();
                              AppendLogLine($" Rx: Raw: {string.Join(" ", rxData.Data.Select(x => x.ToString("X2", CultureInfo.InvariantCulture).AddHexHeader()))}" +
                                  $" Length: {rxData.Length} Address: {rxData.RxAddress} Pipe: {rxData.RxPipe}");

                              if (rxDataParsed)
                              {
                                  AppendLogLine(parseResult!);
                              }
                          });
                      }
                      catch (Exception ex)
                      {
                          if (!((ex is OperationCanceledException) || (ex is AggregateException aggregateEx && aggregateEx.InnerExceptions.Count == 1 && aggregateEx.InnerExceptions[0] is OperationCanceledException)))
                          {
                              Application.Current.Dispatcher.Invoke(() =>
                              {
                                  AppendLogDateTime();
                                  AppendLogLine($" Rx: {nameof(Exception)}: {ex.Message}");
                              });
                          }

                          token.ThrowIfCancellationRequested();
                          return;
                      }
                  }
              }, token);
        }

        private void AppendLog(string message) => ExchangeLog += message;

        private void AppendLogDateTime() => ExchangeLog += $"{DateTime.Now:dd.MM.yyyy HH:mm:ss.fff}";

        private void AppendLogLine(string message) => ExchangeLog += $"{message}{Environment.NewLine}";

        private static IEnumerable<byte> ConvertParseValueContainer(ParseValueContainer container)
        {
            switch (container.ValueType)
            {
                case ParseValueContainer.ValueTypes.U8:
                    {
                        yield return container.U8Value!.Value;
                    }
                    break;

                case ParseValueContainer.ValueTypes.U16:
                    {
                        var bytes = BitConverter.GetBytes(container.U16Value!.Value);
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                case ParseValueContainer.ValueTypes.U32:
                    {
                        var bytes = BitConverter.GetBytes(container.U32Value!.Value);
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                case ParseValueContainer.ValueTypes.U64:
                    {
                        var bytes = BitConverter.GetBytes(container.U64Value!.Value);
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                case ParseValueContainer.ValueTypes.String:
                    {
                        var bytes = Encoding.ASCII.GetBytes(container.String!);
                        foreach (var b in bytes)
                        {
                            yield return b;
                        }
                    }
                    break;

                default:
                    throw new ArgumentException("Unknown container value type.");
            }
        }


        public enum ListenStates
        {
            Stopped,
            Started,
            Transition,
            Corrupted,
        }


        public static class RxDataParser
        {
            public static bool TryParse(Nrf24RxData rxData, [NotNullWhen(true)] out string? result)
            {
                if (Message.TryParse(rxData.Data, out var message))
                {
                    var temp = new StringBuilder();
                    _ = temp.Append("Wireless thermometer sensor message parsed:");
                    _ = temp.Append(Environment.NewLine);
                    _ = temp.Append(message.Information.ToString());
                    _ = temp.Append(' ');
                    _ = temp.Append(message.Uid.ToString());
                    _ = temp.Append(Environment.NewLine);
                    _ = temp.Append($"UpTime: {message.UpTime} ");
                    _ = temp.Append($"Temperature: {(double)message.Temperature / 1000:F3} ");
                    _ = temp.Append($"Chip temperature: {(double)message.ChipTemperature / 1000:F3} ");
                    _ = temp.Append($"Chip voltage: {message.ChipVoltage / 1000}.{message.ChipVoltage % 1000} ");
                    _ = temp.Append($"Reset cause: {message.ResetCause}");

                    result = temp.ToString();
                    return true;
                }

                result = null;
                return false;
            }
        }
    }
}
