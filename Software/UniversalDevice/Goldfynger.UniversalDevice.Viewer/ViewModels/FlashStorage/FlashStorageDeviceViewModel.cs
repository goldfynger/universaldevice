﻿using System.Diagnostics;

using Goldfynger.UniversalDevice.FlashStorage;
using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.Services;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels.FlashStorage
{
    [DebuggerDisplay("FlashStorageDeviceViewModel: {Model.UniqueIdentifier}")]
    public class FlashStorageDeviceViewModel : DeviceInterfaceViewModel<FlashStorageDeviceModel>
    {
        public FlashStorageDeviceViewModel(FlashStorageDeviceModel model) : base(model)
        {
            Description = DescriptionService.Instance.DeviceInterfacesDescriptions[nameof(IFlashStorageDevice)];
        }
    }
}
