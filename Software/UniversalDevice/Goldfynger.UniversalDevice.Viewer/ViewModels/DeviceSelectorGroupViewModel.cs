﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows;

namespace Goldfynger.UniversalDevice.Viewer.ViewModels
{
    [DebuggerDisplay("DeviceSelectorGroupViewModel: {Description}|Count:{ItemsCollection.Count}")]
    public sealed class DeviceSelectorGroupViewModel : DependencyObject
    {
        public static readonly DependencyProperty TypeProperty =
            DependencyProperty.Register(nameof(Type), typeof(string), typeof(DeviceSelectorGroupViewModel), new PropertyMetadata());

        public static readonly DependencyProperty DisplayNameProperty =
            DependencyProperty.Register(nameof(DisplayName), typeof(string), typeof(DeviceSelectorGroupViewModel), new PropertyMetadata());

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register(nameof(SelectedItem), typeof(DeviceSelectorItemViewModel), typeof(DeviceSelectorGroupViewModel), new PropertyMetadata());

        public static readonly DependencyProperty ItemsCollectionProperty =
            DependencyProperty.Register(nameof(ItemsCollection), typeof(ObservableCollection<DeviceSelectorItemViewModel>), typeof(DeviceSelectorGroupViewModel),
                new PropertyMetadata { CoerceValueCallback = ItemsCollection_CoerceValueCallback });


        public DeviceSelectorGroupViewModel() => ItemsCollection = new ObservableCollection<DeviceSelectorItemViewModel>();


        private static object ItemsCollection_CoerceValueCallback(DependencyObject d, object baseValue)
        {
            var deviceSelectorGroupViewModel = (DeviceSelectorGroupViewModel)d;
            var newCollection = deviceSelectorGroupViewModel.ItemsCollection;

            return newCollection == null ? new ObservableCollection<DeviceSelectorItemViewModel>() : baseValue;
        }


        public string? Type
        {
            get => (string?)GetValue(TypeProperty);
            set => SetValue(TypeProperty, value);
        }

        public string? DisplayName
        {
            get => (string?)GetValue(DisplayNameProperty);
            set => SetValue(DisplayNameProperty, value);
        }

        public DeviceSelectorItemViewModel? SelectedItem
        {
            get => (DeviceSelectorItemViewModel?)GetValue(SelectedItemProperty);
            set => SetValue(SelectedItemProperty, value);
        }

        public ObservableCollection<DeviceSelectorItemViewModel> ItemsCollection
        {
            get => (ObservableCollection<DeviceSelectorItemViewModel>)GetValue(ItemsCollectionProperty);
            set => SetValue(ItemsCollectionProperty, value);
        }
    }
}
