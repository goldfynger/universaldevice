﻿namespace Goldfynger.UniversalDevice.Viewer.Storage
{
    public sealed class StorageItem
    {
        public int Id { get; set; }

        public string Key { get; set; } = null!;

        public string? Value { get; set; } = null;
    }
}
