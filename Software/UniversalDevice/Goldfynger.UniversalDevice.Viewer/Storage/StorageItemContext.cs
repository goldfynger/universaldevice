﻿using Microsoft.EntityFrameworkCore;

namespace Goldfynger.UniversalDevice.Viewer.Storage
{
    public sealed class StorageItemContext : DbContext
    {
        private readonly string _connectionString;


        public StorageItemContext(string connectionString) => _connectionString = connectionString;


        public DbSet<StorageItem> StorageItems { get; set; } = null!;


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseSqlite(_connectionString);
    }
}
