﻿using System.Collections.ObjectModel;

using Goldfynger.UniversalDevice.I2C;
using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.ViewModels.I2C;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime.I2C
{
    public class I2CDeviceDesignViewModel : I2CDeviceViewModel
    {
        public static readonly I2CDeviceDesignViewModel Instance = new();


        public I2CDeviceDesignViewModel() : base(new I2CDeviceModel(DesignTimeDevice.Instance, I2CPortNumbers.Port1))
        {
            RawAddress = "b1010000";
            RawAddressType = I2CAddress.Types.Wide7.ToString();
            RawSpeed = I2CSpeeds.Standart.ToString();
            RawReadLength = "10";
            ReadMemoryAddressInUse = true;
            RawReadMemoryAddress = "0x00";
            RawReadMemoryAddressSize = I2CMemoryAddress.Sizes.Size16.ToString();
            ReadData = new ReadOnlyCollection<byte>(new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            WriteMemoryAddressInUse = true;
            RawWriteMemoryAddress = "0x00";
            RawWriteMemoryAddressSize = I2CMemoryAddress.Sizes.Size16.ToString();
            RawWriteData = "0 1 2 3 4 5 6 7 8 9";
        }
    }
}
