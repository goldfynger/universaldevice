﻿using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.ViewModels;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime
{
    public class DeviceDesignViewModel : DeviceViewModel
    {
        public static readonly DeviceDesignViewModel Instance = new();


        public DeviceDesignViewModel() : base(new UniversalDeviceModel(DesignTimeDevice.Instance.Connector.Type, DesignTimeDevice.Instance.Connector.Identifier))
        {
        }
    }
}
