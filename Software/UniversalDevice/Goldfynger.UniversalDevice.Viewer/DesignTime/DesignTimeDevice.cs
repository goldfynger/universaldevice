﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

using Goldfynger.UniversalDevice.Connector;
using Goldfynger.UniversalDevice.FlashStorage;
using Goldfynger.UniversalDevice.I2C;
using Goldfynger.UniversalDevice.Info;
using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.Utils.Stm32.DeviceInfo;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime
{
    public sealed class DesignTimeDevice : IDevice
    {
        public static readonly DesignTimeDevice Instance = new();


        event EventHandler? IDevice.Corrupted { add { throw new NotImplementedException(); } remove { throw new NotImplementedException(); } }


        public IDeviceConnector Connector { get; } = DesignTimeDeviceConnector.Instance;

        public bool IsCorrupted => false;


        public IInfoDevice CreateDeviceInfo() => DesignTimeDeviceInfo.Instance;

        public II2CDevice CreateI2CDevice(I2CPortNumbers portNumber) => DesignTimeI2CDevice.Instance;

        public IFlashStorageDevice CreateFlashStorageDevice() => DesignTimeFlashStorageDevice.Instance;

        public INrf24Device CreateNrf24Device() => DesignTimeNrf24Device.Instance;

        public void Dispose() => throw new NotImplementedException();


        private sealed class DesignTimeDeviceConnector : IDeviceConnector
        {
            public static readonly DesignTimeDeviceConnector Instance = new();


            event EventHandler? IDeviceConnector.Corrupted { add { throw new NotImplementedException(); } remove { throw new NotImplementedException(); } }


            public IDeviceConnectorType Type { get; } = DesignTimeDeviceConnectorType.Instance;

            public IDeviceConnectorIdentifier Identifier { get; } = DesignTimeDeviceConnectorIdentifier.Instance;

            public ReadOnlyDictionary<string, string> Info => new(new Dictionary<string, string>());

            public bool IsCorrupted => false;


            public void Dispose() => throw new NotImplementedException();
        }

        private sealed record DesignTimeDeviceConnectorType : IDeviceConnectorType
        {
            public static readonly DesignTimeDeviceConnectorType Instance = new();


            public string Type => nameof(DesignTimeDeviceConnectorType);


            public bool Equals(IDeviceConnectorType? other) => Equals(other as object);
        }

        private sealed record DesignTimeDeviceConnectorIdentifier : IDeviceConnectorIdentifier
        {
            public static readonly DesignTimeDeviceConnectorIdentifier Instance = new();


            public string Identifier => nameof(DesignTimeDeviceConnectorIdentifier);

            public ReadOnlyDictionary<string, string> Info => throw new NotImplementedException();


            public bool Equals(IDeviceConnectorIdentifier? other) => Equals(other as object);
        }

        private sealed class DesignTimeDeviceInfo : IInfoDevice
        {
            public static readonly DesignTimeDeviceInfo Instance = new();


            public bool IsCorrupted => false;


            public Task<Information> GetInformationAsync() => throw new NotImplementedException();

            public Task<Uid> GetUidAsync() => throw new NotImplementedException();
        }

        private sealed class DesignTimeFlashStorageDevice : IFlashStorageDevice
        {
            public static readonly DesignTimeFlashStorageDevice Instance = new();


            public bool IsCorrupted => false;


            public Task CloseAsync() => throw new NotImplementedException();

            public Task<byte> GetRecordsCountAsync() => throw new NotImplementedException();

            public Task<byte> GetRecordSizeAsync(FlashStorageRecordName name) => throw new NotImplementedException();

            public Task<ReadOnlyCollection<FlashStorageRecordName>> GetRecordsNamesAsync() => throw new NotImplementedException();

            public Task OpenAsync() => throw new NotImplementedException();

            public Task<ReadOnlyCollection<byte>> ReadRecordAsync(FlashStorageRecordName name) => throw new NotImplementedException();

            public Task RemoveRecordAsync(FlashStorageRecordName name) => throw new NotImplementedException();

            public Task WriteRecordAsync(FlashStorageRecordName name, ReadOnlyCollection<byte> content) => throw new NotImplementedException();
        }

        private sealed class DesignTimeI2CDevice : II2CDevice
        {
            public static readonly DesignTimeI2CDevice Instance = new();


            public bool IsCorrupted => false;


            public Task AcquireAsync() => throw new NotImplementedException();

            public Task<bool> IsAcquiredAsync() => throw new NotImplementedException();

            public Task<IList<byte>> ReadAsync(I2CSpeeds speed, I2CAddress address, ushort size) => throw new NotImplementedException();

            public Task<IList<byte>> ReadMemoryAsync(I2CSpeeds speed, I2CAddress address, I2CMemoryAddress memoryAddress, ushort size) => throw new NotImplementedException();

            public Task ReleaseAsync() => throw new NotImplementedException();

            public Task WriteAsync(I2CSpeeds speed, I2CAddress address, IList<byte> data) => throw new NotImplementedException();

            public Task WriteMemoryAsync(I2CSpeeds speed, I2CAddress address, I2CMemoryAddress memoryAddress, IList<byte> data) => throw new NotImplementedException();
        }

        private sealed class DesignTimeNrf24Device : INrf24Device
        {
            public static readonly DesignTimeNrf24Device Instance = new();


            public bool IsCorrupted => false;


            public Task ResetAsync() => throw new NotImplementedException();

            public Task SetConfigAsync(Nrf24Config config) => throw new NotImplementedException();

            public Task SendAsync(Nrf24TxConfig txConfig, IList<byte> data) => throw new NotImplementedException();

            public Task StartListenAsync(Nrf24RxConfig rxConfig) => throw new NotImplementedException();

            public Task StopListenAsync() => throw new NotImplementedException();

            public Listener<Nrf24RxData> GetListener() => throw new NotImplementedException();
        }
    }
}
