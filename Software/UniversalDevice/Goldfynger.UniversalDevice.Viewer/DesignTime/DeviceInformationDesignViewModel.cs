﻿using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.ViewModels;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime
{
    public class DeviceInformationDesignViewModel : DeviceInformationViewModel
    {
        public static readonly DeviceInformationDesignViewModel Instance = new();


        public DeviceInformationDesignViewModel() : base(new DeviceInformationModel(DesignTimeDevice.Instance))
        {
        }
    }
}
