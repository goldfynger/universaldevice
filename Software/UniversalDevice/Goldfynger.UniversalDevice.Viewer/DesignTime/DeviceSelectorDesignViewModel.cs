﻿using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.ViewModels;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime
{
    public class DeviceSelectorDesignViewModel : DeviceSelectorViewModel
    {
        public static readonly DeviceSelectorDesignViewModel Instance = new();


        public DeviceSelectorDesignViewModel() : base(DeviceSelectorModel.Instance)
        {
        }
    }
}
