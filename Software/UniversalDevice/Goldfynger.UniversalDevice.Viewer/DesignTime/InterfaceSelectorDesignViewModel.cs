﻿using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.ViewModels;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime
{
    public class InterfaceSelectorDesignViewModel : InterfaceSelectorViewModel
    {
        public static readonly InterfaceSelectorDesignViewModel Instance = new();


        public InterfaceSelectorDesignViewModel() : base(new UniversalDeviceModel(DesignTimeDevice.Instance.Connector.Type, DesignTimeDevice.Instance.Connector.Identifier))
        {
        }
    }
}
