﻿using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime.Nrf24
{
    public class Nrf24AddressDesignViewModel : Nrf24AddressViewModel
    {
        public static readonly Nrf24AddressDesignViewModel TxAddressInstance = new();

        public static readonly Nrf24AddressDesignViewModel Rx0AddressInstance = new(Nrf24RxConfig.DefaultRx0Address);

        public static readonly Nrf24AddressDesignViewModel Rx1AddressInstance = new(Nrf24RxConfig.DefaultRx1Address);


        public Nrf24AddressDesignViewModel() => SetAddress(Nrf24TxConfig.DefaultTxAddress);

        private Nrf24AddressDesignViewModel(Nrf24Address address) => SetAddress(address);
    }
}
