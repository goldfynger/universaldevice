﻿using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime.Nrf24
{
    public class Nrf24TxConfigDesignViewModel : Nrf24TxConfigViewModel
    {
        public static readonly Nrf24TxConfigDesignViewModel Instance = new();


        public Nrf24TxConfigDesignViewModel() => SetTxConfig(Nrf24TxConfig.Default);
    }
}
