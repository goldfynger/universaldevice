﻿using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime.Nrf24
{
    public class Nrf24ConfigDesignViewModel : Nrf24ConfigViewModel
    {
        public static readonly Nrf24ConfigDesignViewModel Instance = new();


        public Nrf24ConfigDesignViewModel() => SetConfig(Nrf24Config.Default);
    }
}
