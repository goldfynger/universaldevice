﻿using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime.Nrf24
{
    public class Nrf24RxConfigDesignViewModel : Nrf24RxConfigViewModel
    {
        public static readonly Nrf24RxConfigDesignViewModel Instance = new();


        public Nrf24RxConfigDesignViewModel() => SetRxConfig(Nrf24RxConfig.Default);
    }
}
