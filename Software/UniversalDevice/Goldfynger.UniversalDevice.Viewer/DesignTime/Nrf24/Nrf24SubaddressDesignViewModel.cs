﻿using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime.Nrf24
{
    public class Nrf24SubaddressDesignViewModel : Nrf24SubaddressViewModel
    {
        public static readonly Nrf24SubaddressDesignViewModel Rx2SubaddressInstance = new();

        public static readonly Nrf24SubaddressDesignViewModel Rx3SubaddressInstance = new(Nrf24RxConfig.DefaultRx3Subaddress);

        public static readonly Nrf24SubaddressDesignViewModel Rx4SubaddressInstance = new(Nrf24RxConfig.DefaultRx4Subaddress);

        public static readonly Nrf24SubaddressDesignViewModel Rx5SubaddressInstance = new(Nrf24RxConfig.DefaultRx5Subaddress);


        public Nrf24SubaddressDesignViewModel()
        {
            SetBaseAddress(Nrf24RxConfig.DefaultRx1Address);
            SetSubaddress(Nrf24RxConfig.DefaultRx2Subaddress);
        }

        private Nrf24SubaddressDesignViewModel(byte subaddress)
        {
            SetBaseAddress(Nrf24RxConfig.DefaultRx1Address);
            SetSubaddress(subaddress);
        }
    }
}
