﻿using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.ViewModels.Nrf24;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime.Nrf24
{
    public class Nrf24DeviceDesignViewModel : Nrf24DeviceViewModel
    {
        public static readonly Nrf24DeviceDesignViewModel Instance = new();


        public Nrf24DeviceDesignViewModel() : base(new Nrf24DeviceModel(DesignTimeDevice.Instance))
        {
            ConfigViewModel.SetConfig(Nrf24Config.Default);
            RxConfigViewModel.SetRxConfig(Nrf24RxConfig.Default);
            TxConfigViewModel.SetTxConfig(Nrf24TxConfig.Default);
            StopListenCommand.AllowExecute = true;
            RawSendData = "0x00 0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09 0x0A 0x0B 0x0C 0x0D 0x0E 0x0F";
        }
    }
}
