﻿using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.ViewModels;

namespace Goldfynger.UniversalDevice.Viewer.DesignTime
{
    public class ConnectControlDesignViewModel : ConnectControlViewModel
    {
        public static readonly ConnectControlDesignViewModel Instance = new();


        public ConnectControlDesignViewModel() : base(new UniversalDeviceModel(DesignTimeDevice.Instance.Connector.Type, DesignTimeDevice.Instance.Connector.Identifier))
        {
        }
    }
}
