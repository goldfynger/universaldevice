﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Goldfynger.UniversalDevice.Viewer.Extensions
{
    public static class CollectionExtensions
    {
        /// <summary>
        /// Removes all elements from <see cref="ObservableCollection{T}"/> one-by-one.
        /// </summary>
        /// <typeparam name="T">Type of data stored in <see cref="ObservableCollection{T}"/>.</typeparam>
        /// <param name="collection">Collection to remove all data.</param>
        /// <remarks><see cref="ObservableCollection{T}"/> has method <see cref="ObservableCollection{T}.ClearItems"/> but that method cause collection reset (<see cref="NotifyCollectionChangedAction.Reset"/>).
        /// <see cref="NotifyCollectionChangedEventArgs"/> with <see cref="NotifyCollectionChangedAction.Reset"/> does not contain information about old items in <see cref="NotifyCollectionChangedEventArgs.OldItems"/>.
        /// Current extension method in fact calls <see cref="ObservableCollection{T}.CollectionChanged"/> for each removed item that contains in <see cref="NotifyCollectionChangedEventArgs.OldItems"/>.</remarks>
        public static void RemoveAll<T>(this ObservableCollection<T> collection)
        {
            while (collection.Count > 0)
            {
                collection.RemoveAt(collection.Count - 1);
            }
        }
    }
}
