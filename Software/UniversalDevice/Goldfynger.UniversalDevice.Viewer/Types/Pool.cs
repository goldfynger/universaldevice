﻿using System.Collections.Generic;

namespace Goldfynger.UniversalDevice.Viewer.Types
{
    public sealed class Pool<TKey, TValue> where TKey : class where TValue : class, new()
    {
        private readonly Dictionary<TKey, TValue> _dictionary = new();


        public TValue this[TKey key]
        {
            get
            {
                if (_dictionary.TryGetValue(key, out TValue? value))
                {
                    return value;
                }
                else
                {
                    var newValue = new TValue();

                    _dictionary.Add(key, newValue);

                    return newValue;
                }
            }
        }
    }
}
