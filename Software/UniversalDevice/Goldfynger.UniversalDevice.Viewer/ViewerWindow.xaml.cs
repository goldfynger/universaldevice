﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;

using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.Services;
using Goldfynger.UniversalDevice.Viewer.ViewModels;
using Goldfynger.UniversalDevice.Viewer.Views;
using Goldfynger.UniversalDevice.Viewer.WindowPlacement;
using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Wpf.Commands;

namespace Goldfynger.UniversalDevice.Viewer
{
    public partial class ViewerWindow : Window
    {
        private static readonly DependencyPropertyKey DeviceSelectorModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DeviceSelectorModel), typeof(DeviceSelectorModel), typeof(ViewerWindow),
                new PropertyMetadata());
        public static readonly DependencyProperty DeviceSelectorModelProperty = DeviceSelectorModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey DeviceSelectorViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DeviceSelectorViewModel), typeof(DeviceSelectorViewModel), typeof(ViewerWindow),
                new PropertyMetadata());
        public static readonly DependencyProperty DeviceSelectorViewModelProperty = DeviceSelectorViewModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey DeviceViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DeviceViewModel), typeof(DeviceViewModel), typeof(ViewerWindow),
                new PropertyMetadata(defaultValue: null));
        public static readonly DependencyProperty DeviceViewModelProperty = DeviceViewModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey DeviceViewPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(DeviceView), typeof(DeviceView), typeof(ViewerWindow),
                new PropertyMetadata(defaultValue: null));
        public static readonly DependencyProperty DeviceViewProperty = DeviceViewPropertyKey.DependencyProperty;

        public static readonly DependencyProperty ClearStorageCommandProperty =
            DependencyProperty.Register(nameof(ClearStorageCommand), typeof(DependencyCommand), typeof(ViewerWindow));

        /* Own ViewModel instance for each Model. */
        private readonly Dictionary<UniversalDeviceModel, DeviceViewModel> _deviceViewModelPool = new();

        /* Own View instance for each ViewModel. */
        private readonly Dictionary<DeviceViewModel, DeviceView> _deviceViewPool = new();

        private WindowPlacementObserverStorage? _windowPlacementStorage = null;


        public ViewerWindow()
        {
            InitializeComponent();

            SourceInitialized += ViewerWindow_SourceInitialized;
            Loaded += ViewerWindow_Loaded;
        }


        private void ViewerWindow_SourceInitialized(object? sender, EventArgs e)
        {
            _windowPlacementStorage = new WindowPlacementObserverStorage(this, nameof(ViewerWindow));

            _windowPlacementStorage.LoadPlacement();
        }

        private void ViewerWindow_Loaded(object? sender, RoutedEventArgs e)
        {
            DeviceSelectorModel = DeviceSelectorModel.Instance;
            DeviceSelectorViewModel = new DeviceSelectorViewModel(DeviceSelectorModel);

            ObserveDevicesCollection();

            ObserveSelectedDevice();

            InitializeCommands();
        }


        public DeviceSelectorModel DeviceSelectorModel
        {
            get => (DeviceSelectorModel)GetValue(DeviceSelectorModelProperty);
            private set => SetValue(DeviceSelectorModelPropertyKey, value);
        }

        public DeviceSelectorViewModel DeviceSelectorViewModel
        {
            get => (DeviceSelectorViewModel)GetValue(DeviceSelectorViewModelProperty);
            private set => SetValue(DeviceSelectorViewModelPropertyKey, value);
        }

        public DeviceViewModel? DeviceViewModel
        {
            get => (DeviceViewModel?)GetValue(DeviceViewModelProperty);
            private set => SetValue(DeviceViewModelPropertyKey, value);
        }

        public DeviceView? DeviceView
        {
            get => (DeviceView?)GetValue(DeviceViewProperty);
            private set => SetValue(DeviceViewPropertyKey, value);
        }

        public DependencyCommand ClearStorageCommand
        {
            get => (DependencyCommand)GetValue(ClearStorageCommandProperty);
            set => SetValue(ClearStorageCommandProperty, value);
        }


        private void ObserveDevicesCollection()
        {
            void addDeviceViewModelAndViewToPool(UniversalDeviceModel m)
            {
                var vm = new DeviceViewModel(m);
                var v = new DeviceView { DataContext = vm };

                _deviceViewModelPool.Add(m, vm);
                _deviceViewPool.Add(vm, v);
            };

            ((INotifyCollectionChanged)DeviceSelectorModel.DevicesCollection).CollectionChanged += (sender, e) =>
            {
                e.OldItems?.Cast<UniversalDeviceModel>().ForEach(m =>
                {
                    if (_deviceViewModelPool.Remove(m, out DeviceViewModel? vm))
                    {
                        _deviceViewPool.Remove(vm, out DeviceView? v);
                    }
                });

                e.NewItems?.Cast<UniversalDeviceModel>().ForEach(m => addDeviceViewModelAndViewToPool(m));
            };

            DeviceSelectorModel.DevicesCollection.ForEach(m => addDeviceViewModelAndViewToPool(m));
        }

        private void ObserveSelectedDevice()
        {
            void updateDeviceViewModelAndView()
            {
                if (DeviceSelectorModel.SelectedDevice != null)
                {
                    DeviceViewModel = _deviceViewModelPool[DeviceSelectorModel.SelectedDevice];
                    DeviceView = _deviceViewPool[DeviceViewModel];
                }
                else
                {
                    DeviceViewModel = null;
                    DeviceView = null;
                }
            };

            DependencyPropertyDescriptor.FromProperty(DeviceSelectorModel.SelectedDeviceProperty, typeof(DeviceSelectorModel)).
                AddValueChanged(DeviceSelectorModel.Instance, (sender, e) => updateDeviceViewModelAndView());

            updateDeviceViewModelAndView();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            /* Close all child windows. */
            foreach (Window window in Application.Current.Windows)
            {
                if (!ReferenceEquals(this, window))
                {
                    if (window is DeviceWindow deviceWindow)
                    {
                        deviceWindow.Dispose();
                    }
                    else
                    {
                        window.Close();
                    }
                }
            }

            base.OnClosed(e);
        }

        private void InitializeCommands() => ClearStorageCommand = new DependencyCommand(OnClearStorageCommand) { AllowExecute = true };

        private void OnClearStorageCommand() => StorageService.Instance.Clear();
    }
}
