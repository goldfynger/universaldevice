﻿using System;
using System.Collections.Generic;

using Goldfynger.UniversalDevice.FlashStorage;
using Goldfynger.UniversalDevice.I2C;
using Goldfynger.UniversalDevice.Info;
using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.Utils;

namespace Goldfynger.UniversalDevice.Viewer.Services
{
    public sealed class GuidService : SingletonBase<GuidService>
    {
        public GuidsStorage<string> DeviceInterfacesGuids { get; } = new GuidsStorage<string>(new Dictionary<string, Guid>
        {
            { nameof(IFlashStorageDevice), new Guid("A8C2903E-CCDB-425F-A852-5CD1266C68B6") },
            { nameof(II2CDevice), new Guid("9D305FE7-1D3D-400A-8705-133C854DD419") },
            { nameof(IInfoDevice), new Guid("85CEB236-05D6-40C0-951E-3B6DF5DE3D23") },
            { nameof(INrf24Device), new Guid("C376EE62-77C2-4E79-82F8-FF3FDB78B39B") },
        });

        public GuidsStorage<I2CPortNumbers> I2CDevicePortsGuids { get; } = new GuidsStorage<I2CPortNumbers>(new Dictionary<I2CPortNumbers, Guid>
        {
            { I2CPortNumbers.Control, new Guid("70702153-2294-4A13-9FC3-342FCB2C28EA") },
            { I2CPortNumbers.Port1, new Guid("7FA6EB27-66A3-4A43-A80D-8DD45FFB3D6C") },
            { I2CPortNumbers.Port2, new Guid("D9E92AF7-9450-4A18-BB8A-F3F3206C4E99") },
            { I2CPortNumbers.Port3, new Guid("A288A4A6-51A9-4D8F-B04A-37255C30957E") },
            { I2CPortNumbers.Port4, new Guid("09DB4091-E20C-41CA-B594-C062870381EC") },
            { I2CPortNumbers.Port5, new Guid("E0FB123F-4D12-41E9-AA91-325778D279B5") },
            { I2CPortNumbers.Port6, new Guid("5992E94D-5374-4729-9532-377FABA4D0E1") },
            { I2CPortNumbers.Port7, new Guid("6830E392-DC38-433A-B8C9-589B10858445") },
        });


        public sealed class GuidsStorage<TKey> where TKey : notnull
        {
            private readonly Dictionary<TKey, Guid> _dictionary;


            public GuidsStorage(IEnumerable<KeyValuePair<TKey, Guid>> values) => _dictionary = new Dictionary<TKey, Guid>(values);


            public Guid this[TKey key] => _dictionary[key];
        }
    }
}
