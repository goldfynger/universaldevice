﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Windows;

using Goldfynger.UniversalDevice.FlashStorage;
using Goldfynger.UniversalDevice.I2C;
using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.Utils;

namespace Goldfynger.UniversalDevice.Viewer.Services
{
    public sealed class ShowErrorService : SingletonBase<ShowErrorService>
    {
        public void ShowError(string message) => ShowError(message, "Error");

        [SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>")]
        public void ShowError(string message, string caption) => MessageBox.Show(message, caption, MessageBoxButton.OK, MessageBoxImage.Error);

        public void ShowError(Exception ex) => ShowError(ex, "Error");

        public void ShowError(Exception ex, string caption)
        {
            var message = new StringBuilder();

            message.Append(ex.GetType().Name);
            message.Append(Environment.NewLine);
            message.Append(GetExceptionMessage(ex));

            var innerEx = ex.InnerException;

            while (innerEx != null)
            {
                message.Append(Environment.NewLine);
                message.Append(GetExceptionMessage(innerEx));

                innerEx = innerEx.InnerException;
            }

            ShowError(message.ToString(), caption);
        }

        [SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>")]
        private string GetExceptionMessage(Exception ex)
        {
            return ex switch
            {
                FlashStorageException e => $"{e.DeviceError}|{e.FlashStorageError}",

                I2CHalException e => $"{e.DeviceError}|{e.I2CHalError}",

                Nrf24Exception e => $"{e.DeviceError}|{e.Nrf24Error}",

                UniversalDeviceException e => $"{e.DeviceError}",

                _ => ex.Message,
            };
        }
    }
}
