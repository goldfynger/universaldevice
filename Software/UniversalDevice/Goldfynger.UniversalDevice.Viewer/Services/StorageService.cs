﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;

using Goldfynger.UniversalDevice.Viewer.Storage;
using Goldfynger.Utils;

namespace Goldfynger.UniversalDevice.Viewer.Services
{
    public sealed class StorageService : SingletonBase<StorageService>
    {
        public const string DefaultStorageFileName = "storage.db";

        public static readonly string AppDataStorageBaseDirectoryPath;
        public static readonly string AppDataStorageDirectoryName;
        public static readonly string AppDataStorageFullDirectoryPath;
        public static readonly string AppDataStorageFileName;
        public static readonly string AppDataStorageFullFilePath;

        private static readonly DependencyObject __isInDesignModeObject = new();


        static StorageService()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            AppDataStorageBaseDirectoryPath = appDataPath;

            var executablePath = Environment.GetCommandLineArgs()[0];
            var executableName = Path.GetFileNameWithoutExtension(executablePath);
            AppDataStorageDirectoryName = executableName;

            AppDataStorageFullDirectoryPath = $"{AppDataStorageBaseDirectoryPath}\\{AppDataStorageDirectoryName}";

            AppDataStorageFileName = DefaultStorageFileName;

            AppDataStorageFullFilePath = $"{AppDataStorageFullDirectoryPath}\\{AppDataStorageFileName}";
        }

        public StorageService()
        {
            if (!Directory.Exists(AppDataStorageFullDirectoryPath))
            {
                Directory.CreateDirectory(AppDataStorageFullDirectoryPath);
            }

            using var dbContext = new StorageItemContext($"Data Source={AppDataStorageFullFilePath}");

            dbContext.Database.EnsureCreated();
        }


        public void SaveValue(string key, string? value)
        {
            if (DesignerProperties.GetIsInDesignMode(__isInDesignModeObject))
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Key can not be null or white space", nameof(key));
            }

            using var dbContext = new StorageItemContext($"Data Source={AppDataStorageFullFilePath}");

            var item = dbContext.StorageItems.FirstOrDefault(i => i.Key == key);

            if (item == null || item.Value != value)
            {
                if (item == null)
                {
                    dbContext.StorageItems.Add(new StorageItem { Key = key, Value = value });
                }
                else
                {
                    item.Value = value;
                }

                dbContext.SaveChanges();

                Trace.TraceInformation($"{nameof(StorageService)}.{nameof(SaveValue)} \"{key}\":\"{value}\".");
            }
        }

        [SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>")]
        public string? LoadValue(string key)
        {
            if (DesignerProperties.GetIsInDesignMode(__isInDesignModeObject))
            {
                return null;
            }

            if (string.IsNullOrWhiteSpace(key))
            {
                throw new ArgumentException("Key can not be null or white space", nameof(key));
            }

            using var dbContext = new StorageItemContext($"Data Source={AppDataStorageFullFilePath}");

            return dbContext.StorageItems.FirstOrDefault(i => i.Key == key)?.Value;
        }

        public void Clear()
        {
            if (DesignerProperties.GetIsInDesignMode(__isInDesignModeObject))
            {
                return;
            }

            using var dbContext = new StorageItemContext($"Data Source={AppDataStorageFullFilePath}");

            dbContext.StorageItems.RemoveRange(dbContext.StorageItems.ToArray());

            dbContext.SaveChanges();

            Trace.TraceInformation($"{nameof(StorageService)}.{nameof(Clear)}.");
        }

        public static string CreateKey(string typeName, string propertyName, string instanceName = "Static") => $"{typeName}.{propertyName}#{instanceName}";


        public sealed class InstanceStorage
        {
            /* Compiles and contains keys for saving values of properties of some object.
             * First part of key is name of type of object.
             * Second part of key is set of type instance keys. This set should be unique for each type and can be empty if type has only one instance.
             * Third part of key is name of type property which value need to save. */

            /// <summary>Key parts splitter.</summary>
            private const char __splitter = '#';

            /// <summary>First and second parts of key.</summary>
            private readonly string _commonKey;

            /// <summary><see cref="KeyValuePair{TKey, TValue}.Key"/> is name of property and <see cref="KeyValuePair{TKey, TValue}.Value"/> is full key for save or load property value.</summary>
            private readonly Dictionary<string, string> _keys = new();


            public InstanceStorage(string typeName, params string[] instanceKeys)
            {
                var builder = new StringBuilder();

                builder.Append(typeName);
                builder.Append(__splitter);

                foreach (var instanceKey in instanceKeys)
                {
                    builder.Append(instanceKey);
                    builder.Append(__splitter);
                }

                _commonKey = builder.ToString();
            }


            public string? this[string propertyName]
            {
                get => LoadValue(propertyName);
                set => SaveValue(propertyName, value);
            }


            public void SaveValue(string propertyName, string? value) => Instance.SaveValue(GetKey(propertyName), value);

            public string? LoadValue(string propertyName) => Instance.LoadValue(GetKey(propertyName));

            private string GetKey(string propertyName)
            {
                if (!_keys.TryGetValue(propertyName, out string? key))
                {
                    key = _commonKey + propertyName;
                    _keys.Add(propertyName, key);
                }

                return key;
            }
        }
    }
}
