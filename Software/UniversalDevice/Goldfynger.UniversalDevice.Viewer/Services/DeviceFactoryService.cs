﻿using System;
using System.Windows;
using System.Windows.Interop;

using Goldfynger.UniversalDevice.Connector;
using Goldfynger.Utils;

namespace Goldfynger.UniversalDevice.Viewer.Services
{
    public sealed class DeviceFactoryService : SingletonBase<DeviceFactoryService>, IDisposable
    {
        public DeviceFactoryService()
        {
            var handle = new WindowInteropHelper(Application.Current.MainWindow).Handle;
            HwndSource.FromHwnd(handle).AddHook(new HwndSourceHook(WndProc));

            DeviceConnectorObservableFactory = new DeviceConnectorObservableFactory(handle);
        }


        public IDeviceFactory DeviceFactory { get; } = new DeviceFactory();

        public DeviceConnectorObservableFactory DeviceConnectorObservableFactory { get; }


        private IntPtr WndProc(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            DeviceConnectorObservableFactory?.WndProc(msg, wParam);

            return IntPtr.Zero;
        }


        private bool _disposed;

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                    DeviceConnectorObservableFactory?.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                _disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~DeviceFactoryService()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
