﻿using System.Collections.Generic;

using Goldfynger.UniversalDevice.FlashStorage;
using Goldfynger.UniversalDevice.I2C;
using Goldfynger.UniversalDevice.Info;
using Goldfynger.UniversalDevice.Nrf24;
using Goldfynger.Utils;

namespace Goldfynger.UniversalDevice.Viewer.Services
{
    public sealed class DescriptionService : SingletonBase<DescriptionService>
    {
        public DescriptionsStorage<string> ConnectorTypesDescriptions { get; } = new DescriptionsStorage<string>(new Dictionary<string, string>
        {
            { "SerialConnector", "Serial Ports" },
            { "CustomHidConnector", "Custom Human Interface Devices" },
        });

        public DescriptionsStorage<string> DeviceInterfacesDescriptions { get; } = new DescriptionsStorage<string>(new Dictionary<string, string>
        {
            { nameof(IFlashStorageDevice), "Flash storage device" },
            { nameof(II2CDevice), "I2C device" },
            { nameof(IInfoDevice), "Device info" },
            { nameof(INrf24Device), "NRF24 device" },
        });

        public DescriptionsStorage<I2CPortNumbers> I2CDevicePortsDescriptions { get; } = new DescriptionsStorage<I2CPortNumbers>(new Dictionary<I2CPortNumbers, string>
        {
            { I2CPortNumbers.Control, "Control port" },
            { I2CPortNumbers.Port1, "Port 1" },
            { I2CPortNumbers.Port2, "Port 2" },
            { I2CPortNumbers.Port3, "Port 3" },
            { I2CPortNumbers.Port4, "Port 4" },
            { I2CPortNumbers.Port5, "Port 5" },
            { I2CPortNumbers.Port6, "Port 6" },
            { I2CPortNumbers.Port7, "Port 7" },
        });


        public sealed class DescriptionsStorage<TKey> where TKey : notnull
        {
            private readonly Dictionary<TKey, string> _dictionary;


            public DescriptionsStorage(IEnumerable<KeyValuePair<TKey, string>> values) => _dictionary = new Dictionary<TKey, string>(values);


            public string this[TKey key]
            {
                get
                {
                    if (_dictionary.TryGetValue(key, out string? value))
                    {
                        return value;
                    }
                    else
                    {
                        var result = key.ToString();
                        return result == null ? typeof(TKey).Name : result;
                    }
                }
            }
        }
    }
}
