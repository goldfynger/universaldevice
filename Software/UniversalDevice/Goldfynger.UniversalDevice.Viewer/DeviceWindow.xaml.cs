﻿using System.ComponentModel;
using System.Windows;

using Goldfynger.UniversalDevice.Viewer.Models;
using Goldfynger.UniversalDevice.Viewer.ViewModels;
using Goldfynger.UniversalDevice.Viewer.WindowPlacement;

namespace Goldfynger.UniversalDevice.Viewer
{
    public partial class DeviceWindow : Window
    {
        private static readonly DependencyPropertyKey ModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(Model), typeof(UniversalDeviceModel), typeof(DeviceWindow),
                new PropertyMetadata());
        public static readonly DependencyProperty ModelProperty = ModelPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey ViewModelPropertyKey =
            DependencyProperty.RegisterReadOnly(nameof(ViewModel), typeof(InterfaceSelectorViewModel), typeof(DeviceWindow),
                new PropertyMetadata());
        public static readonly DependencyProperty ViewModelProperty = ViewModelPropertyKey.DependencyProperty;

        private WindowPlacementObserverStorage? _windowPlacementStorage = null;

        private bool _dispose = false;


        public DeviceWindow(UniversalDeviceModel model)
        {
            InitializeComponent();

            Model = model;
            ViewModel = new InterfaceSelectorViewModel(model);

            Title = model.ConnectorIdentifier.ToString();

            SourceInitialized += DeviceWindow_SourceInitialized;
        }


        private void DeviceWindow_SourceInitialized(object? sender, System.EventArgs e)
        {
            _windowPlacementStorage = new WindowPlacementObserverStorage(this, nameof(DeviceWindow), Model.UniqueIdentifier);

            _windowPlacementStorage.LoadPlacement();
        }


        public UniversalDeviceModel Model
        {
            get => (UniversalDeviceModel)GetValue(ModelProperty);
            private set => SetValue(ModelPropertyKey, value);
        }

        public InterfaceSelectorViewModel ViewModel
        {
            get => (InterfaceSelectorViewModel)GetValue(ViewModelProperty);
            private set => SetValue(ViewModelPropertyKey, value);
        }


        protected override void OnClosing(CancelEventArgs e)
        {
            if (!_dispose)
            {
                e.Cancel = true;
                Hide();
            }
        }

        public void Dispose()
        {
            _dispose = true;

            Close();
        }
    }
}
