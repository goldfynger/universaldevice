#ifndef __UNIVERSAL_DEVICE_FRAGMENTATION_H
#define __UNIVERSAL_DEVICE_FRAGMENTATION_H


#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_FRAGMENTATION


#include "universal_device.h"


UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_FRAGMENTATION_Defragment (UNIVERSAL_DEVICE_MessageTypeDef *pMessageFragment,
                                                                         UNIVERSAL_DEVICE_MessageTypeDef **ppWholeMessage);
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_FRAGMENTATION_Fragment   (UNIVERSAL_DEVICE_MessageTypeDef *pWholeMessage,
                                                                         uint8_t **ppMessageFragmentsBuffer,
                                                                         uint16_t *pFragmentsCount);


#endif /* UNIVERSAL_DEVICE_CONFIG_FRAGMENTATION */

#endif /* __UNIVERSAL_DEVICE_FRAGMENTATION_H */
