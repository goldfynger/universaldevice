/*
Universal device base file.
All child interfaces functions should return errors only if their can not handle error by himself.
If error responding needed, child interfaces can use UNIVERSAL_DEVICE_ResponseWithError function.
If error is UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC child interface should use his own implementation of error response.
Generally, all pointers that used as function parameters must be freed in caller code.
*/

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "universal_device_config.h"
#include "universal_device_debug.h"
#include "universal_device_i2c.h"
#include "universal_device_info.h"
#include "universal_device_flash_storage.h"
#include "universal_device_fragmentation.h"
#include "universal_device_nrf24.h"
#include "universal_device_usart.h"
#include "universal_device.h"


uint32_t _initiativeMessageId = UNIVERSAL_DEVICE_INITIATIVE_MESSAGE_ID_MIN;

#ifdef UNIVERSAL_DEVICE_CONFIG_OS2
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"

static osMutexId_t _initiativeMessageIdMutexHandle;
static const osMutexAttr_t _initiativeMessageIdMutex_attributes = {
  .name = "initiativeMessageIdMutex"
};
#endif


static UNIVERSAL_DEVICE_ErrorTypeDef    UNIVERSAL_DEVICE_ProcessRequestControlMessage   (UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);
static const char *                     UNIVERSAL_DEVICE_InterfaceBaseToString          (UNIVERSAL_DEVICE_InterfaceBaseTypeDef interfaceBase);


UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_Initialise(void)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();

#ifdef UNIVERSAL_DEVICE_CONFIG_OS2   
    _initiativeMessageIdMutexHandle = osMutexNew(&_initiativeMessageIdMutex_attributes);

    if (_initiativeMessageIdMutexHandle == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_OS_OBJECT_CANNOT_BE_CREATED);
    }
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_INFO
    UNIVERSAL_DEVICE_ErrorTypeDef infoError = UNIVERSAL_DEVICE_ERR_OK;

    if ((infoError = UNIVERSAL_DEVICE_INFO_Initialise()) != UNIVERSAL_DEVICE_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(infoError);
    }
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_USART
    UNIVERSAL_DEVICE_ErrorTypeDef usartError = UNIVERSAL_DEVICE_ERR_OK;

    if ((usartError = UNIVERSAL_DEVICE_USART_Initialise()) != UNIVERSAL_DEVICE_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(usartError);
    }
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C
    UNIVERSAL_DEVICE_ErrorTypeDef i2cError = UNIVERSAL_DEVICE_ERR_OK;

    if ((i2cError = UNIVERSAL_DEVICE_I2C_Initialise()) != UNIVERSAL_DEVICE_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(i2cError);
    }
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_FLASH_STORAGE
    UNIVERSAL_DEVICE_ErrorTypeDef flashStorageError = UNIVERSAL_DEVICE_ERR_OK;

    if ((flashStorageError = UNIVERSAL_DEVICE_FLASH_STORAGE_Initialise()) != UNIVERSAL_DEVICE_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(flashStorageError);
    }
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_NRF24
    UNIVERSAL_DEVICE_ErrorTypeDef nrf24Error = UNIVERSAL_DEVICE_ERR_OK;

    if ((nrf24Error = UNIVERSAL_DEVICE_NRF24_Initialise()) != UNIVERSAL_DEVICE_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(nrf24Error);
    }
#endif

    return UNIVERSAL_DEVICE_ERR_OK;
}

/* pRequestMessage must be freed by calling code. Interface functions must use pRequestMessage only in main thread.  */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_ProcessRequestMessage(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();

    if (pRequestMessage == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_INVALID_PARAMETER);
    }

    /* Check message header. */

    uint16_t messageId = pRequestMessage->Header.MessageId;

    if (messageId > UNIVERSAL_DEVICE_MESSAGE_ID_MAX)
    {
        return UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_MESSAGE_ID);
    }

    uint16_t messageBodySize = pRequestMessage->Header.MessageBodySize;
    uint16_t fragmentBodySize = pRequestMessage->Header.FragmentBodySize;
    uint16_t fragmentBodyOffset = pRequestMessage->Header.FragmentBodyOffset;

    if ((messageBodySize > UNIVERSAL_DEVICE_MESSAGE_BODY_MAX_SIZE || messageBodySize % 4 != 0) ||
        (fragmentBodySize > UNIVERSAL_DEVICE_MESSAGE_BODY_MAX_SIZE || fragmentBodySize % 4 != 0) ||
        (fragmentBodyOffset + fragmentBodySize > messageBodySize || fragmentBodyOffset % 4 != 0))
    {
        return UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_MESSAGE_SIZE_OR_OFFSET);
    }


    UNIVERSAL_DEVICE_InterfaceBaseTypeDef interfaceBase = pRequestMessage->Header.InterfaceBase;

    UNIVERSAL_DEVICE_DEBUG_INFO("%s: interface %s (ID:%u).\r\n", __FUNCTION__, UNIVERSAL_DEVICE_InterfaceBaseToString(interfaceBase), messageId);


    UNIVERSAL_DEVICE_ErrorTypeDef (*pProcessRequestMessageFunction)(UNIVERSAL_DEVICE_MessageTypeDef *) = NULL;

    bool unknownInterface = false;

    switch (interfaceBase)
    {
        case UNIVERSAL_DEVICE_INTERFACE_BASE_CONTROL:
        {
            pProcessRequestMessageFunction = &UNIVERSAL_DEVICE_ProcessRequestControlMessage;            
        }
        break;

        case UNIVERSAL_DEVICE_INTERFACE_BASE_INFO:
        {
        #ifdef UNIVERSAL_DEVICE_CONFIG_INFO
            pProcessRequestMessageFunction = &UNIVERSAL_DEVICE_INFO_ProcessRequestMessage;
        #endif
        }
        break;

        case UNIVERSAL_DEVICE_INTERFACE_BASE_USART:
        {
        #ifdef UNIVERSAL_DEVICE_CONFIG_USART
            pProcessRequestMessageFunction = &UNIVERSAL_DEVICE_USART_ProcessRequestMessage;
        #endif
        }
        break;

        case UNIVERSAL_DEVICE_INTERFACE_BASE_I2C:
        {
        #ifdef UNIVERSAL_DEVICE_CONFIG_I2C
            pProcessRequestMessageFunction = &UNIVERSAL_DEVICE_I2C_ProcessRequestMessage;
        #endif
        }
        break;

        case UNIVERSAL_DEVICE_INTERFACE_BASE_FLASH_STORAGE:
        {
        #ifdef UNIVERSAL_DEVICE_CONFIG_FLASH_STORAGE
            pProcessRequestMessageFunction = &UNIVERSAL_DEVICE_FLASH_STORAGE_ProcessRequestMessage;
        #endif
        }

        case UNIVERSAL_DEVICE_INTERFACE_BASE_NRF24:
        {
        #ifdef UNIVERSAL_DEVICE_CONFIG_NRF24
            pProcessRequestMessageFunction = &UNIVERSAL_DEVICE_NRF24_ProcessRequestMessage;
        #endif
        }
        break;

        default:
        {
            unknownInterface = true;
        }
        break;
    }


    if (unknownInterface)
    {
        return UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_INTERFACE_UNKNOWN_OR_INVALID);
    }
    else if (pProcessRequestMessageFunction == NULL)
    {
        return UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_INTERFACE_NOT_AVAILABLE);
    }


#ifdef UNIVERSAL_DEVICE_CONFIG_FRAGMENTATION

    UNIVERSAL_DEVICE_MessageTypeDef *pWholeMessage = NULL;
    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;

    if ((error = UNIVERSAL_DEVICE_FRAGMENTATION_Defragment(pRequestMessage, &pWholeMessage)) != UNIVERSAL_DEVICE_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(error);
    }

    if (pWholeMessage == NULL) /* Whole message is not completed yet. Wait for new fragment. */
    {
        return UNIVERSAL_DEVICE_ERR_OK;
    }
    else
    {
        error = pProcessRequestMessageFunction(pWholeMessage);

        UNIVERSAL_DEVICE_CONFIG_FREE(pWholeMessage); /* Whole message pointer created in UNIVERSAL_DEVICE_FRAGMENTATION_Defragment and must be freed after use. */

        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(error);
    }

#else

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(pProcessRequestMessageFunction(pRequestMessage));

#endif
}

/* pResponseOrInitiativeMessage must be freed by calling code. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_ProcessResponseOrInitiativeMessage(UNIVERSAL_DEVICE_MessageTypeDef *pResponseOrInitiativeMessage)
{
#ifdef UNIVERSAL_DEVICE_CONFIG_FRAGMENTATION

    uint8_t *pMessageFragmentsBuffer = NULL;
    uint16_t fragmentsCount = 0;
    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_FRAGMENTATION_Fragment(pResponseOrInitiativeMessage, &pMessageFragmentsBuffer, &fragmentsCount);

    if (error != UNIVERSAL_DEVICE_ERR_OK)
    {
        return error;
    }

    for (uint16_t fragmentIdx = 0; fragmentIdx < fragmentsCount; fragmentIdx++)
    {
        size_t fragmentStart = fragmentIdx * UNIVERSAL_DEVICE_CONFIG_FRAGMENT_MAX_SIZE;

        UNIVERSAL_DEVICE_MessageTypeDef *pFragment = (UNIVERSAL_DEVICE_MessageTypeDef *)(((uint8_t *)pMessageFragmentsBuffer) + fragmentStart);

        UNIVERSAL_DEVICE_DEBUG_ASSERT(pFragment->Header.FragmentBodyOffset == fragmentStart - (UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE * fragmentIdx));
        UNIVERSAL_DEVICE_DEBUG_ASSERT(fragmentIdx == (fragmentsCount - 1) ||
            pFragment->Header.FragmentBodySize == (UNIVERSAL_DEVICE_CONFIG_FRAGMENT_MAX_SIZE - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE));
        UNIVERSAL_DEVICE_DEBUG_ASSERT(fragmentIdx != (fragmentsCount - 1) ||
            pFragment->Header.FragmentBodySize == (pFragment->Header.MessageBodySize - ((fragmentsCount - 1) * (UNIVERSAL_DEVICE_CONFIG_FRAGMENT_MAX_SIZE - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE))));

        if ((error = UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback(pFragment)) != UNIVERSAL_DEVICE_ERR_OK)
        {
            UNIVERSAL_DEVICE_CONFIG_FREE(pMessageFragmentsBuffer);

            return error;
        }
    }

    UNIVERSAL_DEVICE_CONFIG_FREE(pMessageFragmentsBuffer);

    return UNIVERSAL_DEVICE_ERR_OK;

#else

    return UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback(pResponseOrInitiativeMessage);

#endif
}

/* pRequestMessage must be freed by calling code. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_ResponseWithOk(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{
    UNIVERSAL_DEVICE_MessageTypeDef *pResponse = UNIVERSAL_DEVICE_CONFIG_MALLOC(sizeof(UNIVERSAL_DEVICE_MessageTypeDef));

    if (pResponse == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED);
    }

    memset(pResponse, 0, sizeof(UNIVERSAL_DEVICE_MessageTypeDef));

    pResponse->Header.MessageId = pRequestMessage->Header.MessageId;
    /* MessageBodySize is already 0. */
    /* FragmentBodyOffset is already 0. */
    /* FragmentBodySize is already 0. */
    pResponse->Header.InterfaceBase = pRequestMessage->Header.InterfaceBase;
    pResponse->Header.InterfacePort = pRequestMessage->Header.InterfacePort;
    pResponse->Header.InterfaceCommand = pRequestMessage->Header.InterfaceCommand;
    /* Error is already 0 (UNIVERSAL_DEVICE_ERR_OK). */

    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback(pResponse);

    UNIVERSAL_DEVICE_CONFIG_FREE(pResponse);

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(error);
}

/* pRequestMessage must be freed by calling code. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_ResponseWithError(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage, UNIVERSAL_DEVICE_ErrorTypeDef responseError)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(responseError);

    UNIVERSAL_DEVICE_MessageTypeDef *pErrorResponse = UNIVERSAL_DEVICE_CONFIG_MALLOC(sizeof(UNIVERSAL_DEVICE_MessageTypeDef));

    if (pErrorResponse == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED);
    }
    
    memset(pErrorResponse, 0, sizeof(UNIVERSAL_DEVICE_MessageTypeDef));

    pErrorResponse->Header.MessageId = pRequestMessage->Header.MessageId;
    /* MessageBodySize is already 0. */
    /* FragmentBodyOffset is already 0. */
    /* FragmentBodySize is already 0. */
    pErrorResponse->Header.InterfaceBase = pRequestMessage->Header.InterfaceBase;
    pErrorResponse->Header.InterfacePort = pRequestMessage->Header.InterfacePort;
    pErrorResponse->Header.InterfaceCommand = pRequestMessage->Header.InterfaceCommand;
    pErrorResponse->Header.Error = responseError;

    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback(pErrorResponse);

    UNIVERSAL_DEVICE_CONFIG_FREE(pErrorResponse);
    
    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(error);
}


size_t UNIVERSAL_DEVICE_IncreaseToMultipleOfFour(size_t value)
{
    size_t remainder = value % 4;
    if (remainder != 0)
    {
        value += 4 - remainder;
    }
    return value;
}

uint32_t UNIVERSAL_DEVICE_NextInitiativeMessageId(void)
{
#ifdef UNIVERSAL_DEVICE_CONFIG_OS2
    osStatus_t acquireResult = osExMutexAcquireWait(_initiativeMessageIdMutexHandle);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(acquireResult == osOK);
#endif

    if (_initiativeMessageId == UNIVERSAL_DEVICE_INITIATIVE_MESSAGE_ID_MAX)
    {
        _initiativeMessageId = UNIVERSAL_DEVICE_INITIATIVE_MESSAGE_ID_MIN;
    }
    else
    {
        _initiativeMessageId += 1;
    }

    uint32_t newId = _initiativeMessageId;

#ifdef UNIVERSAL_DEVICE_CONFIG_OS2    
    osStatus_t releaseResult = osMutexRelease(_initiativeMessageIdMutexHandle);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(releaseResult == osOK);
#endif

    return newId;
}


static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_ProcessRequestControlMessage(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{
    (void)pRequestMessage;

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}


#if (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_INFO)
#define __TO_STRING(x) (#x)
#define TO_STRING(x) __TO_STRING(x)

static const char * UNIVERSAL_DEVICE_InterfaceBaseToString(UNIVERSAL_DEVICE_InterfaceBaseTypeDef interfaceBase)
{
    static const UNIVERSAL_DEVICE_InterfaceBaseTypeDef pInterfaceBaseIndexList[] =
    {
        UNIVERSAL_DEVICE_INTERFACE_BASE_CONTROL,

        UNIVERSAL_DEVICE_INTERFACE_BASE_INFO,

        UNIVERSAL_DEVICE_INTERFACE_BASE_USART,

        UNIVERSAL_DEVICE_INTERFACE_BASE_I2C,

        UNIVERSAL_DEVICE_INTERFACE_BASE_FLASH_STORAGE,

        UNIVERSAL_DEVICE_INTERFACE_BASE_NRF24,

        __UNIVERSAL_DEVICE_INTERFACE_BASE_UNKNOWN,
    };    

    static const char *ssInterfaceList[] =
    {
        TO_STRING(UNIVERSAL_DEVICE_INTERFACE_BASE_CONTROL),

        TO_STRING(UNIVERSAL_DEVICE_INTERFACE_BASE_INFO),

        TO_STRING(UNIVERSAL_DEVICE_INTERFACE_BASE_USART),

        TO_STRING(UNIVERSAL_DEVICE_INTERFACE_BASE_I2C),

        TO_STRING(UNIVERSAL_DEVICE_INTERFACE_BASE_FLASH_STORAGE),

        TO_STRING(UNIVERSAL_DEVICE_INTERFACE_BASE_NRF24),

        TO_STRING(__UNIVERSAL_DEVICE_INTERFACE_BASE_UNKNOWN),
    };

    size_t interfaceBaseCount = sizeof(pInterfaceBaseIndexList) / sizeof(UNIVERSAL_DEVICE_InterfaceBaseTypeDef);

    uint8_t index;
    for (index = 0; index < interfaceBaseCount; index++)
    {
        if (pInterfaceBaseIndexList[index] == interfaceBase)
        {
            break;
        }
    }

    return ssInterfaceList[index];
}

#undef TO_STRING
#undef __TO_STRING
#endif /* (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_INFO) */


/* Function can be called directly only from UNIVERSAL_DEVICE_ProcessResponseOrInitiativeMessage or UNIVERSAL_DEVICE_ResponseWithError functions. pResponseOrInitiativeMessage must be freed by calling code. */
__weak UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback(UNIVERSAL_DEVICE_MessageTypeDef *pResponseOrInitiativeMessage)
{
    /* Response or initiative message weak callback. */
    /* Messages must be allocated and freed by calling code. */
    
    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}
