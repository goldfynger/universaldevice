/*
Processes UniversalDevice I2C HAL weak callbacks.
*/

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_I2C


#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include "universal_device.h"
#include "universal_device_debug.h"
#include "universal_device_i2c.h"

#ifdef UNIVERSAL_DEVICE_CONFIG_OS2
    #include "cmsis_os2.h"
    #include "cmsis_os2_ex.h"
#else
    #error "UNIVERSAL_DEVICE_CONFIG_OS2 must be defined in universal_device_config.h."
#endif

#ifdef STM32F103xB
    #include "stm32f1xx_hal.h"
#else
    #error "Device not specified."
#endif


#define UNIVERSAL_DEVICE_I2C_HAL_SEMAPHORE_WAIT_TIMEOUT_MS  ((uint32_t)1000) /* Bus operation wait timeout. */

#define UNIVERSAL_DEVICE_I2C_HAL_ERR_I2C_CODE_MASK          ((UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef)0x0000FFFF)
#define UNIVERSAL_DEVICE_I2C_HAL_ERR_STATUS_HAL_MASK        ((UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef)0x00FF0000)
#define UNIVERSAL_DEVICE_I2C_HAL_ERR_STATUS_OS_MASK         ((UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef)0xFF000000)

#define UNIVERSAL_DEVICE_I2C_HAL_HANDLE_INVALID             ((I2C_HandleTypeDef *)NULL)
#define UNIVERSAL_DEVICE_I2C_HAL_MUTEX_INVALID              ((osMutexId_t *)NULL)
#define UNIVERSAL_DEVICE_I2C_HAL_SEMAPHORE_INVALID          ((osSemaphoreId_t *)NULL)

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_1
    extern I2C_HandleTypeDef hi2c1;
    #define UNIVERSAL_DEVICE_I2C_HAL_1_HANDLE       (&hi2c1)
    extern osMutexId_t i2c1AccessMutexHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_1_MUTEX        (&i2c1AccessMutexHandle)
    extern osSemaphoreId_t i2c1WaitSemaphoreHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_1_SEMAPHORE    (&i2c1WaitSemaphoreHandle)
#else
    #define UNIVERSAL_DEVICE_I2C_HAL_1_HANDLE       UNIVERSAL_DEVICE_I2C_HAL_HANDLE_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_1_MUTEX        UNIVERSAL_DEVICE_I2C_HAL_MUTEX_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_1_SEMAPHORE    UNIVERSAL_DEVICE_I2C_HAL_SEMAPHORE_INVALID
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_2
    extern I2C_HandleTypeDef hi2c2;
    #define UNIVERSAL_DEVICE_I2C_HAL_2_HANDLE       (&hi2c2)
    extern osMutexId_t i2c2AccessMutexHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_2_MUTEX        (&i2c2AccessMutexHandle)
    extern osSemaphoreId_t i2c2WaitSemaphoreHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_2_SEMAPHORE    (&i2c2WaitSemaphoreHandle)
#else
    #define UNIVERSAL_DEVICE_I2C_HAL_2_HANDLE       UNIVERSAL_DEVICE_I2C_HAL_HANDLE_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_2_MUTEX        UNIVERSAL_DEVICE_I2C_HAL_MUTEX_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_2_SEMAPHORE    UNIVERSAL_DEVICE_I2C_HAL_SEMAPHORE_INVALID
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_3
    extern I2C_HandleTypeDef hi2c3;
    #define UNIVERSAL_DEVICE_I2C_HAL_3_HANDLE       (&hi2c3)
    extern osMutexId_t i2c3AccessMutexHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_3_MUTEX        (&i2c3AccessMutexHandle)
    extern osSemaphoreId_t i2c3WaitSemaphoreHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_3_SEMAPHORE    (&i2c3WaitSemaphoreHandle)
#else
    #define UNIVERSAL_DEVICE_I2C_HAL_3_HANDLE       UNIVERSAL_DEVICE_I2C_HAL_HANDLE_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_3_MUTEX        UNIVERSAL_DEVICE_I2C_HAL_MUTEX_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_3_SEMAPHORE    UNIVERSAL_DEVICE_I2C_HAL_SEMAPHORE_INVALID
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_4
    extern I2C_HandleTypeDef hi2c4;
    #define UNIVERSAL_DEVICE_I2C_HAL_4_HANDLE       (&hi2c4)
    extern osMutexId_t i2c4AccessMutexHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_4_MUTEX        (&i2c4AccessMutexHandle)
    extern osSemaphoreId_t i2c4WaitSemaphoreHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_4_SEMAPHORE    (&i2c4WaitSemaphoreHandle)
#else
    #define UNIVERSAL_DEVICE_I2C_HAL_4_HANDLE       UNIVERSAL_DEVICE_I2C_HAL_HANDLE_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_4_MUTEX        UNIVERSAL_DEVICE_I2C_HAL_MUTEX_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_4_SEMAPHORE    UNIVERSAL_DEVICE_I2C_HAL_SEMAPHORE_INVALID
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_5
    extern I2C_HandleTypeDef hi2c5;
    #define UNIVERSAL_DEVICE_I2C_HAL_5_HANDLE       (&hi2c5)
    extern osMutexId_t i2c5AccessMutexHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_5_MUTEX        (&i2c5AccessMutexHandle)
    extern osSemaphoreId_t i2c5WaitSemaphoreHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_5_SEMAPHORE    (&i2c5WaitSemaphoreHandle)
#else
    #define UNIVERSAL_DEVICE_I2C_HAL_5_HANDLE       UNIVERSAL_DEVICE_I2C_HAL_HANDLE_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_5_MUTEX        UNIVERSAL_DEVICE_I2C_HAL_MUTEX_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_5_SEMAPHORE    UNIVERSAL_DEVICE_I2C_HAL_SEMAPHORE_INVALID
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_6
    extern I2C_HandleTypeDef hi2c6;
    #define UNIVERSAL_DEVICE_I2C_HAL_6_HANDLE       (&hi2c6)
    extern osMutexId_t i2c6AccessMutexHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_6_MUTEX        (&i2c6AccessMutexHandle)
    extern osSemaphoreId_t i2c6WaitSemaphoreHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_6_SEMAPHORE    (&i2c6WaitSemaphoreHandle)
#else
    #define UNIVERSAL_DEVICE_I2C_HAL_6_HANDLE       UNIVERSAL_DEVICE_I2C_HAL_HANDLE_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_6_MUTEX        UNIVERSAL_DEVICE_I2C_HAL_MUTEX_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_6_SEMAPHORE    UNIVERSAL_DEVICE_I2C_HAL_SEMAPHORE_INVALID
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_7
    extern I2C_HandleTypeDef hi2c7;
    #define UNIVERSAL_DEVICE_I2C_HAL_7_HANDLE       (&hi2c7)
    extern osMutexId_t i2c7AccessMutexHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_7_MUTEX        (&i2c7AccessMutexHandle)
    extern osSemaphoreId_t i2c7WaitSemaphoreHandle;
    #define UNIVERSAL_DEVICE_I2C_HAL_7_SEMAPHORE    (&i2c7WaitSemaphoreHandle)
#else
    #define UNIVERSAL_DEVICE_I2C_HAL_7_HANDLE       UNIVERSAL_DEVICE_I2C_HAL_HANDLE_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_7_MUTEX        UNIVERSAL_DEVICE_I2C_HAL_MUTEX_INVALID
    #define UNIVERSAL_DEVICE_I2C_HAL_7_SEMAPHORE    UNIVERSAL_DEVICE_I2C_HAL_SEMAPHORE_INVALID
#endif

#define UNIVERSAL_DEVICE_I2C_HAL_GET_HANDLE_PTR(port)       ((const I2C_HandleTypeDef *)__pHandles[port - 1])
#define UNIVERSAL_DEVICE_I2C_HAL_GET_MUTEX_PTR(port)        ((const osMutexId_t *)__pMutexes[port - 1])
#define UNIVERSAL_DEVICE_I2C_HAL_GET_SEMAPHORE_PTR(port)    ((const osSemaphoreId_t *)__pSemaphores[port - 1])
#define UNIVERSAL_DEVICE_I2C_HAL_GET_HAL_ERRORS_PTR(port)   ((UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef *)&_pHalErrors[port - 1])
#define UNIVERSAL_DEVICE_I2C_HAL_GET_FLAGS_PTR(port)        ((UNIVERSAL_DEVICE_I2C_HAL_FlagTypeDef *)&_pFlags[port - 1])

#define UNIVERSAL_DEVICE_I2C_HAL_ERR_FROM_I2C_CODE(err)     ((UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef)(err & UNIVERSAL_DEVICE_I2C_HAL_ERR_I2C_CODE_MASK))
#define UNIVERSAL_DEVICE_I2C_HAL_ERR_FROM_STATUS_HAL(err)   ((UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef)((err << 16) & UNIVERSAL_DEVICE_I2C_HAL_ERR_STATUS_HAL_MASK))
#define UNIVERSAL_DEVICE_I2C_HAL_ERR_FROM_STATUS_OS(err)    ((UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef)((err << 24) & UNIVERSAL_DEVICE_I2C_HAL_ERR_STATUS_OS_MASK))


typedef enum
{
    UNIVERSAL_DEVICE_I2C_HAL_FLAG_NONE                      = ((uint8_t)0x00),
    
    UNIVERSAL_DEVICE_I2C_HAL_FLAG_TX_COMPLETED              = ((uint8_t)0x01),
    UNIVERSAL_DEVICE_I2C_HAL_FLAG_RX_COMPLETED              = ((uint8_t)0x02),
    
    UNIVERSAL_DEVICE_I2C_HAL_FLAG_RX_MEMORY_COMPLETED       = ((uint8_t)0x04),
    UNIVERSAL_DEVICE_I2C_HAL_FLAG_TX_MEMORY_COMPLETED       = ((uint8_t)0x08),
    
    UNIVERSAL_DEVICE_I2C_HAL_FLAG_ERROR                     = ((uint8_t)0x10),
    UNIVERSAL_DEVICE_I2C_HAL_FLAG_ABORT_COMPLETED           = ((uint8_t)0x20),

    UNIVERSAL_DEVICE_I2C_HAL_FLAG_SEMAPHORE_RELEASE_ERROR   = ((uint8_t)0x40),
}
UNIVERSAL_DEVICE_I2C_HAL_FlagTypeDef;

typedef enum
{
    UNIVERSAL_DEVICE_I2C_HAL_OPERATION_TX,
    UNIVERSAL_DEVICE_I2C_HAL_OPERATION_RX,
    UNIVERSAL_DEVICE_I2C_HAL_OPERATION_TX_MEMORY,
    UNIVERSAL_DEVICE_I2C_HAL_OPERATION_RX_MEMORY,
}
UNIVERSAL_DEVICE_I2C_HAL_OperationTypeDef;


static const I2C_HandleTypeDef *__pHandles[UNIVERSAL_DEVICE_I2C_MAX_PORT_NUMBER] =
{
    UNIVERSAL_DEVICE_I2C_HAL_1_HANDLE,
    UNIVERSAL_DEVICE_I2C_HAL_2_HANDLE,
    UNIVERSAL_DEVICE_I2C_HAL_3_HANDLE,
    UNIVERSAL_DEVICE_I2C_HAL_4_HANDLE,
    UNIVERSAL_DEVICE_I2C_HAL_5_HANDLE,
    UNIVERSAL_DEVICE_I2C_HAL_6_HANDLE,
    UNIVERSAL_DEVICE_I2C_HAL_7_HANDLE,
};

static const osMutexId_t *__pMutexes[UNIVERSAL_DEVICE_I2C_MAX_PORT_NUMBER] =
{
    UNIVERSAL_DEVICE_I2C_HAL_1_MUTEX,
    UNIVERSAL_DEVICE_I2C_HAL_2_MUTEX,
    UNIVERSAL_DEVICE_I2C_HAL_3_MUTEX,
    UNIVERSAL_DEVICE_I2C_HAL_4_MUTEX,
    UNIVERSAL_DEVICE_I2C_HAL_5_MUTEX,
    UNIVERSAL_DEVICE_I2C_HAL_6_MUTEX,
    UNIVERSAL_DEVICE_I2C_HAL_7_MUTEX,
};

static const osSemaphoreId_t *__pSemaphores[UNIVERSAL_DEVICE_I2C_MAX_PORT_NUMBER] =
{
    UNIVERSAL_DEVICE_I2C_HAL_1_SEMAPHORE,
    UNIVERSAL_DEVICE_I2C_HAL_2_SEMAPHORE,
    UNIVERSAL_DEVICE_I2C_HAL_3_SEMAPHORE,
    UNIVERSAL_DEVICE_I2C_HAL_4_SEMAPHORE,
    UNIVERSAL_DEVICE_I2C_HAL_5_SEMAPHORE,
    UNIVERSAL_DEVICE_I2C_HAL_6_SEMAPHORE,
    UNIVERSAL_DEVICE_I2C_HAL_7_SEMAPHORE,
};

static UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef    _pHalErrors[UNIVERSAL_DEVICE_I2C_MAX_PORT_NUMBER]    = {UNIVERSAL_DEVICE_I2C_HAL_ERR_NONE};
static UNIVERSAL_DEVICE_I2C_HAL_FlagTypeDef     _pFlags[UNIVERSAL_DEVICE_I2C_MAX_PORT_NUMBER]        = {UNIVERSAL_DEVICE_I2C_HAL_FLAG_NONE};


static UNIVERSAL_DEVICE_I2C_PortTypeDef         UNIVERSAL_DEVICE_I2C_HAL_GetPortForHandle   (I2C_HandleTypeDef *pHandle);
static void                                     UNIVERSAL_DEVICE_I2C_HAL_ReleaseSemaphore   (UNIVERSAL_DEVICE_I2C_PortTypeDef port);
static UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef    UNIVERSAL_DEVICE_I2C_HAL_ProcessTxRxCallback
    (UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint16_t memoryAddress, uint16_t memoryAddressSize, uint8_t *pData, uint16_t dataSize, UNIVERSAL_DEVICE_I2C_HAL_OperationTypeDef operation);


static UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef UNIVERSAL_DEVICE_I2C_HAL_ProcessTxRxCallback
    (UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint16_t memoryAddress, uint16_t memoryAddressSize, uint8_t *pData, uint16_t dataSize, UNIVERSAL_DEVICE_I2C_HAL_OperationTypeDef operation)
{
    UNIVERSAL_DEVICE_DEBUG_ASSERT(UNIVERSAL_DEVICE_I2C_IsPortAvailable(port));
    UNIVERSAL_DEVICE_DEBUG_ASSERT(UNIVERSAL_DEVICE_I2C_HAL_IsAcquired(port));
    UNIVERSAL_DEVICE_DEBUG_ASSERT(
        (operation != UNIVERSAL_DEVICE_I2C_HAL_OPERATION_TX_MEMORY && operation != UNIVERSAL_DEVICE_I2C_HAL_OPERATION_RX_MEMORY) ||
        memoryAddressSize == UNIVERSAL_DEVICE_I2C_MEM_ADDR_SIZE_8 || memoryAddressSize == UNIVERSAL_DEVICE_I2C_MEM_ADDR_SIZE_16);

    const I2C_HandleTypeDef *pHandle = UNIVERSAL_DEVICE_I2C_HAL_GET_HANDLE_PTR(port);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(pHandle != UNIVERSAL_DEVICE_I2C_HAL_HANDLE_INVALID);

    const osSemaphoreId_t *pSemaphore = UNIVERSAL_DEVICE_I2C_HAL_GET_SEMAPHORE_PTR(port);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(pSemaphore != UNIVERSAL_DEVICE_I2C_HAL_SEMAPHORE_INVALID);    

    UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef *pHalError = UNIVERSAL_DEVICE_I2C_HAL_GET_HAL_ERRORS_PTR(port);
    *pHalError = UNIVERSAL_DEVICE_I2C_HAL_ERR_NONE;

    UNIVERSAL_DEVICE_I2C_HAL_FlagTypeDef *pFlag = UNIVERSAL_DEVICE_I2C_HAL_GET_FLAGS_PTR(port);
    *pFlag = UNIVERSAL_DEVICE_I2C_HAL_FLAG_NONE;

    osStatus_t osStatus = osOK;
    while ((osStatus = osSemaphoreAcquireTry(*pSemaphore)) == osOK); /* Acquire all possible released semaphores while result is not osErrorResource. */

    if (osStatus != osErrorResource)
    {
        UNIVERSAL_DEVICE_DEBUG_ERROR("%s: OS error %d at line %u.\r\n", __FUNCTION__, osStatus, __LINE__);

        return *pHalError | UNIVERSAL_DEVICE_I2C_HAL_ERR_FROM_STATUS_OS(osStatus);
    }

    deviceAddress <<= 1;    
    memoryAddressSize = memoryAddressSize == UNIVERSAL_DEVICE_I2C_MEM_ADDR_SIZE_8 ? I2C_MEMADD_SIZE_8BIT : I2C_MEMADD_SIZE_16BIT;

    HAL_StatusTypeDef halStatus = HAL_OK;
    switch (operation)
    {
        case UNIVERSAL_DEVICE_I2C_HAL_OPERATION_TX:
            halStatus = HAL_I2C_Master_Transmit_IT((I2C_HandleTypeDef *)pHandle, deviceAddress, pData, dataSize);
        break;

        case UNIVERSAL_DEVICE_I2C_HAL_OPERATION_RX:
            halStatus = HAL_I2C_Master_Receive_IT((I2C_HandleTypeDef *)pHandle, deviceAddress, pData, dataSize);
        break;

        case UNIVERSAL_DEVICE_I2C_HAL_OPERATION_TX_MEMORY:
            halStatus = HAL_I2C_Mem_Write_IT((I2C_HandleTypeDef *)pHandle, deviceAddress, memoryAddress, memoryAddressSize, pData, dataSize);
        break;

        case UNIVERSAL_DEVICE_I2C_HAL_OPERATION_RX_MEMORY:
            halStatus = HAL_I2C_Mem_Read_IT((I2C_HandleTypeDef *)pHandle, deviceAddress, memoryAddress, memoryAddressSize, pData, dataSize);
        break;
    }
    
    if (halStatus != HAL_OK)
    {
        UNIVERSAL_DEVICE_DEBUG_ERROR("%s: HAL error %u at line %u.\r\n", __FUNCTION__, halStatus, __LINE__);
        
        return *pHalError | UNIVERSAL_DEVICE_I2C_HAL_ERR_FROM_STATUS_HAL(halStatus);
    }
    
    /* Wait for semaphore that can be released in one of callbacks. */
    osStatus = osSemaphoreAcquire(*pSemaphore, UNIVERSAL_DEVICE_I2C_HAL_SEMAPHORE_WAIT_TIMEOUT_MS);

    __memory_changed();

    *pHalError |= UNIVERSAL_DEVICE_I2C_HAL_ERR_FROM_STATUS_OS(osStatus);
    
    if (osStatus == osOK)
    {
        UNIVERSAL_DEVICE_DEBUG_ASSERT(*pFlag != UNIVERSAL_DEVICE_I2C_HAL_FLAG_NONE);
    }
    else if (osStatus == osErrorTimeout)
    {
        UNIVERSAL_DEVICE_DEBUG_INFO("%s: HAL executing timeout. Aborting...\r\n", __FUNCTION__);

        halStatus = HAL_I2C_Master_Abort_IT((I2C_HandleTypeDef *)pHandle, deviceAddress);

        __memory_changed();

        if (halStatus != HAL_OK)
        {
            UNIVERSAL_DEVICE_DEBUG_ERROR("%s: HAL executing abort failed. HAL error %u at line %u.\r\n", __FUNCTION__, halStatus, __LINE__);

            *pHalError |= UNIVERSAL_DEVICE_I2C_HAL_ERR_FROM_STATUS_HAL(halStatus);
        }
        else
        {
            UNIVERSAL_DEVICE_DEBUG_ASSERT(*pFlag & UNIVERSAL_DEVICE_I2C_HAL_FLAG_ABORT_COMPLETED);
        }
    }

    return *pHalError;
}


/* No debug calls due to using this function from I2C interrupts. */
static UNIVERSAL_DEVICE_I2C_PortTypeDef UNIVERSAL_DEVICE_I2C_HAL_GetPortForHandle(I2C_HandleTypeDef *pHandle)
{
    for (UNIVERSAL_DEVICE_I2C_PortTypeDef port = UNIVERSAL_DEVICE_I2C_PORT_1; port <= UNIVERSAL_DEVICE_I2C_MAX_PORT_NUMBER; port++)
    {
        if (pHandle == UNIVERSAL_DEVICE_I2C_HAL_GET_HANDLE_PTR(port))
        {
            return port;
        }
    }

    return __UNIVERSAL_DEVICE_I2C_PORT_UNKNOWN;
}

/* No debug calls due to using this function from I2C interrupts. */
static void UNIVERSAL_DEVICE_I2C_HAL_ReleaseSemaphore(UNIVERSAL_DEVICE_I2C_PortTypeDef port)
{
    osStatus_t osStatus = osSemaphoreRelease(*UNIVERSAL_DEVICE_I2C_HAL_GET_SEMAPHORE_PTR(port));
    switch (osStatus)
    {
        case osOK:
        break;
        
        default:
            *UNIVERSAL_DEVICE_I2C_HAL_GET_HAL_ERRORS_PTR(port) |= UNIVERSAL_DEVICE_I2C_HAL_ERR_FROM_STATUS_OS(osStatus);
            *UNIVERSAL_DEVICE_I2C_HAL_GET_FLAGS_PTR(port) |= UNIVERSAL_DEVICE_I2C_HAL_FLAG_SEMAPHORE_RELEASE_ERROR;
        break;
    }
}


/* Weak function. */
bool UNIVERSAL_DEVICE_I2C_HAL_IsAcquired(UNIVERSAL_DEVICE_I2C_PortTypeDef port)
{
    UNIVERSAL_DEVICE_DEBUG_ASSERT(UNIVERSAL_DEVICE_I2C_IsPortAvailable(port));
    const osMutexId_t *pMutex = UNIVERSAL_DEVICE_I2C_HAL_GET_MUTEX_PTR(port);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(pMutex != UNIVERSAL_DEVICE_I2C_HAL_MUTEX_INVALID);

    return !osExIsInISR() && osMutexGetOwner(*pMutex) == osThreadGetId();
}

/* Weak function. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_HAL_Acquire(UNIVERSAL_DEVICE_I2C_PortTypeDef port)
{
    UNIVERSAL_DEVICE_DEBUG_ASSERT(UNIVERSAL_DEVICE_I2C_IsPortAvailable(port));
    UNIVERSAL_DEVICE_DEBUG_ASSERT(!UNIVERSAL_DEVICE_I2C_HAL_IsAcquired(port));

    return (!osExIsInISR() && osMutexAcquireTry(*UNIVERSAL_DEVICE_I2C_HAL_GET_MUTEX_PTR(port)) == osOK) ? UNIVERSAL_DEVICE_ERR_OK : UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_CANNOT_BE_ACQUIRED;
}

/* Weak function. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_HAL_Release(UNIVERSAL_DEVICE_I2C_PortTypeDef port)
{
    UNIVERSAL_DEVICE_DEBUG_ASSERT(UNIVERSAL_DEVICE_I2C_IsPortAvailable(port));
    UNIVERSAL_DEVICE_DEBUG_ASSERT(UNIVERSAL_DEVICE_I2C_HAL_IsAcquired(port));

    return (!osExIsInISR() && osMutexRelease(*UNIVERSAL_DEVICE_I2C_HAL_GET_MUTEX_PTR(port)) == osOK) ? UNIVERSAL_DEVICE_ERR_OK : UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_CANNOT_BE_RELEASED;
}

/* Weak function. */
UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef UNIVERSAL_DEVICE_I2C_HAL_Read(UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint8_t *pData, uint16_t dataSize)
{
    return UNIVERSAL_DEVICE_I2C_HAL_ProcessTxRxCallback(port, deviceAddress, NULL, NULL, pData, dataSize, UNIVERSAL_DEVICE_I2C_HAL_OPERATION_RX);
}

/* Weak function. */
UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef UNIVERSAL_DEVICE_I2C_HAL_Write(UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint8_t *pData, uint16_t dataSize)
{
    return UNIVERSAL_DEVICE_I2C_HAL_ProcessTxRxCallback(port, deviceAddress, NULL, NULL, pData, dataSize, UNIVERSAL_DEVICE_I2C_HAL_OPERATION_TX);
}

/* Weak function. */
UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef UNIVERSAL_DEVICE_I2C_HAL_ReadMemory
    (UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint16_t memoryAddress, uint16_t memoryAddressSize, uint8_t *pData, uint16_t dataSize)
{
    return UNIVERSAL_DEVICE_I2C_HAL_ProcessTxRxCallback(port, deviceAddress, memoryAddress, memoryAddressSize, pData, dataSize, UNIVERSAL_DEVICE_I2C_HAL_OPERATION_RX_MEMORY);
}

/* Weak function. */
UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef UNIVERSAL_DEVICE_I2C_HAL_WriteMemory
    (UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint16_t memoryAddress, uint16_t memoryAddressSize, uint8_t *pData, uint16_t dataSize)
{
    return UNIVERSAL_DEVICE_I2C_HAL_ProcessTxRxCallback(port, deviceAddress, memoryAddress, memoryAddressSize, pData, dataSize, UNIVERSAL_DEVICE_I2C_HAL_OPERATION_TX_MEMORY);
}


/* Weak ISR function. */
void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    UNIVERSAL_DEVICE_I2C_PortTypeDef port = UNIVERSAL_DEVICE_I2C_HAL_GetPortForHandle(hi2c);

    *UNIVERSAL_DEVICE_I2C_HAL_GET_FLAGS_PTR(port) |= UNIVERSAL_DEVICE_I2C_HAL_FLAG_TX_COMPLETED;

    UNIVERSAL_DEVICE_I2C_HAL_ReleaseSemaphore(port);
}

/* Weak ISR function. */
void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    UNIVERSAL_DEVICE_I2C_PortTypeDef port = UNIVERSAL_DEVICE_I2C_HAL_GetPortForHandle(hi2c);
    
    *UNIVERSAL_DEVICE_I2C_HAL_GET_FLAGS_PTR(port) |= UNIVERSAL_DEVICE_I2C_HAL_FLAG_RX_COMPLETED;
    
    UNIVERSAL_DEVICE_I2C_HAL_ReleaseSemaphore(port);
}

/* Weak ISR function. */
void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    UNIVERSAL_DEVICE_I2C_PortTypeDef port = UNIVERSAL_DEVICE_I2C_HAL_GetPortForHandle(hi2c);

    *UNIVERSAL_DEVICE_I2C_HAL_GET_FLAGS_PTR(port) |= UNIVERSAL_DEVICE_I2C_HAL_FLAG_TX_MEMORY_COMPLETED;

    UNIVERSAL_DEVICE_I2C_HAL_ReleaseSemaphore(port);
}

/* Weak ISR function. */
void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    UNIVERSAL_DEVICE_I2C_PortTypeDef port = UNIVERSAL_DEVICE_I2C_HAL_GetPortForHandle(hi2c);

    *UNIVERSAL_DEVICE_I2C_HAL_GET_FLAGS_PTR(port) |= UNIVERSAL_DEVICE_I2C_HAL_FLAG_RX_MEMORY_COMPLETED;

    UNIVERSAL_DEVICE_I2C_HAL_ReleaseSemaphore(port);
}

/* Weak ISR function. */
void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c)
{
    UNIVERSAL_DEVICE_I2C_PortTypeDef port = UNIVERSAL_DEVICE_I2C_HAL_GetPortForHandle(hi2c);

    *UNIVERSAL_DEVICE_I2C_HAL_GET_HAL_ERRORS_PTR(port) |= UNIVERSAL_DEVICE_I2C_HAL_ERR_FROM_I2C_CODE(HAL_I2C_GetError(hi2c));
    *UNIVERSAL_DEVICE_I2C_HAL_GET_FLAGS_PTR(port) |= UNIVERSAL_DEVICE_I2C_HAL_FLAG_ERROR;

    UNIVERSAL_DEVICE_I2C_HAL_ReleaseSemaphore(port);
}

/* Weak ISR function. */
void HAL_I2C_AbortCpltCallback(I2C_HandleTypeDef *hi2c)
{
    UNIVERSAL_DEVICE_I2C_PortTypeDef port = UNIVERSAL_DEVICE_I2C_HAL_GetPortForHandle(hi2c);

    *UNIVERSAL_DEVICE_I2C_HAL_GET_FLAGS_PTR(port) |= UNIVERSAL_DEVICE_I2C_HAL_FLAG_ABORT_COMPLETED;
}


#endif /* UNIVERSAL_DEVICE_CONFIG_I2C */
