#ifndef __UNIVERSAL_DEVICE_H
#define __UNIVERSAL_DEVICE_H


#include <stddef.h>
#include <stdint.h>


#define UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE        (12U)
#define UNIVERSAL_DEVICE_MESSAGE_MAX_SIZE           (65532U)
#define UNIVERSAL_DEVICE_MESSAGE_BODY_MAX_SIZE      (UNIVERSAL_DEVICE_MESSAGE_MAX_SIZE - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE)

#define UNIVERSAL_DEVICE_MESSAGE_ID_MIN             ((uint16_t)0)
#define UNIVERSAL_DEVICE_MESSAGE_ID_MAX             ((uint16_t)INT16_MAX)
#define UNIVERSAL_DEVICE_INITIATIVE_MESSAGE_ID_MIN  ((uint16_t)INT16_MAX + 1)
#define UNIVERSAL_DEVICE_INITIATIVE_MESSAGE_ID_MAX  ((uint16_t)UINT16_MAX)


typedef enum
{
    UNIVERSAL_DEVICE_ERR_OK = ((uint8_t)    0x00),              /* No error. */
    
    UNIVERSAL_DEVICE_ERR_INVALID_PARAMETER,                     /* Invalid parameter. */

    UNIVERSAL_DEVICE_ERR_MESSAGE_ID,                            /* Request message has initiative message ID. */
    UNIVERSAL_DEVICE_ERR_MESSAGE_SIZE_OR_OFFSET,                /* Invalid message size or body offset or body size. */
    UNIVERSAL_DEVICE_ERR_MESSAGE_FRAGMENTATION,                 /* Fragmentation error. */

    UNIVERSAL_DEVICE_ERR_INTERFACE_NOT_INITIALISED,             /* Interface not initialised. */
    UNIVERSAL_DEVICE_ERR_INTERFACE_INITIALISATION,              /* Interface initialisation error. */
    UNIVERSAL_DEVICE_ERR_INTERFACE_UNKNOWN_OR_INVALID,          /* Interface unknown or invalid. */
    UNIVERSAL_DEVICE_ERR_INTERFACE_NOT_AVAILABLE,               /* Interface not available in current device. */
    UNIVERSAL_DEVICE_ERR_INTERFACE_BUSY,                        /* Interface busy and cannot process message. */
    
    UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC,                    /* Internal interface error must be specified in interface error response. */

    UNIVERSAL_DEVICE_ERR_PORT_UNKNOWN_OR_INVALID,               /* Port unknown or invalid. */
    UNIVERSAL_DEVICE_ERR_PORT_NOT_AVAILABLE,                    /* Port not available in current device. */

    UNIVERSAL_DEVICE_ERR_COMMAND_UNKNOWN_OR_INVALID,            /* Command unknown or invalid. */
    UNIVERSAL_DEVICE_ERR_COMMAND_INVALID_IN_CURRENT_CONTEXT,    /* Command cannot be executed in current context. */

    UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED,          /* Resource not acquired and cannot be used or released. */
    UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_ALREADY_ACQUIRED,      /* Resource already acquired and cannot be aquired again. */
    UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_CANNOT_BE_ACQUIRED,    /* Resource busy or inaccesible and cannot be aquired. */
    UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_CANNOT_BE_RELEASED,    /* Resource release error. */

    UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED,            /* Not enough memory for allocation. */
    UINVERSAL_DEVICE_ERR_OS_OBJECT_CANNOT_BE_CREATED,           /* OS kernel object creation error. */
    UNIVERSAL_DEVICE_ERR_OS_FUNCTION_FAIL,                      /* OS function executed with unexpected result. */
    
    UNIVERSAL_DEVICE_ERR_EXTERNAL_LIBRARY_SPECIFIC,             /* External library specific error. */

    UNIVERSAL_DEVICE_ERR_HAL,                                   /* Hardware abstraction level error. */

    UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED,      /* Feature not supported or not implemented yet. */
    
    UNIVERSAL_DEVICE_ERR_TIMEOUT,                               /* Timeout error. */

    __UNIVERSAL_DEVICE_ERR_UNKNOWN,                             /* Unknown error. Schould not be used directly. */
}
UNIVERSAL_DEVICE_ErrorTypeDef;

typedef enum
{
    UNIVERSAL_DEVICE_INTERFACE_BASE_CONTROL         = ((uint8_t)    0x00),

    UNIVERSAL_DEVICE_INTERFACE_BASE_INFO            = ((uint8_t)    0x10),

    UNIVERSAL_DEVICE_INTERFACE_BASE_USART           = ((uint8_t)    0x20), 

    UNIVERSAL_DEVICE_INTERFACE_BASE_I2C             = ((uint8_t)    0x28),

    UNIVERSAL_DEVICE_INTERFACE_BASE_FLASH_STORAGE   = ((uint8_t)    0x40),

    UNIVERSAL_DEVICE_INTERFACE_BASE_NRF24           = ((uint8_t)    0x60),
    
    /* Interfaces 0x7F..0xFE reserved for future use. */

    __UNIVERSAL_DEVICE_INTERFACE_BASE_UNKNOWN       = ((uint8_t)    0xFF),
}
UNIVERSAL_DEVICE_InterfaceBaseTypeDef;


typedef struct
{
    uint16_t                                MessageId;          /* Message identifier. All fragments of each fragmented message and request-response pairs have the same identifier. */
    uint16_t                                MessageBodySize;    /* Message body size of whole message. All fragments of each fragmented message have the same body size. */

    uint16_t                                FragmentBodyOffset; /* The offset of that part of the whole message body that is contained in this fragment. */
    uint16_t                                FragmentBodySize;   /* The size of that part of the whole message body that is contained in this fragment. Equals to MessageBodySize if message is not fragmented. */

    UNIVERSAL_DEVICE_InterfaceBaseTypeDef   InterfaceBase;
    uint8_t                                 InterfacePort;      /* Interface-specific field. Used in fragmentation mechanism. */
    uint8_t                                 InterfaceCommand;   /* Interface-specific field. Not used in fragmentation mechanism. */
    UNIVERSAL_DEVICE_ErrorTypeDef           Error;
}
UNIVERSAL_DEVICE_MessageHeaderTypeDef;      /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE (12) */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    uint8_t                                 Body[];
}
UNIVERSAL_DEVICE_MessageTypeDef;            /* UNIVERSAL_DEVICE_MESSAGE_EMPTY_SIZE..UNIVERSAL_DEVICE_MESSAGE_MAX_SIZE (12..65532) */


/* pRequestMessage, pResponseOrInitiativeMessage, units MUST be freed by calling code. */


UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_Initialise                             (void);
UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_ProcessRequestMessage                  (UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);
UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_ProcessResponseOrInitiativeMessage     (UNIVERSAL_DEVICE_MessageTypeDef *pResponseOrInitiativeMessage);
UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_ResponseWithOk                         (UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);
UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_ResponseWithError                      (UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage, UNIVERSAL_DEVICE_ErrorTypeDef responseError);

size_t                          UNIVERSAL_DEVICE_IncreaseToMultipleOfFour               (size_t value);
uint32_t                        UNIVERSAL_DEVICE_NextInitiativeMessageId                (void);

/* __weak functions. */
UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback    (UNIVERSAL_DEVICE_MessageTypeDef *pResponseOrInitiativeMessage);

/* Inline functions. */
inline size_t UNIVERSAL_DEVICE_GetMessageSize(UNIVERSAL_DEVICE_MessageTypeDef *pMessage) { return UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + pMessage->Header.FragmentBodySize; }


#endif /* __UNIVERSAL_DEVICE_H */
