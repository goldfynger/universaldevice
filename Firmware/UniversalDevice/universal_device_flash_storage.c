/*
Processes UniversalDevice FlashStorage messages synchronously in main thread.
*/

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_FLASH_STORAGE


#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "universal_device.h"

#include "universal_device_debug.h"
#include "universal_device_flash_storage.h"

#include "flash_storage.h"
#include "flash_storage_app.h"

#ifdef FLASH_STORAGE_CONFIG_OS2
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#endif


#define UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_NAMES_IN_MESSAGE         ((uint8_t)(UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE - 8) / sizeof(FLASH_STORAGE_RecordNameTypeDef))
#define UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE  ((uint8_t)(UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE - sizeof(FLASH_STORAGE_RecordNameTypeDef) - 8))


typedef enum
{
    /* Error: UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef */

    /* Request and response: UNIVERSAL_DEVICE_FLASH_STORAGE_OpenCloseRequestResponseTypeDef */
    UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_OPEN                 = ((uint8_t)    0x20),
    /* Request and response: UNIVERSAL_DEVICE_FLASH_STORAGE_OpenCloseRequestResponseTypeDef */
    UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_CLOSE                = ((uint8_t)    0x21),

    /* Request: UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsCountRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsCountResponseTypeDef */
    UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORDS_COUNT    = ((uint8_t)    0x30),
    /* Request: UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesResponseTypeDef */
    UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORDS_NAMES    = ((uint8_t)    0x31),
    /* Request: UNIVERSAL_DEVICE_FLASH_STORAGE_RecordSizeRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_FLASH_STORAGE_RecordSizeResponseTypeDef */
    UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORD_SIZE      = ((uint8_t)    0x32),

    /* Request: UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordResponseTypeDef */
    UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_READ_RECORD          = ((uint8_t)    0x40),
    /* Request: UNIVERSAL_DEVICE_FLASH_STORAGE_WriteRecordRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_FLASH_STORAGE_WriteRecordResponseTypeDef */
    UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_WRITE_RECORD         = ((uint8_t)    0x41),
    /* Request: UNIVERSAL_DEVICE_FLASH_STORAGE_RemoveRecordRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_FLASH_STORAGE_RemoveRecordResponseTypeDef */
    UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_REMOVE_RECORD        = ((uint8_t)    0x42),

    __UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_UNKNOWN            = ((uint8_t)    0xFF),
}
UNIVERSAL_DEVICE_FLASH_STORAGE_CommandTypeDef;


typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    FLASH_STORAGE_ErrorTypeDef              FlashStorageError;

    uint8_t                                 __Reserved[UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE - sizeof(FLASH_STORAGE_ErrorTypeDef)];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    uint8_t                                 __Reserved[UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_OpenCloseRequestResponseTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    uint8_t                                 __Reserved[UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsCountRequestTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    uint8_t                                 RecordsCount;

    uint8_t                                 __Reserved[UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE - sizeof(uint8_t)];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsCountResponseTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    uint8_t                                 __Reserved[UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesRequestTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    uint8_t                                 NamesCount;
    uint8_t                                 NamesOffset;
    uint8_t                                 __Reserved[6];

    FLASH_STORAGE_RecordNameTypeDef         Names[UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_NAMES_IN_MESSAGE];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesResponseTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    FLASH_STORAGE_RecordNameTypeDef         Name;

    uint8_t                                 __Reserved[UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE - sizeof(FLASH_STORAGE_RecordNameTypeDef)];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_RecordSizeRequestTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    FLASH_STORAGE_RecordNameTypeDef         Name;
    uint8_t                                 Size;

    uint8_t                                 __Reserved[UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE - sizeof(FLASH_STORAGE_RecordNameTypeDef) - sizeof(uint8_t)];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_RecordSizeResponseTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    FLASH_STORAGE_RecordNameTypeDef         Name;

    uint8_t                                 __Reserved[UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE - sizeof(FLASH_STORAGE_RecordNameTypeDef)];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordRequestTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    FLASH_STORAGE_RecordNameTypeDef         Name;

    uint8_t                                 ContentSize;
    uint8_t                                 ContentReadOffset;
    uint8_t                                 __Reserved[6];

    uint8_t                                 Content[UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordResponseTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    FLASH_STORAGE_RecordNameTypeDef         Name;

    uint8_t                                 ContentSize;
    uint8_t                                 ContentWriteOffset;
    uint8_t                                 __Reserved[6];

    uint8_t                                 Content[UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_WriteRecordRequestTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    FLASH_STORAGE_RecordNameTypeDef         Name;

    uint8_t                                 ContentSize;

    uint8_t                                 __Reserved[UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE - sizeof(FLASH_STORAGE_RecordNameTypeDef) - sizeof(uint8_t)];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_WriteRecordResponseTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    FLASH_STORAGE_RecordNameTypeDef         Name;

    uint8_t                                 __Reserved[UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE - sizeof(FLASH_STORAGE_RecordNameTypeDef)];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_RemoveRecordRequestTypeDef;

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    FLASH_STORAGE_RecordNameTypeDef         Name;

    uint8_t                                 __Reserved[UNIVERSAL_DEVICE_MESSAGE_BODY_SIZE - sizeof(FLASH_STORAGE_RecordNameTypeDef)];
}
UNIVERSAL_DEVICE_FLASH_STORAGE_RemoveRecordResponseTypeDef;


static bool                                                         _continuousWriteInProcess           = false;
static UNIVERSAL_DEVICE_FLASH_STORAGE_WriteRecordRequestTypeDef   * _pContinuousWritePreviousRequest    = NULL;
static uint8_t                                                    * _pContinuousWriteContent            = NULL;

#ifndef FLASH_STORAGE_CONFIG_OS2
static bool _isOpened = false;
#endif


static bool                             UNIVERSAL_DEVICE_FLASH_STORAGE_IsOpened         (void);
static UNIVERSAL_DEVICE_ErrorTypeDef    UNIVERSAL_DEVICE_FLASH_STORAGE_Open             (void);
static UNIVERSAL_DEVICE_ErrorTypeDef    UNIVERSAL_DEVICE_FLASH_STORAGE_Close            (void);

extern void                           * UNIVERSAL_DEVICE_CONFIG_MALLOC                  (size_t size);
extern void                             UNIVERSAL_DEVICE_CONFIG_FREE                    (void *ptr);


UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_FLASH_STORAGE_Initialise(void)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();

    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;
    
    FLASH_STORAGE_ErrorTypeDef flashStorageError = FLASH_STORAGE_ERR_OK;
    
    bool isInitialized = false;
    
    
    if ((error = UNIVERSAL_DEVICE_FLASH_STORAGE_Open()) != UNIVERSAL_DEVICE_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(error);
    }
    
    flashStorageError = FLASH_STORAGE_APP_IsInitialised(&isInitialized);
    
    if (flashStorageError == FLASH_STORAGE_ERR_OK && !isInitialized)
    {
        flashStorageError = FLASH_STORAGE_APP_Initialise();
    }

    if ((error = UNIVERSAL_DEVICE_FLASH_STORAGE_Close()) != UNIVERSAL_DEVICE_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(error);
    }
    
    return flashStorageError == FLASH_STORAGE_ERR_OK ? UNIVERSAL_DEVICE_ERR_OK : UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_INTERFACE_INITIALISATION);
}

UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_FLASH_STORAGE_ProcessMessage(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage, UNIVERSAL_DEVICE_MessageTypeDef **ppResponseMessages, uint8_t *pResponseCount)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();
    
    UNIVERSAL_DEVICE_DEBUG_ASSERT(pRequestMessage != NULL);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(ppResponseMessages != NULL);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(pResponseCount != NULL);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(pRequestMessage->Header.Interface == UNIVERSAL_DEVICE_INTERFACE_FLASH_STORAGE);
    
    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;
    
    FLASH_STORAGE_ErrorTypeDef flashStorageError = FLASH_STORAGE_ERR_OK;
    
    
    UNIVERSAL_DEVICE_FLASH_STORAGE_CommandTypeDef command = (UNIVERSAL_DEVICE_FLASH_STORAGE_CommandTypeDef)pRequestMessage->Header.Command;
    
    UNIVERSAL_DEVICE_DEBUG_INFO("%s: command 0x%02X.\r\n", __FUNCTION__, command);
    
    if (_continuousWriteInProcess)
    {
        bool needBreak = false;
        
        if (command != UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_WRITE_RECORD || pRequestMessage->Header.MessageId != _pContinuousWritePreviousRequest->Header.MessageId)
        {
            needBreak = true;
        }
        else
        {
            UNIVERSAL_DEVICE_FLASH_STORAGE_WriteRecordRequestTypeDef *pWriteRecordRequest = (UNIVERSAL_DEVICE_FLASH_STORAGE_WriteRecordRequestTypeDef *)pRequestMessage;
            
            if (memcmp(&pWriteRecordRequest->Name, &_pContinuousWritePreviousRequest->Name, sizeof(FLASH_STORAGE_RecordNameTypeDef)) != 0) /* Returns 0 (false) if equal. */
            {
                needBreak = true;
            }
            else
            {
                if (pWriteRecordRequest->ContentSize != _pContinuousWritePreviousRequest->ContentSize ||
                    pWriteRecordRequest->ContentWriteOffset != _pContinuousWritePreviousRequest->ContentWriteOffset + UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE)
                {
                    needBreak = true;
                }
            }
        }
        
        if (needBreak)            
        {
            UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)_pContinuousWritePreviousRequest; /* Use saved request message. */
                
            pErrorResponse->Header.Error = UNIVERSAL_DEVICE_ERR_COMMAND_INVALID_IN_CURRENT_CONTEXT;
            pErrorResponse->FlashStorageError = flashStorageError;
            
            _continuousWriteInProcess = false;
            UNIVERSAL_DEVICE_CONFIG_FREE(_pContinuousWriteContent);
            _pContinuousWriteContent = NULL;

            if ((error = UNIVERSAL_DEVICE_FLASH_STORAGE_InitiativeMessageCallback((UNIVERSAL_DEVICE_MessageTypeDef *)_pContinuousWritePreviousRequest, 1)) != UNIVERSAL_DEVICE_ERR_OK)
            {
                UNIVERSAL_DEVICE_CONFIG_FREE(_pContinuousWritePreviousRequest); /* Free saved request message. */
                _pContinuousWritePreviousRequest = NULL;
                
                return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(error);
            }
        }
        else
        {
            UNIVERSAL_DEVICE_CONFIG_FREE(_pContinuousWritePreviousRequest); /* Free saved request message. */
            _pContinuousWritePreviousRequest = NULL;
        }
    }
    
    switch (command)
    {
        case UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_OPEN:
        {
            pRequestMessage->Header.Error = UNIVERSAL_DEVICE_ERR_OK;
            
            if ((error = UNIVERSAL_DEVICE_FLASH_STORAGE_Open()) != UNIVERSAL_DEVICE_ERR_OK)
            {
                UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage; /* Reuse request message. */
                
                pErrorResponse->Header.Error = error;
                pErrorResponse->FlashStorageError = flashStorageError;
            }
            
            *ppResponseMessages = pRequestMessage; /* Reuse request message. */
            *pResponseCount = 1;
            
            return UNIVERSAL_DEVICE_ERR_OK;
        }
        
        
        case UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_CLOSE:
        {
            pRequestMessage->Header.Error = UNIVERSAL_DEVICE_ERR_OK;
            
            if ((error = UNIVERSAL_DEVICE_FLASH_STORAGE_Close()) != UNIVERSAL_DEVICE_ERR_OK)
            {
                UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                
                pErrorResponse->Header.Error = error;
                pErrorResponse->FlashStorageError = flashStorageError;
            }
            
            *ppResponseMessages = pRequestMessage; /* Reuse request message. */
            *pResponseCount = 1;
            
            return UNIVERSAL_DEVICE_ERR_OK;
        }
        
        
        case UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORDS_COUNT:
        {
            pRequestMessage->Header.Error = UNIVERSAL_DEVICE_ERR_OK;
            
            if (!UNIVERSAL_DEVICE_FLASH_STORAGE_IsOpened())
            {
                UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                
                pErrorResponse->Header.Error = UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED;
                pErrorResponse->FlashStorageError = flashStorageError;
            }
            else
            {
                uint_fast8_t recordsCount = 0;
                
                if ((flashStorageError = FLASH_STORAGE_APP_GetRecordsCount(&recordsCount)) != FLASH_STORAGE_ERR_OK)
                {
                    UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                    
                    pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_INTERFACE_INTERNAL;
                    pErrorResponse->FlashStorageError = flashStorageError;
                }
                else
                {
                    UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsCountResponseTypeDef *pRecordsCountResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsCountResponseTypeDef *)pRequestMessage;
                    
                    pRecordsCountResponse->RecordsCount = (uint8_t)recordsCount;
                }
            }
            
            *ppResponseMessages = pRequestMessage; /* Reuse request message. */
            *pResponseCount = 1;
            
            return UNIVERSAL_DEVICE_ERR_OK;
        }
        
        
        case UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORDS_NAMES:
        {
            pRequestMessage->Header.Error = UNIVERSAL_DEVICE_ERR_OK;
            
            if (!UNIVERSAL_DEVICE_FLASH_STORAGE_IsOpened())
            {
                UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                
                pErrorResponse->Header.Error = UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED;
                pErrorResponse->FlashStorageError = flashStorageError;
                
                *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                *pResponseCount = 1;
            }
            else
            {
                uint_fast8_t recordsCount = 0;
                
                if ((flashStorageError = FLASH_STORAGE_APP_GetRecordsCount(&recordsCount)) != FLASH_STORAGE_ERR_OK)
                {
                    UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                    
                    pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_INTERFACE_INTERNAL;
                    pErrorResponse->FlashStorageError = flashStorageError;
                    
                    *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                    *pResponseCount = 1;
                }
                else
                {
                    if (recordsCount == 0)
                    {
                        UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesResponseTypeDef *pRecordsNamesResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesResponseTypeDef *)pRequestMessage;
                        
                        pRecordsNamesResponse->NamesCount = 0;
                        pRecordsNamesResponse->NamesOffset = 0;
                        
                        *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                        *pResponseCount = 1;
                    }
                    else
                    {
                        FLASH_STORAGE_RecordNameTypeDef *pNames = UNIVERSAL_DEVICE_CONFIG_MALLOC(sizeof(FLASH_STORAGE_RecordNameTypeDef) * recordsCount);
                        
                        if (pNames == NULL)
                        {
                            UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                        
                            pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED;
                            pErrorResponse->FlashStorageError = flashStorageError;
                            
                            *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                            *pResponseCount = 1;
                        }
                        else
                        {
                            if ((flashStorageError = FLASH_STORAGE_APP_GetRecordsNames(pNames, recordsCount)) != FLASH_STORAGE_ERR_OK)
                            {
                                UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                                
                                pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_INTERFACE_INTERNAL;
                                pErrorResponse->FlashStorageError = flashStorageError;
                                
                                *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                                *pResponseCount = 1;
                            }
                            else
                            {
                                if (recordsCount <= UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_NAMES_IN_MESSAGE) /* Reuse request message. */
                                {                                
                                    UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesResponseTypeDef *pRecordsNamesResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesResponseTypeDef *)pRequestMessage;
                                    
                                    pRecordsNamesResponse->NamesCount = recordsCount;
                                    pRecordsNamesResponse->NamesOffset = 0;
                                    
                                    for (uint8_t idx = 0; idx < recordsCount; idx++)
                                    {
                                        memcpy(&pRecordsNamesResponse->Names[idx], &pNames[idx], sizeof(FLASH_STORAGE_RecordNameTypeDef));
                                    }
                                    
                                    *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                                    *pResponseCount = 1;
                                }
                                else /* recordsCount > UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_NAMES_IN_MESSAGE */
                                {
                                    uint8_t responseCount = 1;
                                    uint8_t namesCount = recordsCount;
                                    while (namesCount > UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_NAMES_IN_MESSAGE)
                                    {
                                        responseCount += 1;
                                        namesCount -= UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_NAMES_IN_MESSAGE;
                                    }
                                    
                                    
                                    UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesResponseTypeDef *pRecordsNamesResponses =
                                        UNIVERSAL_DEVICE_CONFIG_MALLOC(sizeof(UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesResponseTypeDef) * responseCount);
                        
                                    if (pRecordsNamesResponses == NULL)
                                    {
                                        UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                                    
                                        pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED;
                                        pErrorResponse->FlashStorageError = flashStorageError;
                                        
                                        *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                                        *pResponseCount = 1;
                                    }
                                    else
                                    {
                                        uint8_t recordsCountToTransmit = recordsCount;
                                        
                                        for (uint8_t responseIdx = 0; responseIdx < responseCount; responseIdx++)
                                        {
                                            UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesResponseTypeDef *pRecordsNamesResponse =
                                                ((UNIVERSAL_DEVICE_FLASH_STORAGE_RecordsNamesResponseTypeDef *)pRecordsNamesResponses) + responseIdx;
                                            
                                            pRecordsNamesResponse->NamesCount = recordsCount;
                                            pRecordsNamesResponse->NamesOffset = responseIdx * UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_NAMES_IN_MESSAGE;
                                            uint8_t recordsCountInResponse =
                                                recordsCountToTransmit > UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_NAMES_IN_MESSAGE ? UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_NAMES_IN_MESSAGE : recordsCountToTransmit;
                                            
                                            for (uint8_t idx = 0; idx < recordsCountInResponse; idx++)
                                            {
                                                memcpy(&pRecordsNamesResponse->Names[idx], &pNames[idx], sizeof(FLASH_STORAGE_RecordNameTypeDef));
                                            }
                                            
                                            recordsCountToTransmit -= recordsCountInResponse;
                                        }
                                        
                                        *ppResponseMessages = (UNIVERSAL_DEVICE_MessageTypeDef *)pRecordsNamesResponses; /* Use new request message. */
                                        *pResponseCount = responseCount;
                                        
                                        UNIVERSAL_DEVICE_CONFIG_FREE(pRequestMessage); /* Free request message. */
                                    }
                                }
                            }
                            
                            UNIVERSAL_DEVICE_CONFIG_FREE(pNames);
                        }
                    }
                }
            }
            
            return UNIVERSAL_DEVICE_ERR_OK;
        }
        
        
        case UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_GET_RECORD_SIZE:
        {
            pRequestMessage->Header.Error = UNIVERSAL_DEVICE_ERR_OK;
            
            if (!UNIVERSAL_DEVICE_FLASH_STORAGE_IsOpened())
            {
                UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                
                pErrorResponse->Header.Error = UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED;
                pErrorResponse->FlashStorageError = flashStorageError;
            }
            else
            {
                UNIVERSAL_DEVICE_FLASH_STORAGE_RecordSizeRequestTypeDef *pRecordSizeRequest = (UNIVERSAL_DEVICE_FLASH_STORAGE_RecordSizeRequestTypeDef *)pRequestMessage;
                                
                uint_fast8_t recordSize = 0;                
                
                if ((flashStorageError = FLASH_STORAGE_APP_GetRecordSize(&pRecordSizeRequest->Name, &recordSize)) != FLASH_STORAGE_ERR_OK)
                {
                    UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                    
                    pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_INTERFACE_INTERNAL;
                    pErrorResponse->FlashStorageError = flashStorageError;
                }
                else
                {
                    UNIVERSAL_DEVICE_FLASH_STORAGE_RecordSizeResponseTypeDef *pRecordSizeResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_RecordSizeResponseTypeDef *)pRequestMessage;
                    
                    pRecordSizeResponse->Size = (uint8_t)recordSize;
                }
            }
            
            *ppResponseMessages = pRequestMessage; /* Reuse request message. */
            *pResponseCount = 1;
            
            return UNIVERSAL_DEVICE_ERR_OK;
        }
        
        
        case UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_READ_RECORD:
        {
            pRequestMessage->Header.Error = UNIVERSAL_DEVICE_ERR_OK;
            
            if (!UNIVERSAL_DEVICE_FLASH_STORAGE_IsOpened())
            {
                UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                
                pErrorResponse->Header.Error = UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED;
                pErrorResponse->FlashStorageError = flashStorageError;
                
                *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                *pResponseCount = 1;
            }
            else
            {
                UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordRequestTypeDef *pReadRecordRequest = (UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordRequestTypeDef *)pRequestMessage;
                                
                uint_fast8_t recordSize = 0;                
                
                if ((flashStorageError = FLASH_STORAGE_APP_GetRecordSize(&pReadRecordRequest->Name, &recordSize)) != FLASH_STORAGE_ERR_OK)
                {
                    UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                    
                    pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_INTERFACE_INTERNAL;
                    pErrorResponse->FlashStorageError = flashStorageError;
                    
                    *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                    *pResponseCount = 1;
                }
                else
                {
                    uint8_t *pContent = UNIVERSAL_DEVICE_CONFIG_MALLOC(recordSize);
                    
                    if (pContent == NULL)
                    {
                        UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                    
                        pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED;
                        pErrorResponse->FlashStorageError = flashStorageError;
                        
                        *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                        *pResponseCount = 1;
                    }
                    else
                    {
                        uint_fast8_t contentSize = 0;  
                        
                        if ((flashStorageError = FLASH_STORAGE_APP_ReadRecord(&pReadRecordRequest->Name, pContent, (size_t)recordSize, &contentSize)) != FLASH_STORAGE_ERR_OK)
                        {
                            UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                            
                            pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_INTERFACE_INTERNAL;
                            pErrorResponse->FlashStorageError = flashStorageError;
                            
                            *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                            *pResponseCount = 1;
                        }
                        else
                        {
                            if (contentSize <= UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE) /* Reuse request message. */
                            {                                
                                UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordResponseTypeDef *pReadRecordResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordResponseTypeDef *)pRequestMessage;
                                
                                pReadRecordResponse->ContentSize = contentSize;
                                pReadRecordResponse->ContentReadOffset = 0;
                                
                                memcpy(&pReadRecordResponse->Content, pContent, contentSize);
                                
                                *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                                *pResponseCount = 1;
                            }
                            else /* contentSize > UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE */
                            {
                                uint8_t responseCount = 1;
                                uint8_t contentTempSize = contentSize;
                                while (contentTempSize > UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE)
                                {
                                    responseCount += 1;
                                    contentTempSize -= UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE;
                                }
                                
                                UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordResponseTypeDef *pReadRecordResponses =
                                    UNIVERSAL_DEVICE_CONFIG_MALLOC(sizeof(UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordResponseTypeDef) * responseCount);
                    
                                if (pReadRecordResponses == NULL)
                                {
                                    UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                                
                                    pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED;
                                    pErrorResponse->FlashStorageError = flashStorageError;
                                    
                                    *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                                    *pResponseCount = 1;
                                }
                                else
                                {
                                    uint8_t contentToTransmit = contentSize;
                                    
                                    for (uint8_t responseIdx = 0; responseIdx < responseCount; responseIdx++)
                                    {
                                        UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordResponseTypeDef *pReadRecordResponse =
                                            ((UNIVERSAL_DEVICE_FLASH_STORAGE_ReadRecordResponseTypeDef *)pReadRecordResponses) + responseIdx;
                                        
                                        memcpy(&pReadRecordResponse->Name, &pReadRecordRequest->Name, sizeof(FLASH_STORAGE_RecordNameTypeDef));
                                        pReadRecordResponse->ContentSize = contentSize;
                                        pReadRecordResponse->ContentReadOffset = responseIdx * UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE;
                                        uint8_t contentSizeInResponse =
                                            contentToTransmit > UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE ? UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE : contentToTransmit;
                                        
                                        memcpy(pReadRecordResponse->Content, pContent + pReadRecordResponse->ContentReadOffset, contentSizeInResponse);
                                        
                                        contentToTransmit -= contentSizeInResponse;
                                    }
                                    
                                    *ppResponseMessages = (UNIVERSAL_DEVICE_MessageTypeDef *)pReadRecordResponses; /* Use new request message. */
                                    *pResponseCount = responseCount;
                                    
                                    UNIVERSAL_DEVICE_CONFIG_FREE(pRequestMessage); /* Free request message. */
                                }
                            }
                        }
                        
                        UNIVERSAL_DEVICE_CONFIG_FREE(pContent);
                    }
                }
            }
            
            return UNIVERSAL_DEVICE_ERR_OK;
        }
        
        
        case UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_WRITE_RECORD:
        {
            pRequestMessage->Header.Error = UNIVERSAL_DEVICE_ERR_OK;
            
            if (!UNIVERSAL_DEVICE_FLASH_STORAGE_IsOpened())
            {
                UNIVERSAL_DEVICE_DEBUG_ASSERT(!_continuousWriteInProcess);
                
                UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                
                pErrorResponse->Header.Error = UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED;
                pErrorResponse->FlashStorageError = flashStorageError;
                
                *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                *pResponseCount = 1;
            }
            else
            {
                UNIVERSAL_DEVICE_FLASH_STORAGE_WriteRecordRequestTypeDef *pWriteRecordRequest = (UNIVERSAL_DEVICE_FLASH_STORAGE_WriteRecordRequestTypeDef *)pRequestMessage;
                
                if (_continuousWriteInProcess)
                {
                    UNIVERSAL_DEVICE_DEBUG_ASSERT(_pContinuousWritePreviousRequest == NULL);
                    UNIVERSAL_DEVICE_DEBUG_ASSERT(_pContinuousWriteContent != NULL);
                    
                    uint8_t remainingSize = pWriteRecordRequest->ContentSize - pWriteRecordRequest->ContentWriteOffset;
                    uint8_t copySize = remainingSize > UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE ? UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE : remainingSize;
                    
                    memcpy(_pContinuousWriteContent + pWriteRecordRequest->ContentWriteOffset, pWriteRecordRequest->Content, copySize);
                    
                    if (remainingSize > UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE)
                    {
                        _pContinuousWritePreviousRequest = pWriteRecordRequest;
                        
                        *pResponseCount = 0;
                    }
                    else
                    {
                        if ((flashStorageError = FLASH_STORAGE_APP_WriteRecord(&pWriteRecordRequest->Name, _pContinuousWriteContent, pWriteRecordRequest->ContentSize)) != FLASH_STORAGE_ERR_OK)
                        {
                            UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                            
                            pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_INTERFACE_INTERNAL;
                            pErrorResponse->FlashStorageError = flashStorageError;
                        }
                        
                        _continuousWriteInProcess = false;
                        UNIVERSAL_DEVICE_CONFIG_FREE(_pContinuousWriteContent);
                        _pContinuousWriteContent = NULL;
                        
                        *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                        *pResponseCount = 1;
                    }
                }
                else if (pWriteRecordRequest->ContentSize <= UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE)
                {
                    if ((flashStorageError = FLASH_STORAGE_APP_WriteRecord(&pWriteRecordRequest->Name, pWriteRecordRequest->Content, pWriteRecordRequest->ContentSize)) != FLASH_STORAGE_ERR_OK)
                    {
                        UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                        
                        pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_INTERFACE_INTERNAL;
                        pErrorResponse->FlashStorageError = flashStorageError;
                    }
                    
                    *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                    *pResponseCount = 1;
                }
                else /* pWriteRecordRequest->ContentSize > UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE */
                {
                    if (pWriteRecordRequest->ContentWriteOffset != 0)
                    {
                        UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                    
                        pErrorResponse->Header.Error = UNIVERSAL_DEVICE_ERR_INVALID_PARAMETER;
                        pErrorResponse->FlashStorageError = flashStorageError;
                        
                        *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                        *pResponseCount = 1;
                    }
                    else
                    {
                        _pContinuousWriteContent = UNIVERSAL_DEVICE_CONFIG_MALLOC(pWriteRecordRequest->ContentSize);
                        
                        if (_pContinuousWriteContent == NULL)
                        {
                            UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                        
                            pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED;
                            pErrorResponse->FlashStorageError = flashStorageError;
                            
                            *ppResponseMessages = pRequestMessage; /* Reuse request message. */
                            *pResponseCount = 1;
                        }
                        else
                        {
                            _continuousWriteInProcess = true;
                            _pContinuousWritePreviousRequest = pWriteRecordRequest;
                            
                            memcpy(_pContinuousWriteContent, pWriteRecordRequest->Content, UNIVERSAL_DEVICE_FLASH_STORAGE_MAX_CONTENT_SIZE_IN_MESSAGE);
                            
                            *pResponseCount = 0;
                        }
                    }
                }
            }
            
            return UNIVERSAL_DEVICE_ERR_OK;
        }
        
        
        case UNIVERSAL_DEVICE_FLASH_STORAGE_CMD_REMOVE_RECORD:
        {
            pRequestMessage->Header.Error = UNIVERSAL_DEVICE_ERR_OK;
            
            if (!UNIVERSAL_DEVICE_FLASH_STORAGE_IsOpened())
            {
                UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                
                pErrorResponse->Header.Error = UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED;
                pErrorResponse->FlashStorageError = flashStorageError;
            }
            else
            {
                UNIVERSAL_DEVICE_FLASH_STORAGE_RemoveRecordRequestTypeDef *pRemoveRecordRequest = (UNIVERSAL_DEVICE_FLASH_STORAGE_RemoveRecordRequestTypeDef *)pRequestMessage;                
                
                if ((flashStorageError = FLASH_STORAGE_APP_RemoveRecord(&pRemoveRecordRequest->Name)) != FLASH_STORAGE_ERR_OK)
                {
                    UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
                    
                    pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_INTERFACE_INTERNAL;
                    pErrorResponse->FlashStorageError = flashStorageError;
                }
            }
            
            *ppResponseMessages = pRequestMessage; /* Reuse request message. */
            *pResponseCount = 1;
            
            return UNIVERSAL_DEVICE_ERR_OK;
        }
        
        default:
        {
            UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *pErrorResponse = (UNIVERSAL_DEVICE_FLASH_STORAGE_ErrorResponseTypeDef *)pRequestMessage;
            
            pErrorResponse->Header.Error = UNIVERSAL_DEVICE_ERR_COMMAND_UNKNOWN_OR_INVALID;
            pErrorResponse->FlashStorageError = flashStorageError;
            
            *ppResponseMessages = pRequestMessage; /* Reuse request message. */
            *pResponseCount = 1;
            
            return UNIVERSAL_DEVICE_ERR_OK;
        }
    }
}

static bool UNIVERSAL_DEVICE_FLASH_STORAGE_IsOpened(void)
{
#ifdef FLASH_STORAGE_CONFIG_OS2
    return FLASH_STORAGE_APP_IsOpened();
#else
    return _isOpened;
#endif
}

static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_FLASH_STORAGE_Open(void)
{
    if (UNIVERSAL_DEVICE_FLASH_STORAGE_IsOpened())
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_ALREADY_ACQUIRED);
    }
    
#ifdef FLASH_STORAGE_CONFIG_OS2
    if (FLASH_STORAGE_APP_OpenTry() != osOK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_CANNOT_BE_ACQUIRED);
    }
#else
    _isOpened = true;
#endif
    
    return UNIVERSAL_DEVICE_ERR_OK;
}

static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_FLASH_STORAGE_Close(void)
{
    if (!UNIVERSAL_DEVICE_FLASH_STORAGE_IsOpened())
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED);
    }
    
#ifdef FLASH_STORAGE_CONFIG_OS2
    if (FLASH_STORAGE_APP_Close() != osOK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_CANNOT_BE_RELEASED);
    }
#else
    _isOpened = false;
#endif
    
    return UNIVERSAL_DEVICE_ERR_OK;
}

__weak UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_FLASH_STORAGE_InitiativeMessageCallback(UNIVERSAL_DEVICE_MessageTypeDef *pInitiativeMessages, uint8_t initiativeCount)
{
    /* All pInitiativeMessages units MUST be freed in code that wait for messages. */
    for (uint8_t initiativeIndex = 0; initiativeIndex < initiativeCount; initiativeIndex++)
    {
        UNIVERSAL_DEVICE_CONFIG_FREE((UNIVERSAL_DEVICE_MessageTypeDef *)(pInitiativeMessages + initiativeIndex));
    }
    
    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}


#endif /* UNIVERSAL_DEVICE_CONFIG_FLASH_STORAGE */
