/*
 * Provides HAL for nRF24.
 * GPIO_WritePin_NRF24_NSS, GPIO_WritePin_NRF24_CE, GPIO_ReadPin_NRF24_IRQ and GPIO_GetIRQSemaphore should be implemented in gpio.c and gpio.h.
 */

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_NRF24


#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "nrf24.h"

#if defined STM32F103xB
    #include "gpio.h"
    #include "spi.h"
    #include "stm32f1xx_hal.h"
#else
    #error "Device not specified."
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_OS2
    #include "cmsis_os2.h"
    #include "cmsis_os2_ex.h"
#else
    #error "UNIVERSAL_DEVICE_CONFIG_OS2 must be defined in universal_device_config.h."
#endif


static __align(2) uint8_t _spiTxAlignedBuffer[NRF24_MAX_DATA_LENGTH]; /* SPI buffer must be aligned to 2. */
static __align(2) uint8_t _spiRxAlignedBuffer[NRF24_MAX_DATA_LENGTH]; /* SPI buffer must be aligned to 2. */


static NRF24_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_HAL_TransmitReceiveSync(uint8_t *pTxRxData, uint16_t length, bool txFlag, bool rxFlag)
{
    if (length > NRF24_MAX_DATA_LENGTH)
    {
        return NRF24_ERR_HAL;
    }

    if (txFlag)
    {    
        memcpy(_spiTxAlignedBuffer, pTxRxData, length);
    }
    else
    {
        memset(_spiTxAlignedBuffer, 0, length);
    }

    if (SPI_TransmitReceive_NRF24(_spiTxAlignedBuffer, _spiRxAlignedBuffer, length) != HAL_OK)
    {
        return NRF24_ERR_HAL;
    }

    if (rxFlag)
    {
        memcpy(pTxRxData, _spiRxAlignedBuffer, length);
    }

    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_TransmitReceiveSync(void *pHalContext, uint8_t *pTxRxData, uint16_t length)
{
    return UNIVERSAL_DEVICE_NRF24_HAL_TransmitReceiveSync(pTxRxData, length, true, true);
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_TransmitSync(void *pHalContext, uint8_t *pTxData, uint16_t length)
{
    return UNIVERSAL_DEVICE_NRF24_HAL_TransmitReceiveSync(pTxData, length, true, false);
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_ReceiveSync(void *pHalContext, uint8_t *pRxData, uint16_t length)
{
    return UNIVERSAL_DEVICE_NRF24_HAL_TransmitReceiveSync(pRxData, length, false, true);
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_TakeControl(void *pHalContext)
{
    GPIO_WritePin_NRF24_NSS(GPIO_PIN_RESET); /* Active low */

    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_ReleaseControl(void *pHalContext)
{
    GPIO_WritePin_NRF24_NSS(GPIO_PIN_SET); /* Active low */

    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_ActivateCe(void *pHalContext)
{
    GPIO_WritePin_NRF24_CE(GPIO_PIN_SET); /* Active high */

    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_DeactivateCe(void *pHalContext)
{
    GPIO_WritePin_NRF24_CE(GPIO_PIN_RESET); /* Active high */

    return NRF24_ERR_OK;
}

/* Weak callback. */
NRF24_ErrorTypeDef NRF24_HAL_IsIrq(void *pHalContext, bool *pFlag)
{
    *pFlag = GPIO_ReadPin_NRF24_IRQ() == GPIO_PIN_RESET; /* Active low, logical state inversion */

    return NRF24_ERR_OK;
}

/* Weak callback. */
osSemaphoreId_t UNIVERSAL_DEVICE_NRF24_GetIRQSemaphore(void)
{
    return GPIO_GetIRQSemaphore();
}

#endif /* UNIVERSAL_DEVICE_CONFIG_NRF24 */
