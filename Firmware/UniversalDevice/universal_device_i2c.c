/*
Processes UniversalDevice I2C messages asynchronously in background thread.
*/

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_I2C


#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "universal_device.h"
#include "universal_device_debug.h"
#include "universal_device_i2c.h"

#ifdef UNIVERSAL_DEVICE_CONFIG_OS2
    #include "cmsis_os2.h"
    #include "cmsis_os2_ex.h"
#else
    #error "UNIVERSAL_DEVICE_CONFIG_OS2 must be defined in universal_device_config.h."
#endif


#define UNIVERSAL_DEVICE_I2C_MAX_READ_WRITE_SIZE        ((size_t)(UNIVERSAL_DEVICE_MESSAGE_BODY_MAX_SIZE - sizeof(UNIVERSAL_DEVICE_I2C_InfoTypeDef)))
#define UNIVERSAL_DEVICE_I2C_MAX_READ_WRITE_MEMORY_SIZE ((size_t)(UNIVERSAL_DEVICE_MESSAGE_BODY_MAX_SIZE - (sizeof(UNIVERSAL_DEVICE_I2C_InfoTypeDef) + sizeof(UNIVERSAL_DEVICE_I2C_MemoryInfoTypeDef))))

#define UNIVERSAL_DEVICE_I2C_IS_GENERAL_CALL_ADDRESS(a) (((a) & UNIVERSAL_DEVICE_I2C_ADDRESS_GENERAL_CALL_MASK)     == UNIVERSAL_DEVICE_I2C_ADDRESS_GENERAL_CALL)
#define UNIVERSAL_DEVICE_I2C_IS_START_BYTE_ADDRESS(a)   (((a) & UNIVERSAL_DEVICE_I2C_ADDRESS_START_BYTE_MASK)       == UNIVERSAL_DEVICE_I2C_ADDRESS_START_BYTE)
#define UNIVERSAL_DEVICE_I2C_IS_CBUS_ADDRESS(a)         (((a) & UNIVERSAL_DEVICE_I2C_ADDRESS_CBUS_MASK)             == UNIVERSAL_DEVICE_I2C_ADDRESS_CBUS)
#define UNIVERSAL_DEVICE_I2C_IS_DIFFERENT_BUS_ADDR(a)   (((a) & UNIVERSAL_DEVICE_I2C_ADDRESS_DIFFERENT_BUS_MASK)    == UNIVERSAL_DEVICE_I2C_ADDRESS_DIFFERENT_BUS)
#define UNIVERSAL_DEVICE_I2C_IS_FUTURE_1_ADDRESS(a)     (((a) & UNIVERSAL_DEVICE_I2C_ADDRESS_FUTURE_1_MASK)         == UNIVERSAL_DEVICE_I2C_ADDRESS_FUTURE_1)
#define UNIVERSAL_DEVICE_I2C_IS_HIGH_SPEED_ADDRESS(a)   (((a) & UNIVERSAL_DEVICE_I2C_ADDRESS_HIGH_SPEED_MASK)       == UNIVERSAL_DEVICE_I2C_ADDRESS_HIGH_SPEED)
#define UNIVERSAL_DEVICE_I2C_IS_10_BIT_ADDRESS(a)       (((a) & UNIVERSAL_DEVICE_I2C_ADDRESS_10_BIT_MASK)           == UNIVERSAL_DEVICE_I2C_ADDRESS_10_BIT)
#define UNIVERSAL_DEVICE_I2C_IS_FUTURE_2_ADDRESS(a)     (((a) & UNIVERSAL_DEVICE_I2C_ADDRESS_FUTURE_2_MASK)         == UNIVERSAL_DEVICE_I2C_ADDRESS_FUTURE_2)

#define UNIVERSAL_DEVICE_I2C_GET_7_BIT_ADDRESS(a)       ((uint8_t)((a) >> 1))

#define UNIVERSAL_DEVICE_I2C_PORT_IS_AVAILABLE          ((bool)true)
#define UNIVERSAL_DEVICE_I2C_PORT_IS_NOT_AVAILABLE      ((bool)false)

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_1
    #define UNIVERSAL_DEVICE_I2C_PORT_1_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_AVAILABLE)
#else
    #define UNIVERSAL_DEVICE_I2C_PORT_1_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_NOT_AVAILABLE)
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_2
    #define UNIVERSAL_DEVICE_I2C_PORT_2_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_AVAILABLE)
#else
    #define UNIVERSAL_DEVICE_I2C_PORT_2_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_NOT_AVAILABLE)
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_3
    #define UNIVERSAL_DEVICE_I2C_PORT_3_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_AVAILABLE)
#else
    #define UNIVERSAL_DEVICE_I2C_PORT_3_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_NOT_AVAILABLE)
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_4
    #define UNIVERSAL_DEVICE_I2C_PORT_4_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_AVAILABLE)
#else
    #define UNIVERSAL_DEVICE_I2C_PORT_4_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_NOT_AVAILABLE)
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_5
    #define UNIVERSAL_DEVICE_I2C_PORT_5_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_AVAILABLE)
#else
    #define UNIVERSAL_DEVICE_I2C_PORT_5_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_NOT_AVAILABLE)
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_6
    #define UNIVERSAL_DEVICE_I2C_PORT_6_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_AVAILABLE)
#else
    #define UNIVERSAL_DEVICE_I2C_PORT_6_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_NOT_AVAILABLE)
#endif

#ifdef UNIVERSAL_DEVICE_CONFIG_I2C_7
    #define UNIVERSAL_DEVICE_I2C_PORT_7_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_AVAILABLE)
#else
    #define UNIVERSAL_DEVICE_I2C_PORT_7_IS_AVAILABLE    (UNIVERSAL_DEVICE_I2C_PORT_IS_NOT_AVAILABLE)
#endif

#define UNIVERSAL_DEVICE_I2C_AVAILABLE_PORTS ((uint32_t) \
    ((UNIVERSAL_DEVICE_I2C_PORT_1_IS_AVAILABLE ? 1 : 0) << UNIVERSAL_DEVICE_I2C_PORT_1) | \
    ((UNIVERSAL_DEVICE_I2C_PORT_2_IS_AVAILABLE ? 1 : 0) << UNIVERSAL_DEVICE_I2C_PORT_2) | \
    ((UNIVERSAL_DEVICE_I2C_PORT_3_IS_AVAILABLE ? 1 : 0) << UNIVERSAL_DEVICE_I2C_PORT_3) | \
    ((UNIVERSAL_DEVICE_I2C_PORT_4_IS_AVAILABLE ? 1 : 0) << UNIVERSAL_DEVICE_I2C_PORT_4) | \
    ((UNIVERSAL_DEVICE_I2C_PORT_5_IS_AVAILABLE ? 1 : 0) << UNIVERSAL_DEVICE_I2C_PORT_5) | \
    ((UNIVERSAL_DEVICE_I2C_PORT_6_IS_AVAILABLE ? 1 : 0) << UNIVERSAL_DEVICE_I2C_PORT_6) | \
    ((UNIVERSAL_DEVICE_I2C_PORT_7_IS_AVAILABLE ? 1 : 0) << UNIVERSAL_DEVICE_I2C_PORT_7))

#ifndef UNIVERSAL_DEVICE_CONFIG_I2C_TASK_PRIORITY
    #define UNIVERSAL_DEVICE_CONFIG_I2C_TASK_PRIORITY   osPriorityBelowNormal
#endif

#ifndef UNIVERSAL_DEVICE_CONFIG_I2C_QUEUE_SIZE
    #define UNIVERSAL_DEVICE_CONFIG_I2C_QUEUE_SIZE      4U
#endif


typedef enum
{
    /* Error: UNIVERSAL_DEVICE_I2C_ErrorResponseTypeDef */

    /* Request: UNIVERSAL_DEVICE_MessageTypeDef */
    /* Response: UNIVERSAL_DEVICE_I2C_IsAcquiredResponseTypeDef */
    UNIVERSAL_DEVICE_I2C_CMD_IS_ACQUIRED    = ((uint8_t)    0x20),
    /* Request and response: UNIVERSAL_DEVICE_MessageTypeDef */
    UNIVERSAL_DEVICE_I2C_CMD_ACQUIRE        = ((uint8_t)    0x21),
    /* Request and response: UNIVERSAL_DEVICE_MessageTypeDef */
    UNIVERSAL_DEVICE_I2C_CMD_RELEASE        = ((uint8_t)    0x22),

    /* Request: UNIVERSAL_DEVICE_I2C_ReadRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_I2C_ReadResponseTypeDef */
    UNIVERSAL_DEVICE_I2C_CMD_READ           = ((uint8_t)    0x31),
    /* Request: UNIVERSAL_DEVICE_INFO_WriteRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_INFO_WriteResponseTypeDef */
    UNIVERSAL_DEVICE_I2C_CMD_WRITE          = ((uint8_t)    0x32),

    /* Request: UNIVERSAL_DEVICE_I2C_ReadMemoryRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_I2C_ReadMemoryResponseTypeDef */
    UNIVERSAL_DEVICE_I2C_CMD_READ_MEMORY    = ((uint8_t)    0x33),
    /* Request: UNIVERSAL_DEVICE_INFO_WriteMemoryRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_INFO_WriteMemoryResponseTypeDef */
    UNIVERSAL_DEVICE_I2C_CMD_WRITE_MEMORY   = ((uint8_t)    0x34),

    __UNIVERSAL_DEVICE_I2C_CMD_UNKNOWN      = ((uint8_t)    0xFF),
}
UNIVERSAL_DEVICE_I2C_CommandTypeDef;

typedef enum
{
    UNIVERSAL_DEVICE_I2C_ADDRESS_GENERAL_CALL       = ((uint8_t)   0x00),
    UNIVERSAL_DEVICE_I2C_ADDRESS_GENERAL_CALL_MASK  = ((uint8_t)   0xFF),

    UNIVERSAL_DEVICE_I2C_ADDRESS_START_BYTE         = ((uint8_t)   0x01),
    UNIVERSAL_DEVICE_I2C_ADDRESS_START_BYTE_MASK    = ((uint8_t)   0xFF),

    UNIVERSAL_DEVICE_I2C_ADDRESS_CBUS               = ((uint8_t)   0x02),
    UNIVERSAL_DEVICE_I2C_ADDRESS_CBUS_MASK          = ((uint8_t)   0xFE),

    UNIVERSAL_DEVICE_I2C_ADDRESS_DIFFERENT_BUS      = ((uint8_t)   0x04),
    UNIVERSAL_DEVICE_I2C_ADDRESS_DIFFERENT_BUS_MASK = ((uint8_t)   0xFE),

    UNIVERSAL_DEVICE_I2C_ADDRESS_FUTURE_1           = ((uint8_t)   0x06),
    UNIVERSAL_DEVICE_I2C_ADDRESS_FUTURE_1_MASK      = ((uint8_t)   0xFE),

    UNIVERSAL_DEVICE_I2C_ADDRESS_HIGH_SPEED         = ((uint8_t)   0x08),
    UNIVERSAL_DEVICE_I2C_ADDRESS_HIGH_SPEED_MASK    = ((uint8_t)   0xF8),

    UNIVERSAL_DEVICE_I2C_ADDRESS_10_BIT             = ((uint8_t)   0xF0),
    UNIVERSAL_DEVICE_I2C_ADDRESS_10_BIT_MASK        = ((uint8_t)   0xF8),

    UNIVERSAL_DEVICE_I2C_ADDRESS_FUTURE_2           = ((uint8_t)   0xF8),
    UNIVERSAL_DEVICE_I2C_ADDRESS_FUTURE_2_MASK      = ((uint8_t)   0xF8),
}
UNIVERSAL_DEVICE_I2C_AddressTypeDef;


typedef struct
{
    uint32_t                                BusSpeed;
    uint8_t                                 MainAddress;
    uint8_t                                 Wide10Address;
    uint16_t                                DataSize;
}
UNIVERSAL_DEVICE_I2C_InfoTypeDef; /* 8 */

typedef struct
{
    uint16_t                                MemoryAddress;
    uint16_t                                MemoryAddressSize;
}
UNIVERSAL_DEVICE_I2C_MemoryInfoTypeDef; /* 4 */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef   I2CHalError;
}
UNIVERSAL_DEVICE_I2C_ErrorResponseTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 4 */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    bool                                    IsAcquired;

    uint8_t                                 __Reserved[3];
}
UNIVERSAL_DEVICE_I2C_IsAcquiredResponseTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 4 */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    UNIVERSAL_DEVICE_I2C_InfoTypeDef        Info;
}
UNIVERSAL_DEVICE_I2C_ReadRequestTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 8 */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    UNIVERSAL_DEVICE_I2C_InfoTypeDef        Info;

    uint8_t                                 Data[];
}
UNIVERSAL_DEVICE_I2C_ReadResponseTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 8 + (DataSize multiple by 4) */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    UNIVERSAL_DEVICE_I2C_InfoTypeDef        Info;

    uint8_t                                 Data[];
}
UNIVERSAL_DEVICE_I2C_WriteRequestTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 8 + (DataSize multiple by 4) */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    UNIVERSAL_DEVICE_I2C_InfoTypeDef        Info;
}
UNIVERSAL_DEVICE_I2C_WriteResponseTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 8 */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    UNIVERSAL_DEVICE_I2C_InfoTypeDef        Info;

    UNIVERSAL_DEVICE_I2C_MemoryInfoTypeDef  MemoryInfo;
}
UNIVERSAL_DEVICE_I2C_ReadMemoryRequestTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 12 */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    UNIVERSAL_DEVICE_I2C_InfoTypeDef        Info;

    UNIVERSAL_DEVICE_I2C_MemoryInfoTypeDef  MemoryInfo;

    uint8_t                                 Data[];
}
UNIVERSAL_DEVICE_I2C_ReadMemoryResponseTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 12 + (DataSize multiple by 4) */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    UNIVERSAL_DEVICE_I2C_InfoTypeDef        Info;

    UNIVERSAL_DEVICE_I2C_MemoryInfoTypeDef  MemoryInfo;

    uint8_t                                 Data[];
}
UNIVERSAL_DEVICE_I2C_WriteMemoryRequestTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 12 + (DataSize multiple by 4) */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    UNIVERSAL_DEVICE_I2C_InfoTypeDef        Info;

    UNIVERSAL_DEVICE_I2C_MemoryInfoTypeDef  MemoryInfo;
}
UNIVERSAL_DEVICE_I2C_WriteMemoryResponseTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 12 */


static osThreadId_t _i2cWorkerTaskHandle;
static const osThreadAttr_t _i2cWorkerTask_attributes = {
  .name = "i2cWorkerTask",
  .priority = (osPriority_t) UNIVERSAL_DEVICE_CONFIG_I2C_TASK_PRIORITY,
  .stack_size = 128 * 4
};

static osMessageQueueId_t _i2cWorkerInQueueHandle;
static const osMessageQueueAttr_t _i2cWorkerInQueue_attributes = {
  .name = "i2cWorkerInQueue"
};

static CMSIS_OS2_EX_TaskStateTypeDef _i2cWorkerTaskState;


static __NO_RETURN void UNIVERSAL_DEVICE_I2C_Task(void *argument);
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_ProcessAcquireRelease(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_ProcessReadWrite(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_ResponseWithError(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage, UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef responseHalError);
static bool UNIVERSAL_DEVICE_I2C_IsAddressSupported(uint8_t mainAddress);

static const char * UNIVERSAL_DEVICE_I2C_PortToString(UNIVERSAL_DEVICE_I2C_PortTypeDef port);
static const char * UNIVERSAL_DEVICE_I2C_CommandToString(UNIVERSAL_DEVICE_I2C_CommandTypeDef command);


UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_Initialise(void)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();


    /* Queue contains pointers on messages. */
    _i2cWorkerInQueueHandle = osMessageQueueNew(UNIVERSAL_DEVICE_CONFIG_I2C_QUEUE_SIZE, sizeof(UNIVERSAL_DEVICE_MessageTypeDef *), &_i2cWorkerInQueue_attributes);

    if (_i2cWorkerInQueueHandle == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_OS_OBJECT_CANNOT_BE_CREATED);
    }

    _i2cWorkerTaskHandle = osThreadNew(UNIVERSAL_DEVICE_I2C_Task, NULL, &_i2cWorkerTask_attributes);

    if (_i2cWorkerTaskHandle == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_OS_OBJECT_CANNOT_BE_CREATED);
    }


    return UNIVERSAL_DEVICE_ERR_OK;
}

UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_ProcessRequestMessage(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();

    UNIVERSAL_DEVICE_DEBUG_ASSERT(pRequestMessage != NULL);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(pRequestMessage->Header.InterfaceBase == UNIVERSAL_DEVICE_INTERFACE_BASE_I2C);


    UNIVERSAL_DEVICE_I2C_PortTypeDef port = (UNIVERSAL_DEVICE_I2C_PortTypeDef)pRequestMessage->Header.InterfacePort;
    UNIVERSAL_DEVICE_I2C_CommandTypeDef command = (UNIVERSAL_DEVICE_I2C_CommandTypeDef)pRequestMessage->Header.InterfaceCommand;

    UNIVERSAL_DEVICE_DEBUG_INFO("%s: port %s command %s.\r\n", __FUNCTION__, UNIVERSAL_DEVICE_I2C_PortToString(port), UNIVERSAL_DEVICE_I2C_CommandToString(command));


    size_t requestMessageSize = UNIVERSAL_DEVICE_GetMessageSize(pRequestMessage);
    UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessageCopy = (UNIVERSAL_DEVICE_MessageTypeDef *)UNIVERSAL_DEVICE_CONFIG_MALLOC(requestMessageSize);

    if (pRequestMessageCopy == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED));
    }

    memcpy(pRequestMessageCopy, pRequestMessage, requestMessageSize);


    /* Put pointer on message in queue. In case of success message must be freed in worker thread. */
    if (osExMessageQueuePutTry(_i2cWorkerInQueueHandle, &pRequestMessageCopy) != osOK)
    {
        UNIVERSAL_DEVICE_CONFIG_FREE(pRequestMessageCopy);
        
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_INTERFACE_BUSY));
    }


    return UNIVERSAL_DEVICE_ERR_OK;
}

bool UNIVERSAL_DEVICE_I2C_IsPortAvailable(UNIVERSAL_DEVICE_I2C_PortTypeDef port)
{
    if (port > UNIVERSAL_DEVICE_I2C_MAX_PORT_NUMBER || port == UNIVERSAL_DEVICE_I2C_PORT_CONTROL)
    {
        return false;
    }

    return (UNIVERSAL_DEVICE_I2C_AVAILABLE_PORTS & (1 << port)) != 0;
}


static __NO_RETURN void UNIVERSAL_DEVICE_I2C_Task(void *argument)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE("%s started.\r\n", __FUNCTION__);

    _i2cWorkerTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;    


    while (true)
    {
        UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage = NULL;

        if (osExMessageQueueGetWait(_i2cWorkerInQueueHandle, &pRequestMessage) != osOK)
        {
            UNIVERSAL_DEVICE_DEBUG_ASSERT_FAILED();
            goto Error;
        }


        UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;

        UNIVERSAL_DEVICE_I2C_PortTypeDef port = (UNIVERSAL_DEVICE_I2C_PortTypeDef)pRequestMessage->Header.InterfacePort;

        if (!UNIVERSAL_DEVICE_I2C_IsPortAvailable(port))
        {
            error = UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_PORT_NOT_AVAILABLE);

            if (error != UNIVERSAL_DEVICE_ERR_OK)
            {
                UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(error);
                goto FreeAndError;
            }
        }
        else
        {
            UNIVERSAL_DEVICE_I2C_CommandTypeDef command = (UNIVERSAL_DEVICE_I2C_CommandTypeDef)pRequestMessage->Header.InterfaceCommand;

            switch (command)
            {
                case UNIVERSAL_DEVICE_I2C_CMD_IS_ACQUIRED:
                case UNIVERSAL_DEVICE_I2C_CMD_ACQUIRE:
                case UNIVERSAL_DEVICE_I2C_CMD_RELEASE:
                {
                    if ((error = UNIVERSAL_DEVICE_I2C_ProcessAcquireRelease(pRequestMessage)) != UNIVERSAL_DEVICE_ERR_OK)
                    {
                        UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(error);
                        goto FreeAndError;
                    }
                }
                break;

                case UNIVERSAL_DEVICE_I2C_CMD_READ:
                case UNIVERSAL_DEVICE_I2C_CMD_READ_MEMORY:
                case UNIVERSAL_DEVICE_I2C_CMD_WRITE:
                case UNIVERSAL_DEVICE_I2C_CMD_WRITE_MEMORY:
                {
                    if ((error = UNIVERSAL_DEVICE_I2C_ProcessReadWrite(pRequestMessage)) != UNIVERSAL_DEVICE_ERR_OK)
                    {
                        UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(error);
                        goto FreeAndError;
                    }
                }
                break;

                default:
                {
                    error = UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_COMMAND_UNKNOWN_OR_INVALID);

                    if (error != UNIVERSAL_DEVICE_ERR_OK)
                    {
                        UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(error);
                        goto FreeAndError;
                    }
                }
            }
        }

        FreeAndError:
        UNIVERSAL_DEVICE_CONFIG_FREE(pRequestMessage);

        if (error != UNIVERSAL_DEVICE_ERR_OK)
        {
            goto Error;
        }
    }

    Error:    
    _i2cWorkerTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;
    
    UNIVERSAL_DEVICE_DEBUG_ERROR("%s: stop task.\r\n", __FUNCTION__);

    while (true)
    {
        osDelay(osWaitForever);
    }
}

/* pRequestMessage must be freed by calling code. */
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_ProcessAcquireRelease(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{    
    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;

    UNIVERSAL_DEVICE_I2C_PortTypeDef port = (UNIVERSAL_DEVICE_I2C_PortTypeDef)pRequestMessage->Header.InterfacePort;    
    UNIVERSAL_DEVICE_I2C_CommandTypeDef command = (UNIVERSAL_DEVICE_I2C_CommandTypeDef)pRequestMessage->Header.InterfaceCommand;


    size_t responseSize = 0;

    switch (command)
    {
        case UNIVERSAL_DEVICE_I2C_CMD_IS_ACQUIRED:
        {
            responseSize = sizeof(UNIVERSAL_DEVICE_I2C_IsAcquiredResponseTypeDef);
        }
        break;

        case UNIVERSAL_DEVICE_I2C_CMD_ACQUIRE:
        case UNIVERSAL_DEVICE_I2C_CMD_RELEASE:
        {
            responseSize = sizeof(UNIVERSAL_DEVICE_MessageTypeDef);
        }
        break;

        default:
            UNIVERSAL_DEVICE_DEBUG_ASSERT_FAILED();
            return UNIVERSAL_DEVICE_ERR_COMMAND_UNKNOWN_OR_INVALID;
    }


    UNIVERSAL_DEVICE_MessageTypeDef *pResponse = (UNIVERSAL_DEVICE_MessageTypeDef *)UNIVERSAL_DEVICE_CONFIG_MALLOC(responseSize);

    if (pResponse == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED));
    }

    memset(pResponse, 0, responseSize);

    pResponse->Header.MessageId = pRequestMessage->Header.MessageId;
    pResponse->Header.MessageBodySize = responseSize - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE;
    /* FragmentBodyOffset is already 0. */
    pResponse->Header.FragmentBodySize = pResponse->Header.MessageBodySize;
    pResponse->Header.InterfaceBase = pRequestMessage->Header.InterfaceBase;
    pResponse->Header.InterfacePort = pRequestMessage->Header.InterfacePort;
    pResponse->Header.InterfaceCommand = pRequestMessage->Header.InterfaceCommand;
    /* Error is already 0 (UNIVERSAL_DEVICE_ERR_OK). */


    switch (command)
    {
        case UNIVERSAL_DEVICE_I2C_CMD_IS_ACQUIRED:
        {
            UNIVERSAL_DEVICE_I2C_IsAcquiredResponseTypeDef *pIsAcquiredResponse = (UNIVERSAL_DEVICE_I2C_IsAcquiredResponseTypeDef *)pResponse;
            
            pIsAcquiredResponse->IsAcquired = UNIVERSAL_DEVICE_I2C_HAL_IsAcquired(port);
        }
        break;

        case UNIVERSAL_DEVICE_I2C_CMD_ACQUIRE:
        {
            if (UNIVERSAL_DEVICE_I2C_HAL_IsAcquired(port))
            {
                error = UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_ALREADY_ACQUIRED;
            }
            else
            {
                error = UNIVERSAL_DEVICE_I2C_HAL_Acquire(port);
            }
        }
        break;

        case UNIVERSAL_DEVICE_I2C_CMD_RELEASE:
        {
            if (!UNIVERSAL_DEVICE_I2C_HAL_IsAcquired(port))
            {
                error = UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED;
            }
            else
            {
                error = UNIVERSAL_DEVICE_I2C_HAL_Release(port);
            }
        }
        break;

        default:
        break;
    }


    if (error != UNIVERSAL_DEVICE_ERR_OK)
    {
        error = UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, error);
    }
    else
    {
        error = UNIVERSAL_DEVICE_ProcessResponseOrInitiativeMessage(pResponse);
    }

    UNIVERSAL_DEVICE_CONFIG_FREE(pResponse);

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(error);
}

/* pRequestMessage must be freed by calling code. */
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_ProcessReadWrite(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{
    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;
    UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef halError = UNIVERSAL_DEVICE_I2C_HAL_ERR_NONE;

    UNIVERSAL_DEVICE_I2C_PortTypeDef port = (UNIVERSAL_DEVICE_I2C_PortTypeDef)pRequestMessage->Header.InterfacePort;    
    UNIVERSAL_DEVICE_I2C_CommandTypeDef command = (UNIVERSAL_DEVICE_I2C_CommandTypeDef)pRequestMessage->Header.InterfaceCommand;


    size_t responseSize = 0; /* Response size must be multiple of four and can be larger than real data size in read responses. */
    uint16_t deviceAddress = 0;

    if (!UNIVERSAL_DEVICE_I2C_HAL_IsAcquired(port))
    {
        error = UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED;
    }
    else
    {
        UNIVERSAL_DEVICE_I2C_InfoTypeDef *pInfo = NULL;
        UNIVERSAL_DEVICE_I2C_MemoryInfoTypeDef *pMemoryInfo = NULL;
        size_t responseBaseSize = 0;        
        size_t maxDataSize = 0;

        switch (command)
        {
            case UNIVERSAL_DEVICE_I2C_CMD_READ:
            {
                UNIVERSAL_DEVICE_I2C_ReadRequestTypeDef *pReadRequest = (UNIVERSAL_DEVICE_I2C_ReadRequestTypeDef *)pRequestMessage;

                pInfo = &pReadRequest->Info;

                responseBaseSize = sizeof(UNIVERSAL_DEVICE_I2C_ReadResponseTypeDef);
                maxDataSize = UNIVERSAL_DEVICE_I2C_MAX_READ_WRITE_SIZE;
                
            }
            break;

            case UNIVERSAL_DEVICE_I2C_CMD_READ_MEMORY:
            {
                UNIVERSAL_DEVICE_I2C_ReadMemoryRequestTypeDef *pReadMemoryRequest = (UNIVERSAL_DEVICE_I2C_ReadMemoryRequestTypeDef *)pRequestMessage;

                pInfo = &pReadMemoryRequest->Info;
                pMemoryInfo = &pReadMemoryRequest->MemoryInfo;

                responseBaseSize = sizeof(UNIVERSAL_DEVICE_I2C_ReadMemoryResponseTypeDef);
                maxDataSize = UNIVERSAL_DEVICE_I2C_MAX_READ_WRITE_MEMORY_SIZE;
            }
            break;

            case UNIVERSAL_DEVICE_I2C_CMD_WRITE:
            {
                UNIVERSAL_DEVICE_I2C_WriteRequestTypeDef *pWriteRequest = (UNIVERSAL_DEVICE_I2C_WriteRequestTypeDef *)pRequestMessage;

                pInfo = &pWriteRequest->Info;

                responseBaseSize = sizeof(UNIVERSAL_DEVICE_I2C_WriteResponseTypeDef);
                maxDataSize = UNIVERSAL_DEVICE_I2C_MAX_READ_WRITE_SIZE;
            }
            break;

            case UNIVERSAL_DEVICE_I2C_CMD_WRITE_MEMORY:
            {
                UNIVERSAL_DEVICE_I2C_WriteMemoryRequestTypeDef *pWriteMemoryRequest = (UNIVERSAL_DEVICE_I2C_WriteMemoryRequestTypeDef *)pRequestMessage;

                pInfo = &pWriteMemoryRequest->Info;
                pMemoryInfo = &pWriteMemoryRequest->MemoryInfo;

                responseBaseSize = sizeof(UNIVERSAL_DEVICE_I2C_WriteMemoryResponseTypeDef);
                maxDataSize = UNIVERSAL_DEVICE_I2C_MAX_READ_WRITE_MEMORY_SIZE;
            }
            break;

            default:
                UNIVERSAL_DEVICE_DEBUG_ASSERT_FAILED();
                return UNIVERSAL_DEVICE_ERR_COMMAND_UNKNOWN_OR_INVALID;
        }

        size_t dataContainerSize = UNIVERSAL_DEVICE_IncreaseToMultipleOfFour(pInfo->DataSize);

        if (command == UNIVERSAL_DEVICE_I2C_CMD_READ || command == UNIVERSAL_DEVICE_I2C_CMD_READ_MEMORY)
        {
            responseSize = responseBaseSize + dataContainerSize;
        }
        else
        {
            responseSize = responseBaseSize;
        }

        if (!UNIVERSAL_DEVICE_I2C_IsAddressSupported(pInfo->MainAddress))
        {
            error = UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED;
        }
        else if (pInfo->DataSize == 0 || dataContainerSize > maxDataSize)
        {
            error = UNIVERSAL_DEVICE_ERR_INVALID_PARAMETER;
        }
        else if (pInfo->BusSpeed != UNIVERSAL_DEVICE_I2C_BUS_SPEED_STANDART)
        {
            error = UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED;
        }
        else if (pMemoryInfo != NULL && pMemoryInfo->MemoryAddressSize != UNIVERSAL_DEVICE_I2C_MEM_ADDR_SIZE_8 && pMemoryInfo->MemoryAddressSize != UNIVERSAL_DEVICE_I2C_MEM_ADDR_SIZE_16)
        {
            error = UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED;
        }

        deviceAddress = UNIVERSAL_DEVICE_I2C_GET_7_BIT_ADDRESS(pInfo->MainAddress); /* UNIVERSAL_DEVICE_I2C_IsAddressSupported returns true only for 7-bit wide address. */
    }


    if (error != UNIVERSAL_DEVICE_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, error));
    }


    UNIVERSAL_DEVICE_MessageTypeDef *pResponse = (UNIVERSAL_DEVICE_MessageTypeDef *)UNIVERSAL_DEVICE_CONFIG_MALLOC(responseSize);

    if (pResponse == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED));
    }

    memset(pResponse, 0, responseSize);

    pResponse->Header.MessageId = pRequestMessage->Header.MessageId;
    pResponse->Header.MessageBodySize = responseSize - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE;
    /* FragmentBodyOffset is already 0. */
    pResponse->Header.FragmentBodySize = pResponse->Header.MessageBodySize;
    pResponse->Header.InterfaceBase = pRequestMessage->Header.InterfaceBase;
    pResponse->Header.InterfacePort = pRequestMessage->Header.InterfacePort;
    pResponse->Header.InterfaceCommand = pRequestMessage->Header.InterfaceCommand;
    /* Error is already 0 (UNIVERSAL_DEVICE_ERR_OK). */


    switch (command)
    {
        case UNIVERSAL_DEVICE_I2C_CMD_READ:
        {
            UNIVERSAL_DEVICE_I2C_ReadRequestTypeDef *pReadRequest = (UNIVERSAL_DEVICE_I2C_ReadRequestTypeDef *)pRequestMessage;
            UNIVERSAL_DEVICE_I2C_ReadResponseTypeDef *pReadResponse = (UNIVERSAL_DEVICE_I2C_ReadResponseTypeDef *)pResponse;


            memcpy(&(pReadResponse->Info), &(pReadRequest->Info), sizeof(UNIVERSAL_DEVICE_I2C_InfoTypeDef));


            if ((halError = UNIVERSAL_DEVICE_I2C_HAL_Read(port, deviceAddress, pReadResponse->Data, pReadResponse->Info.DataSize)) != UNIVERSAL_DEVICE_I2C_HAL_ERR_NONE)
            {
                error = UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC;
            }
        }
        break;

        case UNIVERSAL_DEVICE_I2C_CMD_READ_MEMORY:
        {
            UNIVERSAL_DEVICE_I2C_ReadMemoryRequestTypeDef *pReadMemoryRequest = (UNIVERSAL_DEVICE_I2C_ReadMemoryRequestTypeDef *)pRequestMessage;
            UNIVERSAL_DEVICE_I2C_ReadMemoryResponseTypeDef *pReadMemoryResponse = (UNIVERSAL_DEVICE_I2C_ReadMemoryResponseTypeDef *)pResponse;


            memcpy(&(pReadMemoryResponse->Info), &(pReadMemoryRequest->Info), sizeof(UNIVERSAL_DEVICE_I2C_InfoTypeDef));
            memcpy(&(pReadMemoryResponse->MemoryInfo), &(pReadMemoryRequest->MemoryInfo), sizeof(UNIVERSAL_DEVICE_I2C_MemoryInfoTypeDef));


            if ((halError = UNIVERSAL_DEVICE_I2C_HAL_ReadMemory(port, deviceAddress, pReadMemoryResponse->MemoryInfo.MemoryAddress, pReadMemoryResponse->MemoryInfo.MemoryAddressSize,
                pReadMemoryResponse->Data, pReadMemoryResponse->Info.DataSize)) != UNIVERSAL_DEVICE_I2C_HAL_ERR_NONE)
            {
                error = UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC;
            }
        }
        break;
        
        case UNIVERSAL_DEVICE_I2C_CMD_WRITE:
        {
            UNIVERSAL_DEVICE_I2C_WriteRequestTypeDef *pWriteRequest = (UNIVERSAL_DEVICE_I2C_WriteRequestTypeDef *)pRequestMessage;
            UNIVERSAL_DEVICE_I2C_WriteResponseTypeDef *pWriteResponse = (UNIVERSAL_DEVICE_I2C_WriteResponseTypeDef *)pResponse;


            memcpy(&(pWriteResponse->Info), &(pWriteRequest->Info), sizeof(UNIVERSAL_DEVICE_I2C_InfoTypeDef));


            if ((halError = UNIVERSAL_DEVICE_I2C_HAL_Write(port, deviceAddress, pWriteRequest->Data, pWriteRequest->Info.DataSize)) != UNIVERSAL_DEVICE_I2C_HAL_ERR_NONE)
            {
                error = UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC;
            }
        }
        break;

        case UNIVERSAL_DEVICE_I2C_CMD_WRITE_MEMORY:
        {
            UNIVERSAL_DEVICE_I2C_WriteMemoryRequestTypeDef *pWriteMemoryRequest = (UNIVERSAL_DEVICE_I2C_WriteMemoryRequestTypeDef *)pRequestMessage;
            UNIVERSAL_DEVICE_I2C_WriteMemoryResponseTypeDef *pWriteMemoryResponse = (UNIVERSAL_DEVICE_I2C_WriteMemoryResponseTypeDef *)pResponse;


            memcpy(&(pWriteMemoryResponse->Info), &(pWriteMemoryRequest->Info), sizeof(UNIVERSAL_DEVICE_I2C_InfoTypeDef));
            memcpy(&(pWriteMemoryResponse->MemoryInfo), &(pWriteMemoryRequest->MemoryInfo), sizeof(UNIVERSAL_DEVICE_I2C_MemoryInfoTypeDef));


            if ((halError = UNIVERSAL_DEVICE_I2C_HAL_WriteMemory(port, deviceAddress, pWriteMemoryRequest->MemoryInfo.MemoryAddress, pWriteMemoryRequest->MemoryInfo.MemoryAddressSize,
                pWriteMemoryRequest->Data, pWriteMemoryRequest->Info.DataSize)) != UNIVERSAL_DEVICE_I2C_HAL_ERR_NONE)
            {
                error = UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC;
            }
        }
        break;

        default:
        break;
    }


    if (error != UNIVERSAL_DEVICE_ERR_OK)
    {
        if (error == UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC)
        {
            error = UNIVERSAL_DEVICE_I2C_ResponseWithError(pRequestMessage, halError);
        }
        else
        {
            error = UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, error);
        }
    }
    else
    {
        error = UNIVERSAL_DEVICE_ProcessResponseOrInitiativeMessage(pResponse);
    }

    UNIVERSAL_DEVICE_CONFIG_FREE(pResponse);

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(error);
}

/* pRequestMessage must be freed by calling code. */
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_ResponseWithError(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage, UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef responseHalError)
{
    UNIVERSAL_DEVICE_DEBUG_ERROR("%s: HAL error %u.\r\n", __FUNCTION__, responseHalError);

    UNIVERSAL_DEVICE_I2C_ErrorResponseTypeDef *pErrorResponse = UNIVERSAL_DEVICE_CONFIG_MALLOC(sizeof(UNIVERSAL_DEVICE_I2C_ErrorResponseTypeDef));

    if (pErrorResponse == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED);
    }

    memset(pErrorResponse, 0, sizeof(UNIVERSAL_DEVICE_I2C_ErrorResponseTypeDef));

    pErrorResponse->Header.MessageId = pRequestMessage->Header.MessageId;
    pErrorResponse->Header.MessageBodySize = sizeof(UNIVERSAL_DEVICE_I2C_ErrorResponseTypeDef) - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE;
    /* FragmentBodyOffset is already 0. */
    pErrorResponse->Header.FragmentBodySize = sizeof(UNIVERSAL_DEVICE_I2C_ErrorResponseTypeDef) - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE;
    pErrorResponse->Header.InterfaceBase = pRequestMessage->Header.InterfaceBase;
    pErrorResponse->Header.InterfacePort = pRequestMessage->Header.InterfacePort;
    pErrorResponse->Header.InterfaceCommand = pRequestMessage->Header.InterfaceCommand;
    pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC;
    pErrorResponse->I2CHalError = responseHalError;

    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback((UNIVERSAL_DEVICE_MessageTypeDef *)pErrorResponse);

    UNIVERSAL_DEVICE_CONFIG_FREE(pErrorResponse);

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(error);
}

static bool UNIVERSAL_DEVICE_I2C_IsAddressSupported(uint8_t mainAddress)
{
    return (UNIVERSAL_DEVICE_I2C_IS_GENERAL_CALL_ADDRESS(mainAddress) ||
            UNIVERSAL_DEVICE_I2C_IS_START_BYTE_ADDRESS(mainAddress) ||
            UNIVERSAL_DEVICE_I2C_IS_CBUS_ADDRESS(mainAddress) ||
            UNIVERSAL_DEVICE_I2C_IS_DIFFERENT_BUS_ADDR(mainAddress) ||
            UNIVERSAL_DEVICE_I2C_IS_FUTURE_1_ADDRESS(mainAddress) ||
            UNIVERSAL_DEVICE_I2C_IS_HIGH_SPEED_ADDRESS(mainAddress) ||
            UNIVERSAL_DEVICE_I2C_IS_10_BIT_ADDRESS(mainAddress) ||
            UNIVERSAL_DEVICE_I2C_IS_FUTURE_2_ADDRESS(mainAddress)) == false;
}


#if (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_INFO)
#define __TO_STRING(x) (#x)
#define TO_STRING(x) __TO_STRING(x)

static const char * UNIVERSAL_DEVICE_I2C_PortToString(UNIVERSAL_DEVICE_I2C_PortTypeDef port)
{
    static const char *ssPortList[] =
    {
        TO_STRING(UNIVERSAL_DEVICE_I2C_PORT_CONTROL),
        TO_STRING(UNIVERSAL_DEVICE_I2C_PORT_1),
        TO_STRING(UNIVERSAL_DEVICE_I2C_PORT_2),
        TO_STRING(UNIVERSAL_DEVICE_I2C_PORT_3),
        TO_STRING(UNIVERSAL_DEVICE_I2C_PORT_4),
        TO_STRING(UNIVERSAL_DEVICE_I2C_PORT_5),
        TO_STRING(UNIVERSAL_DEVICE_I2C_PORT_6),
        TO_STRING(UNIVERSAL_DEVICE_I2C_PORT_7),
        TO_STRING(__UNIVERSAL_DEVICE_I2C_PORT_UNKNOWN),
    };

    if (port > __UNIVERSAL_DEVICE_I2C_PORT_UNKNOWN)
    {
        port = __UNIVERSAL_DEVICE_I2C_PORT_UNKNOWN;
    }

    return ssPortList[port];
}

static const char * UNIVERSAL_DEVICE_I2C_CommandToString(UNIVERSAL_DEVICE_I2C_CommandTypeDef command)
{
    static const UNIVERSAL_DEVICE_I2C_CommandTypeDef pCommandIndexList[] =
    {
        UNIVERSAL_DEVICE_I2C_CMD_IS_ACQUIRED,        
        UNIVERSAL_DEVICE_I2C_CMD_ACQUIRE,
        UNIVERSAL_DEVICE_I2C_CMD_RELEASE,

        UNIVERSAL_DEVICE_I2C_CMD_READ,
        UNIVERSAL_DEVICE_I2C_CMD_WRITE,

        UNIVERSAL_DEVICE_I2C_CMD_READ_MEMORY,
        UNIVERSAL_DEVICE_I2C_CMD_WRITE_MEMORY,

        __UNIVERSAL_DEVICE_I2C_CMD_UNKNOWN,
    };

    static const char *ssCommandList[] =
    {
        TO_STRING(UNIVERSAL_DEVICE_I2C_CMD_IS_ACQUIRED),
        TO_STRING(UNIVERSAL_DEVICE_I2C_CMD_ACQUIRE),
        TO_STRING(UNIVERSAL_DEVICE_I2C_CMD_RELEASE),

        TO_STRING(UNIVERSAL_DEVICE_I2C_CMD_READ),
        TO_STRING(UNIVERSAL_DEVICE_I2C_CMD_WRITE),

        TO_STRING(UNIVERSAL_DEVICE_I2C_CMD_READ_MEMORY),
        TO_STRING(UNIVERSAL_DEVICE_I2C_CMD_WRITE_MEMORY),

        TO_STRING(__UNIVERSAL_DEVICE_I2C_CMD_UNKNOWN),
    };

    size_t commandCount = sizeof(pCommandIndexList) / sizeof(UNIVERSAL_DEVICE_I2C_CommandTypeDef);

    uint8_t index;
    for (index = 0; index < commandCount; index++)
    {
        if (pCommandIndexList[index] == command)
        {
            break;
        }
    }

    return ssCommandList[index];
}

#undef TO_STRING
#undef __TO_STRING
#endif /* (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_INFO) */


__weak bool UNIVERSAL_DEVICE_I2C_IsAcquired(UNIVERSAL_DEVICE_I2C_PortTypeDef port)
{
    (void)port;

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}

__weak UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_Acquire(UNIVERSAL_DEVICE_I2C_PortTypeDef port)
{
    (void)port;

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}

__weak UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_I2C_Release(UNIVERSAL_DEVICE_I2C_PortTypeDef port)
{
    (void)port;

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}

__weak UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef UNIVERSAL_DEVICE_I2C_HAL_Read(UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint8_t *pData, uint16_t dataSize)
{
    (void)port;
    (void)deviceAddress;
    (void)pData;
    (void)dataSize;

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}

__weak UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef UNIVERSAL_DEVICE_I2C_HAL_Write(UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint8_t *pData, uint16_t dataSize)
{
    (void)port;
    (void)deviceAddress;
    (void)pData;
    (void)dataSize;

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}

__weak UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef UNIVERSAL_DEVICE_I2C_HAL_ReadMemory
    (UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint16_t memoryAddress, uint16_t memoryAddressSize, uint8_t *pData, uint16_t dataSize)
{
    (void)port;
    (void)deviceAddress;
    (void)memoryAddress;
    (void)memoryAddressSize;
    (void)pData;
    (void)dataSize;

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}

__weak UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef UNIVERSAL_DEVICE_I2C_HAL_WriteMemory
    (UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint16_t memoryAddress, uint16_t memoryAddressSize, uint8_t *pData, uint16_t dataSize)
{
    (void)port;
    (void)deviceAddress;
    (void)memoryAddress;
    (void)memoryAddressSize;
    (void)pData;
    (void)dataSize;

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}


#endif /* UNIVERSAL_DEVICE_CONFIG_I2C */
