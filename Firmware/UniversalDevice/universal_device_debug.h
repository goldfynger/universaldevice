#ifndef __UNIVERSAL_DEVICE_DEBUG_H
#define __UNIVERSAL_DEVICE_DEBUG_H


#include <stdbool.h>
#include <stdint.h>
#include "universal_device.h"


#define UNIVERSAL_DEVICE_DEBUG_LEVEL_NONE   0U
#define UNIVERSAL_DEVICE_DEBUG_LEVEL_ERROR  1U
#define UNIVERSAL_DEVICE_DEBUG_LEVEL_INFO   2U
#define UNIVERSAL_DEVICE_DEBUG_LEVEL_TRACE  3U


#ifndef UNIVERSAL_DEVICE_DEBUG_LEVEL
    #define UNIVERSAL_DEVICE_DEBUG_LEVEL                UNIVERSAL_DEVICE_DEBUG_LEVEL_NONE
#endif

#if (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_ERROR)
    #define UNIVERSAL_DEVICE_DEBUG_ERROR(fmt, args...)  UNIVERSAL_DEVICE_DEBUG_TraceFormat(fmt, ##args)
    #define UNIVERSAL_DEVICE_DEBUG_ASSERT_FAILED()      UNIVERSAL_DEVICE_DEBUG_AssertFailed(__FILE__, __FUNCTION__, __LINE__)
    #define UNIVERSAL_DEVICE_DEBUG_ASSERT(condition)    do { if (!(condition)) UNIVERSAL_DEVICE_DEBUG_ASSERT_FAILED(); } while (false)
#else
    #define UNIVERSAL_DEVICE_DEBUG_ERROR(fmt, args...)  do { } while (false)
    #define UNIVERSAL_DEVICE_DEBUG_ASSERT_FAILED()      do { } while (false)
    #define UNIVERSAL_DEVICE_DEBUG_ASSERT(condition)    do { } while (false)
#endif

#if (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_INFO)
    #define UNIVERSAL_DEVICE_DEBUG_INFO(fmt, args...)   UNIVERSAL_DEVICE_DEBUG_TraceFormat(fmt, ##args)
#else
    #define UNIVERSAL_DEVICE_DEBUG_INFO(fmt, args...)   do { } while (false)
#endif

#if (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_TRACE)
    #define UNIVERSAL_DEVICE_DEBUG_TRACE(fmt, args...)  UNIVERSAL_DEVICE_DEBUG_TraceFormat(fmt, ##args)
#else
    #define UNIVERSAL_DEVICE_DEBUG_TRACE(fmt, args...)  do { } while (false)
#endif

#define UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED()                 UNIVERSAL_DEVICE_DEBUG_TraceFunctionInvoked(__FUNCTION__)
#define UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(error)                       UNIVERSAL_DEVICE_DEBUG_TraceError(__FUNCTION__, __LINE__, error)
#define UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(error)            UNIVERSAL_DEVICE_DEBUG_TraceErrorAndReturn(__FUNCTION__, __LINE__, error)
#define UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(error)    UNIVERSAL_DEVICE_DEBUG_TraceOnlyIfErrorAndReturn(__FUNCTION__, __LINE__, error)


void                                UNIVERSAL_DEVICE_DEBUG_TraceFunctionInvoked     (const char *pFunctionName);
void                                UNIVERSAL_DEVICE_DEBUG_TraceError               (const char *pFunctionName, uint32_t line, UNIVERSAL_DEVICE_ErrorTypeDef error);
UNIVERSAL_DEVICE_ErrorTypeDef       UNIVERSAL_DEVICE_DEBUG_TraceErrorAndReturn      (const char *pFunctionName, uint32_t line, UNIVERSAL_DEVICE_ErrorTypeDef error);
UNIVERSAL_DEVICE_ErrorTypeDef       UNIVERSAL_DEVICE_DEBUG_TraceOnlyIfErrorAndReturn(const char *pFunctionName, uint32_t line, UNIVERSAL_DEVICE_ErrorTypeDef error);

void                                UNIVERSAL_DEVICE_DEBUG_TraceFormat              (const char *fmt, ...);
void                                UNIVERSAL_DEVICE_DEBUG_AssertFailed             (const char *pFileName, const char *pFunctionName, uint32_t line);


#endif /* __UNIVERSAL_DEVICE_DEBUG_H */
