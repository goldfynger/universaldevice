#ifndef __UNIVERSAL_DEVICE_NRF24_H
#define __UNIVERSAL_DEVICE_NRF24_H

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_NRF24


#include "universal_device.h"

#ifdef UNIVERSAL_DEVICE_CONFIG_OS2
    #include "cmsis_os2.h"
    #include "cmsis_os2_ex.h"
#else
    #error "UNIVERSAL_DEVICE_CONFIG_OS2 must be defined in universal_device_config.h."
#endif


UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_NRF24_Initialise               (void);
UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_NRF24_Deinitialise             (void);
UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_NRF24_ProcessRequestMessage    (UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);

/* __weak functions. */
osSemaphoreId_t                 UNIVERSAL_DEVICE_NRF24_GetIRQSemaphore          (void);
void                            UNIVERSAL_DEVICE_NRF24_TxEvent                  (void);
void                            UNIVERSAL_DEVICE_NRF24_RxEvent                  (void);


#endif /* UNIVERSAL_DEVICE_CONFIG_NRF24 */

#endif /* __UNIVERSAL_DEVICE_NRF24_H */
