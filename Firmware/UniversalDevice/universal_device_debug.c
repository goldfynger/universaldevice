#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include "trace.h"
#include "universal_device.h"
#include "universal_device_config.h"
#include "universal_device_debug.h"

#ifdef UNIVERSAL_DEVICE_CONFIG_OS2
    #include "cmsis_os2.h"
    #include "cmsis_os2_ex.h"
#endif


#if (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_ERROR)
    static const char * UNIVERSAL_DEVICE_DEBUG_ErrorToString(UNIVERSAL_DEVICE_ErrorTypeDef error);
#endif


void UNIVERSAL_DEVICE_DEBUG_TraceFunctionInvoked(const char *pFunctionName)
{
#if (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_TRACE)

    static const char sTraceFunctionInvoked[] = "%s invoked.\r\n";

#endif

    UNIVERSAL_DEVICE_DEBUG_TRACE(sTraceFunctionInvoked, pFunctionName);
}

void UNIVERSAL_DEVICE_DEBUG_TraceError(const char *pFunctionName, uint32_t line, UNIVERSAL_DEVICE_ErrorTypeDef error)
{
#if (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_ERROR)

    static const char sErrorAndReturn[] = "Error in %s at %u: %s.\r\n";

#endif

    UNIVERSAL_DEVICE_DEBUG_ERROR(sErrorAndReturn, pFunctionName, line, UNIVERSAL_DEVICE_DEBUG_ErrorToString(error));
}

UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_DEBUG_TraceErrorAndReturn(const char *pFunctionName, uint32_t line, UNIVERSAL_DEVICE_ErrorTypeDef error)
{
    UNIVERSAL_DEVICE_DEBUG_TraceError(pFunctionName, line, error);

    return error;
}

UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_DEBUG_TraceOnlyIfErrorAndReturn(const char *pFunctionName, uint32_t line, UNIVERSAL_DEVICE_ErrorTypeDef error)
{
    if (error != UNIVERSAL_DEVICE_ERR_OK)
    {
        UNIVERSAL_DEVICE_DEBUG_TraceError(pFunctionName, line, error);
    }

    return error;
}

void UNIVERSAL_DEVICE_DEBUG_TraceFormat(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    TRACE_VFormat(fmt, args);

    va_end(args);
}

void UNIVERSAL_DEVICE_DEBUG_AssertFailed(const char *pFileName, const char *pFunctionName, uint32_t line)
{
    (void)pFileName;

    TRACE_Format("Assert failed in %s at %u.\r\n", pFunctionName, line);

    while (true)
    {
    #ifdef UNIVERSAL_DEVICE_CONFIG_OS2
        osDelay(osWaitForever);
    #endif
    }
}


#if (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_ERROR)
#define __TO_STRING(x) (#x)
#define TO_STRING(x) __TO_STRING(x)

static const char * UNIVERSAL_DEVICE_DEBUG_ErrorToString(UNIVERSAL_DEVICE_ErrorTypeDef error)
{
    static const char *ssErrorList[] =
    {
        TO_STRING(UNIVERSAL_DEVICE_ERR_OK),

        TO_STRING(UNIVERSAL_DEVICE_ERR_INVALID_PARAMETER),

        TO_STRING(UNIVERSAL_DEVICE_ERR_MESSAGE_ID),
        TO_STRING(UNIVERSAL_DEVICE_ERR_MESSAGE_SIZE_OR_OFFSET),
        TO_STRING(UNIVERSAL_DEVICE_ERR_MESSAGE_FRAGMENTATION),

        TO_STRING(UNIVERSAL_DEVICE_ERR_INTERFACE_NOT_INITIALISED),
        TO_STRING(UNIVERSAL_DEVICE_ERR_INTERFACE_INITIALISATION),
        TO_STRING(UNIVERSAL_DEVICE_ERR_INTERFACE_UNKNOWN_OR_INVALID),
        TO_STRING(UNIVERSAL_DEVICE_ERR_INTERFACE_NOT_AVAILABLE),
        TO_STRING(UNIVERSAL_DEVICE_ERR_INTERFACE_BUSY),

        TO_STRING(UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC),

        TO_STRING(UNIVERSAL_DEVICE_ERR_PORT_UNKNOWN_OR_INVALID),
        TO_STRING(UNIVERSAL_DEVICE_ERR_PORT_NOT_AVAILABLE),

        TO_STRING(UNIVERSAL_DEVICE_ERR_COMMAND_UNKNOWN_OR_INVALID),
        TO_STRING(UNIVERSAL_DEVICE_ERR_COMMAND_INVALID_IN_CURRENT_CONTEXT),

        TO_STRING(UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_NOT_ACQUIRED),
        TO_STRING(UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_ALREADY_ACQUIRED),
        TO_STRING(UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_CANNOT_BE_ACQUIRED),
        TO_STRING(UNIVERSAL_DEVICE_ERR_RESOURCE_ACCESS_CANNOT_BE_RELEASED),

        TO_STRING(UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED),
        TO_STRING(UINVERSAL_DEVICE_ERR_OS_OBJECT_CANNOT_BE_CREATED),
        TO_STRING(UNIVERSAL_DEVICE_ERR_OS_FUNCTION_FAIL),
        
        TO_STRING(UNIVERSAL_DEVICE_ERR_EXTERNAL_LIBRARY_SPECIFIC),

        TO_STRING(UNIVERSAL_DEVICE_ERR_HAL),

        TO_STRING(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED),
        
        TO_STRING(UNIVERSAL_DEVICE_ERR_TIMEOUT),

        TO_STRING(__UNIVERSAL_DEVICE_ERR_UNKNOWN),
    };

    if (error > __UNIVERSAL_DEVICE_ERR_UNKNOWN)
    {
        error = __UNIVERSAL_DEVICE_ERR_UNKNOWN;
    }

    return ssErrorList[error];
}

#undef TO_STRING
#undef __TO_STRING
#endif /* (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_ERROR) */
