#ifndef __UNIVERSAL_DEVICE_I2C_H
#define __UNIVERSAL_DEVICE_I2C_H

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_I2C


#include <stdbool.h>
#include <stdint.h>
#include "universal_device.h"


#define UNIVERSAL_DEVICE_I2C_BUS_SPEED_STANDART         ((uint32_t) 100000)
#define UNIVERSAL_DEVICE_I2C_MEM_ADDR_SIZE_8            ((uint16_t) 8)
#define UNIVERSAL_DEVICE_I2C_MEM_ADDR_SIZE_16           ((uint16_t) 16)

#define UNIVERSAL_DEVICE_I2C_HAL_ERR_NONE               ((UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef)0x00000000)

#define UNIVERSAL_DEVICE_I2C_MAX_PORT_NUMBER            (UNIVERSAL_DEVICE_I2C_PORT_7)


typedef enum
{
    UNIVERSAL_DEVICE_I2C_PORT_CONTROL = ((uint8_t)  0x00),
    
    UNIVERSAL_DEVICE_I2C_PORT_1,
    UNIVERSAL_DEVICE_I2C_PORT_2,
    UNIVERSAL_DEVICE_I2C_PORT_3,
    UNIVERSAL_DEVICE_I2C_PORT_4,
    UNIVERSAL_DEVICE_I2C_PORT_5,
    UNIVERSAL_DEVICE_I2C_PORT_6,
    UNIVERSAL_DEVICE_I2C_PORT_7,

    __UNIVERSAL_DEVICE_I2C_PORT_UNKNOWN,
}
UNIVERSAL_DEVICE_I2C_PortTypeDef;


typedef uint32_t UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef;


UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_I2C_Initialise                         (void);
UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_I2C_ProcessRequestMessage              (UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);
bool                            UNIVERSAL_DEVICE_I2C_IsPortAvailable                    (UNIVERSAL_DEVICE_I2C_PortTypeDef port);

/* __weak functions. */
bool                                    UNIVERSAL_DEVICE_I2C_HAL_IsAcquired             (UNIVERSAL_DEVICE_I2C_PortTypeDef port);
UNIVERSAL_DEVICE_ErrorTypeDef           UNIVERSAL_DEVICE_I2C_HAL_Acquire                (UNIVERSAL_DEVICE_I2C_PortTypeDef port);
UNIVERSAL_DEVICE_ErrorTypeDef           UNIVERSAL_DEVICE_I2C_HAL_Release                (UNIVERSAL_DEVICE_I2C_PortTypeDef port);
UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef   UNIVERSAL_DEVICE_I2C_HAL_Read                   (UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint8_t *pData, uint16_t dataSize);
UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef   UNIVERSAL_DEVICE_I2C_HAL_Write                  (UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint8_t *pData, uint16_t dataSize);
UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef   UNIVERSAL_DEVICE_I2C_HAL_ReadMemory
    (UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint16_t memoryAddress, uint16_t memoryAddressSize, uint8_t *pData, uint16_t dataSize);
UNIVERSAL_DEVICE_I2C_HAL_ErrorTypeDef   UNIVERSAL_DEVICE_I2C_HAL_WriteMemory
    (UNIVERSAL_DEVICE_I2C_PortTypeDef port, uint16_t deviceAddress, uint16_t memoryAddress, uint16_t memoryAddressSize, uint8_t *pData, uint16_t dataSize);


#endif /* UNIVERSAL_DEVICE_CONFIG_I2C */

#endif /* __UNIVERSAL_DEVICE_I2C_H */
