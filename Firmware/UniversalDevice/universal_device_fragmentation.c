/*
Fragmentation and defragmentation mechanisms.
*/

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_FRAGMENTATION


#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "linked_list.h"
#include "universal_device.h"

#include "universal_device_debug.h"
#include "universal_device_fragmentation.h"


#if defined(UNIVERSAL_DEVICE_CONFIG_FRAGMENT_MAX_SIZE) && ((UNIVERSAL_DEVICE_CONFIG_FRAGMENT_MAX_SIZE <= UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE) || \
            (UNIVERSAL_DEVICE_CONFIG_FRAGMENT_MAX_SIZE > UNIVERSAL_DEVICE_MESSAGE_MAX_SIZE) || (UNIVERSAL_DEVICE_CONFIG_FRAGMENT_MAX_SIZE % 4 != 0))
    #error "Invalid value of UNIVERSAL_DEVICE_CONFIG_FRAGMENT_MAX_SIZE"
#endif


/* Each interface have own linked list item that contains buffer for defragmentation of whole message. */
LINKED_LIST_NodeTypeDef * _pDefragmentListHeadNode = NULL;


/* ppWholeMessage is NULL if defragmentation not completed. pMessageFragment and ppWholeMessage must be freed by calling code. UNIVERSAL_DEVICE_ResponseWithError used for error processing. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_FRAGMENTATION_Defragment(UNIVERSAL_DEVICE_MessageTypeDef *pMessageFragment,
                                                                        UNIVERSAL_DEVICE_MessageTypeDef **ppWholeMessage)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();

    UNIVERSAL_DEVICE_DEBUG_ASSERT(pMessageFragment != NULL);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(ppWholeMessage != NULL);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(*ppWholeMessage == NULL);

    uint16_t messageBodySize = pMessageFragment->Header.MessageBodySize;
    uint16_t fragmentBodySize = pMessageFragment->Header.FragmentBodySize;
    uint16_t fragmentBodyOffset = pMessageFragment->Header.FragmentBodyOffset;

    UNIVERSAL_DEVICE_DEBUG_ASSERT(messageBodySize <= UNIVERSAL_DEVICE_MESSAGE_BODY_MAX_SIZE && messageBodySize % 4 == 0);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(fragmentBodySize <= UNIVERSAL_DEVICE_MESSAGE_BODY_MAX_SIZE && fragmentBodySize % 4 == 0);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(fragmentBodyOffset + fragmentBodySize <= messageBodySize && fragmentBodyOffset % 4 == 0);


    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;


    /* Search for whole message for fragment interface. */
    int32_t nodeKey = (pMessageFragment->Header.InterfaceBase << 8) | pMessageFragment->Header.InterfacePort;

    LINKED_LIST_NodeTypeDef *pNode = LINKED_LIST_GetNode(&_pDefragmentListHeadNode, nodeKey);

    if (pNode == NULL)
    {
        pNode = LINKED_LIST_AddHead(&_pDefragmentListHeadNode, nodeKey, NULL);

        if (pNode == NULL)
        {
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pMessageFragment, UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED));
        }
    }


    /* Previous saved message found. */
    if (pNode->Value != NULL)
    {
        UNIVERSAL_DEVICE_MessageTypeDef *pNodeMessage = (UNIVERSAL_DEVICE_MessageTypeDef *)pNode->Value;

        /* Previous saved message and new fragment have different identifiers. Prevoius message must be freed. */
        if (pNodeMessage->Header.MessageId != pMessageFragment->Header.MessageId)
        {
            /* Fragmentation error for prevoius message, but current can be processed. */
            if ((error = UNIVERSAL_DEVICE_ResponseWithError(pNodeMessage, UNIVERSAL_DEVICE_ERR_MESSAGE_FRAGMENTATION)) != UNIVERSAL_DEVICE_ERR_OK)
            {
                return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(error);
            }

            UNIVERSAL_DEVICE_CONFIG_FREE(pNode->Value);
            pNode->Value = NULL;

            /* New fragment offset must be 0. */
            if (pMessageFragment->Header.FragmentBodyOffset != 0)
            {
                return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pMessageFragment, UNIVERSAL_DEVICE_ERR_MESSAGE_FRAGMENTATION));
            }
        }
        /* Fragmentation error. */
        else if (pNodeMessage->Header.FragmentBodySize != fragmentBodyOffset || pNodeMessage->Header.MessageBodySize != messageBodySize)
        {
            UNIVERSAL_DEVICE_CONFIG_FREE(pNode->Value);
            pNode->Value = NULL;

            return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pMessageFragment, UNIVERSAL_DEVICE_ERR_MESSAGE_FRAGMENTATION));
        }

        /* New fragment has right ID and can be added in whole message. */
    }
    else
    {
        /* New fragment offset must be 0. */
        if (pMessageFragment->Header.FragmentBodyOffset != 0)
        {
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pMessageFragment, UNIVERSAL_DEVICE_ERR_MESSAGE_FRAGMENTATION));
        }
    }


    /* Allocate new whole message if need. */
    UNIVERSAL_DEVICE_MessageTypeDef *pNodeMessage;

    if (pNode->Value != NULL)
    {
        /* Check interface by assert macros. */

        pNodeMessage = (UNIVERSAL_DEVICE_MessageTypeDef *)pNode->Value;

        UNIVERSAL_DEVICE_DEBUG_ASSERT(pNodeMessage->Header.InterfaceBase == pMessageFragment->Header.InterfaceBase);
        UNIVERSAL_DEVICE_DEBUG_ASSERT(pNodeMessage->Header.InterfacePort == pMessageFragment->Header.InterfacePort);
    }
    else
    {
        size_t nodeMessageSize = UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + messageBodySize;
        pNodeMessage = (UNIVERSAL_DEVICE_MessageTypeDef *)UNIVERSAL_DEVICE_CONFIG_MALLOC(nodeMessageSize);

        if (pNodeMessage == NULL)
        {
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pMessageFragment, UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED));
        }

        memset(pNodeMessage, 0, nodeMessageSize);

        pNodeMessage->Header.MessageId = pMessageFragment->Header.MessageId;
        pNodeMessage->Header.MessageBodySize = messageBodySize;

        pNodeMessage->Header.InterfaceBase = pMessageFragment->Header.InterfaceBase;
        pNodeMessage->Header.InterfacePort = pMessageFragment->Header.InterfacePort;
        pNodeMessage->Header.InterfaceCommand = pMessageFragment->Header.InterfaceCommand;
        pNodeMessage->Header.Error = pMessageFragment->Header.Error;
        
        pNode->Value = pNodeMessage;
    }


    /* Copy body of fragment. */
    if (fragmentBodySize != 0)
    {
        memcpy(pNodeMessage->Body + fragmentBodyOffset, pMessageFragment->Body, fragmentBodySize);
    }

    pNodeMessage->Header.FragmentBodySize += fragmentBodySize;


    /* Set whole message if defragmentation is complete. */
    if (pNodeMessage->Header.FragmentBodySize == messageBodySize)
    {
        *ppWholeMessage = pNodeMessage;
        pNode->Value = NULL;
    }


    return UNIVERSAL_DEVICE_ERR_OK;
}

/* Last fragment can be shorter than others. pWholeMessage and ppMessageFragmentsBuffer must be freed by calling code. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_FRAGMENTATION_Fragment(UNIVERSAL_DEVICE_MessageTypeDef *pWholeMessage,
                                                                      uint8_t **ppMessageFragmentsBuffer,
                                                                      uint16_t *pFragmentsCount)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();

    UNIVERSAL_DEVICE_DEBUG_ASSERT(pWholeMessage != NULL);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(ppMessageFragmentsBuffer != NULL);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(*ppMessageFragmentsBuffer == NULL);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(pFragmentsCount != NULL);

    uint16_t messageBodySize = pWholeMessage->Header.MessageBodySize;

    UNIVERSAL_DEVICE_DEBUG_ASSERT(messageBodySize <= UNIVERSAL_DEVICE_MESSAGE_BODY_MAX_SIZE && messageBodySize % 4 == 0);

    uint16_t fragmentBodySize = pWholeMessage->Header.FragmentBodySize;

    UNIVERSAL_DEVICE_DEBUG_ASSERT(fragmentBodySize <= UNIVERSAL_DEVICE_MESSAGE_BODY_MAX_SIZE && fragmentBodySize % 4 == 0);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(fragmentBodySize <= messageBodySize);

    uint16_t fragmentBodyOffset = pWholeMessage->Header.FragmentBodyOffset;

    UNIVERSAL_DEVICE_DEBUG_ASSERT(fragmentBodyOffset + fragmentBodySize <= messageBodySize && fragmentBodyOffset % 4 == 0);


    /* Compute count of fragments. */
    uint16_t sourceBodySize = fragmentBodySize;
    uint16_t fragmentMaxBodySize = UNIVERSAL_DEVICE_CONFIG_FRAGMENT_MAX_SIZE - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE;
    uint16_t lastFragmentBodySize;
    bool isLastFragmentFull;
    uint16_t fullFragmentsCount;

    if (sourceBodySize <= fragmentMaxBodySize)
    {
        if (sourceBodySize == fragmentMaxBodySize)
        {
            lastFragmentBodySize = fragmentMaxBodySize;
            isLastFragmentFull = true;
            fullFragmentsCount = 1;
        }
        else
        {
            lastFragmentBodySize = sourceBodySize;
            isLastFragmentFull = false;
            fullFragmentsCount = 0;
        }
    }
    else
    {
        fullFragmentsCount = sourceBodySize / fragmentMaxBodySize;

        if (sourceBodySize % fragmentMaxBodySize == 0)
        {
            lastFragmentBodySize = fragmentMaxBodySize;
            isLastFragmentFull = true;
        }
        else
        {
            lastFragmentBodySize = sourceBodySize - (fullFragmentsCount * fragmentMaxBodySize); /* Whole message is multiple of 4, so last fragment body size is multiple of 4 automatically. */
            isLastFragmentFull = false;
        }
    }


    /* Allocate buffer for fragments. */
    size_t bufferSize = UNIVERSAL_DEVICE_CONFIG_FRAGMENT_MAX_SIZE * fullFragmentsCount;

    if (!isLastFragmentFull)
    {
        bufferSize += UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + lastFragmentBodySize;
    }

    uint8_t *pMessageFragmentsBuffer = UNIVERSAL_DEVICE_CONFIG_MALLOC(bufferSize);

    if (pMessageFragmentsBuffer == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED);
    }

    memset(pMessageFragmentsBuffer, 0, sizeof(bufferSize));


    /* Fragmentate message. */
    for (uint16_t fragmentIdx = 0; fragmentIdx <= fullFragmentsCount; fragmentIdx++)
    {
        size_t headerDestinationIdx = fragmentIdx * UNIVERSAL_DEVICE_CONFIG_FRAGMENT_MAX_SIZE;
        size_t bodyOffset = fragmentIdx * fragmentMaxBodySize;
        size_t bodySourceIdx = UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + bodyOffset;
        size_t bodyDestinationIdx = headerDestinationIdx + UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE;
        size_t bodySize;

        if (fragmentIdx == fullFragmentsCount)
        {
            if (isLastFragmentFull)
            {
                break;
            }
            else
            {
                bodySize = lastFragmentBodySize;
            }
        }
        else
        {
            bodySize = fragmentMaxBodySize;
        }


        /* Copy header. */
        memcpy(pMessageFragmentsBuffer + headerDestinationIdx, pWholeMessage, UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE);

        UNIVERSAL_DEVICE_MessageHeaderTypeDef *pDestinationHeader = (UNIVERSAL_DEVICE_MessageHeaderTypeDef *)(pMessageFragmentsBuffer + headerDestinationIdx);
        pDestinationHeader->FragmentBodyOffset = bodyOffset + fragmentBodyOffset;
        pDestinationHeader->FragmentBodySize = bodySize;

        /* Copy part of body. */
        memcpy(pMessageFragmentsBuffer + bodyDestinationIdx, ((uint8_t *)pWholeMessage) + bodySourceIdx, bodySize);
    }

    *ppMessageFragmentsBuffer = pMessageFragmentsBuffer;
    *pFragmentsCount = fullFragmentsCount + (isLastFragmentFull ? 0 : 1);


    return UNIVERSAL_DEVICE_ERR_OK;
}


#endif /* UNIVERSAL_DEVICE_CONFIG_FRAGMENTATION */
