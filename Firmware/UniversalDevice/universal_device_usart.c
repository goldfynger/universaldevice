/*
Not implemented yet.
Processes UniversalDevice USART messages asynchronously in background thread.
*/

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_USART


#include <stdint.h>
#include "universal_device.h"
#include "universal_device_debug.h"
#include "universal_device_usart.h"


UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_USART_Initialise(void)
{
    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}

UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_USART_ProcessMessage(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage, UNIVERSAL_DEVICE_MessageTypeDef **ppResponseMessages, uint8_t *pResponseCount)
{
    (void)pRequestMessage;
    (void)ppResponseMessages;
    (void)pResponseCount;

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}

__weak UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_USART_InitiativeMessageCallback(UNIVERSAL_DEVICE_MessageTypeDef *pInitiativeMessages, uint8_t initiativeCount)
{
    /* All pInitiativeMessages units MUST be freed in code that wait for messages. */
    for (uint8_t initiativeIndex = 0; initiativeIndex < initiativeCount; initiativeIndex++)
    {
        UNIVERSAL_DEVICE_CONFIG_FREE((UNIVERSAL_DEVICE_MessageTypeDef *)(pInitiativeMessages + initiativeIndex));
    }

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_NOT_SUPPORTED_OR_NOT_IMPLEMENTED);
}


#endif /* UNIVERSAL_DEVICE_CONFIG_USART */
