#ifndef __UNIVERSAL_DEVICE_INFO_H
#define __UNIVERSAL_DEVICE_INFO_H

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_INFO


#include <stdint.h>
#include "universal_device.h"


UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_INFO_Initialise            (void);
UNIVERSAL_DEVICE_ErrorTypeDef   UNIVERSAL_DEVICE_INFO_ProcessRequestMessage (UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);


#endif /* UNIVERSAL_DEVICE_CONFIG_INFO */

#endif /* __UNIVERSAL_DEVICE_INFO_H */
