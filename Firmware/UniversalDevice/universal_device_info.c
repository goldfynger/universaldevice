/*
Provides access to device info functions.
*/

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_INFO


#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "device_info.h"
#include "universal_device.h"
#include "universal_device_debug.h"
#include "universal_device_info.h"

#ifdef UNIVERSAL_DEVICE_CONFIG_OS2
    #include "cmsis_os2.h"
    #include "cmsis_os2_ex.h"
#endif


typedef enum
{
    /* Error: UNIVERSAL_DEVICE_MessageTypeDef */

    /* Request: UNIVERSAL_DEVICE_MessageTypeDef */
    /* Response: UNIVERSAL_DEVICE_INFO_InformationResponseTypeDef */
    UNIVERSAL_DEVICE_INFO_CMD_GET_INFORMATION   = ((uint8_t)    0x20),
    /* Request: UNIVERSAL_DEVICE_MessageTypeDef */
    /* Response: UNIVERSAL_DEVICE_INFO_UidResponseTypeDef */
    UNIVERSAL_DEVICE_INFO_CMD_GET_UID           = ((uint8_t)    0x21),

    __UNIVERSAL_DEVICE_INFO_CMD_UNKNOWN         = ((uint8_t)    0xFF),
}
UNIVERSAL_DEVICE_INFO_CommandTypeDef;


/* Error response same with UNIVERSAL_DEVICE_MessageTypeDef so we can use UNIVERSAL_DEVICE_ResponseWithError function. */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    DEVICE_INFO_InformationTypeDef          Information;
}
UNIVERSAL_DEVICE_INFO_InformationResponseTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 4 */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    DEVICE_INFO_UidTypeDef                  Uid;
}
UNIVERSAL_DEVICE_INFO_UidResponseTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 12 */


static const char * UNIVERSAL_DEVICE_INFO_CommandToString(UNIVERSAL_DEVICE_INFO_CommandTypeDef command);


UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_INFO_Initialise(void)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();

    return UNIVERSAL_DEVICE_ERR_OK;
}

UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_INFO_ProcessRequestMessage(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();

    UNIVERSAL_DEVICE_DEBUG_ASSERT(pRequestMessage != NULL);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(pRequestMessage->Header.InterfaceBase == UNIVERSAL_DEVICE_INTERFACE_BASE_INFO);


    if (pRequestMessage->Header.InterfacePort != 0)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_PORT_UNKNOWN_OR_INVALID));
    }


    UNIVERSAL_DEVICE_INFO_CommandTypeDef command = (UNIVERSAL_DEVICE_INFO_CommandTypeDef)pRequestMessage->Header.InterfaceCommand;

    UNIVERSAL_DEVICE_DEBUG_INFO("%s: command %s.\r\n", __FUNCTION__, UNIVERSAL_DEVICE_INFO_CommandToString(command));


    size_t responseSize = 0;

    switch (command)
    {
        case UNIVERSAL_DEVICE_INFO_CMD_GET_INFORMATION:
        {
            responseSize = sizeof(UNIVERSAL_DEVICE_INFO_InformationResponseTypeDef);
        }
        break;

        case UNIVERSAL_DEVICE_INFO_CMD_GET_UID:
        {
            responseSize = sizeof(UNIVERSAL_DEVICE_INFO_UidResponseTypeDef);
        }
        break;

        default:
        {
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_COMMAND_UNKNOWN_OR_INVALID));
        }
    }


    UNIVERSAL_DEVICE_MessageTypeDef *pResponse = (UNIVERSAL_DEVICE_MessageTypeDef *)UNIVERSAL_DEVICE_CONFIG_MALLOC(responseSize);

    if (pResponse == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED));
    }

    memset(pResponse, 0, responseSize);

    pResponse->Header.MessageId = pRequestMessage->Header.MessageId;
    pResponse->Header.MessageBodySize = responseSize - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE;
    /* FragmentBodyOffset is already 0. */
    pResponse->Header.FragmentBodySize = pResponse->Header.MessageBodySize;
    pResponse->Header.InterfaceBase = pRequestMessage->Header.InterfaceBase;
    pResponse->Header.InterfacePort = pRequestMessage->Header.InterfacePort;
    pResponse->Header.InterfaceCommand = pRequestMessage->Header.InterfaceCommand;
    /* Error is already 0 (UNIVERSAL_DEVICE_ERR_OK). */


    switch (command)
    {
        case UNIVERSAL_DEVICE_INFO_CMD_GET_INFORMATION:
        {
            UNIVERSAL_DEVICE_INFO_InformationResponseTypeDef *pInformationResponse = (UNIVERSAL_DEVICE_INFO_InformationResponseTypeDef *)pResponse;

            const DEVICE_INFO_InformationTypeDef *pDeviceInformation = DEVICE_INFO_GetInformation();

            memcpy(&pInformationResponse->Information, pDeviceInformation, sizeof(DEVICE_INFO_InformationTypeDef));
        }
        break;

        case UNIVERSAL_DEVICE_INFO_CMD_GET_UID:
        {
            UNIVERSAL_DEVICE_INFO_UidResponseTypeDef *pUidResponse = (UNIVERSAL_DEVICE_INFO_UidResponseTypeDef *)pResponse;

            const DEVICE_INFO_UidTypeDef *pDeviceUid = DEVICE_INFO_GetUid();

            memcpy(&pUidResponse->Uid, pDeviceUid, sizeof(DEVICE_INFO_UidTypeDef));
        }
        break;

        default:
        break;
    }


    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ProcessResponseOrInitiativeMessage(pResponse);

    UNIVERSAL_DEVICE_CONFIG_FREE(pResponse);

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(error);
}


#if (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_INFO)
#define __TO_STRING(x) (#x)
#define TO_STRING(x) __TO_STRING(x)

static const char * UNIVERSAL_DEVICE_INFO_CommandToString(UNIVERSAL_DEVICE_INFO_CommandTypeDef command)
{
    static const UNIVERSAL_DEVICE_INFO_CommandTypeDef pCommandIndexList[] =
    {
        UNIVERSAL_DEVICE_INFO_CMD_GET_INFORMATION,
        UNIVERSAL_DEVICE_INFO_CMD_GET_UID,
    
        __UNIVERSAL_DEVICE_INFO_CMD_UNKNOWN,
    };    

    static const char *ssCommandList[] =
    {
        TO_STRING(UNIVERSAL_DEVICE_INFO_CMD_GET_INFORMATION),
        TO_STRING(UNIVERSAL_DEVICE_INFO_CMD_GET_UID),
        
        TO_STRING(__UNIVERSAL_DEVICE_INFO_CMD_UNKNOWN),
    };

    size_t commandCount = sizeof(pCommandIndexList) / sizeof(UNIVERSAL_DEVICE_INFO_CommandTypeDef);

    uint8_t index;
    for (index = 0; index < commandCount; index++)
    {
        if (pCommandIndexList[index] == command)
        {
            break;
        }
    }

    return ssCommandList[index];
}

#undef TO_STRING
#undef __TO_STRING
#endif /* (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_INFO) */


#endif /* UNIVERSAL_DEVICE_CONFIG_INFO */
