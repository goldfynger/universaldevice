/*
 * Processes UniversalDevice Nrf24 messages asynchronously in background thread.
 */

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_NRF24


#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "nrf24.h"
#include "universal_device.h"
#include "universal_device_debug.h"
#include "universal_device_nrf24.h"

#ifdef UNIVERSAL_DEVICE_CONFIG_OS2
    #include "cmsis_os2.h"
    #include "cmsis_os2_ex.h"
#else
    #error "UNIVERSAL_DEVICE_CONFIG_OS2 must be defined in universal_device_config.h."
#endif


#ifndef UNIVERSAL_DEVICE_CONFIG_NRF24_TASK_PRIORITY
    #define UNIVERSAL_DEVICE_CONFIG_NRF24_TASK_PRIORITY osPriorityBelowNormal
#endif

#ifndef UNIVERSAL_DEVICE_CONFIG_NRF24_QUEUE_SIZE
    #define UNIVERSAL_DEVICE_CONFIG_NRF24_QUEUE_SIZE    4U
#endif

#define UNIVERSAL_DEVICE_NRF24_SEND_TIMEOUT             100U

#define UNIVERSAL_DEVICE_DEBUG_ERROR_NRF24(error)       UNIVERSAL_DEVICE_DEBUG_ERROR("NRF24 error in %s at %u: 0x%02X.\r\n", __FUNCTION__, __LINE__, error);


typedef enum
{
    /* Error: UNIVERSAL_DEVICE_NRF24_ErrorResponseTypeDef */

    /* Request: UNIVERSAL_DEVICE_MessageTypeDef */
    /* Response: UNIVERSAL_DEVICE_MessageTypeDef */
    UNIVERSAL_DEVICE_NRF24_CMD_RESET        = ((uint8_t)    0x08),

    /* Request: UNIVERSAL_DEVICE_NRF24_SetConfigRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_MessageTypeDef */
    UNIVERSAL_DEVICE_NRF24_CMD_SET_CONFIG   = ((uint8_t)    0x30),

    /* Request: UNIVERSAL_DEVICE_NRF24_SendRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_MessageTypeDef */
    UNIVERSAL_DEVICE_NRF24_CMD_SEND         = ((uint8_t)    0x40),

    /* Request: UNIVERSAL_DEVICE_NRF24_StartListenRequestTypeDef */
    /* Response: UNIVERSAL_DEVICE_MessageTypeDef */
    UNIVERSAL_DEVICE_NRF24_CMD_START_LISTEN = ((uint8_t)    0x50),
    /* Request: UNIVERSAL_DEVICE_MessageTypeDef */
    /* Response: UNIVERSAL_DEVICE_MessageTypeDef */
    UNIVERSAL_DEVICE_NRF24_CMD_STOP_LISTEN  = ((uint8_t)    0x51),
    /* Initiative response: UNIVERSAL_DEVICE_NRF24_ListenResponseTypeDef */
    UNIVERSAL_DEVICE_NRF24_CMD_LISTEN       = ((uint8_t)    0x52),

    __UNIVERSAL_DEVICE_NRF24_CMD_UNKNOWN    = ((uint8_t)    0xFF),
}
UNIVERSAL_DEVICE_NRF24_CommandTypeDef;


typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    NRF24_ErrorTypeDef                      NRF24Error;

    uint8_t                                 __Reserved[3];
}
UNIVERSAL_DEVICE_NRF24_ErrorResponseTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 4 */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    NRF24_ConfigTypeDef                     Config;
}
UNIVERSAL_DEVICE_NRF24_SetConfigRequestTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 4 */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    NRF24_TxConfigTypeDef                   TxConfig;

    uint8_t                                 DataLength;

    uint8_t                                 __Reserved[3];

    uint8_t                                 Data[]; /* Up to 32 */
}
UNIVERSAL_DEVICE_NRF24_SendRequestTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 12 + (DataLength multiple by 4 - up to 32) */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    NRF24_RxConfigTypeDef                   RxConfig;
}
UNIVERSAL_DEVICE_NRF24_StartListenRequestTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 16 */

typedef struct
{
    UNIVERSAL_DEVICE_MessageHeaderTypeDef   Header;

    uint8_t                                 RxAddress[NRF24_MAX_ADDR_LENGTH];

    uint8_t                                 DataLength;

    NRF24_RxDataPipesTypeDef                RxPipe;

    uint8_t                                 __Reserved[1];

    uint8_t                                 Data[]; /* Up to 32 */
}
UNIVERSAL_DEVICE_NRF24_ListenResponseTypeDef; /* UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + 8 + (DataLength multiple by 4 - up to 32) */


static osThreadId_t _nrf24WorkerTaskHandle;
static const osThreadAttr_t _nrf24WorkerTask_attributes = {
    .name = "uniDevNrf24Task",
    .attr_bits = osThreadDetached,
    .cb_mem = NULL,
    .cb_size = 0,
    .stack_mem = NULL,
    .stack_size = 192 * 4,
    .priority = (osPriority_t) UNIVERSAL_DEVICE_CONFIG_NRF24_TASK_PRIORITY,
    .tz_module = 0,
    .reserved = 0,
};

static osMessageQueueId_t _nrf24WorkerInQueueHandle;
static const osMessageQueueAttr_t _nrf24WorkerInQueue_attributes = {
    .name = "nrf24WorkerInQueue",
    .attr_bits = 0,
    .cb_mem = NULL,
    .cb_size = 0,
    .mq_mem = NULL,
    .mq_size = 0,
};

static CMSIS_OS2_EX_TaskStateTypeDef _nrf24WorkerTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;

static bool _isInitialised = false;

static NRF24_HandleTypeDef _nrf24Handle = {NULL};


static __NO_RETURN void UNIVERSAL_DEVICE_NRF24_Task(void *argument);
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ProcessSetConfig(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ProcessSend(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ProcessStartListen(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ProcessStopListen(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage);
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ProcessListen(void);
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ResponseWithError(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage, NRF24_ErrorTypeDef nrf24Error);
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ResponseWithData(uint8_t *pData, uint8_t length, uint8_t *pAddress, NRF24_RxDataPipesTypeDef rxPipe);

static const char * UNIVERSAL_DEVICE_NRF24_CommandToString(UNIVERSAL_DEVICE_NRF24_CommandTypeDef command);


UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_Initialise(void)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();


    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;

    if ((error = UNIVERSAL_DEVICE_NRF24_Deinitialise()) != UNIVERSAL_DEVICE_ERR_OK)
    {
        return error; /* Error traced inside of function. */
    }


    /* Queue contains pointers on messages. */
    _nrf24WorkerInQueueHandle = osMessageQueueNew(UNIVERSAL_DEVICE_CONFIG_NRF24_QUEUE_SIZE, sizeof(UNIVERSAL_DEVICE_MessageTypeDef *), &_nrf24WorkerInQueue_attributes);

    if (_nrf24WorkerInQueueHandle == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_OS_OBJECT_CANNOT_BE_CREATED);
    }


    /* Create main worker task after another OS objects. */
    _nrf24WorkerTaskHandle = osThreadNew(UNIVERSAL_DEVICE_NRF24_Task, NULL, &_nrf24WorkerTask_attributes);

    if (_nrf24WorkerTaskHandle == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_OS_OBJECT_CANNOT_BE_CREATED);
    }


    _isInitialised = true;

    return UNIVERSAL_DEVICE_ERR_OK;
}

UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_Deinitialise(void)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();

    _isInitialised = false;


    /* Terminate main worker thread first. */
    if (_nrf24WorkerTaskHandle != NULL)
    {        
        if (osThreadTerminate(_nrf24WorkerTaskHandle) != osOK)
        {
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_OS_FUNCTION_FAIL);
        }

        _nrf24WorkerTaskState = CMSIS_OS2_EX_TASK_STATE_TERMINATED;
    }


    /* Than delete another OS objects. */
    if (_nrf24WorkerInQueueHandle != NULL)
    {
        if (osMessageQueueDelete(_nrf24WorkerInQueueHandle) != osOK)
        {
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_OS_FUNCTION_FAIL);
        }
    }


    bool isNrf24Init = false;

    if (NRF24_IsInit(&_nrf24Handle, &isNrf24Init) != NRF24_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC);
    }

    if (isNrf24Init)
    {
        if (NRF24_DeInit(&_nrf24Handle) != NRF24_ERR_OK)
        {
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC);
        }
    }


    return UNIVERSAL_DEVICE_ERR_OK;
}

UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ProcessRequestMessage(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE_FUNCTION_INVOKED();

    UNIVERSAL_DEVICE_DEBUG_ASSERT(pRequestMessage != NULL);
    UNIVERSAL_DEVICE_DEBUG_ASSERT(pRequestMessage->Header.InterfaceBase == UNIVERSAL_DEVICE_INTERFACE_BASE_NRF24);


    if (pRequestMessage->Header.InterfacePort != 0) /* There is no implementation for multiple ports - only one nRF24 can be used with universal device. */
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_PORT_UNKNOWN_OR_INVALID));
    }


    UNIVERSAL_DEVICE_NRF24_CommandTypeDef command = (UNIVERSAL_DEVICE_NRF24_CommandTypeDef)pRequestMessage->Header.InterfaceCommand;

    UNIVERSAL_DEVICE_DEBUG_INFO("%s: command %s.\r\n", __FUNCTION__, UNIVERSAL_DEVICE_NRF24_CommandToString(command));


    /* Process reset command here. */
    if (command == UNIVERSAL_DEVICE_NRF24_CMD_RESET)
    {
        UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;

        if ((error = UNIVERSAL_DEVICE_NRF24_Initialise()) != UNIVERSAL_DEVICE_ERR_OK)
        {
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, error));
        }

        UNIVERSAL_DEVICE_DEBUG_INFO("%s: interface reset completed.\r\n", __FUNCTION__);

        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithOk(pRequestMessage));
    }


    if (!_isInitialised)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_INTERFACE_NOT_INITIALISED);
    }


    /* Allocate copy of message to put it in queue. */
    size_t requestMessageSize = UNIVERSAL_DEVICE_GetMessageSize(pRequestMessage);
    UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessageCopy = (UNIVERSAL_DEVICE_MessageTypeDef *)osExMalloc(requestMessageSize);

    if (pRequestMessageCopy == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED));
    }

    memcpy(pRequestMessageCopy, pRequestMessage, requestMessageSize);


    /* Put pointer on message in queue. In case of success message (copy of message) must be freed in worker thread. Response also will be created by worker thread. */
    if (osExMessageQueuePutTry(_nrf24WorkerInQueueHandle, &pRequestMessageCopy) != osOK)
    {
        osExFree(pRequestMessageCopy);

        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_INTERFACE_BUSY));
    }


    return UNIVERSAL_DEVICE_ERR_OK;
}


static __NO_RETURN void UNIVERSAL_DEVICE_NRF24_Task(void *argument)
{
    UNIVERSAL_DEVICE_DEBUG_TRACE("%s started.\r\n", __FUNCTION__);

    _nrf24WorkerTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;


    while(true)
    {
        UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;

        UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage = NULL;

        osStatus_t queueGetResult = osMessageQueueGet(_nrf24WorkerInQueueHandle, &pRequestMessage, NULL, 1); /* Wait up to 1 OS tick. */

        if (queueGetResult == osOK) /* New universal device message. */
        {
            UNIVERSAL_DEVICE_NRF24_CommandTypeDef command = (UNIVERSAL_DEVICE_NRF24_CommandTypeDef)pRequestMessage->Header.InterfaceCommand;

            switch (command)
            {
                case UNIVERSAL_DEVICE_NRF24_CMD_SET_CONFIG:
                {
                    if ((error = UNIVERSAL_DEVICE_NRF24_ProcessSetConfig(pRequestMessage)) != UNIVERSAL_DEVICE_ERR_OK)
                    {
                        UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(error);
                        goto Free;
                    }
                }
                break;

                case UNIVERSAL_DEVICE_NRF24_CMD_SEND:
                {
                    if ((error = UNIVERSAL_DEVICE_NRF24_ProcessSend(pRequestMessage)) != UNIVERSAL_DEVICE_ERR_OK)
                    {
                        UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(error);
                        goto Free;
                    }
                }
                break;

                case UNIVERSAL_DEVICE_NRF24_CMD_START_LISTEN:
                {
                    if ((error = UNIVERSAL_DEVICE_NRF24_ProcessStartListen(pRequestMessage)) != UNIVERSAL_DEVICE_ERR_OK)
                    {
                        UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(error);
                        goto Free;
                    }
                }
                break;

                case UNIVERSAL_DEVICE_NRF24_CMD_STOP_LISTEN:
                {
                    if ((error = UNIVERSAL_DEVICE_NRF24_ProcessStopListen(pRequestMessage)) != UNIVERSAL_DEVICE_ERR_OK)
                    {
                        UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(error);
                        goto Free;
                    }
                }
                break;

                default:
                {
                    if ((error = UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_COMMAND_UNKNOWN_OR_INVALID)) != UNIVERSAL_DEVICE_ERR_OK)
                    {
                        UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(error);
                        goto Free;
                    }
                }
            }

            UNIVERSAL_DEVICE_DEBUG_TRACE("%s: request with ID:%u processed.\r\n", __FUNCTION__, pRequestMessage->Header.MessageId);

            Free:
            osExFree(pRequestMessage);

            if (error != UNIVERSAL_DEVICE_ERR_OK)
            {
                goto Error;
            }
        }
        else if (queueGetResult == osErrorTimeout) /* Universal device queue empty - check for new received nRF24 messages. */
        {
            if ((error = UNIVERSAL_DEVICE_NRF24_ProcessListen()) != UNIVERSAL_DEVICE_ERR_OK)
            {
                UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR(error);
                goto Error;
            }
        }
        else
        {
            UNIVERSAL_DEVICE_DEBUG_ASSERT_FAILED();
            goto Error;
        }
    }

    Error:
    NRF24_DeInit(&_nrf24Handle);

    _nrf24WorkerTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;

    UNIVERSAL_DEVICE_DEBUG_ERROR("%s: stop task.\r\n", __FUNCTION__);

    while (true)
    {
        osDelay(osWaitForever);
    }
}

/* pRequestMessage must be freed by calling code. */
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ProcessSetConfig(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{
    NRF24_ErrorTypeDef nrf24Error = NRF24_ERR_OK;

    UNIVERSAL_DEVICE_NRF24_SetConfigRequestTypeDef *pSetConfigRequest = (UNIVERSAL_DEVICE_NRF24_SetConfigRequestTypeDef *)pRequestMessage;

    if ((nrf24Error = NRF24_Init(&_nrf24Handle, &pSetConfigRequest->Config, NULL)) != NRF24_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_NRF24_ResponseWithError(pRequestMessage, nrf24Error));
    }

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithOk(pRequestMessage));
}

/* pRequestMessage must be freed by calling code. */
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ProcessSend(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{
    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ERR_OK;
    NRF24_ErrorTypeDef nrf24Error = NRF24_ERR_OK;

    UNIVERSAL_DEVICE_NRF24_SendRequestTypeDef *pSendRequest = (UNIVERSAL_DEVICE_NRF24_SendRequestTypeDef *)pRequestMessage;

    /* When entering in transmit mode, all IRQ flags are cleared and there is no new IRQ before we start transmit. */

    if ((nrf24Error = NRF24_EnterTransmitMode(&_nrf24Handle, &pSendRequest->TxConfig)) != NRF24_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_NRF24_ResponseWithError(pRequestMessage, nrf24Error));
    }

    /* Acquire IRQ semaphore if it not acquired yet. */

    osSemaphoreId_t irqSemaphore = NULL;

    if ((irqSemaphore = UNIVERSAL_DEVICE_NRF24_GetIRQSemaphore()) == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_HAL);
    }

    switch (osExSemaphoreAcquireTry(irqSemaphore))
    {
        case osOK:
            UNIVERSAL_DEVICE_DEBUG_TRACE("%s at %u: IRQ semaphore acquired.\r\n", __FUNCTION__, __LINE__);
        case osErrorResource: /* Semaphore already acquired. */
            break;

        default:
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_OS_FUNCTION_FAIL);
    }

    /* Begin data transmit. */

    if ((nrf24Error = NRF24_BeginTransmit(&_nrf24Handle, pSendRequest->Data, pSendRequest->DataLength)) != NRF24_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_NRF24_ResponseWithError(pRequestMessage, nrf24Error));
    }

    /* Wait for IRQ. */
    
    switch (osSemaphoreAcquire(irqSemaphore, UNIVERSAL_DEVICE_NRF24_SEND_TIMEOUT))
    {
        case osOK:
            UNIVERSAL_DEVICE_DEBUG_TRACE("%s at %u: IRQ semaphore acquired.\r\n", __FUNCTION__, __LINE__);
            break;

        case osErrorTimeout:
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_TIMEOUT));

        default:
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ERR_OS_FUNCTION_FAIL);
    }

    /* IRQ is activated. */

    switch (nrf24Error = NRF24_EndTransmit(&_nrf24Handle, false))
    {
        case NRF24_ERR_TX_NOT_COMPLETE: /* TX not completed yet, but IRQ is activated. Unexpected result, response with error. */
        {
            return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithError(pRequestMessage, UNIVERSAL_DEVICE_ERR_HAL));
        }

        case NRF24_ERR_TX_COMPLETE: /* TX completed, response with OK. */
        {
            UNIVERSAL_DEVICE_NRF24_TxEvent();

            if ((error = UNIVERSAL_DEVICE_ResponseWithOk(pRequestMessage)) != UNIVERSAL_DEVICE_ERR_OK)
            {
                return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(error);
            }
        }
        break;

        case NRF24_ERR_TX_NO_ACK: /* TX NO_ACK flag, response with error. */
        {
            if ((error = UNIVERSAL_DEVICE_NRF24_ResponseWithError(pRequestMessage, nrf24Error)) != UNIVERSAL_DEVICE_ERR_OK)
            {
                return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(error);
            }
        }
        break;

        default: /* Unexpected TX error. */
        {
            UNIVERSAL_DEVICE_DEBUG_ERROR_NRF24(nrf24Error);

            return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC);
        }
    }

    /* Exit transmit mode. */

    if ((nrf24Error = NRF24_ExitTransmitMode(&_nrf24Handle, true)) != NRF24_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_NRF24_ResponseWithError(pRequestMessage, nrf24Error));
    }

    return UNIVERSAL_DEVICE_ERR_OK;
}

/* pRequestMessage must be freed by calling code. */
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ProcessStartListen(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{
    NRF24_ErrorTypeDef nrf24Error = NRF24_ERR_OK;

    UNIVERSAL_DEVICE_NRF24_StartListenRequestTypeDef *pStartListenRequest = (UNIVERSAL_DEVICE_NRF24_StartListenRequestTypeDef *)pRequestMessage;    

    if ((nrf24Error = NRF24_EnterReceiveMode(&_nrf24Handle, &pStartListenRequest->RxConfig)) != NRF24_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_NRF24_ResponseWithError(pRequestMessage, nrf24Error));
    }


    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithOk(pRequestMessage));
}

/* pRequestMessage must be freed by calling code. */
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ProcessStopListen(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage)
{
    NRF24_ErrorTypeDef nrf24Error = NRF24_ERR_OK;

    if ((nrf24Error = NRF24_ExitReceiveMode(&_nrf24Handle, true)) != NRF24_ERR_OK)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_NRF24_ResponseWithError(pRequestMessage, nrf24Error));
    }


    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(UNIVERSAL_DEVICE_ResponseWithOk(pRequestMessage));
}

static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ProcessListen(void)
{
    NRF24_ErrorTypeDef nrf24Error = NRF24_ERR_OK;


    /* Is NRF24 initialized? */
    bool isInitializedFlag = false;

    if ((nrf24Error = NRF24_IsInit(&_nrf24Handle, &isInitializedFlag)) != NRF24_ERR_OK)
    {
        UNIVERSAL_DEVICE_DEBUG_ERROR_NRF24(nrf24Error);

        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC);
    }

    if (isInitializedFlag)
    {
        /* Is NRF24 RX started? */
        bool rxBusyFlag = false;

        if ((nrf24Error = NRF24_IsInReceiveMode(&_nrf24Handle, &rxBusyFlag)) != NRF24_ERR_OK)
        {
            UNIVERSAL_DEVICE_DEBUG_ERROR_NRF24(nrf24Error);

            return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC);
        }


        if (rxBusyFlag)
        {
            while (true)
            {
                static uint8_t buffer[NRF24_MAX_DATA_LENGTH];
                static uint8_t address[NRF24_MAX_ADDR_LENGTH];
                NRF24_RxDataPipesTypeDef rxPipe = NRF24_RX_DATA_PIPE_NONE;
                uint8_t length = 0;

                nrf24Error = NRF24_Receive(&_nrf24Handle, buffer, NRF24_MAX_DATA_LENGTH, address, &rxPipe, &length);

                switch (nrf24Error)
                {
                    case NRF24_ERR_RX_NOT_COMPLETE: /* If RX not completed yet, just return w/o error. */
                    {
                        return UNIVERSAL_DEVICE_ERR_OK;
                    }

                    case NRF24_ERR_RX_COMPLETE: /* If RX completed, response with data. */
                    {
                        UNIVERSAL_DEVICE_DEBUG_TRACE("%s: RX message received (pipe bit:0x%02X length:%u).\r\n", __FUNCTION__, rxPipe, length);

                        UNIVERSAL_DEVICE_NRF24_RxEvent();

                        UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_NRF24_ResponseWithData(buffer, length, address, rxPipe);

                        if (error != UNIVERSAL_DEVICE_ERR_OK)
                        {
                            return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(error);
                        }
                    }
                    break; /* Try receive one more packet. */

                    default: /* Unexpected RX error. */
                    {
                        UNIVERSAL_DEVICE_DEBUG_ERROR_NRF24(nrf24Error);

                        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC);
                    }
                }
            }
        }
    }


    return UNIVERSAL_DEVICE_ERR_OK;
}

/* pRequestMessage must be freed by calling code. */
static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ResponseWithError(UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage, NRF24_ErrorTypeDef nrf24Error)
{
    UNIVERSAL_DEVICE_DEBUG_ERROR("%s at %u: response with NRF24 error 0x%02X.\r\n", __FUNCTION__, __LINE__, nrf24Error);

    UNIVERSAL_DEVICE_NRF24_ErrorResponseTypeDef *pErrorResponse = osExMalloc(sizeof(UNIVERSAL_DEVICE_NRF24_ErrorResponseTypeDef));

    if (pErrorResponse == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED);
    }

    memset(pErrorResponse, 0, sizeof(UNIVERSAL_DEVICE_NRF24_ErrorResponseTypeDef));

    pErrorResponse->Header.MessageId = pRequestMessage->Header.MessageId;
    pErrorResponse->Header.MessageBodySize = sizeof(UNIVERSAL_DEVICE_NRF24_ErrorResponseTypeDef) - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE;
    /* FragmentBodyOffset is already 0. */
    pErrorResponse->Header.FragmentBodySize = sizeof(UNIVERSAL_DEVICE_NRF24_ErrorResponseTypeDef) - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE;
    pErrorResponse->Header.InterfaceBase = pRequestMessage->Header.InterfaceBase;
    pErrorResponse->Header.InterfacePort = pRequestMessage->Header.InterfacePort;
    pErrorResponse->Header.InterfaceCommand = pRequestMessage->Header.InterfaceCommand;
    pErrorResponse->Header.Error = UINVERSAL_DEVICE_ERR_INTERFACE_SPECIFIC;
    pErrorResponse->NRF24Error = nrf24Error;

    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback((UNIVERSAL_DEVICE_MessageTypeDef *)pErrorResponse);

    osExFree(pErrorResponse);

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(error);
}

static UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_NRF24_ResponseWithData(uint8_t *pData, uint8_t length, uint8_t *pAddress, NRF24_RxDataPipesTypeDef rxPipe)
{
    size_t responseSize = sizeof(UNIVERSAL_DEVICE_NRF24_ListenResponseTypeDef) + UNIVERSAL_DEVICE_IncreaseToMultipleOfFour(length);
    size_t responseBodySize = responseSize - UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE;

    UNIVERSAL_DEVICE_NRF24_ListenResponseTypeDef *pListenResponse = osExMalloc(responseSize);

    if (pListenResponse == NULL)
    {
        return UNIVERSAL_DEVICE_DEBUG_TRACE_ERROR_AND_RETURN(UINVERSAL_DEVICE_ERR_MEMORY_CANNOT_BE_ALLOCATED);
    }

    memset(pListenResponse, 0, sizeof(UNIVERSAL_DEVICE_MessageTypeDef));

    pListenResponse->Header.MessageId = UNIVERSAL_DEVICE_NextInitiativeMessageId();
    pListenResponse->Header.MessageBodySize = responseBodySize;
    /* FragmentBodyOffset is already 0. */
    pListenResponse->Header.FragmentBodySize = responseBodySize;
    pListenResponse->Header.InterfaceBase = UNIVERSAL_DEVICE_INTERFACE_BASE_NRF24;
    pListenResponse->Header.InterfacePort = 0;
    pListenResponse->Header.InterfaceCommand = UNIVERSAL_DEVICE_NRF24_CMD_LISTEN;
    /* Error is already 0 (UNIVERSAL_DEVICE_ERR_OK). */

    memcpy(&pListenResponse->Data, pData, length);    
    pListenResponse->DataLength = length;
    memcpy(&pListenResponse->RxAddress, pAddress, NRF24_MAX_ADDR_LENGTH);
    pListenResponse->RxPipe = rxPipe;

    UNIVERSAL_DEVICE_ErrorTypeDef error = UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback((UNIVERSAL_DEVICE_MessageTypeDef *)pListenResponse);

    osExFree(pListenResponse);

    return UNIVERSAL_DEVICE_DEBUG_TRACE_ONLY_IF_ERROR_AND_RETURN(error);
}


#if (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_INFO)
#define __TO_STRING(x) (#x)
#define TO_STRING(x) __TO_STRING(x)

static const char * UNIVERSAL_DEVICE_NRF24_CommandToString(UNIVERSAL_DEVICE_NRF24_CommandTypeDef command)
{
    static const UNIVERSAL_DEVICE_NRF24_CommandTypeDef pCommandIndexList[] =
    {
        UNIVERSAL_DEVICE_NRF24_CMD_RESET,

        UNIVERSAL_DEVICE_NRF24_CMD_SET_CONFIG,

        UNIVERSAL_DEVICE_NRF24_CMD_SEND,

        UNIVERSAL_DEVICE_NRF24_CMD_START_LISTEN,
        UNIVERSAL_DEVICE_NRF24_CMD_STOP_LISTEN,
        UNIVERSAL_DEVICE_NRF24_CMD_LISTEN,

        __UNIVERSAL_DEVICE_NRF24_CMD_UNKNOWN,
    };

    static const char *ssCommandList[] =
    {
        TO_STRING(UNIVERSAL_DEVICE_NRF24_CMD_RESET),

        TO_STRING(UNIVERSAL_DEVICE_NRF24_CMD_SET_CONFIG),

        TO_STRING(UNIVERSAL_DEVICE_NRF24_CMD_SEND),

        TO_STRING(UNIVERSAL_DEVICE_NRF24_CMD_START_LISTEN),
        TO_STRING(UNIVERSAL_DEVICE_NRF24_CMD_STOP_LISTEN),
        TO_STRING(UNIVERSAL_DEVICE_NRF24_CMD_LISTEN),

        TO_STRING(__UNIVERSAL_DEVICE_I2C_CMD_UNKNOWN),
    };

    size_t commandCount = sizeof(pCommandIndexList) / sizeof(UNIVERSAL_DEVICE_NRF24_CommandTypeDef);

    uint8_t index;
    for (index = 0; index < commandCount; index++)
    {
        if (pCommandIndexList[index] == command)
        {
            break;
        }
    }

    return ssCommandList[index];
}

#undef TO_STRING
#undef __TO_STRING
#endif /* (UNIVERSAL_DEVICE_DEBUG_LEVEL >= UNIVERSAL_DEVICE_DEBUG_LEVEL_INFO) */


__weak osSemaphoreId_t UNIVERSAL_DEVICE_NRF24_GetIRQSemaphore(void)
{
    return NULL;
}

__weak void UNIVERSAL_DEVICE_NRF24_TxEvent(void)
{
    /* This callback can be used to display NRF24 success TX event. */
}

__weak void UNIVERSAL_DEVICE_NRF24_RxEvent(void)
{
    /* This callback can be used to display NRF24 success RX event. */
}


#endif /* UNIVERSAL_DEVICE_CONFIG_NRF24 */
