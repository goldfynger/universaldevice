#ifndef __UNIVERSAL_DEVICE_USART_H
#define __UNIVERSAL_DEVICE_USART_H

#include "universal_device_config.h"
#ifdef UNIVERSAL_DEVICE_CONFIG_USART


#include <stdint.h>
#include "universal_device.h"


UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_USART_Initialise                 (void);
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_USART_ProcessMessage             (UNIVERSAL_DEVICE_MessageTypeDef *pRequestMessage, UNIVERSAL_DEVICE_MessageTypeDef **ppResponseMessages, uint8_t *pResponseCount);

/* __weak functions. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_USART_InitiativeMessageCallback  (UNIVERSAL_DEVICE_MessageTypeDef *pInitiativeMessages, uint8_t initiativeCount);


#endif /* UNIVERSAL_DEVICE_CONFIG_USART */

#endif /* __UNIVERSAL_DEVICE_USART_H */
