#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "trace.h"
#include "customhid_app.h"
#include "universal_device.h"
#include "universal_device_app.h"


/* Kernel objects. */
extern osThreadId_t universalDeviceTaskHandle;
extern CMSIS_OS2_EX_TaskStateTypeDef universalDeviceTaskState;
/* Kernel objects. */


static void UNIVERSAL_DEVICE_APP_ErrorHandler(uint32_t line);


__NO_RETURN void UNIVERSAL_DEVICE_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);

    universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_START;

    if (UNIVERSAL_DEVICE_Initialise() != UNIVERSAL_DEVICE_ERR_OK)
    {
        UNIVERSAL_DEVICE_APP_ErrorHandler(__LINE__);
    }

    TRACE_Format("%s initialised.\r\n", __FUNCTION__);

    universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;


    /* Gets new messages from USB HID queue, and process it. */

    static CUSTOMHID_APP_QueueMessageTypeDef customhidMessage; /* Local static variable; should be used only in this thread. */

    while(true)
    {        
        while (!CUSTOMHID_APP_GetFromQueue(&customhidMessage))
        {
            osDelay(1);
        }

        if (UNIVERSAL_DEVICE_ProcessRequestMessage((UNIVERSAL_DEVICE_MessageTypeDef *)&customhidMessage) != UNIVERSAL_DEVICE_ERR_OK)
        {
            UNIVERSAL_DEVICE_APP_ErrorHandler(__LINE__);
        }
    }
}

static void UNIVERSAL_DEVICE_APP_ErrorHandler(uint32_t line)
{
    /* Stops modem task if error occurred in this thread; otherwise disable all interrupts. */

    const char *errorFormat = "%s on line %u.\r\n";

    if (osThreadGetId() == universalDeviceTaskHandle)
    {
        universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;

        TRACE_Format(errorFormat, __FUNCTION__, line);

        while(true)
        {
            osDelay(osWaitForever);
        }
    }
    else
    {
        oxExDisableInterrupts();

        printf(errorFormat, __FUNCTION__, line);

        while (true)
        {
        }
    }    
}


/* Weak callback. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback(UNIVERSAL_DEVICE_MessageTypeDef *pResponseOrInitiativeMessage)
{
    /* Receives response or initiative messages from universal device and posts them to USB HID queue. */
    /* Messages must be allocated and freed by caller. */

    uint16_t messageSize = UNIVERSAL_DEVICE_MESSAGE_HEADER_SIZE + pResponseOrInitiativeMessage->Header.FragmentBodySize;

    if (messageSize > USB_CUSTOMHID_CONFIG_REPORT_SIZE)
    {
        UNIVERSAL_DEVICE_APP_ErrorHandler(__LINE__);
    }

    static CUSTOMHID_APP_QueueMessageTypeDef customhidMessage; /* Local static variable; should be used only in this thread. */

    memcpy(&customhidMessage, pResponseOrInitiativeMessage, messageSize);

    while (!CUSTOMHID_APP_PostInQueue(&customhidMessage))
    {
        osDelay(1);
    }

    return UNIVERSAL_DEVICE_ERR_OK;
}
