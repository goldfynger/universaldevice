#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "trace.h"
#include "usb_customhid_api.h"
#include "customhid_app.h"


/* Kernel objects. */
extern osThreadId_t customhidTaskHandle;
extern osMessageQueueId_t customhidInQueueHandle;
extern osMessageQueueId_t customhidOutQueueHandle;
extern CMSIS_OS2_EX_TaskStateTypeDef customhidTaskState;
/* Kernel objects. */


static void CUSTOMHID_APP_ErrorHandler(uint32_t line);


__NO_RETURN void CUSTOMHID_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);

    customhidTaskState = CMSIS_OS2_EX_TASK_STATE_START;

    USB_CUSTOMHID_API_Initialize();

    while (!USB_CUSTOMHID_API_IsConnected())
    {
        osDelay(10);
    }

    TRACE_Format("%s connected.\r\n", __FUNCTION__);

    customhidTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;


    /* Gets new messages from USB HID IN queue and transmit them to USB. */

    static CUSTOMHID_APP_QueueMessageTypeDef itemToTransmit;

    while(true)
    {
        if (osExMessageQueueGetWait(customhidInQueueHandle, &itemToTransmit) != osOK)
        {
            CUSTOMHID_APP_ErrorHandler(__LINE__);
        }

        while(USB_CUSTOMHID_API_IsTransmitBusy())
        {
            osDelay(1);
        }

        USB_CUSTOMHID_API_Transmit(itemToTransmit.Report);
    }
}

/* Posts message in USB HID queue. */
bool CUSTOMHID_APP_PostInQueue(CUSTOMHID_APP_QueueMessageTypeDef *pMessage)
{
    /* Used to post new USB HID IN message from another thread. */
    /* Message must be allocated and freed by caller. */

    if (pMessage == NULL)
    {
        CUSTOMHID_APP_ErrorHandler(__LINE__);
    }

    osStatus_t result = osExMessageQueuePutTry(customhidInQueueHandle, pMessage);

    if (result == osOK)
    {
        return true;
    }
    else if (result == osErrorResource)
    {
        return false;
    }
    else
    {
        CUSTOMHID_APP_ErrorHandler(__LINE__);
        return false;
    }
}

/* Gets mesage from USB HID queue. */
bool CUSTOMHID_APP_GetFromQueue(CUSTOMHID_APP_QueueMessageTypeDef *pMessage)
{
    /* Used to get new USB HID OUT message from another thread. */
    /* Message must be allocated and freed by caller. */

    if (pMessage == NULL)
    {
        CUSTOMHID_APP_ErrorHandler(__LINE__);
    }

    osStatus_t result = osExMessageQueueGetTry(customhidOutQueueHandle, pMessage);

    if (result == osOK)
    {
        return true;
    }
    else if (result == osErrorResource)
    {
        return false;
    }
    else
    {
        CUSTOMHID_APP_ErrorHandler(__LINE__);
        return false;
    }
}

static void CUSTOMHID_APP_ErrorHandler(uint32_t line)
{
    /* Stops USB HID task if error occurred in this thread; otherwise disable all interrupts. */

    const char *errorFormat = "%s on line %u.\r\n";

    if (osThreadGetId() == customhidTaskHandle)
    {
        customhidTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;

        TRACE_Format(errorFormat, __FUNCTION__, line);

        while(true)
        {
            osDelay(osWaitForever);
        }
    }
    else
    {
        oxExDisableInterrupts();

        printf(errorFormat, __FUNCTION__, line);

        while (true)
        {
        }
    }    
}


/* Weak callback. */
void USB_CUSTOMHID_API_ReceiveCompleteCallback(uint8_t *pBuffer)
{
    /* Receives data from USB HID and posts it to the queue. */

    if (osExMessageQueuePutTry(customhidOutQueueHandle, (CUSTOMHID_APP_QueueMessageTypeDef *)pBuffer) != osOK)
    {
        CUSTOMHID_APP_ErrorHandler(__LINE__);
    }
}
