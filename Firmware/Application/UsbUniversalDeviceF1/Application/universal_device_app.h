#ifndef __UNIVERSAL_DEVICE_APP_H
#define __UNIVERSAL_DEVICE_APP_H


#include "cmsis_os2.h"


__NO_RETURN void UNIVERSAL_DEVICE_APP_Task(void *argument);


#endif /* __UNIVERSAL_DEVICE_APP_H */
