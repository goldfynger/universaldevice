#include "stm32f1xx_hal.h"
#include "main.h"
#include "board.h"


void BOARD_StateIndicationTimerCallback(void *argument)
{
    HAL_GPIO_TogglePin(LED_STATE_GPIO_Port, LED_STATE_Pin);
}


/* Weak callback. */
void MX_USB_DEVICE_HardwareStart(void)
{
    HAL_GPIO_WritePin(USB_PULL_UP_GPIO_Port, USB_PULL_UP_Pin, GPIO_PIN_SET);
}
