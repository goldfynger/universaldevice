#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "trace.h"
#include "usb_cdc_api.h"
#include "cdc_app.h"


/* Kernel objects. */
extern osThreadId_t cdcTaskHandle;
extern osMessageQueueId_t cdcInQueueHandle;
extern osMessageQueueId_t cdcOutQueueHandle;
extern CMSIS_OS2_EX_TaskStateTypeDef cdcTaskState;
/* Kernel objects. */

/* Buffer and flag variables MUST be used only in CDC_APP_TimerCallback and USB_CDC_API_ReceiveCompleteCallback(). */
static CDC_APP_QueueMessageTypeDef _rxMessageBuffer = {0};
static bool _rxMessageBufferUpdated = false;


static void CDC_APP_ErrorHandler(uint32_t line);


__NO_RETURN void CDC_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);

    cdcTaskState = CMSIS_OS2_EX_TASK_STATE_START;

    USB_CDC_API_Initialize();

    while (!USB_CDC_API_IsConnected())
    {
        osDelay(10);
    }

    TRACE_Format("%s connected.\r\n", __FUNCTION__);

    cdcTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;


    /* Gets new messages from CDC IN queue and transmit them to USB. */

    static CDC_APP_QueueMessageTypeDef itemToTransmit;    

    while(true)
    {
        if (osExMessageQueueGetWait(cdcInQueueHandle, &itemToTransmit) != osOK)
        {
            CDC_APP_ErrorHandler(__LINE__);
        }

        while(USB_CDC_API_IsTransmitBusy())
        {
            osDelay(1);
        }

        USB_CDC_API_Transmit(itemToTransmit.Data, itemToTransmit.DataSize);
    }
}

/* Posts message in USB CDC queue. */
bool CDC_APP_PostInQueue(CDC_APP_QueueMessageTypeDef *pMessage)
{
    /* Used to post new CDC IN message from another thread. */
    /* Message must be allocated and freed by caller. */

    if (pMessage == NULL)
    {
        CDC_APP_ErrorHandler(__LINE__);
    }

    osStatus_t result = osExMessageQueuePutTry(cdcInQueueHandle, pMessage);

    if (result == osOK)
    {
        return true;
    }
    else if (result == osErrorResource)
    {
        return false;
    }
    else
    {
        CDC_APP_ErrorHandler(__LINE__);
        return false;
    }
}

/* Gets mesage from USB CDC queue. */
bool CDC_APP_GetFromQueue(CDC_APP_QueueMessageTypeDef *pMessage)
{
    /* Used to get new CDC OUT message from another thread. */
    /* Message must be allocated and freed by caller. */

    if (pMessage == NULL)
    {
        CDC_APP_ErrorHandler(__LINE__);
    }

    osStatus_t result = osExMessageQueueGetTry(cdcOutQueueHandle, pMessage);

    if (result == osOK)
    {
        return true;
    }
    else if (result == osErrorResource)
    {
        return false;
    }
    else
    {
        CDC_APP_ErrorHandler(__LINE__);
        return false;
    }
}

static void CDC_APP_ErrorHandler(uint32_t line)
{
    /* Stops USB CDC task if error occurred in this thread; otherwise disable all interrupts. */

    const char *errorFormat = "%s on line %u.\r\n";

    if (osThreadGetId() == cdcTaskHandle)
    {
        cdcTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;

        TRACE_Format(errorFormat, __FUNCTION__, line);

        while(true)
        {
            osDelay(osWaitForever);
        }
    }
    else
    {
        oxExDisableInterrupts();

        printf(errorFormat, __FUNCTION__, line);

        while (true)
        {
        }
    }    
}

void CDC_APP_TimerCallback(void *argument)
{
    osExEnterCritical();

    __memory_changed();

    /* Clear update flag. */
    if (_rxMessageBufferUpdated)
    {
        _rxMessageBufferUpdated = false;
    }
    /* If buffer contains data and flag already cleared, push existing data to the queue. */
    else if (_rxMessageBuffer.DataSize != 0)
    {
        if (osExMessageQueuePutTry(cdcOutQueueHandle, &_rxMessageBuffer) != osOK)
        {
            CDC_APP_ErrorHandler(__LINE__);
        }

        _rxMessageBuffer.DataSize = 0;
    }

    osExExitCritical();
}

/* Weak callback. */
void USB_CDC_API_ReceiveCompleteCallback(uint8_t *pBuffer, uint16_t length)
{
    uint16_t copied = 0;

    while (length)
    {
        /* Bytes that can be copied now.  */
        uint16_t bytesToCopy = (length + _rxMessageBuffer.DataSize) <= CDC_APP_QUEUE_DATA_SIZE ? length : (CDC_APP_QUEUE_DATA_SIZE - _rxMessageBuffer.DataSize);

        memcpy(_rxMessageBuffer.Data + _rxMessageBuffer.DataSize, pBuffer + copied, bytesToCopy);
        _rxMessageBuffer.DataSize += bytesToCopy;

        if (_rxMessageBuffer.DataSize == CDC_APP_QUEUE_DATA_SIZE) /* Buffer is full now. */
        {
            if (osExMessageQueuePutTry(cdcOutQueueHandle, &_rxMessageBuffer) != osOK)
            {
                CDC_APP_ErrorHandler(__LINE__);
            }

            _rxMessageBuffer.DataSize = 0;
        }
        else /* Update flag for timer only if buffer has items and not full. */
        {
            _rxMessageBufferUpdated = true;
        }

        length -= bytesToCopy;
        copied += bytesToCopy;
    }
}
