#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "cdc_app.h"
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "stream_to_packet.h"
#include "trace.h"
#include "universal_device.h"
#include "universal_device_app.h"


/* Kernel objects. */
extern osThreadId_t universalDeviceTaskHandle;
extern CMSIS_OS2_EX_TaskStateTypeDef universalDeviceTaskState;
/* Kernel objects. */

STREAM_TO_PACKET_HandleTypeDef _streamToPacketHandle = {0};


static void UNIVERSAL_DEVICE_APP_ErrorHandler(uint32_t line);


__NO_RETURN void UNIVERSAL_DEVICE_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);

    universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_START;

    if (UNIVERSAL_DEVICE_Initialise() != UNIVERSAL_DEVICE_ERR_OK)
    {
        UNIVERSAL_DEVICE_APP_ErrorHandler(__LINE__);
    }

    TRACE_Format("%s initialised.\r\n", __FUNCTION__);

    universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;


    /* Gets new messages from USB CDC queue, and process it. */

    static CDC_APP_QueueMessageTypeDef cdcMessage;  /* Local static variable; should be used only in this thread. */

    size_t streamSize = sizeof(STREAM_TO_PACKET_StreamTypeDef) + CDC_APP_QUEUE_DATA_SIZE;
    STREAM_TO_PACKET_StreamTypeDef *pStream = (STREAM_TO_PACKET_StreamTypeDef *)osExMalloc(streamSize); /* Allocated in heap, but should be used only in this thread. */

    while(true)
    {
        while (!CDC_APP_GetFromQueue(&cdcMessage))
        {
            osDelay(1);
        }

        memset(pStream, 0, streamSize);

        pStream->Size = cdcMessage.DataSize;
        memcpy(pStream->Data, cdcMessage.Data, cdcMessage.DataSize);

        STREAM_TO_PACKET_PacketTypeDef *pPacket = NULL;

        while (true)
        {
            /* Try to make packet using stream. */
            if (STREAM_TO_PACKET_MakePacket(&_streamToPacketHandle, pStream, &pPacket) != STREAM_TO_PACKET_ERR_OK)
            {
                UNIVERSAL_DEVICE_APP_ErrorHandler(__LINE__);
            }

            /* Packet is completed and can be proceed. */
            if (pPacket != NULL)
            {
                if (UNIVERSAL_DEVICE_ProcessRequestMessage((UNIVERSAL_DEVICE_MessageTypeDef *)(pPacket->Data)) != UNIVERSAL_DEVICE_ERR_OK)
                {
                    UNIVERSAL_DEVICE_APP_ErrorHandler(__LINE__);
                }
            }
            
            osExFree(pPacket);

            if (pStream->Offset >= pStream->Size)
            {
                break;
            }

            /* If stream has more than one packet, try to make another packet. */
        }
    }
}

static void UNIVERSAL_DEVICE_APP_ErrorHandler(uint32_t line)
{
    const char *errorFormat = "%s on line %u.\r\n";

    if (osThreadGetId() == universalDeviceTaskHandle)
    {
        universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;

        TRACE_Format(errorFormat, __FUNCTION__, line);

        while(true)
        {
            osDelay(osWaitForever);
        }
    }
    else
    {
        oxExDisableInterrupts();

        printf(errorFormat, __FUNCTION__, line);

        while (true)
        {
        }
    }
}


/* Weak callback. */
UNIVERSAL_DEVICE_ErrorTypeDef UNIVERSAL_DEVICE_ResponseOrInitiativeMessageCallback(UNIVERSAL_DEVICE_MessageTypeDef *pResponseOrInitiativeMessage)
{
    /* Receives response or initiative messages from universal device and posts them to USB CDC queue. */
    /* Messages must be allocated and freed by caller. */

    uint16_t messageSize = UNIVERSAL_DEVICE_GetMessageSize(pResponseOrInitiativeMessage);

    STREAM_TO_PACKET_PacketTypeDef *pPacket = (STREAM_TO_PACKET_PacketTypeDef *)osExMalloc(STREAM_TO_PACKET_GetPacketSize(messageSize));

    memset(pPacket, 0, sizeof(STREAM_TO_PACKET_PacketTypeDef));  /* Clear packet except Data field. */

    pPacket->Size = messageSize;
    memcpy(pPacket->Data, pResponseOrInitiativeMessage, messageSize);

    /* Convert packet (message) to stream. */
    STREAM_TO_PACKET_StreamTypeDef *pStream = NULL;
    if (STREAM_TO_PACKET_MakeStream(pPacket, &pStream) != STREAM_TO_PACKET_ERR_OK)
    {
        UNIVERSAL_DEVICE_APP_ErrorHandler(__LINE__);
    }

    osExFree(pPacket);

    static CDC_APP_QueueMessageTypeDef cdcMessage;  /* Local static variable; should be used only in this thread. */

    memset(&cdcMessage, 0, sizeof(CDC_APP_QueueMessageTypeDef));

    while (pStream->Offset < pStream->Size)
    {
        size_t sizeToSend = pStream->Size - pStream->Offset;
        size_t sizeToCopy = sizeToSend > CDC_APP_QUEUE_DATA_SIZE ? CDC_APP_QUEUE_DATA_SIZE : sizeToSend;

        cdcMessage.DataSize = sizeToCopy;
        memcpy(cdcMessage.Data, pStream->Data + pStream->Offset, sizeToCopy);
        pStream->Offset += sizeToCopy;

        /* Post each part of stream in queue. */
        while (!CDC_APP_PostInQueue((CDC_APP_QueueMessageTypeDef *)&cdcMessage))
        {
            osDelay(1);
        }
    }

    osExFree(pStream);

    return UNIVERSAL_DEVICE_ERR_OK;
}
