#include <stdbool.h>
#include <stdint.h>
#include "board.h"
#include "cdc_app.h"
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "universal_device_app.h"
#include "trace.h"
#include "cmsis_os2_app.h"


osThreadId_t cdcTaskHandle;
const osThreadAttr_t cdcTask_attributes = {
  .name = "cdcTask",
  .priority = (osPriority_t) osPriorityNormal5,
  .stack_size = 128 * 4
};

osThreadId_t universalDeviceTaskHandle;
const osThreadAttr_t universalDeviceTask_attributes = {
  .name = "universalDeviceTask",
  .priority = (osPriority_t) osPriorityNormal3,
  .stack_size = 192 * 4
};


osMessageQueueId_t cdcInQueueHandle;
const osMessageQueueAttr_t cdcInQueue_attributes = {
  .name = "cdcInQueue"
};

osMessageQueueId_t cdcOutQueueHandle;
const osMessageQueueAttr_t cdcOutQueue_attributes = {
  .name = "cdcOutQueue"
};


osMutexId_t crc32AccessMutexHandle;
const osMutexAttr_t crc32AccessMutex_attributes = {
  .name = "crc32AccessMutex"
};

osMutexId_t flashStorageMutexHandle;
const osMutexAttr_t flashStorageMutex_attributes = {
  .name = "flashStorageMutex"
};

osMutexId_t flashAccessMutexHandle;
const osMutexAttr_t flashAccessMutex_attributes = {
  .name = "flashAccessMutex"
};

osMutexId_t i2c1AccessMutexHandle;
const osMutexAttr_t i2c1AccessMutex_attributes = {
  .name = "i2c1AccessMutex"
};

osMutexId_t i2c2AccessMutexHandle;
const osMutexAttr_t i2c2AccessMutex_attributes = {
  .name = "i2c2AccessMutex"
};

osMutexId_t usart1AccessMutexHandle;
const osMutexAttr_t usart1AccessMutex_attributes = {
  .name = "usart1AccessMutex"
};


osSemaphoreId_t flashEraseBinarySemaphoreHandle;
const osSemaphoreAttr_t flashEraseBinarySemaphore_attributes = {
  .name = "flashEraseBinarySemaphore"
};

osSemaphoreId_t i2c1WaitSemaphoreHandle;
const osSemaphoreAttr_t i2c1WaitSemaphore_attributes = {
  .name = "i2c1WaitSemaphore"
};

osSemaphoreId_t i2c2WaitSemaphoreHandle;
const osSemaphoreAttr_t i2c2WaitSemaphore_attributes = {
  .name = "i2c2WaitSemaphore"
};

osSemaphoreId_t usart1WaitSemaphoreHandle;
const osSemaphoreAttr_t usart1WaitSemaphore_attributes = {
  .name = "usart1WaitSemaphore"
};


osTimerId_t cdcTimerHandle;
const osTimerAttr_t cdcTimer_attributes = {
  .name = "cdcTimer"
};

osTimerId_t stateIndicationTimerHandle;
const osTimerAttr_t stateIndicationTimer_attributes = {
  .name = "stateIndicationTimer"
};


CMSIS_OS2_EX_TaskStateTypeDef cdcTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;
CMSIS_OS2_EX_TaskStateTypeDef universalDeviceTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;


__NO_RETURN void CMSIS_OS2_APP_Start(void)
{
    osKernelInitialize();


    cdcTaskHandle = osThreadNew(CDC_APP_Task, NULL, &cdcTask_attributes);

    universalDeviceTaskHandle = osThreadNew(UNIVERSAL_DEVICE_APP_Task, NULL, &universalDeviceTask_attributes);


    cdcInQueueHandle = osMessageQueueNew(CDC_APP_IN_QUEUE_COUNT, sizeof(CDC_APP_QueueMessageTypeDef), &cdcInQueue_attributes);

    cdcOutQueueHandle = osMessageQueueNew(CDC_APP_OUT_QUEUE_COUNT, sizeof(CDC_APP_QueueMessageTypeDef), &cdcOutQueue_attributes);


    crc32AccessMutexHandle = osMutexNew(&crc32AccessMutex_attributes);

    flashStorageMutexHandle = osMutexNew(&flashStorageMutex_attributes);

    flashAccessMutexHandle = osMutexNew(&flashAccessMutex_attributes);

    i2c1AccessMutexHandle = osMutexNew(&i2c1AccessMutex_attributes);

    i2c2AccessMutexHandle = osMutexNew(&i2c2AccessMutex_attributes);

    usart1AccessMutexHandle = osMutexNew(&usart1AccessMutex_attributes);


    flashEraseBinarySemaphoreHandle = osSemaphoreNew(1, 0, &flashEraseBinarySemaphore_attributes);

    i2c1WaitSemaphoreHandle = osSemaphoreNew(1, 0, &i2c1WaitSemaphore_attributes);

    i2c2WaitSemaphoreHandle = osSemaphoreNew(1, 0, &i2c2WaitSemaphore_attributes);

    usart1WaitSemaphoreHandle = osSemaphoreNew(1, 0, &usart1WaitSemaphore_attributes);


    cdcTimerHandle = osTimerNew(CDC_APP_TimerCallback, osTimerPeriodic, NULL, &cdcTimer_attributes);

    stateIndicationTimerHandle = osTimerNew(BOARD_StateIndicationTimerCallback, osTimerPeriodic, NULL, &stateIndicationTimer_attributes);


    osTimerStart(cdcTimerHandle, CDC_APP_TIMER_PERIOD);

    osTimerStart(stateIndicationTimerHandle, BOARD_STATE_INDICATION_TIMER_PERIOD);


    TRACE_InitialiseTask();


    osKernelStart();


    /* Should never get here. */
    while (true)
    {
    }
}
