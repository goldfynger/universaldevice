#ifndef __CDC_APP_H
#define __CDC_APP_H


#include <stdbool.h>
#include <stdint.h>
#include "cmsis_os2.h"


#define CDC_APP_QUEUE_DATA_SIZE 64U

#define CDC_APP_IN_QUEUE_COUNT  4U
#define CDC_APP_OUT_QUEUE_COUNT 4U

#define CDC_APP_TIMER_PERIOD    10U


typedef struct
{
    uint8_t Data[CDC_APP_QUEUE_DATA_SIZE];
    
    uint16_t DataSize;
}
CDC_APP_QueueMessageTypeDef;


__NO_RETURN void CDC_APP_Task(void *argument);
bool CDC_APP_PostInQueue(CDC_APP_QueueMessageTypeDef *pMessage);
bool CDC_APP_GetFromQueue(CDC_APP_QueueMessageTypeDef *pMessage);

void CDC_APP_TimerCallback(void *argument);


#endif /* __CDC_APP_H */
