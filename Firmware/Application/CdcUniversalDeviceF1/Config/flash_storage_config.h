#ifndef __FLASH_STORAGE_CONFIG_H
#define __FLASH_STORAGE_CONFIG_H


#include <stddef.h>
#include <stdint.h>


/* Use default APP implementation. */
//#define FLASH_STORAGE_CONFIG_APP

/* Use default HAL implementation. */
//#define FLASH_STORAGE_CONFIG_HAL

/* CRC handle. */
#define FLASH_STORAGE_CONFIG_CRC_HANDLE         hcrc

/* Use OS mechanisms. */
#define FLASH_STORAGE_CONFIG_OS2

/* Mutex that used for Flash Storage access. */
#define FLASH_STORAGE_CONFIG_ACCESS_MUTEX       flashStorageMutexHandle

/* Mutex that used for flash write access. */
#define FLASH_STORAGE_CONFIG_FLASH_MUTEX        flashAccessMutexHandle

/* Mutex that used for CRC access. */
#define FLASH_STORAGE_CONFIG_CRC32_MUTEX        crc32AccessMutexHandle

/* Semaphore that used for flash erase. */
#define FLASH_STORAGE_CONFIG_ERASE_SEMAPHORE    flashEraseBinarySemaphoreHandle

/* Flash page size. */
#define FLASH_STORAGE_CONFIG_PAGE_SIZE          FLASH_PAGE_SIZE

/* Page 0 address. */
#define FLASH_STORAGE_CONFIG_PAGE_0_ADDR        ((uint32_t) 0x0800F800U)

/* Page 1 address. */
#define FLASH_STORAGE_CONFIG_PAGE_1_ADDR        ((uint32_t) 0x0800FC00U)

/* Cache size. */
#define FLASH_STORAGE_CONFIG_CACHE_SIZE         ((size_t)   8U)


#endif /* __FLASH_STORAGE_CONFIG_H */
