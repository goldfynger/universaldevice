#ifndef __STREAM_TO_PACKET_CONFIG_H
#define __STREAM_TO_PACKET_CONFIG_H


/* CRC handle. */
#define STREAM_TO_PACKET_CONFIG_CRC_HANDLE              hcrc

/* Mutex that used for CRC access. */
#define STREAM_TO_PACKET_CONFIG_CRC32_MUTEX             crc32AccessMutexHandle

/* Buffer initial size and enlarging step. */
#define STREAM_TO_PACKET_CONFIG_ENLARGE_BUFFER_STEP     64U


#endif /* __STREAM_TO_PACKET_CONFIG_H */
